Common Lisp is an implementation of lisp. Like Scheme, it is almost entirely based on parentheses. Common Lisp can be seen as the "commercial" version of lisp. It is faster and has a larger open source ecosystem than Scheme, but is also less elegant. Much less elegant.

# Identifiers

Identifiers in Common Lisp are case insensitive. That is `hello`, `HELLO`, and `hElLo` are all considered the same.

# Atoms

Lisp has several atoms.

You can check if a value is an atom using `atom`.

```lisp
(atom 'thingaling)  ; t
(atom 78)   ; t
(atom nil)  ; t, stupidly!
```

# Dotted Pairs and List

One of the most primitive constructs in Lisp is the *dotted pair*. A dotted pair, like the name suggests, consists of two elements in a pair. A pair can be constructed using the `cons` function.

```lisp
(cons 1 2) -> '(1 . 2)
```

Note that alternatively, a dotted pair literal can be constructed using (as the name suggests) a dotted pair.

```lisp
'(1 . 2)
'(a . b)
```

The first element of a dotted pair can be retrieved using `car`, and the second using `cdr`.

```lisp
(car '(a . b)) -> 'a
(cdr '(a . b)) -> 'b
```

A list is constructed from multiple dotted pairs. First, we start with the empty list, denoted `nil`. Then, we repeatedly `cons` elements to the front of the list.

```lisp
'(1 2 3 4) is equivalent to (cons 1 (cons 2 (cons 3 (cons 4 nil))))
which is equivalent to (1 . (2 . (3 . (4 . nil))))
This is also equivalent to
```

There are several equivalent ways of writing a list using dots.

```lisp
(a b c)
(a b c . nil)
(a b . (c . nil))
(a . (b . (c . nil)))
(a . (b c))
(a b . (c))
```

In general, you can simplify an expression by eliminating a dot followed by a pair of parentheses (and eliminate those parentheseses too).

A dotted pair can be represented as a pair of boxes, with each box having a pointer to its element.

A list is merely a bunch of dotted pairs linked together (like a linked list).

You can also construct a list using the `list` function.

```lisp
(list 1 2 3)
```

The empty list `(list)` is commonly denoted `nil`.

You can use `cons` to append to the start of a list.

```lisp
(cons 5 (list 1 2 3))
```

You can access the first and rest of a list using `car` and `cdr`. There are also the `first` and `rest` functions, which are better, but oh well. Note that (stupidly), it is valid to call `car` and `cdr` on the empty list, which returns `nil` in both cases.

You can check if something is the empty list using `null`. It returns `t` for the empty list and `nil` for everything else.

There is also the `append` function, which concats two lists together.

```lisp
(append '(a b c) '(1 2 3)) -> '(a b c 1 2 3)
```

# Symbols

Any symbol can be constructed using `quote` or `'`.

# Special Forms

## Let

The `let` special form can be used to assign multiple variables within an expression. For example,

```lisp
(let ((a 3)
      (b 4)
      (c 5))
     (* (+ a b) c))
```

This returns the value of the final expression.

Note that the bindings in one clause cannot access the bindings in another. If needed, you can use the `let*` form, which allows bindings to reference earlier ones.

```lisp
(let* ((v 4)
       (u v))
   (+ u v))
```

## Cond

```lisp
(cond
  ((eql a b) 1)
  ((eql a c) "2")
  ((eql a d) 3))
```

You can use the `t` constant as the last case for a fall-through.

## Quote

You can use the `quote` special form to construct data. It can also be done by putting a `'` in front.

## Defun

You can define a function using the `defun` special form. It looks like this:

```lisp
(defun add (x y) (+ x y))
(defun cons () 3)  ; Just returns a constant value
```

It is very similar to function definition in Racket, except that the function name is outside the parameter list.

# Lambda

A lambda function can be declared as

```lisp
(lambda (x y) (+ x y))
```

So it's identical to a function declaration except there is no function name. As usual, this returns an anonymous function that can be used elsewhere.

Stupidly, lambda functions cannot be directly used as higher order functions. The need to be wrapped in the `function` thing, which is stupid. Or, apparently this isn't needed?

```lisp
(mapcar (function (lambda (x) (+ x 1))) '(1 2 3 4))
```

Note that it is only necessary to work with lambda functions that take one argument. This is called currying. Any function that accepts multiple arguments can be implemented by a lambda function that takes a single argument, and then returns a lambda function that takes another argument, and on and on until all the arguments have been passed. This is called currying. For example,

```lisp
(lambda (x) (lambda (y) (+ x y)))
```

is equivalent to the lambda we defined above.

## Values

Multiple values can be returned from a function using the `values` form.

```lisp
(values 1 2 3)
```

Values can be unpacked using `multiple-value-bind`.

```lisp
(multiple-value-bind (a b c) (values 1 2 3))
```

If there are more values than symbols, excess values are ignored. if you provide fewer values than symbols, the excess symbols are bound to `nil`.

# Boolean Values and Predicates

Unfortunately, Lisp does not have proper boolean values. Instead, the empty list `nil` is false, and everything that is not the empty list is true. It's really messy, but such is life. Conventionally, there is also the `t` constant which stands for true.

The `eq` predicate checks if two identifiers point to the same object. As you might expect, it's not used that often.

There is also the `equal` predicate, which checks if general values (not just atoms) are the same. In general, you should always use this to check for equality.

# Higher-Order Functions

The `mapcar` function in Lisp takes a function and a list, and returns the result of applying the function to each element of the list.

```lisp
(mapcar 'plus1 '(1 2 3)) -> '(2 3 4)
```

Mapcar can be implemented as follows:

```lisp
(defun mapcar (f l)
    (if (null l)
        nil
        (cons (funcall f (car l))) (mapcar f (cdr l))))
```

The `reduce` function in Lisp takes a function and a list, and returns the result of reducing the list using the function. Unfortunately, reduce does not take an initial argument, so it is not a nice equivalent of foldl. This can cause problems when trying to reduce an empty list.

```lisp
(reduce '+ '(1 2 3)) -> 6

; This is a foldr
; I think a foldl requires an accumulator
(defun reduce (f id l)
    (if (null l)
        id
        (f (car l) (reduce f id (cdr l)))))
```

There is also `filter`, which takes a predicate function and a list, and returns a list of those elements that satisfy the predicate.

```lisp
(filter 'numberp '(a b 1 2)) -> '(1 2)

(defun filter (f l)
    (cond
      ((null l) nil)
      ((f (car l)) (cons (car l) (filter f (cdr l))))
      (t (filter f (cdr l)))))
```

Filter is actually `remove-if-not`.

There is also `apply`, which takes a function and a list of arguments, and applies the function to the arguments.

## Lambda Calculus

We can simplify the notation for a lambda function and move to something more mathematical. In this notation, a lambda function (lambda (x) body) will be represented by (λx | body). Function application is denoted (N M). For notational simplicity, we can drop some parentheses, and identify

N M Q = ((N M) Q)

Using currying, we can add extra arguments to a function. Eg.

(L x (L y | x y)) = (Lxy | xy)
(L xyz | x y z) = (Lx | (Ly | (Lz | xyz)))

There are two types of variables in LC: bound and free. Bound variables are bound to variables in the lambda term, and free variables are not.

Alpha reduction - Alpha reduction is renaming bound variables. You can rename a bound variable to any other variable that is not free. You cannot rename free variables.
Beta Reduction (function evaluation) - Given an expression of the form (Lx | body) N, beta reduction chops off the x, and replaces all variables that were bound to x in body with N. Eg.

```lisp
((lambda (x) (x 2)) (lambda (z) (+ z 1))) -> ((lambda (z) (+ z 1)) 2) -> (+ 2 1) -> 3
```

Note that when doing a beta reduction, it is possible that the variable being substituted will become bound when substituted. Eg.

(Lxz | xz) z -> (Lz | zz) (big trouble!)

To avoid this, do an alpha reduction to rename the bound variable to something else so the substitution will succeed.

(Lxz | xz) z -> (Lxy | xy) z -> (Ly | zy)

## Reduction Orders

A lambda expression that does not have any more beta reductions is called a *normal form*. For example, x (Ly | y) is a normal form. However, (Ly | y) x is not a normal form. This reduces to x, which is.

Note that not all lambda expressions have normal forms. For example,

(Lx | xx) (Lx | xx) -> (Lx | xx) (Lx | xx)

Given a lambda expression, it is possible that several beta reductions could be applied. Of these, which one do we pick? In general, there are two strategies:

Applicative order reduction (AOR) - expand the leftmost innermost application first
Normal order reduction (NOR) - expand the leftmost outermost application first

For example,

f(x) = x + x
g(x) = x + 1

AOR: f(g(2)) -> f(2 + 1) -> f(3) -> 3 + 3 -> 6
NOR: f(g(2)) -> g(2) + g(2) -> 3 + g(2) -> 3 + 3 -> 6

More examples:
NOR: ((lambda (x) (+ x x)) ((lambda (z) (+ 3 z)) 2))
  -> ((lambda (z) (+ 3 z)) 2) + ((lambda (z) (+ 3 z)) 2)
  -> (+ 3 2) + ((lambda (z) (+ 3 z)) 2)
  -> 5 + (+ 3 2)
  -> 5 + 5 -> 10

AOR: ((lambda (x) (+ x x)) ((lambda (z) (+ 3 z)) 2))
  -> ((lambda (x) (+ x x)) (+ 3 2))
  -> ((lambda (x) (+ x x)) 5)
  -> (+ 5 5)
  -> 10

In general, AOR reduction is more efficient, because this often prevents the same argument from being evaluated multiple times. However, NOR terminates more often. Indeed, consider the following example:

g(x) = cons(x, g(x + 1))
f(x) = 5

AOR: f(g(0)) -> f(cons(0, g(1))) -> f(cons(0, cons(1, g(2)))) -> ...
NOR: f(g(0)) -> 5

The Church-Rosser theorem states that if A -> B and A -> C, then there is an expression D such that B -> D and C -> D (the diamond diagram). So in some sense, the order of reduction doesn't matter that much, except ...

If an expression A has a normal form E, then there is a normal order reduction A -> E.

As a corollary, if an expression has a normal form, then normal order reduction will terminate and arrive at that normal form.

SUM = (Lfn | IF (ZEROP n) ZERO (ADD n (f (SUB n ONE))))

The Y combinator is defined as follows

Y = (Ly | (Lx | y(xx)) (Lx | y(xx)))

This combinator is essential for implementing recursion. This combinator duplicates its arguments. For example,

Y N = (Ly | (Lx | y(xx)) (Lx | y(xx))) N
   -> (Lx | N(xx)) (Lx | N(xx))
   -> N ((Lx | N(xx)) (Lx | N(xx)))
   -> N (N ((Lx | N(xx)) (Lx | N(xx))))
   -> N (N (N (N ...)))

Alternatively, we can write

Y N = N (Y N)

which is recursive. Using this, we calculate Y SUM 3

Y SUM 3 -> ((Y SUM) 3)
        -> SUM (Y SUM) 3
        -> IF (ZEROP 3) 0 (ADD 3 ((Y SUM) (SUB 3 1)))
        // using NOR
        -> (ADD 3 ((Y SUM) (SUB 3 1)))
        -> (ADD 3 (Y SUM 2))
        -> (ADD 3 (SUM (Y SUM) 2)))
        -> (ADD 3 (ADD 2 (Y SUM 1)))
        -> (ADD 3 (ADD 2 (ADD 1 0)))
        -> 6

Doing this evaluation requires normal order reduction to terminate.

# SECD

The SECD virtual machine uses four stacks:

- s Stack used for evaluation
- e List of lists of function arguments
- c Control used to store the instruction
- d Dump used to store invocation context

The main operations in the SECD VM are

NIL     push a nil pointer
LD      load from the environment   // get a value from context
LDC     load constant
LDF     load function               // get a closure

AP      apply function              //
RTN     return calling environment

SEL     select in if-statement
JOIN    rejoin main control         // used with sel

RAP     recursive apply
DUM     dummy variable

Also have builtin functions +, \*, ATOM, CAR, CONS, EQ, etc.

Each operation is defined by its effects on the four stacks.

s e c d -> s' e' c' d'

Each stack is represented by an s-expression where the leftmost position is the top of the stack.

A. Push Objects to Stack

Compilation Rules:

1. A nil is compiled to (NIL)
2. A number or constant x is compiled to (LDC x)
3. An identifier is compiled to (LD (i . j)), where (i . j) is an index into stack e.

Stack Operations:

NIL:  s e (NIL . c) d     -> (nil . s) e c d
LDC:  s e (LDC x . c) d   -> (x . s) e c d
LD:   s e (LD (i . j) . c) d -> (locate((i . j), e).s) e c d

Locate is an auxiliary function. It returns the jth element of the ith sublist in e.

Note that e is a list of sublists each of which is a list of function arguments. There are no parameters here, as each parameter is compiled to LD (i.j), which has the index of the corresponding actual parameter.

Built-In Functions

Compilation:

A builtin function of the form (OP e1 ... ek) is compiled to

(ek' ... e1' OP)

Where ei' is the compiled code for ei. This is essentially reverse Polish notation.

Eg.

(* (+ 6 2) 3) is compiled to (LDC 3 LDC 2 LDC 6 + *)

To perform an operation is to pop up the front element(s) from s, perform the operation, and put the result back to s.

For a unary operator OP
    (a.s) e (OP.c) d -> ((OP a).s) e c d

The . thing just extracts the first element of the list.

For a binary operator OP
    (a b . s) e (OP . c) d -> ((OP a b).s) e c d

Eg
    s e (LDC 3 LDC 2 LDC 6 + * . c) d
-> (3.s) e (LDC 2 LDC 6 + *.c) d
-> (2 3 .s) e (LDC 6 + *.c) d
-> (6 2 3.s) e (+ *.c) d
-> (8 3.s) e (*.c) d
-> (24.s) e c d

The rule If Then Else

(if e1 e2 e3)

is compiled to

(e1' (SEL) (e2' JOIN) (e3' JOIN))

Eg.
(if (atom 5) 9 7) is compiled to
(LDC 5 ATOM SEL (LDC 9 JOIN) (LDC 7 JOIN))

SEL     (x.s) e (SEL ct cf.c) d -> s e c' (c.d)
            where c' = ct if x is T, and cf if x is F
JOIN    s e (JOIN . c) (cr . d) -> s e cr d

   s e (LDC 5 ATOM SEL (LDC 9 JOIN) (LDC 7 JOIN).c) d
-> (5 . s) e (ATOM SEL (LDC 9 JOIN) (LDC 7 JOIN).c) d
-> (T . s) e (SEL (LDC 9 JOIN) (LDC 7 JOIN).c) d
-> s e (LDC 9 JOIN) (c . d)
-> (9 . s) e (JOIN) (c . d)
-> (9.s) e c d

Compilation Rule:

A lambda function (lambda plist body) is compiled to

(LDF (body' RTN))

Eg. (lambda (x y) (+ x y)) is compiled to (LDF (LD (1.2) LD (1.1) + RTN))

A function application (e e1 ... ek) is compiled to
(NIL ek' CONS ... e1' CONS e' AP)

LDF     s e (LDF f.c) d     -> ((f.e).s) e c d
AP      ((f.e') v.s) e (AP.c) d -> NIL (v.e') f (s e c.d)
RTN     (x.z) e' (RTN.q) (s e c.d) -> (x.s) e c d

Eg. ((lambda (x y) (+ x y)) 2 3) compiles to
    (NIL LDC 3 CONS LDC 2 CONS LDF (LD (1.2) LD (1.1) + RTN) AP)

When this is executed:
- The code NIL LDC 3 CONS LDC 2 CONS places the list (2 3) onto the s stack
- LDF makes the compiled code of the body (the symbol f in Stack Op's) ready, along with the current context e, and places them on top of s stack.
- AP makes it ready to execute the body of the applied function (the code in f) in the extended context (v.e') where v is the list of (fully evaluated) arguments.

Since a let expression has the same semantics as a lambda function, the compilation is essentially the same:

(let (x1 ... xk) (e1 ... ek) exp) is compiled to
(NIL ek' CONS e1' CONS LDF (e' RTN) AP)

For functions, indices are generated according to the order in which the functions are called. For example, in

((lambda (z) ((lambda (x y) (+ (- x y) z)) 3 5)) 6)

# Context and Closure

Given an s-expression, another way of evaluating it is based on context and closure. With this technique, at each point of the evaluation we have a *context*, which contains a list of variables and their current bindings. Variables are added to the context as we go along, and substitution is delayed until we actually use a variable in the code. In this scheme, lambda functions are evaluated as *closures*, which is a [function, context] pair. The context contains variable bindings used to evaluate the lambda function.

For an expression e and context CT, it is evaluated in the following way:

eval e in CT
    - Constants (eg. 1, 2) evaluate to themselves
        eval 1 in CT => 1
    - Variables evaluate to the first matching value in the context
        eval x in {x -> 3, y -> 4} => 3
    - Builtin function
        eval (+ e1 e2) in CT => (eval e1 in CT) + (eval e2 in CT)
    - A lambda function
        eval (lambda parameters body) in CT => a closure [the lambda, CT]
    - A function application
        eval (F args) in CT
            eval F in CT => Based on F, this may result in several steps, but eventually returns a closure [some lambda, CT']
            eval args in CT => evaluated args
            (This next step is where the magic happens. Extend the closure using the arguments and
            the parameters from the lambda, and then evaluate the lambda body in this new context).
            eval lambda body in new closure CT'' = {args -> lambda params} U CT'

eval (lambda (x) (x 2)) (lambda (y) (+ y 1)) in CT0
    eval F in CT0 (the function lambda)
        => [F, CT0]
    eval E in CT0 (the arg lambda)
        => [E, CT0]
    eval (x 2) in {x -> [E, CT0]} U CT0 = CT1
        eval x in CT1
            => [E, CT0]
        eval 2 in CT1
            => 2
        eval (+ y 1) in {y -> 2} U CT0 = CT2
            eval y in CT2
                => 2

((((lambda (x) (lambda (y) (if > x y) x y))) 5) 6)
(Lx | (Ly | (if (> x y) x y))) 5
eval F1 in CT0
    eval F2 in CT0
        => [F2, CT0]
    eval 5 in CT0
        => 5
    eval F3 in {x -> 5} U CT0 = CT1
        => [F3, CT1]
eval 6 in CT0
    => 6
eval (if (> x y) x y) in {y -> 6} U CT1

eval (lamda (x) (x 2)) (lambda (x) (+ x 1)) in CT0
eval F in CT0
    => [F, CT0]
eval E in CT0
    => [E, CT0]
eval (x 2) in {x -> [E, CT0]} U CT0 = CT1
    eval x in CT1 => [E, CT0]
    eval 2 .. => 2
    eval (+ x 1) in {x -> 2} U CT0

Always pick the variable binding first, even if there are multiple ones
