# Terminal Notes

## UNIX

The UNIX operating system is made of three parts: the kernel, the shell, and the programs.

- The **kernel** is the "command centre" of the computer. It allocates time and memory to programs and handles the filestore and communications of system commands. Whenever something is done, it's done by the kernel.
- The **shell** is a command line interface (CLI). The user uses the shell to give commands to the kernel by typing them. For example, if you give the command `rm myfile` in the shell, the shell will find the location of `rm` in the filestore, and then tell the kernel to run that command on the file `myfile`.
- Everything in UNIX is either a file or a process. A process is a program that is currently executing, and every process is given a unique id number (called the PID). A file is a collection of data, and these are used by processes.

## Directories

All the files on a computer are stored in **directories** (called folders for non-technical users). These directories are arranged in a tree. The top directory is called the **root**, and is usually given the name `/`.

Every file has a filename, which gives the name of the file followed by an extension (`.txt` for example), which indicates the file type.

Every file also has a pathname, which shows the location of the file within the directory structure. Forward slashes `/` separate directory names. For example, this file has the path `/Users/jacobgarber/Documents/terminal_notes.txt`.

The terminal by default begins in your home directory, which stores your personal files and subdirectories. The home directory will always have the same name as your username, `jacobgarber` in my case. However, because this folder will always have a different name for different people, it is given the common abbreviation `~`.

The current working directory has a special name, `./`. This is nice, because you can abbreviate path names to lower directories.

```fish
./Other/Physics  # Here the ./ stands for Documents/
```

The parent directory also has a special name, `../`.

Directories and filenames that begin with a `.` are hidden. You cannot see them by default, and usually contain UNIX config information.

Every executable can be used as a command in the shell. If the executable is in your `PATH`, you can simply call its name. Otherwise, you have to use its path (either relative or absolute).

More than one command can be put on the same line by separating them with semicolons.

```fish
echo 'hello'; whoami
```

## Useful Commands

Simply look at the man pages for the following commands.

- `echo`: The echo args command will print the arguments to the screen, separated by spaces. Useful in conjunction with piping and writing strings to files.

ls: The ls command lists the contents of your current directory. By default, it only shows non-hidden files. To show all files, pass the flag -a. You can also list the contents of some other directory by passing it a pathname (this is very similar to the cd command).

mkdir: The mkdir 'dir' command makes a new directory with name 'dir'. If your directory has spaces in its name, make sure you put quotation marks around it. You can also specify relative and absolute pathnames for directories to make in the same style as cd and ls.

cd: The cd 'dir' command changes your current working directory to 'dir'. For example, going cd . will do nothing, while cd .. will take you back to the parent directory. Entering cd with no arguments will take you back to the home directory. When using this command, 'dir' must be some lower directory (relative pathname), or one of the special directories (/ ~ . ..) followed by another pathname (absolute pathname).

pwd: The pwd command prints the absolute pathname of your current working directory.

- `cp`: The cp 'path1' 'path2' command takes the file located at path1 and copies it to path2. If path2 leads to a directory, the file will be copied leaving the filename the same. If path2 contains a filename, the file will be given that name in the new path. Ex.

cp science.txt astro.txt  # changes filename
cp Downloads/unixtut/science.txt .  # moves to current directory

- `man`: The man command command will open the manual page for that command.
- `whatis`: The whatis command command will give you a one-liner describing that command.
- `apropos`: The apropos keyword command will give you a list of all the commands with 'keyword' in their man page header. Useful for finding a command you don't quite know the name of but you know what it does.

mv: The mv 'path1' 'path2' command takes the file located at path1 and moves (really renames) it to path2. You can use this to actually move a file, or simply rename it, or both.

rm: The rm 'path' command removes the file located at path. Remember, this could be any absolute or relative path (not necessarily a file in your directory). Make sure you really want to delete a file if you want to use this, because it doesn't ask for confirmation and doesn't send things to the trash.

rmdir: The rmdir 'path' command removes the directory located at path. Make sure the directory is empty, because UNIX will not remove a directory with stuff in it.

clear: The clear command clears the screen of the previous commands.

cat: The cat path command will open text files in the shell for viewing.

less: The less path command will open text files in the shell for viewing, like cat, but only one page at a time. Hit the spacebar to go to the next page, and hit "q" to exit. Using less, you can search through a text file. While you are in the viewing mode, type the command /[string to search for]. Type n to move to the next occurrence.

head: The head path command will write the first 10 lines of a text file to the screen. You can change the number of lines by passing the flag -number.

tail: The tail path command writes the last 10 lines of a text file to the screen. You can likewise change the number of lines by passing the flag -number.

grep: The grep string path command will search for string in the text file located at path and print out all the lines containing the string. Make sure you use '' if the string has spaces. Grep is case sensitive. To ignore cases, pass the -i flag. To display the lines that do not have the string, pass the -v flag. To show the line numbers, pass the -n flag. To print the number of matched lines, pass the -c flag.

wc: The wc 'path' command prints the number of lines/words/characters in a file. Pass the -l, -w, or -c flags to show only one of them.

sort: The sort 'path' command will sort the lines of a file according to the ords of the first characters and print to the standard output.

who: The who command lists the users on the computer.

quota: If you are on a system that gives users a quota, you can check yours by using the quota command.

df: The df command tells you how much room you have left on the file system.

du: The du 'path' command tells you the size in kb of every subdirectory of 'path'. If no path is supplied, it does it to the current working directory. This command works recursively, so it will print out the subdirectories of the subdirectories, etc.

compress: The compress 'path' command will compress the file located at path. It appends a .Z to the compressed file. (This does not create a new file, but changes the old one.)

uncompress: Opposite of above.

gzip: The gzip 'path' command will gzip a file and append the extension .gz to it.

gunzip: The opposite of above.

tar: The tar

file: The file 'string' command will classify the files with 'string' in their name in the current working directory according to their type. For example, `file *.txt` will list all text files. To do this to all files, simply type file `*`.

history: The history command will show you the history of your commands.

open: The open 'path' command will open the file (or directory) at path in the default application for its type.

help: The help subject/command command will take you to the online fish documentation page about that subject/command. Man contains all the same information, but help takes you to the website.

seq: The seq num1 num2 command will return a list of integers starting at num1 and ranging to num2. If only one number of supplied, the sequence will by default start at 1. Ex.

```fish
> set var (seq 2 5)
> echo $var
1 2 3 4 5
```

## Input and Output

Processes usually take their input from the standard input (the keyboard), write their output to the standard output (the terminal screen), and write their error messages to the standard error (the terminal screen).

For example, if you type the cat command without any arguments, it will take input from the standard input (what you type in the shell), and write it to the standard output (the terminal screen). Hit ^D to close (this is the EOF character).

You can use > to redirect the output of a command. For example, if you type cat > list.txt, cat will still read from the standard input (the keyboard), but write it to list.txt.

If you type cat list1.txt list2.txt > list3.txt, the contents of list1.txt and list2.txt will be written to list3.txt. Note that the writing here is similar to how Python writes files. The old file will be overwritten if it exists, and a new file will be created with the new output.

If you want to append to a file, use >>. In this case,

cat list1.txt >> list2.txt

will append the contents of list1.txt to list2.txt.

The input of a command can be redirected using <. For example, using the sort command

sort < list1.txt

will sort list1.txt and print to the standard output. To redirect this output to another file, simply write

sort < list1.txt > list2.txt

Note that the < can usually be omitted, because files are usually provided as arguments to a command. Ex.

sort list1.txt > list2.txt  # will do same thing as above

You can use ^ to redirect the standard error. For example

pip install matplotlib ^ error.txt

To append the standard error to an already existing file, use ^^.

pip install matplotlib ^^ error.txt

## Pipes

To directly connect the output of one command to the input of another, use a pipe |. For example, to sort the users of a system, type

```fish
who | sort
```

To write this to a file, type

```fish
who | sort > users.txt
```

To recap, use <, >, >>, ^, ^^ to read/write to files, and | to connect command input/outputs.

For naming files and directories, only use letters, numbers, and _ and . (no spaces). For files, the first letter is lowercase, and add an extension to the end.

## Access Rights

To view the access rights of a file, type ls -l in the directory it's in. In order, the information is

- Information on the access rights.
- Indicates the number of files in the directory (1 if the thing is a file)
- The user who owns the file/directory.
- The group of users who own the file/directory.
- The size of the file/directory in bytes.
- The date the file/directory was last modified.
- The name of the file/directory.

The first column gives information on the access rights of a file or directory. This string will contain d, r, w, x, and -. If the file is a directory, the first character will be d. If it is not, it will be -.

The remaining 9 characters are split up into three groups of 3.

- The first group specifies the rights for the user who owns the file.
- The second group specifies the rights for the group the file belongs to.
- The third group specifies the rights for all others.

The letters r, w, x mean different things depending if the thing is a file or directory.

For a file:
- r indicates read permission (the right to read and copy a file)
- w indicates write permission (the right to change a file)
- x indicates execution permission (the right to run the file if applicable)

For a directory:
- r allows users to list the files in the directory
- w allows users to move files into the directory or delete them from it
- x allows users to access and open the files in the directory (where the file permissions now take over)

The owner (and only the owner) can change the access rights of a file using the chmod command.

chmod: The chmod options path command is used to change access rights. The various options are u: user, g: group, o: others, a: all, r, w, x the rights, - for taking away, and + for giving. Ex.

chmod ug+rwx list1.txt  # will give users and group rwx permissions
chmod o-rwx list1.txt  # will take away all rwx permissions from others

The default file permissions are 644, and the default directory permissions are 755.

## Umask


## Processes and Jobs

A process is an executing program identified by a PID (process identification number). To see the processes running in the shell, type the ps command.

ps: The ps command shows the processes running in the shell

A process may be in the foreground, background, or be suspended. If it's in the foreground, the shell will not return the prompt until the process is finished (note there can be only one foreground process at any one time). This will hold up the terminal for long processes, so you can put these processes in the background and let them run there. You can continue to do other things while this is happening. Suspended processes are ones that have been paused.

To background a command, type an & at the end of the command line. Ex.

sleep 10 &

When this process is finished, the shell will inform you with a message.

To suspend the foreground process, hit ^Z.

To view all the user-initiated processes (ones you started with commands) that are running, backgrounded, or suspended, type the jobs command.

bg: The bg %job number command will put the job number process in the background, where it will resume. If no job number is supplied, the last suspended process will be put in the background.

fg: The fg %job number command will put the suspended job number process in the foreground, where it will resume. If no job number is supplied, the last suspended process will be put in the foreground.

To kill the process running in the foreground, type ^C.

To kill a suspended or background process based on job number, type kill %job number. To kill a process based on PID, type kill PID. Ex.

kill %4
kill 825

Sometimes you want to supply a string containing a special character (such as the space, `*`, `%`, `/`, `\\`) as an argument to a function. To prevent things from messing up, protect the string within either ' or ". The important difference between the two is that variable expansion is allowed within ", but not within '. For example,

'this is the $PATH'  # No expansion
"this is the $PATH"  # Works

All escape sequences within quotes are ignored, except for \' and \\ in the single quote, and \", \$, \\, and \ followed by a newline (which deletes the \ and the newline). Single quotes can be used within double quotes, and vice versa.

## Functions

Functions in fish follow a simple syntax.

```fish
function FUNCNAME
    BODY
end
```

The arguments to the function are automatically stored in the argv variable. You might have to pass this on to some other command within the function definition. Ex.

```fish
function ll
    ls -l $argv
end
```

Functions can also be used to slightly modify existing commands. These functions are called aliases. For example, we could define

```fish
function ls
    command ls -l $argv
end
```

The `command` keyword is needed to tell the function to call an underlying command, not the function itself (otherwise you would get an infinite recursion loop).

You can list all predefined functions using the functions command. You can see the source for any function by passing the function name to the functions command. Ex.

> functions ls
function ls --description 'List contents of directory'
    command ls -G $argv
end

### Autoloading Functions

Functions can be defined in the command line or in the fish.config file, but the real way to do it is to autoload them.

When a function is called, fish will automatically search through the directories in the $fish_function_path array variable and use them as needed. Making a function file is easy: just throw the function (and just the function, nothing else!!!) in a file, and call it `name_of_function.fish`.

The default value of `$fish_function_path` is `~/.config/fish/functions`, `usr/local/Cellar/fish/2.1.0/etc/fish/functions`, and `usr/local/Cellar/fish/2.1.0/share/fish/functions`. The first path is for user specific functions, the second is for system wide functions, and the last one is for default fish functions. The path list is searched in order, so the user can override administrator functions, who can override the default functions.

## Exit Status

The exit status records the status of the last completed command. It is stored in the $status variable. A 0 indicates success, and anything nonzero indicates failure, error, no results, did not work, etc. The fish prompt will turn red if the last command failed. Fish also comes with two boolean commands: true and false. true will return a 0 exit status, and false will return a 1 exit status. That's all they do.

### Boolean Operators

Exit status can be used for control flow in the shell. This is what the boolean operators not, and, or are based on.

not: not negates the exit status of another command.

```fish
if not grep hello hello.txt  # If you did not find hello
    echo 'no hello'
end
```

not does not change the current exit status.

and: and is used to run a command if the previous one succeeded (that is, if the exit status is 0). Ex.

make; and make install  # make install will only run if make succeeded

and will not change the exit status set by the previous command if it runs.

or: or will run a command if the previous command failed (if the exit status is not 0). Ex.

make; and make install; or make clean  # make clean will run if make failed

Like and, or will not change the current exit status.

## Conditional

The if, else if, else block will execute commands based on their exit status. If the status is 0 (it succeeded), the block will execute. Otherwise, it will skip it. Ex.

```fish
if grep fish /etc/shells  # if it finds the string 'fish'
    echo got fish
else if grep bash /etc/shells  # if it finds the string 'bash'
    echo got bash
else  # if it found neither
    echo found nothing
end
```

Begin

You can create a block of code that will run as a whole using the begin syntax. It's pretty much just making an unnamed function. This is useful if you want to introduce a local scope, redirect input and output of a set of commands, or do other interesting stuff. Ex.

```fish
begin  # this whole thing will be executed at once
    set var hello
    grep $var bub.txt > hello.txt
    echo "what's up doc?"
end
```

Switch

The switch case block compares the value of a string in the switch line to various case values. Each case takes several arguments, and if the string matches one of those arguments, the resulting code under the case is run. Ex.

```fish
switch $animal
    case cat
        echo evil
    case wolf dog human whale  # Each case contains multiple things to compare
        echo mammal
    case duck goose ostrich
        echo bird
    case shark trout salmon
        echo fish
end
```

For

You can use a for loop to iterate over a list, slice, or sequence. Ex.

for val in $PATH
    echo entry: $val
end

While

A while loop will execute as long as a command is succesful (as long as the exit status is 0). Ex.

while test -f foo.txt  # this will run as long as foo.txt exists
    echo foo.txt exists
    sleep 10
end

Note you can break out of a for or while loop using the break command.

for val in $PATH
    switch $val
        case smurfs elephants
            echo "that's" strange
            break
        case math
            echo I want to look at that directory
    end
end

Environment Variables

To create a variable, use the set command.

```fish
set var_name value
```

For example,

```fish
set hello yolo  # has value yolo
```

Variable Expansion

After a variable has been created, you can use its value within the shell using variable expansion. To do this, simply put a $ in front of the variable. Ex.

```fish
echo $hello
echo salutations mr. $hello
```

Undefined and empty variables expand to nothing (it doesn't throw an error at you).

To separate a variable from the text that should surround it, you can either surround the expansion with braces, or surround the text with quotation marks. Ex.

```fish
echo this is the {$HOME}directory  # would return /Users/jacobgarberdirectory
echo this is the $HOME'directory'  # same thing

set var one two three
echo $var's'  # would return ones twos threes
echo $unstatneuaeuht'hello'  # would return nothing, because variable is not defined
```

Variable expansion can also occur within double quoted strings. Ex.

```fish
set name Jimmy
echo "how are you Mr. $name"
```

However, note that double quoted variable expansion will always return a single value. If the variable has multiple values, they will be concatenated together with spaces. If the variable has no value or is not defined, it will return the empty string. Ex.

```fish
set list Jimmy Winnie Pooh
echo "my friends are $list"  # will return my friends are Jimmy Winnie Pooh
echo "my friends are "$list  # will return my friends are Jimmy my friends are Winnie my friends are Pooh
```

Though fish does not officially allow you to have sublists, it can be done by treating the elements of a list as variables. Ex.

```fish
> set list a b c
> set a A B C
> set b how are you
> set c much math
> echo $list
a b c
> echo $$list
A B C how are you much math
```

So, you can always use $ to treat a string like a variable, no matter where it is. If you had iterated through the $list, it would still contain the strings a b c. However, we can use the $ operator to treat those strings like variables and access their values. This means that count $list would return 3, while count $$list would return 8.

However, note that $$list[3] DOES NOT return the third element of $$list (in this case C). When using the $ with list brackets, the brackets will always match the innermost $ operator. So by saying $$list[3], you mean $($list[3]), which would evaluate to $c which is equal to much math. Following this reasoning,

```fish
> echo $$list[2][3]  # returns the third element of the second variable
you
```

Lists

All fish variables are actually lists. It's just that some of them only have one value. Lists with multiple values can be created by separating the arguments with spaces. For example,

> set hello yolo swag

creates a variable hello, which is a list of length 2. An important note is that

> set hello yolo swag
> set hello 'yolo swag'

are two entirely different things. The first creates a list of length 2 with two values: yolo and swag. The second creates a list of length 1 with just one value: 'yolo swag'. If you really wanted to emphasize the difference, you could write

> set hello 'yolo' 'swag'

which does the same thing as set hello yolo swag, but is clearer. (As you may have noticed, putting ' or " around strings is usually optional.)

Lists cannot contain other lists. They only contain strings.

You can get the length of a list using the count command.

> count hello
2

You can access the individual elements of a list using square brackets. Note that indexing from the front starts with 1, and indexing from the back with -1.

> echo $PATH
/usr/bin /bin /usr/sbin /sbin /usr/local/bin
> echo $PATH[1]
/usr/bin
> echo $PATH[-1]
/usr/local/bin

You can also created slices of lists.

> echo $PATH[1..2]
/usr/bin /bin
> echo $PATH[-1..2]
/usr/local/bin /sbin /usr/sbin /bin  # Note the reversed order.

Slicing from the back will reverse the slice.

You can do assignments and deletions of slices and values. For example,

> set PATH[2..3] mwhaha hahaha

> set -e PATH[4]  # erases the fourth element

You can append or prepend to a list by assigning it to itself, with some extra arguments. Ex.

set PATH $PATH /usr/local/bin  # all the $PATH stuff is in the front
set PATH /usr/local/bin $PATH  # all the $PATH stuff is in the back

## Variable Scope

Fish has three variable scopes: universal, global, and local.

Universal variables are shared between all fish sessions on a computer, both now and in the future. These variables must be declared with the -U flag. Fish keeps most of its config options (like the colours) stored in universal variables. Universal variables are stored in the file ~/.config/fish/fishd.MACHINE_ID, where MACHINE_ID is usually your MAC address. Do not modify this file directly, but instead use the shell to change universal variables.

Global variables are specific to the current fish session, but not to any particular block within the session. They will not be erased within the session unless you specifically do so with the set -e command. Any variable declared with the -g flag or within the main shell will be global.

Local variables are specific to the current fish session and a particular block of code. They will be erased once the block is finished running. A block is any section of code that begins with an if, while, for, function, switch, begin and ends with end. Any variable declared within one of these blocks will be local unless specified otherwise. Local variables can also be declared with the -l flag.

It is possible to have different variables with the same name, as long as they are within different scopes. Fish searches variable scopes from the inside out; that is, local, then global, then universal. Ex.

set var pirate  # global variable

begin
    set var ship  # local variable
end

echo $var  # will print pirate

To keep things simple, you should avoid using the -g and -l flags.

Note that variables will maintain their scope upon reassingment. Ex.

set -U fish_greeting 'Hey Everybody!'
set fish_greeting 'Hello World!'  # fish_greeting is still universal

Also note that each function has its own local scope that does not intersect with any others. Ex.

> function avast
      set phrase 'avast me heartys'
  end
> function shiver
      set phrase 'shiver me timbers'
      avast
      echo $phrase
  end
> shiver
shiver me timbers  # phrase was not changed, because the avast phrase was local
                   # to the avast function

Exporting Variables

In general, there are two types of variables: shell variables and environment variables. Shell variables are the ones that are local to the shell; they cannot be accessed by outside programs unless you pass them as an argument. Environment variables are available to the entire OS and all programs that run. By convention, all shell variables are written in lower case, and all environment variables in upper case. To show all shell variables, use the set command. To show all environment variables, use the printenv command.

By default, all variables created by fish are shell variables. To make a variable an environment variable, you need to export it using the -x flag. Ex.

set -x HELLO 'hello Jimmy!'

Updating a variable will keep its previous exported/nonexported status.

set PATH $PATH /usr/local/bin  # PATH is still an exported variable

Variables can be unexported by passing the -u flag, though I really don't see the use of this.

Useful Variables

You can change these ones.

- fish_greeting: the greeting fish prints when it starts up. This is a universal variable.
- fish_user_paths: a list of directories that will be automatically prepended to PATH when fish starts. This is also a universal variable.
- BROWSER: the browser to use when opening the fish documentation (from using the help command)
- PATH: a list of directories to search for commands

You better not change these ones.

- `_`: the name of the currently running command
- argv: a list of variables passed as arguments. Used inside function definitions, and stores any values that are given when fish is invoked. Ex. if you said 'fish myscript.fish foo bar', argv would contain myscript.fish, foo, and bar.
- status: the exit status number.
- HOME: the path name of your home directory. Ex. /Users/jacobgarber
- PWD: the current working directory.
- USER: your login name. Ex. jacobgarber

You can also define the fish_prompt and fish_title functions (and even the fish_greeting function if you want one). Put these function definitions in the autoloading folder.

Argument Expansion

When you provide a list as an argument to a function, each value in the list will expand to become one argument. This is called argument expansion. Ex.

> set var i love math
> echo $var  # same thing as echo i love math (each word becomes own argument)
i love math

Wild Cards

If a star * or a question mark ? is present in an argument, fish will attempt to match the argument to a file in the current working directory, where

`?` will match a single character, except /
`*` will match any string (including the empty string) not containing /
** will match any string of characters (this means the string may match files in subdirectories)

Ex.

a `*` matches any files that begin with 'a' in the current directory
`???` matches any files in the current directory that are exactly three characters long
`**` will match all files and directories in the current directory and all of its subdirectories

Note that wildcards will not match hidden files unless a `.` is specifically given, such as `.*.txt`.

All wildcards will return a list of all the files that match the wild card. Thus it makes sense to say

```fish
for file in *.txt
set matches *.jpg
```

If there are no matches for a wildcard, the wildcard will return nothing. This will return a warning in the shell.

## Command Substitution

The output of a command can be used as the argument(s) for another command. Simply enclose the command in parentheses and it will return a list of strings, where each string is a line of the output. Ex.

```fish
for file in (ls)
```

You can also add strings to each element of the output list. Ex.

```fish
echo hello(grep math phys.txt).jgp  # you get the picture

for file in *.jpg
    convert $file (basename $file .jpg).png
end  # this converts all JPG files the current directory to PNG files
```

Brace Expansion

A comma separated list of characters enclosed in braces will return a list of those characters expanded. Ex.

```fish
echo input.{c,h,txt}  # will return 'input.c input.h input.txt'

mv *.{c,h} src  # will move all files that end in .c or .h to src
```

Index Range Expansion

You can access a certain element or slice of a command substitution or variable using the square bracket notation. Note that the indexing of elements starts at 1, not 0. Elements are also indexed from the back, starting with -1.

```fish
> echo (seq 4)[2]
2

> echo (grep science science.txt)[2]
exhibitors from science centres all over Europe arriving to demonstrate

> set list hello mr bubba jim pooh
> echo $list[1]
hello
> echo $list[-3]
bubba
```

You can also create slices from a command substitution or variable using the .. notation. Ex.

```fish
> echo $list[2..3]
mr bub

> echo (seq 5)[3..5]
3 4 5

> echo $list[2..-1]
mr bubba jim pooh

> echo $list[-1..2]
pooh jim bubba mr  # note the reversed order

> echo $list[2..4 3..5]
mr bubba jim bubba jim pooh
```

You can also do list assignments using the slice notation.

```fish
> set list[4] calculus
> set list[2..3] linear algebra
```

You can also use variables to store the indices for variable expansion (but not command substitution).

> set n 4
> echo $list[1..n]  # works
hello linear algebra calculus
> echo (seq 5)[1..n]  # doesn't work

Home Directory Expansion

As an argument, the ~ character followed by a username is expanded into the home directory of the specified user. A lone ~ is expanded into the home directory of the process owner.

Process Expansion

As an argument, the % character followed by a string is expanded into a PID. Here are the rules.

* If the string is the word self, as in %self, the shell's PID is returned.
* If the string is a job number, like %1 or %2, that job's PID will be returned.
* If the string matches or partially matches the name of a process, the PID of that process will be expanded.
* If none of these match, an error will be raised.

Ex. %emacs will return the PID of the emacs process, and %1 will return the PID of the first job.

Combining Expansions

All of the above expansions can be combined. If any expansion results in more than one argument, all possible combinations will be created. Ex.

> set mylist hello world
> echo "howdy "$mylist" ok "$mylist
howdy hello ok hello howdy hello ok world howdy world ok hello howdy world ok world  # all four combinations

Expansions are performed in the following order:

* command substitutions
* variable expansions
* brace expansions
* PID expansions
* wild card expansions

Expansions are performed from right to left, and nested brace expansions are performed from the inside out.

Ex. If the current directory contains the files "bar" and "foo", then

> echo a(ls){1, 2, 3}
abar1 abar2 abar3 afoo1 afoo2 afoo3  # all possible combinations

Compiling and Installing Source Code

Software is usually distributed in one of two forms: either in precompiled binaries, which are executables that will run straight on your system, or in source code. If you opt for using source code, you will have to compile it yourself.

Compilation requires knowledge of the underlying system, ex. what architecture it is using. You could supply these arguments by hand, or you can use the configure command. Each source package will contain a configure utility, which will create a file called the Makefile in every directory of the source code. This Makefile guides the compilation of the code.

The make command is used to compile source code. It is smart, and only will only recompile code that has changed since the last compile. Using the Makefile, the make command will build your code. The Makefile contains things like the optimization level, where to install the binaries once they are compiled, manual pages, and other config information.

The general way to compile a package is

- download and extract the source code, which is usually placed in a tarball
- cd into the extracted directory
- read the README and INSTALL textfiles and any other pertinent documentation
- type ./configure to create the Makefile
- make to compile the package (no arguments necessary)
- optionally, make check to run any tests that came with the system
- make install to install the executables
- optionally, make clean to remove the binaries from the source code directory

The configure utility comes with a large variety of options. You can pass the --help flag to see these.

The most common options you will use for configure are --prefix and --exec-prefix, which point to installation directories. The --prefix directory will hold machine independent files, such as documentation, data, and config files. The --exec-prefix directory (which is normally a subdirectory of the --prefix directory) holds machine dependant files, such as executables. To use these options, simply pass --(exec)-prefix=directorypath.

When programming, it is useful for the programmer to include debugging information in the executable. That way, if there are problems encountered when running the executable, the programmer can load it into a debugging package and track down the bugs.

However, the end user has no need for this debugging information: we assume the program has already been tested and debugged. Thus, we can remove all the debugging code from the executable, which should increase performance.

To strip all the debug information from an executable, use the strip command. Ex.

strip units

You can do this while installing the package. Simply run make install-strip instead of make install.
