# Introduction

Fundamentally, computers can only understand **bit patterns**.

An **instruction** is a sequence of bits that the computer understands as a command. For example, 1000110010100000 might tell the computer to add two numbers. This binary code that instructs the computer is called **machine languguage**. As you can guess, it is very error prone and tedious to write. **Assembly language** is a version of machine language written in a way a human can read. Assembly language has a direct one-to-one correspondence with machine language, except instead of using 0 and 1 we use words and letters. An **assembler** translates assembly language into machine language.

Each architecture has its own **instruction set**, which is the set of predefined instructions it understands. In this course, we will be looking at the MIPS instruction set.

# Bit Patterns

A `bit` is either a `0` or a `1`. A `bit pattern` is an ordered sequence of `0`s and `1`s, such as `0100111010110`. The meaning of a bit pattern depends on its interpretation. You could interpret the pattern as a number, a character, a machine instruction, or the music to Ave Verum. It all depends on the interpretation. In general however, a bit pattern is interpreted as some sort of number, and then that number is mapped to another object, like a character or note. In and of themselves however, these bit patterns don't mean anything. They are just strings. Fundamentally, all computers do is manipulate bit patterns. Remember, a bit pattern is a string, so every bit is significant, you can't just lop off a leading 0 because it doesn't matter.

Converting From Decimal to Base B

Converting from a base to decimal is easy. TODO

# Binary Addition Algorithm

The **binary addition algorithm** is a manipulation of bits that is equivalent to

## Unsigned Number Representation

Bit batterns can be interpreted as positive numbers using the **unsigned representation**. This is done quite simply. For example, the 8 bit pattern `0110 0110` intepreted as an unsigned integer represents 102 in base ten.

- with N bits, the integers from 0 to 2^N - 1 can be represented
- 2^N - 1 is represented as `11...11` N times

In any base B, the base B represented within the base is simply 10.

In decimal, you already know how to multiply a number by 10. Simply add a 0 to the end. So 83 × 10 = 830. This is true in any base: if you have XYZ in some base B, then XYZ0 is XYZ × B. In general for a number in base B

- adding n 0s to the end of the number is equivalent to multiplying by B^n
- removing n 0s from the end of the number is equivalent to dividing by B^n
    - in general, to divide by B, remove the right most digit. The result is the quotient, and the removed digit is the remainder. So 8B3 / 10 = 8B remainder 3 (in base 16)

These operations can often be accomplished using bit shifts, but you have to be careful, because when shifting sometimes important bits (like 1s) can be shifted out of the number. So this only works in some cases.

# Bit Shifts

The

# Comments

Comments consist of everything between a hash tag `#` and the end of the line. In this respect, comments are identical to the ones in Python.

# Words

A **word** is the natural unit of access on a computer. The size of a word depends on the architecture of the computer. For example, a word for a 32-bit computer is 32 bits, and a word for a 64-bit computer is 64 bits. In this course, we are using MIPS-32, which means all words are 32 bits wide.

# Registers

A **register** is a section of memory kept within the processor. Registers are used for doing immediate calculations on bit patterns. Each register is 32 bits (one word) wide, and MIPS only has 32 registers.

- `$zero` holds the constant 0. It cannot be changed, and is most useful for copying values and doing branch statements.
- `$at` reserved for the assembler
- `$v0 - $v1` return values for functions
- `$a0 - $a3` arguments for functions
- `$t0 - $t9` registers for immediate calculations
- `$s0 - $s7` registers for more longer lasting values
- `$k0 - $k1` used by operating system (for exceptions)
- `$gp` address of middle of data segment
- `$sp` address of top of stack
- `$fp` address of top of stack frame
- `$ra` return address for procedures

Because it only has 32 registers, large pieces of data, like arrays and structs, cannot be kept within the registers. These pieces of data are stored within memory, and then must be loaded into the registers so we can do calculations with them.

# Working With Memory

Memory is organized into bytes, with each byte having a unique address. These addresses are represented as unsigned integers. MIPS has 32 bit registers, so it can only access 2^32 = 4 GiB of memory. There are various MIPS instructions for loading bit paterns from memory into a register. They come in three variants, for loading 4 bytes, 2 bytes, and 1 byte.

```mips
lw      $t0, offset($t1)     # For loading 4 bytes = 1 word
lw      $t0, -134($t1)
```

This instruction interprets `$t1` as an unsigned integer that represents an address in memory. `offset` is a 16-bit signed integer (easiest to write in decimal) that is sign extended to 32 bits. This is added to `$t1` to give some other address of memory. Call that address `addr`. Then, the next 4 bytes from memory starting at `addr` are loaded into register `$t0`.

```mips
sw      $t0, offset($t1)    # For storing 4 bytes = 1 word
```

Same, except the 4 bytes in `$t0` are stored in the next 4 bytes of memory of the address.

```mips
lh      $t0, offset($t1)
lh      $t0, 32($t1)
```

This is the same as above, except the next 2 bytes are loaded into the lower 2 bytes of `$t0`. The lower 2 bytes are then sign extended to 4 bytes. Use this for loading signed numbers.

```mips
lhu     $t0, offset($t1)
```

Same as above, except the lower 2 bytes are not sign extended, they are zero extended. Use this for loading unsigned numbers or general bit patterns.

```mips
sh      $t0, offset($t1)
```

Same, except the lower 2 bytes of `$t0` are stored in the next 2 bytes of the memory.

```mips
lb      $t0, offset($t1)
```

Same, except 1 byte at the memory address is loaded into the bottom byte of `$t0`, and then sign extended to 4 bytes. Use for signed integers.

```mips
lbu     $t0, offset($t1)
```

Same, except the lower byte is zero extended to 4 bytes. Use for unsigned integers (like characters) and general bit patterns.

```mips
sb      $t0, offset($t1)
```

Store the lower byte of `$t0` into the given memory address.

All the above memory operations must be **aligned**. That is, the base memory address you work with (`$t1 + offset`) must be a multiple of 4, 2, or 1 depending if you are working with words, half words, or bytes. This makes memory access faster.

# Working with Constants

All immediate instructions accept a 16 bit constant in the last field. This works well if the upper 16 bits of your constant are all zeros, but what if you need to work with a true 32 bit constant? For this case, you need to load the constant separately into a register, and then work with it from there.

To load a 32 bit constant, you need to use two instructions. The load upper immediate `lui` instruction will load the upper 16 bits of a register with the given 16 bit constant, and then set the lower 16 bits to 0. The lower 16 bits can then be loaded with `ori`.

For example, to load the constant 0x003D0900 into register `$s0`, do this:

```mips
lui     $s0, 0x003D         # becomes 0x003D0000
ori     $s0, $s0, 0x0900    # becomes 0x00000900
```

The `addi` instruction will not work for loading the lower 16 bits, because it sign extends the immediate value into the upper 16 bits, which may give then give the incorrect result.

The load immediate `li` pseudo instruction will expand to one or both of these instructions.

# Math

```mips
add     $t0, $t1, $t2   # $t0 = $t1 + $t2
```

This instruction interprets `$t0, $t1, $t2` as integers (signed or unsigned). It performs the binary addition algorithm on the bit patterns in `$t1` and `$t2`, and then places the result in `$t0`. This instruction traps overflow, and raises an exception.

```mips
sub     $t0, $t1, $t2   # $t0 = $t1 - $t2
```

This instruction interprets `$t0, $t1, t2` as integers (signed or unsigned). It calculates the arithmetical negation of `$t2`, then performs the binary addition algorithm on `$t1` with the negation, placing the result in `$t0`.

```mips
addi    $t0, $t1, imm   # $t0 = $t1 + imm
addi    $t0, $t1, -1245
```

This instruction interprets `$t0, $t1` as integers (signed or unsigned). `imm` is a 16-bit signed integer (easiest to write in decimal), which is then sign extended to 32 bits. The binary addition algorithm is performed on `$t1` and the sign extension, and the result is placed in `$t0`.

All the above instructions trap on overflow. There are also versions with a `u` appended that do not trap, so `addu`, `subu`, and `addiu`. These are the instructions produced when compiling C code, becaues it does not trap on overflow.

Because multiplication of 32 bit numbers results in a 64 bit product, MIPS has two registers set aside for multiplication: *Hi* and *Lo*. The values in these registers need to be moved to general purpose registers before they can be used. This is done with two instructions.

```mips
mflo    $t0     # $t0 = lo
mfhi    $t1     # $t1 = hi
```

Moves the value in *Lo* to `$t0`, and the value in *Hi* to `$t1`.

```mips
mult    $t0, $t1    # signed multiplication
```

Interprets `$t0` and `$t1` as signed integers and calculates the signed product. Places the 32 most significant bits into Hi and the 32 least significant into Lo. There is no overflow if Hi only contains the sign extension of Lo.

```mips
multu   $t0, $t1    # unsigned multiplication
```

As above, but interprets `$t0` and `$t1` as unsigned integers and calculates the unsigned product. There is no overflow if Hi is 0.

The same hardware can be used for multiplication and division, so Hi and Lo are used for division as well.

```mips
div     $t0, $t1    # $t0 / $t1
```

Interprets `$t0` and `$t1` as signed integers and performs integer division. The signed quotient and signed remainder are placed in Lo and Hi respectively.

```mips
divu    $t0, $t1    # $t0 / $t1
```

As above, except all registers are interpreted as unsigned integers.

Note that division by 0 is not checked in MIPS, so you must do it in the program.


# Floating Point Numbers

MIPS supports IEEE 754 single and double precision floating point numbers. Floating point calculations are handled within coprocessor 1. It has 32 floating point registers, labelled `f0` to `f31`. It is only within these registers that you can do floating point operations (you can load floating point bit patterns into the general purpose registers of course, but you won't be able to do any floating point calculations with them). Each of these registers is 32 bits long, and can thus store one single. Doubles are stored in two sequential floating point registers, with the first register being an even number. Because MIPS supports singles and doubles, every instruction has a single `.s` and double `.d` version.

```mips
add.s   $f1, $f2, $f3   # $f1 = $f2 + $f3
add.d   $f0, $f2, $f4   # $f0 = $f2 + $f4
```

Performs floating point addition on `$f2` and `f3`, and stores the result in `$f1`.

```mips
sub.s   $f1, $f2, $f3   # $f1 = $f2 - $f3
sub.d   $f0, $f2, $f4   # $f0 = $f2 - $f4
```

Performs floating point subtraction on `$f2` and `f3`, and stores the result in `$f1`.

```mips
mul.s   $f1, $f2, $f3   # $f1 = $f2 * $f3
mul.d   $f0, $f2, $f4   # $f0 = $f2 * $f4
```

Performs floating point multiplication on `$f2` and `f3`, and stores the result in `$f1`.

```mips
div.s   $f1, $f2, $f3   # $f1 = $f2 / $f3
div.d   $f0, $f2, $f4   # $f0 = $f2 / $f4
```

Performs floating point division on `$f2` and `f3`, and stores the result in `$f1`.

There are special instructions for loading and storing floating point values in memory. Each instruction has a single version and a double version.

```mips
lwc1    $f1, 12($s0)    # load the word at 12($s0) into $f1
ldc1    $f0, 12($s0)    # load the next two words into $f0

swc1    $f1, 12($s0)    # store the fp single into memory
sdc1    $f0, 12($s0)    # store the fp double (two words) into memory
```

These instructions are identical to the regular memory instructions, except they load and store values from memory directly into coprocessor 1.

There are also instructions for comparing floating point numbers. The coprocessor has 8 condition code flags labelled 0 to 7, and the results of a comparison are stored in here. If a cc flag is omitted, it is assumed to be 0.

```mips
c.eq.s
c.lt.s
c.le.s

c.eq.d
c.lt.d
c.le.d
```

These are branch instructions. By default, they act on the cc flag 0.

```mips
bc1f    Label
bc1t    Label
```

Probably don't have to worry about this actually.

# Bit Shifting

Bit shifting is very important for isolating fields in bit patterns.

```mips
sll     $t0, $t1, h     # $t0 = $t1 << h
sll     $t0, $t1, 16
```

This instruction interprets `$t0, $t1` as bit patterns. `h` is an unsigned 5-bit integer (written in decimal). `$t1` is shifted left by `h` bits, and the result is placed in `$t0`. The new bits are filled with zeros.

```mips
srl     $t0, $t1, h     # $t0 = $t1 >> h
```

As above, except `$t1` is shifted right by `h` bits. The new bits are filled with zeros.

```mips
sra     $t0, $t1, h
```

As above, except the sign bit is preserved in the shift right, unlike simply filling with zeros. This is extremely important if working with a signed integer.

# Logical Operations

```mips
and     $t0, $t1, $t2   # $t0 = $t1 & $t2
```

This instruction interprets `$t0, $t1, $t2` as bit patterns. It performs bitwise AND on `$t1` and `$t2`, then places the result in `$t0`. The and instruction is very useful for zeroing out bit patterns. Anding with 0 gives 0, while anding with 1 will preserve whatever is in the bit pattern. A common thing to do is load a bit mask into another register, then and it with another register. Ex

```mips
li      $t2, 0x00FF0CB3
and     $t0, $t1, $t2
```

This is so common that there is an immediate version.

```mips
andi    $t0, $t1, imm
andi    $t0, $t1, 0x0314    # extended to 0x00000314
```

This instruction interprets `$t0, $t1` as bit patterns. `imm` is a 16-bit bit pattern that is zero extended to 32 bits. This is then anded with `$t1`, with the result placed in `$t0`. It is easiest to give the immediate value in hexadecimal.

```mips
or      $t0, $t1, $t2
```

Same as `and`, except it does bitwise OR. The or instruction is very useful for oneing out bit patterns. Oring with 1 gives 1, while oring with 0 will preserve whatever is in the bit pattern.

There is also an immediate version.

```mips
ori     $t0, $t1, imm
ori     $t0, $t1, 0x0314
```

This instruction interprets `$t0, $t1` as bit patterns. `imm` is a 16-bit bit pattern that is zero extended to 32 bits. This is then ored with `$t1`, with the result placed in `$t0`. It is easiest to give the immediate value in hexadecimal.

```mips
nor     $t0, $t1, $t2
```

Same as `or` except NOT OR. Does or, then negates the bits.

```mips
xor     $t0, $t1, $t2   # $t0 = $t1 XOR $t2
```

Same, but does XOR. Checks if two bits are different.

# Pseudo Instructions

```mips
not     $t0, $t1
nor     $t0, $t1, $t1
```

Noring something with itself will negate the bits.


# Converting Assembly Into Machine Language

The assembler translates assembly language into machine language, which is just binary code. Each MIPS instruction is translated into a 32 bit number, which follows one of two **instruction formats**: R-type (for *register*) or I-type (for *immediate*). R-type formats are used for commands that only take register numbers as arguments, while I-type formats are for operations that take numerical constants.

The R-type format looks like this:

| op | rs | rt | rd | shamt | funct |
|:--:|:--:|:--:|:--:|:-----:|:-----:|
| 6  | 5  | 5  | 5  |   5   |   6   |

where:
- *op*, or *opcode*, is the basic instruction operation (like `add`)
- *rs* is the first input register
- *rt* is the second input register
- *rd* is the destination register
- *shamt* is the shift amount if the operation is a bit shift. This accepts up to 2⁵ = 32 numbers, which correspond to all possible bit shifts (there are up to 32 bits in a register to shift). This is set to 0 if the operation is not a shift.
- *funct* is the function code. It specifies the variant of the operation in the op field.

Note that each of the register can accept one of 2⁵ = 32 numbers, which correspond to the 32 MIPS register numbers.

For example, the MIPS instruction `add $t0, $s1, $s2` gets translated into `0 17 18 8 0 32` in decimal or

```
000000 10001 01000 00000 100000
```

in binary.

The I-type format looks like this:

| op | rs | rt | number |
|:--:|:--:|:--:|:------:|
| 6  | 5  | 5  |   16   |

where
- *op* is the opcode of the instruction
- *rs* is the first input register
- *rt* is the destination register
- *number* is a numerical constant, which could be an address

Note that the number field is limited to accepting 16 bit numbers, which corresponds to a maximum value of 2^16 or 4 nibbles.

For example, the MIPS instruction `lw $t0, 32($s3)` gets translated into `35 19 8 32` in decimal or

```
100101 10101 01000 0000000000100000
```

in binary.

The J-type format look like this:

| op | const |
|:--:|:-----:|
| 6  |  26   |

where
- op is the opcode of the instruction
- const is a numerical constant

# Addressing

The branching instructions `beq` and `bne` are I-type instructions, which leaves only 16 bits for the immediate field. This restricts the branch from jumping to 2^16 different addresses, which is much smaller than a normal program. To make it more flexible, these instructions use `PC-relative addressing`. The immediate field is interpreted as a signed 16 bit number that represents the number of *words* to the label relative to PC + 4 (the instruction after the branch).

```mips
        bne     $t0, $t1, Label
        addi    $t0, $t2, 2     # Label is +2 instructions relative to this line, so that is the field value
        and     $t0, $t3, $t4
Label:
        jal     Proc
```

If you need to jump to a label that is farther away than the 16 bits can hold, a jump instruction to the label is inserted, and the branch is inverted to determine whether to skip the jump or not.

```mips
        beq     $s0, $s1, Label
# replaced with
        bne     $s0, $s1, Skip
        j       Label
Skip:
```

The `j` and `jal` instructions use a J-type format. The constant field in these instructions is still only 26 bits, so we stillcannot use absolute addressing. Instead, we use **pseudo direct addressing**, where you look at the memory address of the label, and take the following 26 bits. Then you shift that left by 2, and concatenate that with the first 4 bits of PC + 4 (making sure that those 4 bits match the ones of the label memory address). If the PC + 4 bits do not match, you need to fall back to absolute addressing.

```mips
jal     Label

Label:  # has following memory address
1011 0000 1110 1001 1001 0010 0110 0100     # the instruction must be word aligned, so last 2 bits are 0
     --------------------------------
```

For absolute addressing, you need to use the jump register instruction. Load the 32 bit address of memory you want to jump to into a register, and then jump to that register.

```mips
li      $t0, 0xA12C0826
jr      $t0
```

# Translating and Starting a Program

# Compiling

The **compiler** translates a C program into assembly language. An **assembler** then translates the assembly language into an **object file**. The assembler needs to translate the labels into memory addresses, and it uses a **symbol tabel** to keep a correspondence between labels and their addresses. A standard UNIX object file usually contains six pieces:

- The *header* describes the size and positions of the other pieces of the object file
- The *text segment* contains the machine code
- The *static data segment* contains the statically allocated data
- The *relocation information* identifies instructions that depend on absolute addresses (because absolute addresses are not known until the program is loaded into memory)
- The *symbol tables* contains the remaining labels that haven't been defined, such as external references
- The *debugging information* specifies how the module was compiled

Most program files are compiled separately. The **linker** takes these program files and stitches them together to make an executable. The linker rearranges the data and text segments in each object file into one data and text segment for the executable, and also resolves all external and undefined labels.

When you want to run an executable, the **loader** loads it into memory for execution. It does this:

- Reads the executable file header to determine the sizes of the text and data segments
- Allocates memory for the text and data segments
- Copies the text and data segments from the file into memory
- Copies the arguments to the main program onto the stack
- Initializes the machine registers and the stack pointer
- Jumps to a start up routine that copies the arguments into the `$a` registers and then calls the main routine. When the main routine returns, the start up program terminates with an `exit` syscall (hence you do not need to do this in the main routine).

With a lazy dynamically linked library, the routines are not linked to the external code until they are actually called. This has the advantage of having smaller executable sizes, since a copy of the library is not actually put in the code.

Java is different. The Java compiler first translates the program into java **bytecode**, which is then interpreted by the Java Virtual Machine. You can also use **just in time** compilers to compile code hot spots as the program is running.

# Jumps and Labels

In assembly, you can create a **label** in your program. The general syntax is this:

```mips
label:
```

A label has no effect on the execution of a program. It is only used by branching statements to skip to a certain part of the program. It is not like a function definition, because sequential execution will go in and execute the commands within a label without explicitly jumping there.

```mips
        add $s1, $s2, $s3

Mylabel:
        sub $s1, $s2, $s3  # This line is executed next

        add $s1, $s2, $s3
```

The `j` instruction is used to jump to a certain label. It is equivalent to a `goto` statement in C. When you jump to a label, execution continues with the statements right after the label.

```mips

Mylabel:
        add $s1, $s2, $s3

        j   Mylabel  # Jump to Mylabel, causing an infinite loop.
```

# Decision Making

MIPS includes two instructions for decision making. They are called the **conditional branches**. These instructions are similar to an *if* statement combined with a *goto*. The first *branch if equal*

```mips
beq $t0, $t1, L1
```

This statement interprets `$t0, $t1` as bit patterns. If the patterns are the same, jump to label `L1`. If they are not, continue to the next statement.

```mips
bne $t0, $t1, L1
```

This statement interprets `$t0, $t1` as bit patterns. If the patterns are not the same, jump to label `L1`. If they are, continue to the next statement.

Here is some sample C code translated into MIPS. Assume the variables `i` through `h` are assigned to `$s0` through `$s4`.

```c
if (i == j) {
    f = g + h;
} else {
    f = g - h;
}
```

```mips
        bne     $s0, $s1, Else
        add     $s2, $s3, $s4    # This is skipped if $s0 != $s1
        j       Exit             # Need to skip the next line
Else:
        sub     $s2, $s3, $s4
Exit:
# more code
```

Note that as before, each line of code is compiled into a binary word that is stored in memory. When creating these labels, they are really just replacements for addresses of memory. The assembler will automatically replace these labels with addresses when the code is assembled.


```mips
slt      $t0, $s3, $s4  # equivalent to $t0 = $s3 < $s4
```

This instruction interprets `$s3, $s4` as signed integers. It sets `$t0` to 1 if `$s3` is less than `$s4`, and 0 if not. To conditionally act on the output of a `slt` command, simply use a conditional branch and compare the value to 0, like `beq $t0, $zero, Label`.

```mips
slti     $t0, $s3, imm  # equivalent to $t0 := $s3 < imm
slti     $t0, $s3, -10  # equivalent to $t0 := $s3 < -10
```

This instruction is the immediate version of the above. `imm` is a 16 bit signed integer (write it in decimal).

```mips
sltu    $t0, $t1, $t2
```

Same as `slt`, but `$t1, $t2` are interpreted as unsigned integers.

```mips
sltiu   $t0, $t1, imm
```

Same as `sltiu`, but `imm` is an unsigned 16 bit integer.

You can do a shortcut when comparing two signed integers. For example, if `$s2 > 0` is a signed integer, then `sltu $t0, $s1, $s2` will check if `0 <= $s1 < $s2`. If $s1 is positive, then normal less than stuff is performed. If $s1 is negative on the other hand, it is less than $s2, but because it starts with a 1 it will be interpreted as a very large integer, which will be larger than $s2, which has a 0 in the front. This trick is very useful for doing array bounds checking.

# Loops

Looping can be done using labels and conditional branches. For example,

```c
while (save[i] == k) {
    i += 1;
}
```

Suppose `i` and `k` are stored in `$s3` and `$s5`, and that the base of the array `save` is in `$s6` (and that `save` is an array of words).

```mips
Loop:
        sll     $t1, $s3, 2     # t1 = s3 * 4 (number of bytes ahead)
        add     $t1, $t1, $s6   # get address of where we need to go
        lw      $t0, 0($t1)     # load the word at that location
        bne     $t0, $s5, Exit  # Do the conditional check
        addi    $s3, $s3, 1     # Add 1 to i (not 4; that is taken care of at the top)
        j       Loop            # Jump back to the loop
Exit:
```

A **basic block** is a sequence of code without branches and branch labels.

# Procedures

**Procedures** or functions can also be implemented in MIPS. To execute a function, the program must perform six steps:

In general, I think it is best to avoid using caller saved registers (this is if you want to save some `$t` registers). If you need to save something, just store it in the stack at the beginning of the function (like $ra), or put it in an $s register (also at the beginning).

- Adjust the stack, and save the caller saved registers in memory (no point in doing this)
- Move the arguments into the `$a` registers
- jal to the procedure
- Adjust the stack, and save the callee saved registers in memory
- Perform the procedure
- Put the return values in the `$v` registers
- Restore the callee saved registers
- `jr $ra`
- Restore the caller saved registers

MIPS allocates several registers specifically for function calls

- `$a0` to `$a3` are registers for passing arguments
- `$v0` and `$v1` are registers for storing return values
- `$ra` is a return address register that points to the point of origin

The *jump and link* instruction `jal` is specifically for procedures. It jumps to an address and saves the address of the following instruction (following where `jal` was called) in `$ra`. It is used like this

```mips
jal     ProcedureLabel
```

The `$ra` register stores the address of the next instruction after the `jal`, called the **return address**. This allows the procedure to return control flow back to where it was called. This can be done using the jump register command, like so

```mips
jr      $ra
```

So as you can see, procedures are rather primitive functions.

There is also a register that stores the address of the currently executing instruction. This register is called the **program counter** and is abbreviated PC. The `jal` instruction actually saves *PC* + 4 in the `$ra` register (because each instruction takes up 4 bytes of memory) to link to the instruction after the procedure.

A procedure can be defined using a label, as would make sense. Here is a sample transcription of a C function to MIPS.

```c
int leaf_example(int g, int h, int i, int j) {
    int f = (g + h) - (i + j);
    return f;
}
```

```mips
# g, h, i, j correspond to $a0, $a1, $a2, $a3
# f corresponds to $s0
leaf_example:
        addi    $sp, $sp, -12   # Make room for 3 stack registers
        sw      $t1, 8($sp)     # Push the variables to the stack
        sw      $t0, 4($sp)
        sw      $s0, 0($sp)

        add     $t0, $a0, $a1   # t0 = g + h
        add     $t1, $a2, $a3   # t1 = i + j
        sub     $s0, $t0, $t1   # f = t0 - t1

        add     $v0, $s0, $zero # return f

        # Now restore the old register values by poppin 'em from the stack
        lw      $s0, 0($sp)
        lw      $t0, 4($sp)
        lw      $t1, 8($sp)
        addi    $sp, $sp, 12    # Now adjust stack pointer

        # Return to caller. Don't forget this!
        jr      $ra
```

In the above example, we preserved the values of the temporary registers. However, this is not required. The `$t` registers are temporary registers that do not have to be preserved by a callee. Thus, they may change during a procedure call. The `$s` saved registers on the other hand must be preserved by a procedure and thus are stored on the stack if they are used.


For example, here is the factorial function.

```mips
# The unsinged integer n is stored in $a0
sum:
        addi    $s0, $zero, 1
        add     $v0, $zero, 0
Loop:
        sltu    $t0, $s0, $a0
        beq     $t0, $a0, Exit
        add     $v0, $v0, $s0
        addi    $s0, $s0, 1
        j       Loop
Exit:
        jr      $ra
```

```mips
# with recursion
# $a0 is n
fact:
        beq     $a0, $zero, base_case   # if $a0 == 0, we have the base case

        # save $ra and $a0 in the stack
        addi    $sp, $sp, -8    # make room for two words
        sw      $ra, 4($sp)     # store $ra on the stack
        sw      $a0, 0($sp)     # store $a0 on the stack

        addi    $a0, $a0, -1    # $a0 = $a0 - 1
        jal     fact            # make the recursive call

        # restore the saved values from the stack
        lw      $a0, 0($sp)
        lw      $ra, 4($sp)
        addi    $sp, $sp, 8

        # multiply $v0 = $a0 * $v0
        mul     $v0, $a0, $v0
        jr      $ra

base_case:
        # return 1
        addi    $v0, $zero, 1
        jr      $ra
```

```mips
# loop instead of recursion
fact:
        addi    $v0, $zero, 1       # ans
        beq     $a0, $zero, Done
Loop:
        mult    $v0, $a0
        mflo    $v0
        addi    $a0, $a0, -1
        bne     $a0, $zero, Loop
Done:
        jr      $ra
```

```c
void swap(int v[], int k) {
    int temp = v[k];
    v[k] = v[k + 1];
    v[k + 1] = temp;
}
```

```c
void sort(int v[], int n) {
    for (int i = 0; i < n; i += 1) {
        for (int j = i - 1; j >= 0; j -= 1) {
            if (v[j] > v[j +1]) {
                swap(v, j);
            }
        }
    }
}

```

```mips
# v = $a0, k = $a1
swap:
```

```c
void clear(int array[], int size) {
    for (int i = 0; i < size; i += 1) {
        array[i] = 0;
    }
}

void clear(int* array, int size) {
    for (int* p = array; p < array + size; p += 1) {
        *p = 0;
    }
}
```

```c
#include <stdio.h>
#define STRING_LENGTH 20

typedef struct c_node {
    int vehicleID;
    char make[STRING_LENGTH];
    char model[STRING_LENGTH];
    int year;
    int mileage;
    double cost;
    struct c_node* next;
}
```

```mips
        lw      $t0, 44($sp)    # $t1 = address of start of array
        addi    $t1, $t0, 0     # set $t1 = $t0

        lbu     $t2, 0($t1)
        beq     $t2, $zero, Exit

Loop:
        addi    $t1, $t1, 1
        beq     $t1, $zero, Loop
Exit:
        sub     $t3, $t1, $t0   # t3 = t1 - t0

```

```mips
# $a0 contains the address of a null terminated string
length:
        add     $s0, $a0, $zero
Loop:   lbu     $t0, 0($s0)
        beq     $t0, $zero, End
        addi    $s0, $s0, 1
        j       Loop
End:
        sub     $v0, $s0, $a0
        jr      $ra
```

```mips
# same as above, but more optimized
length:
        add     $s0, $a0, $zero
        lbu     $t0, 0($s0)
        beq     $t0, $zero, End
Loop:
        addi    $s0, $s0, 1
        lbu     $t0, 0($s0)
        bne     $t0, $zero, Loop
End:
        sub     $v0, $s0, $a0
        jr      $ra
```

## The Stack

Sometimes a procedure needs to use more registers than the four argument and two return value registers (like it needs to use the `$s` registers). In this case, the procedure needs to save the original `$s` values into memory, do the calculations, and then restore them (so the caller never noticed a difference). A stack is used to keep track of the spilled over registers. A special register called the **stack pointer** `$sp` contains the address of the most recently allocated register in memory. This pointer indicates where new register values should be spilled and where old register values can be found. For each register that is saved or restored, the stack pointer is adjusted by one word, or 4 bytes. The stack grows from higher addresses to lower addresses. This means you subtract 4 bytes when pushing onto the stack and add 4 bytes when popping off it.

The segment of the stack containing a procedure's saved registers and local variables is called the **procedure frame** for that segment.

## Frame Pointer

The stack is used for register spilling, but it is also used for storing large objects local to the procedure that do not fit into registers, such as arrays or structs. Since the stack pointer may change during the course of a procedure, data structures may have different offsets from the stack pointer depending on when you use them. To simplify this, there is a special register called the **frame pointer** `$fp` that you can use to store the address of the top of the stack when the procedure is called. This allows you to access data from a fixed reference point. Note however, that the `$fp` must be preserved across procedure calls, so you must first save the old value onto the stack, then initialize it to the new `$sp` value for the current frame. From then on, only use the frame pointer to store and load memory (decrementing the stack pointer when needed)

```mips
func:
        addi    $sp, $sp, -4    # move stack pointer down
        sw      $fp, 0($sp)     # save the value of $fp from the calling function
        addi    $fp, $sp, 0     # save fp to current value of sp

        addi    $sp, $sp, -8    # make room for 2 more values to save
        sw      $s0, -4($fp)    # save old value of $s0
        sw      $s1, -8($fp)    # save old value of $s1

        # do stuff

        lw      $s1, -8($fp)
        lw      $s0, -4($fp)
        addi    $sp, $sp, 8     # move $sp back up 8

        lw      $fp, 0($sp)     # restore old $fp
        addi    $sp, $sp, 4     # move $sp back to where it was originally

        jr      $ra             # return to caller
```

## Static Data

A 64 KiB section of memory is set aside for global static data. This segment stores all variables declared in the `.data` segment of a MIPS program. This segment is above the text segment, so it might move around depending on how large your program is. The global pointer `$gp` is used to access the data segment. It points to the middle of the `.data` segment so it can use both positive and negative 16-bit offsets when loading memory.

## Register Convention

When making a procedure call, some registers are guaranteed to preserved (have the same value) before and after the procedure call. That is, if the procedure needs to change these registers, it needs to save the old values in memory and restore them before it returns. These are called **callee saved** registers. They are:

- `$s0` - `$s7`
- `$sp`
- `$fp`
- `$gp`

Some registers can be modified freely by a procedure. You cannot assume these registers will have the same value before and after a procedure call. If you want to preserve their values, the caller must save them in memory, call the procedure, and then restore them. These are called **caller saved** registers, and they are:

- `$t0 - $t9`
- `$a0 - $a3`
- `$v0 - $v1`
- `$ra`

# Synchronization

A **data race** is when the result of a program can change depending on the order events happen. Two memory accesses form a data race if they are from different threads to the same location, at least one is a write, and they ocuur after each other. This often occurs when one thread is writing to several memory locations, and the other thread is reading from those locations. The threads need to be **synchronized** to prevent data races. One thing we need is the ability to **atomically** read and write to a memory location, which means no other processor can access that memory while the processor is working with it (atomic, because the operation is indivisible). There are many different synchronizations operations that you can work with. One common one is the **atomic swap**, which switches a value in memory with a value in a register. This is commonly used when implementing locks.

In MIPS, you can use a pair of instructions to implement an atomic swap.

```mips
ll      $t0, offset($t1)
```

Identical to `lw`, except that the value in memory is recorded.

```mips
sc      $t0, offset($t1)
```

Stores the contents of `$t0` into the given memory address, just like `sw`. However, if the contents of that memory location have been changed since the last `ll`, the memory store fails, and 0 is stored in `$t0`. If the memory has not been modified, the memory write is done, and 1 is stored in `$t0`.

```mips
# atomic memory swap
try:
        addi    $t0, $s4, 0     # value to store
        ll      $t1, 0($s1)
        sc      $t0, 0($s1)
        beq     $t0, $zero, try
        addi    $s4, $t1, 0     # we cannot load directly into $s4, because the load store might fail
                                # which means we need to try again, and $t0 gets overwritten each time, so
                                # we need to save $s4
```

```mips
lock:
        addi    $t0, $zero, 1   # value of lock
loadLock:
        ll      $t1, 0($a0)     # load the lock variable
        bne     $t1, $zero, loadLock    # lock in use
        sc      $t0, 0($a0)     # try setting the lock
        beq     $t0, $zero, lock    # try again if setting failed

        lw      $t2, 0($a1)     # t2 = shvar
        slt     $t3, $zero, $t2 # t3 = 0 < shvar
        beq     $t3, $zero, unlock

        slt     $t4, $t2, $a2   # t4 = shvar < x
        beq     $t4, $zero, unlock
        sw      $a2, 0($a1)
unlock:
        sw      $zero, 0($a0)
```

You loose performance when you have shared variables, because many threads might try to access the same variable at the time, but cannot if there is a lock.


# Exceptions

An **exception** is an unscheduled event internal to the processor that disrupts program execution. Things that trigger exceptions include arithmetic overflow and unaligned memory access.

An **interrupt** is like an exception, except that comes from outside the processor. IO devices use interrupts to communicate with the processor.

When an exception occurs, the program jumps to a predefined section of memory called the **exception handler**. This is located within the kernel segment of memory at address 0x8000 0810. The OS is responsible for dealing with exceptions because an exception indicates something is wrong with the user portion of the program, so the OS needs to step in externally and deal with it. After the appropriate action has been taken, the OS may terminate the program or resume normal execution.

There are 6 hardware interrupt levels and 2 software interrupt levels.

A special part of the CPU called *coprocessor 0* has registers that record the information needed to deal with an exception. They are:

BadVAddr $8. If the instruction that caused the exception made a memory access, BadVAddr contains the address of the memory it tried to access.

Count $9. A timer that gets incremented every 10 ms.

Compare $11. When this register is equal to Count, a level 5 hardware interrupt is raised.

Status $12. Contains information about which interrupts are allowed.
- The *interrupt mask* field contains a bit for each of the interrupt levels. A mask bit 1 enables that interrupt level, while a mask bit 0 disables that level (interrupts of that level are ignored).
- The *user mode* bit is set to 0 if the processor is running in kernel mode and 1 if in user mode. This is always set to 1 in SPIM.
- The *exception level* bit is set to 1 after an exception occurs. When this bit is 1, interrupts are disabled and the EPC is not updated if another exception occurs. This prevents the exception handler from being interrupted by an exception or interrupt. This should be reset to 0 when the exception handler is finished.
- When the *interrupt enable* bit is 1, interrupts are allowed, and they are disallowed when it is 0.

Cause $13. This contains information about the cause of the exception.
- The *branch delay* bit is 1 if the exception occured in a branch.
- Each interrupt level has an *interrupt pending* bit. This is set to 1 when an iterrupt of that level is raised. This is ignored if the corresponding interrupt mask bit is 0, and let through if it is 1.
- The *exception code* is a 5 bit field that contains the exception code for the exception.

EPC $14. The *exception program counter*. This stores the address of the instruction where the exception occured.

Config $16. Configuration of machine.

The registers in coprocessor 0 cannot be worked with directly. You need to move their values into general purpose registers first.

```mips
mfc0    $k0, $13    # $k0 = $13
```

This stores the contents of register `$13` in `$k0`.

```mips
mtc0    $k0, $13    # $13 = $k0
```

This places the contents of register `$k0` into register `$13`.

## Writing Exception Handler

There are two general purpose registers reserved for writing exceptions: `$k0` and `$k1`. You can use these registers freely when writing an exception handler and do not have to worry about saving and restoring their values.

```mips
        .kdata
        # Set aside space for two registers in the kernel data segment
save0:
        .word   0
save1:
        .word   0

        # This is the memory location for an exception handler
        .ktext  0x80000180

        # We first need to save the $at register, because some pseudo instructions within the handler
        # need to use it. We cannot save it in memory, because doing so changes $at before we can save
        # it. Thus, it goes in a $k register, usually $k1

        # The $at register is usually protected, so this allows us to work with it
        .set    noat
        addi    $k1, $at, $zero
        .set    at

        # This leaves us only with the $k0 register. In most cases we will need to use other registers,
        # so we need to save their initial values in memory to restore them after. We cannot save them
        # in the stack however, because the cause of the exception might have been a bad $sp value.
        # So, we need to put aside space in the kernel data segment of memory and store the registers
        # there.

        sw      $a0, save0
        sw      $a1, save1

        # Next, we need to work with the Cause and EPC registers. These are stored in coprocessor 0,
        # and we cannot work with them in there. So, we need to move them into general purpose
        # registers using mfc0

        # Get the Cause register
        mfc0    $k0, $13

        # Extract the exception code field
        sll     $a0, $k0, 25
        srl     $a0, $k0, 27

        # Branch if the code is an interrupt
        beq     $a0, $zero, Done

        # Otherwise, it is an exception, so print the exception message
        addi    $a0, $k0, $zero     # $a0 = Cause
        mfc0    $a1, $14            # $a1 = EPC
        jal     print_exp           # print exception error message
Done:
        mfc0    $k0, $14            # $k0 = EPC
        addiu   $k0, $k0, 4         # bump EPC to next instruction, so we don't repeat the bad one
                                    # we need to do addiu here to avoid potentially causing an exception

        mtc0    $k0, $14            # EPC = $k0
        mtc0    $zero, $13          # Cause = 0. We need to clear this for the next exception

        # Reset the Status register
        mfc0    $k0, $12            # $k0 = Status
        andi    $k0, $k0, 0xFFFD    # set EXL bit to 0 allow exceptions again
        ori     $k0, $k0, 0x0001    # enable interrupts
        mtc0    $k0, $12            # Status = $k0

        # Restore registers
        lw      $a0, save0
        lw      $a1, save1

        .set    noat
        addi    $at, $k1, $zero
        .set    at

        # return to EPC
        eret

```

# I/O

Many I/O devices are **memory mapped**. This means they have special segments of memory allocated to them that the processor uses to control them. For example, one specific register might have the status of the I/O device, while another might contain data that the device uses.

SPIM uses four memory mapped registers to interface between the keyboard and the terminal.

- The *Reciever Control* is at location `0xFFFF0000`. This register contains the status of the keyboard. Only two bits of this register are actually used. Bit 0 is called the *ready* bit. It is 1 when a character has arrived from the keyboard that has not yet been read from the Receiver Data. When the character is read, this bit is set to 0. Bit 1 is called the *interrupt enable*. This bit can be modified by the program. The interrupt enable is initially 0. If it is set to 1 by the program, a level 1 hardware interrupt is sent whenever a character is type on the keyboard (and the ready bit is then set to 1). Note that in order for this interrupt to succeed, the corresponding interrupt must be enabled in the Status register.
- The *Reciever Data* is at location `0xFFFF0004`. The lower byte of this register contains the character typed into the keyboard. When the character is read from this register, the ready bit in Reciever Control is set to 0.
- The *Transmitter Control* register is at location `0xFFFF0008`. It contains the status of the terminal. Bit 0 is called the ready bit. If it is 1, the transmitter is ready to accept a new character to write to output. If it is 0, it is still writing the old one. Bit 1 is the interrupt enable. If this bit is set to 1, a level 0 hardware interrupt is raised when the terminal is ready to write a new character (and so the ready bit is 1). Setting this bit to 0 disables the hardware interrupt.
- The *Transmitter Data* register is at location `0xFFFF000C`. The lower byte of this register contains the character to be written to the terminal. While the character is being written, the ready bit in the Transmitter Control is set to 0, and all writes to Transmitter Data are ignored. When the write succeeds, the ready bit is set back to 1.

# Pipelining

Multiplexor/Data Selector - select data from one of several different lines based on the setting of the control line
Control Unit - decides which paths in the processor are followed based on the instruction. The multiplexor is controlled using this
Combinational element - a "function" element. It returns a value based on some inputs. The ALU for example
State elements/Sequential - have internal storage. Includes memory and registers

Each stage takes one cycle time to complete, so cycle time = length of the longest stage (including pipeline registers if needed)
Latency - the time to execute a single instruction. Latency = number of stages x cycle time
Throughput - once things get going, how often is an instruction finished executing? This is measured in instructions per second, and is 1/cycle time.

In a single-cycle data path, each instruction completes in one clock cycle (hence the name). Thus, the length of a clock cycle depends on the longest time to execute an instruction (which is usually lw). Each instruction needs to be completely executed before another one begins.

**Pipelining** breaks apart the execution into several stages, and then begins to execute the next instruction while the old one is still working. This does not speed up latency, but it does increase throughput. As the number of instructions goes to infinity, the potential speedup tends to the number of stages (so a 4x speedup for 4 stages).

The MIPS pipeline has five stages

- IF: Fetch instruction from memory
- ID: Decode instruction and read the registers
- EX: Use the ALU to calculate something
- MEM: access memory
- WB: Write the result to a register

In a pipeline, each stage takes one clock cycle to execute, so the clock cycle is determined by the time length of the longest stage.

Pipeline Hazards

There are various hazards that make pipelining a little difficult.

Structurial Hazard - When multiple instructions need to access the same pipeline stage at once. This is fixed by making one instruction stall for a clock cycle, and then continue. Stalls are done through **nops**, which are operations that do nothing.

Data Hazard - when one instruction needs the output of a previous one to execute. For example,

```mips
add     $s0, $t0, $t1
sub     $t2, $s0, $t3   # needs the result of the previous instruction
```

**Forwarding** can be used to minimize the effect of data hazards. Instead of waiting for a result to write back to memory, extra hardware will forward the result to the other instruction as soon as it is calculated. Another way of avoiding hazards is called **code scheduling**. This involves rearranging the code to avoid hazards.

Control Hazard - when the instruction to execute next depends on a previous instruction. For example, when calculating a branch instruction, it is not know whether to begin loading the next instruction on the branch target. Normally, this requires a pipeline stall until the result of the branch is known. However, computers can improve on this using **prediction**. They predict the outcome of the branch, and then begin executing the appropriate instruction. If the wrong prediction is made, the wrong instruction is flushed from the pipeline, and then the correct instruction begins to execute. There are two types of branch prediction. **Static branch prediction** has hardwired prediction rules; for example, it might predict backward branches are taken, while forward branches are not taken. **Dynamic branch prediction** remembers the behaviour of each branch, and then uses that to predict the future.

In general, longer pipelines increase the risk of data hazards.

Pipeline registers carry the information needed for the current instruction along the pipeline. In theory, up to five instructions could be executing at once, and the pipeline registers carry along the data for each instruction.

