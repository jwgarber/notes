Good books to use:
- Effective C++
- Effective Modern C++
- Effective STL
- More Effective C++
- The C++ Programming Language
https://stackoverflow.com/questions/388242/the-definitive-c-book-guide-and-list

# Introduction

C++ is based off of C, so almost everything you do in C carries over to C++. However, almost everything you do in C **should not** be done in C++. A lot of this will just be how to do things in C better in C++.

# Functions

Functions in C++ are identical to those in C, except that you do not pass `void` to a function to indicate it takes no arguments. For example, the `main` function now becomes

```cpp
int main() {
    // stuff
}
```

Note that you can ignore a parameter by not naming it. For example,

```cpp
int f(int, int x) {
    return x;
}
```

Normally, it is very bad to ignore function paramaters, but is occasionally useful when say, overloading a function for a certain type.

# Variables

All variables can be initialized in two ways. The first is the classic C style:

```cpp
int x = 3;
```

Nothing new there. The second uses **initializer lists**. For example,

```cpp
double d2 {2.3};
complex<double> z {1, 2};
vector<int> v = {1, 2, 3, 4, 5, 6};     // the = is optional, but aids readability in my opinion
```

Note that initializer lists work with all types, including built in ones. Furthermore, initializer lists raise errors when narrowing numerical types.

```cpp
int i = 7.2;    // raises a warning
int i {7.2};  // raises an error
```

The types of variables are inferred when you use the `auto` keyword. I think this is fine for large standard library types that are cumbersome to write, but prefer to be explicit for built-in types, and particularly numeric ones.

```cpp
auto x = std::some::awkward::type;
int64_t y = 56;     // mandatory in this case, because the compiler cannot infer the size
```

## Constants

There are two ways for creating immutable variables in C++: `const` and `constexpr`.

Variables labelled `const` cannot be modified once they are created. Easy as that.

Variables labelled `constexpr` have their values evaluated and available at compile time. In some sense, they are a more powerful way of `#defining` a variable. For example,

```cpp
constexpr int size = 10;    // in this context, equivalent to #define size 10
char array[size];   // works fine, because size gets replaced with 10
constexpr int x = factorial(17);
```

Functions used within a constant expression must be declared `constexpr` and consist of a single `return` statement. For example,

```cpp
constexpr int square(const int x) {
    return x * x;
}
```

Functions labelled `constexpr` can be evaluated with any arguments you like. However, you can only identify the return value as `constexpr` if all the arguments to the function are constant expressions.

```cpp
constexpr int x = 3;
constexpr int y = square(x);
```

In general, make variables `const` when they are only known at runtime, and `constexpr` when they are know at compile time. As usual, all constants must be initialized at declaration time.

```cpp
const int size = 10;        // works
constexpr int size = 10;    // this is better though
```

It is possible to declare the return value of a function as `const`. However, this prevents move semantics in C++11, and is really unecessary, so don't bother. Besides, what if the caller wants to mutate the return value? Don't worry about it. You should only declare a return value `const` if you are returning a reference to some internal data structure that you don't want to be mutated.

## Operators

The order in which operators are evaluated in a compound expression is called **operator precedence**. Those with a higher operator precedence are evaluated before those with a lower. For operators with the same precendence, such as `*` and `/`, the **associativity rules** govern in what order the expression is evaluated. In C++, you can overload operators, but the operator precedence and associativity of those operators will remain the same as before.

The **comma** operator and the **ternary if** operator should not be used in general. Things are just clearer if you don't.

### Relational Operators

C++ has the standard relational operators: `<`, `>`, `<=`, `>=`, `==`, and `!=`. All these operators return a boolean value, either `true` (`1`) or `false` (`0`). Note that using these operators with floating point values is dangerous: you need to take special measures.

# Classes

Classes are better structs. Structs are actually almost the same as classes in C++. However, resist the urge to use one. The largest reason is that with classes you are forced to define sane constructors for the class that work nicely with other C++ features, while structs have a default initialization syntax that doesn't play nicely and is sometimes confusing.

A basic class looks like this:

```cpp
class Vector final {
  // only accessible within the class
private:
    double* elem;
    size_t sz;

  // accessible outside the class
public:
    // this is a member initializer list
    // note that providing an explicit initalization list will override the
    // initializer list (yeah, I know)
    // one major advantage is that you can initialize const fields using this
    // syntax
    explicit Vector(const size_t s) :
        elem{new double[s]},
        sz{s}
        {}

    // as above, with a different syntax
    // this might not work of the constructors for the fields are explicit
    explicit Vector(const size_t s) :
        elem = {new double[s]},
        sz = {s}
        {}

    // traditional syntax, but not recommended
    explicit Vector(const size_t s) {
        elem = new double[s];
        sz = s;
    }

    // using initializer list
    // allows us to do
    // Vector v1 = {1.23, 3.45, 6.7};
    explicit Vector(const std::initializer_list<double> lst) : elem{new double[lst.size()]}, sz{lst.size()} {
        std::copy(lst.begin(), lst.end(), elem);
    }

    // destructor
    ~Vector() {
        delete[] elem;
    }

    double& operator[](const size_t i) {
        return elem[i];
    }

    int size() {
        return sz;
    }
};  // derp, classes need a semicolon at the end
```

## Constructors

A member function with the same name as the class is called a constructor. Make sure you always provide a constructor for your class. Constructors are called **with braces**. In Java and old C++, you can use parenthesises, but in C++11 YOU SHOULD USE BRACES. This also means constructors with no arguments are called as you would think they would. ALWAYS USE BRACED INITIALIZATION!

```cpp
Vector v{};     // no argument constructor
Vector v{3};    // one argument constructor
```

Constructors are one of the single most confusing aspects of C++, because there seems to be many different syntaxes. Another advantage of braced initialization is that it avoids the most vexing parse.

### Explicit

Stupidly, C++ is allowed to do implicit conversions on the arguments to a constructor. If one of the arguments to the constructor has the wrong type, but the needed type has a constructor that takes an argument of the wrong type, that argument will be implicitly converted to the needed type through that constructor. For example,

```cpp
class String final {
public:
    String(const int n);     // allocate space for a n-length string
    String(const char* p);   // initialize string from a C-string
};

String mystring = 'x';  // derp! 'x' is of type char, which gets implicitly converted to int, not what we wanted!

class BetterString final {
public:
    explicit BetterString(const int n);      // allocate space for a n-length string
    explicit BetterString(const char* p);    // initialize string from a C-string
};

BetterString my_better_string = 'x';    // now it's an error!
```

This can also happen with conversion operators. I'm not sure how yet, but I'll get there.

Make all constructors and conversion operators explicit. It prevents bugs and also prevents some confusing initialization syntax.

## Modularity

Note that it is rare to actually implement functions inside a class. If you want to use that class in other files (which you usually want to do, right?), you just provide the class definition and prototypes inside a header file, then define the functions in the cpp file. Recall that as usual, you DO NOT put any code that can be compiled in a header file. Header files only provide type information, that's it.

```cpp
// vector.hpp
class Vector final {
  private:
    double* elem;
    int size
  public:
    explicit Vector(int s);
    double& operator[](int i);
    int size();
};

// vector.cpp
#include "vector.hpp"

Vector::Vector(int s) : elem{}, size{} {
    elem = new double[s];
    size = s;
}

// equivalent to the above
Vector::Vector(int s) {
    elem = new double[s];
    size = s;
}

// alternative syntax
// This syntax provides arguments to the constructors of each element
// If you do not provide this in an initialization list,
// the default constructors (empty) constructors of each element are
// called, and then you do assignment within the body
// This is why you cannot assign constant members within the constructor
// body, because they have already been intialized to their default values
// and so obviously can't be reasgined
Vector::Vector(int s) : elem{new double[s]}, size{s} {}

double& Vector::operator[](int i) {
    return elem[i];
}

int Vector::size() {
    return size;
}
```

As with all functions, it is possible to overload constructors.

In particular, it is possible to define a *default constructor*. This constructor takes no arguments. For example,

```cpp
Complex() : re{0}, im{0} {}

Complex var;
```

A default constructor is called without parenthesis, as shown above. This is partially due to the Most Vexing Parse, but also ensures the variable will be initialized.

## Destructors

A **destructor** is responsible for cleaning up an object once it is no longer needed. By default, all variables that go out of scope have their values popped off the stack. In simple cases, this automatic cleanup is fine. However, what if that object had internal references to some dynamically allocated memory, or perhaps an open file? We could get memory leaks, resource drains, or worse. In C, you would have to explicitly clean up such an object with a function call that deinitializes it (like `free` in simple cases). In C++, such cleanup is automatically done through a **destructor** (hooray!). This is a function that is automatically called when a class variable goes out of scope, and does any necessary cleanup work. Note that defining a destructor is not necessary in all cases.

A destructor looks like this:

```cpp
~Vector() {
    // cleanup
}
```

It looks like a zero argument construtor prefaced by a `~`.

This technique is called Resource Acquisition is Initialization or RAII, and it effectively eliminates memory leaks. It is so good that you should not use naked `new` or `delete` operators outside the implementation of a data structure. Aww yeah! In fact, with `unique_ptr` and `shared_ptr`, you don't even need that any more.

Destructors can be `const` or non-const.

## Const Member Function

The `const` keyword is extremely powerful in C++. Declaring a class `const` ensures that the class object can never be mutated. Some member functions can mutate an object, and some don't, so how do we know which ones we can call on a `const` object? Simple. If the method doesn't modify an object, add the `const` keyword at the end of the signature. Now this method can be called for both `const` and non-const objects.

```cpp
// does not modify `this`
Complex add(const Complex other) const {
    // stuff
}
```

As usual, you should make all methods const that can be const.

## Initializer Lists

An **initializer list** allows you to pass an arbitrary number of elements to the constructor of a class (without creating a vector or something like that). It mimics the syntax used to initialize arrays and structs in C. For example,

```cpp
Vector v1 = {1, 2, 3, 4, 5};

// implimentation
Vector::Vector(std::initializer_list<double> list) {
    elem = new double[list.size()];             // create a new array for the elements
    size = static_cast<int>(list.size());       // initialize its length
    std::copy(list.begin(), list.end(), elem);  // copy the elements over
}
```

Simply create a constructor that takes an argument of type `std::initializer_list`.

## Abstract Classes

*Concrete classes*, like those described above, mimic ordinary types. Abstract classes on the other hand contain absolutely no information about implementation details, not even local variables (they are like abstract classes in Java, *not* like interfaces). Abstract classes provide subtyping polymorphism, but this is much better achieved with parametric polymorphism via templates. Abstract classes are confusing, require lots of dynamic allocation, have bad performance, and are weird. Avoid them.

## Virtual Methods

Virtual methods are used in abstract classes. Again, avoid these.

## Classes in Headers

To use a class within other files, you need to put the necessary compiler information in a header file. This is easy. Just define the class as normal, but only put in the type declarations (for everything, both public and private members).

```cpp
// vector.hpp
class Vector {
  private:
    double* elem;
    int size;
  public:
    Vector(int s);
    double& operator[](int i);
    int size();
}
```

You then provide the appropriate member definitions in another file.

```cpp
// vector.cpp
Vector::Vector(int s) {
    elem = new double[s];
    sz = s;
}

double& Vector::operator[](int i) {
    return elem[i];
}

int Vector::size() {
    return sz;
}
```

# Copy and Move

### Copy

Note that whenever you are declaring an object for the first time, a constructor is always called. There is one particular form of initialization that calls a special constructor, called the *copy constructor*. This constructor initializes an instance of a class from another instance.

```cpp
Complex a{3};   // normal constructor
Complex b{a};   // copy constructor
Complex c = a;  // also calls the copy constructor. Note this syntax won't work for explicit constructors
```

For every class, the compiler generates a default (non-explicit) copy constructor. This constructor simply calls the copy constructor for each member of the class. For most classes, this default behaviour is fine. However, for classes that manage resources (and in particular have a destructor), it may be necessary to define a custom copy constructor.

```cpp
// copy constructor
Vector(const Vector& v) : elem{new double[v.sz]}, sz{v.sz} {
    // copy over the elements
    std::copy(v.elem, v.elem + v.sz, elem);
}
```

Note that when passing by value to a function, it is the copy constructor that creates a new copy for the parameter. Also note that it is best to not declare a copy constructor as `explicit`, since doing so would prevent the natural `=` syntax.

There is also a *copy assignment*. This happens when you have an already initialized object, and then assign it to another one.

```cpp
Vector v{3};
Vector u{3};
u = v;  // copy assignment
```

The compiler also generates a default method for this, which simply calls the copy assignment of each member of the class. Again though, sometimes we don't want this, so we need to implement one ourselves.

```cpp
Vector& operator=(const Vector& v) {
    // we assume the sizes are the same
    // and copy over the elements of the array
    std::copy(v.elem, v.elem + v.sz, elem);
    return *this;
}
```

There is a common idiom called *copy-and-swap* that aims to reduce code duplication, the possibility of execptions, and self-assignment, but I'll look into that when I need to.

### Move

There are many cases in C++ when copying is something we want to avoid. For example,

```cpp
Vector calculate(const size_t a) {
    Vector result;
    // do some stuff with result
    return result;
}

Vector calc = calculate(3);
```

Normally, the last line of code involves calling the copy constructor of `calc` with `result` (so it copies from `result` to `calc`). However, this is a bit wasteful, since `result` is going to be destructed at the end of the function call and never used again. So, we can use *rvalue references* and moving to take ownership of the data of another class. An rvalue reference is a reference to an object that will never be used again. Thus, it is safe to steal its data.

A *move constructor* is used to initialize a new value from an rvalue reference. It could look something like this:

```cpp
Vector(Vector&& v) : elem{v.elem}, sz{v.sz} {
    // Steal v's resources, and then ensure that it is safe to run them for v's destructor
    v.elem = nullptr;
    v.sz = 0;
}
```

After stealing a reference to the data, you should leave the referenced object in a state ready for the destructor. Since destructors clean up data, you need to make sure no references to the stolen data exist in the referenced object, otherwise they will get cleaned up when the destructor runs.

A simple way of implementing a move constructor is to default init the object, and then swap the object's state with the other object. This ensures that the other object is in a safe state for destruction.

You can likewise define a *move assignment*.

```cpp
void operator=(Vector&& v) {
    // need to destroy this's data first though
    // for this, I think you use copy-and-swap
    elem = v.elem;
    sz = v.sz;
    v.elem = nullptr;
    v.sz = 0;
}
```

Implementing a move assignment manually is quite tricky. As above, a simple method is two swap this object's state with the other. The object thus has acquired the other one's resources, and the other object will automatically destruct cleaning up the resources.

The move constructor and assignment should both be labelled noexcept.

The compiler will automatically pass rvalue references when appropriate, such as when returning from a function. However, you can explicitly return an rvalue reference using `std::move`.

```cpp
Vector a{3};
Vector b = std::move(a);    // move constructor
// make sure you don't use a after this point, because it is now an empty object!
```

Keep in mind the *Rule of Five*. A class should implement either none or all of these:

- destructor
- copy constructor
- copy assignmet
- move constructor
- move assignment

This ensures ownership of resources is efficiently and properly managed.

## Inheritance

I heartily dislike inheritance. However, since C++ does not have interfaces, you sometimes need to use inheritance to emulate them. (This is mostly useful for dynamic dispatch. In all other cases, avoid it.)

To prevent a class from being subclassed, add `final` to the class declaration.

```cpp
class Vehicle final {
    // stuff
};
```

## Static and Dynamic Dispatch

How do we achieve polymorphism in code? For example, suppose I have a vector, and I want it to contain a bunch of objects. How do I do this? If I know that all my objects will be of the same type, I can use a template. This is very simple, straightfoward, and has high performance. When I retrieve an object from the vector and call a particular method on it, say `<<`, the compiler knows at compile time which function to choose because it knows at compile time the type of the object. Easy peasy. However, what if I want to store a bunch of different objects in there. For example, say I have an interface called `Expr` that represents a mathematical expression, and I want to store a bunch of different expressions in the vector, not necessarily of the same type? Suppose I get an `Expr` from the vector and want to print its information using `<<`. How does the compiler know which function to call? The answer is through using `virtual` functions. In C++, we have to implement this using inheritance, since it has no concept of interfaces. In other languages though, like with interfaces in Java and traits in Rust, it is possible to implement this without using inheritance. This allows us to do dynamic dispatch, which is unavoidable in some cases. (Note that this is better than simply throwing the types you want into an ADT, and then matching on the type and then calling the appropriate function. It is equivalent, but not as elegant and likely less performant.) Sigh. This is so much more elegant in Rust. In Rust I just have to make a struct implement a trait, and all that virtual-dynamic stuff is taken care of. Gah! That's the thing! Because of the clean demarcation between structs and traits, there is a nice clean boundary between static and dynamic dispatch. This doesn't really exist in C++. In some sense, traits in Rust implement subtyping polymorphism, but in a very clean and obvious way. In that sense, the only object oriented thing to avoid is inheritance. Blast it! I hate inheritance.

With static dispatch you know the type information at compile time. Dynamic dispatch indicates that you lack some information, and only know the precise type at runtime.

Any time you want to know the "best" way to do something, see how the people in Haskell do it, then adapt that for the language you want. In Haskell, dynamic dispatch is done using closures. The "trait" or "interface" is simply the type signature of the function you want to use. You could do something like this in C++ (Rust as well), but those languages have simpler and likely more performant support for that sort of stuff using trait objects and virtual functions. Plus, if did the closure thing in C++, we would have to worry about lifetime issues if we captured by reference (in a lambda), and copying would be quite unperformant if we captured by value. In a GC language like Haskell, this is no problem, but in C++ (and Rust) it is more of a problem, which is why they implement dynamic dispatch using virtual pointers and such. On an abstract level, closures are equivalent to interface, but they are implemented very differently.

There are two ways I think to do dynamic dispatch. The first is to use inheritance and virtual functions; the second is to use an enum in Rust or boost::variant in C++, and then switch on the type there. These methods are very similar.

# Virtual Functions




# Namespaces

By default, all identifiers are put in the global scope. This is rather bad, because it can easily lead to name clashes. Namespaces allow you to group together identifiers.

```cpp
// mycode.hpp
namespace mycode {
    // all the header information goes up here
    class Complex {
        // all the class declarations
    };

    Complex sqrt(Complex);
}

// mycode.cpp

Complex mycode::Complex::Complex(const double re, const double im) {
    // constructor
}

// note the prefix
Complex mycode::sqrt(Complex) {
    // implementation for this function
}
```

You should always put everything in a namespace.

If I am correct, namespaces are usually placed in header files (where they encapsulate the type declarations).

# Exceptions

Personally, I believe all error handling should be done using the `Maybe` type. For my own code, this is what I will use. However, the vast majority of the C++ world disagrees with me, so I do need to know how to catch and deal with exceptions, even if I do not implement them myself. Basic exception handling code looks like this:

```cpp
try {
    // code that might cause an exception
} catch (exception_to_catch) {
    // handle the exception
} catch (another_exception) {
    // handle that exception
}   // etc.
```

For example, the `out_of_range` exception type is defined in the standard library `<stdexcept>`, and is used by some standard library containers.

Whenever you are using a function or keyword you did not define yourself, you need to check for any exceptions that it may raise and prepare to deal with them appropriately.

For example, the `new` operator throws a `std::bad_alloc` exception if it cannot allocate the requested object.

Boost has `optional` and `variant` data types that mimic `Maybe` and `Either` from Haskell, so I suggest using those (rather than rolling your own). C++17 might come with these in the stdlib though, so keep an eye out.

## noexcept

You can declare a function `noexcept` to indicate a function should not throw an exception. If the function does throw an exception, no stack unwinding is done and the program calls `std::terminate` immediately. This doesn't call any destructors like a normal exception throw does, so is obviously a bad thing, even if you are throwing an exception that is not meant to be caught (like a logic error). So, you should only add `noexcept` to functions that are *guaranteed* to not throw exceptions.

```cpp
int add(const int a, const int b) noexcept {
    return a + b;
}
```

Note that any dynamic memory allocation could throw a `std::bad_alloc`, so do not use `noexcept` when mutating an STL container. In general, you should only declare a function `noexcept` if all the functions within it are also `noexcept`.

Adding `noexcept` to constructors, copy, and move functions is rather important, because this allows the compiler to do more stuff with move semantics I think. But again, don't sweat it, because exceptions are very nice for indicating bad errors.

# Static Assertions

The `static_assert` function is used to check conditions at compile time. If the condition is not met, compilation stops and a message is printed. For example,

```cpp
static_assert(4 <= sizeof(int)), "integers are too small");
```

In general, `static_assert(condition, message)` will print `message` if the `condition` is not met. The `condition` can be anything that can be expressed using constant expressions.

```cpp
constexpr double pi = 3.14;
static_assert(pi > 3, "uh oh, math broke");
```

# Templates

**Templates** allow you to write polymorphic (or generic) code. The basic syntax of a template looks like this.

```cpp
template<typename TypeVariable>
```

You can use templates to write generic classes.

```cpp
template<typename T>
class vector {
  private:
    T* elem;
    int sz;
  public:
    vector(int s);
    ~vector() {
        delete[] elem;
    }

    T& operator[](int i);
    const T& operator[](int i) const;
    int size() const;
};

template<typename T>
vector<T>::vector(int s) {};

template<typename T>
const T& vector<T>::operator[](int i) const {};
```

The class can be instantiated with a type like this.

```cpp
vector<char> vc{200};
```

The type `T` is replaced with `char` at compile time, so there is no performance overhead.

We need to define `begin` and `end` functions to use our `vector` in a range based `for` loop.

```cpp
template<typename T>
T* begin(vector<T>& x) {
    return x.empty() ? nullptr : &x[0];
}

template<typename T>
T* end(vector<T>& x) {
    return begin(x) + x.size();
}

for (auto& s : vc) {
    std::cout << s << std::endl;
}
```

All templates must be in header files, unless you explicitly instantiate the template yourself.

## Explicit Instantiation

Normally, all templated functions must go in header files, since the compiler must know the function definition in order to instantiate it when it is used. However, if the template is only instantiated for a few cases, it might be nice to

```cpph

// The template parameters can be ommited if they can be inferred
template void func(int a);
```

Note that when doing a full function specialization, that specialization will cause duplicate symbols if it is in a header file. The solution is to make the specialization inline, or to put the specialization in a cpp file and then declare an `template <>` in the header (which gives the prototype).

# Dynamic Memory

Allocating memory in C is done with `malloc` and `free`. These functions suck. C++ has better ways of dealing with memory.

- `new` dynamically allocates memory and initializes an object through its constructor
- `delete` then runs the destructor for that object and deallocates the memory

The calculation of how much space you need for an object is done automatically for you. You don't need to calculate it using `sizeof`, like in C.

```cpp
// new returns a pointer to the object
ClassA* x = new ClassA{10};
// you need to explicitly call delete for everything you allocate with new
// this will call the destructor for the object, and then deallocate the memory
delete x;

// length 20 array
int* arr = new int[20];
// do stuff with arr
// do not forget to use the [] version of delete!
delete[] arr;
```

# I/O

I/O functions in C suck. Really suck. C++ has much better I/O functions.

## Cin and Cout

The `cin` and `cout` iostreams can be used to get input from stdin and print output to stdout. They are part of the `std` namespace, so keep that in mind when using them. To use `cout`, simply provide the arguments to print to the console between the bitshift operator `<<`.

```cpp
int x = 1;
char name[] = "hello";
std::cout << name << ' ' << x << std::endl;  // prints hello 1\n
```

To display a newline, you need to print the `endl` thing in `std`. `cin` reads text from the console, which can then be saved in a variable (use the `>>` operator). The given input will automatically be coerced to the type of the input variable (which may be a bad idea if the user enters invalid input).

```cpp
std::cout << "Enter a number ";
int x;  // Not much point in initializing x
std::cin >> x;  // Read a number from the console and store it in x
std::cout << "You entered " << x << std::endl;
```

# Integers

As usual, the native integer types should be avoided. Use the fixed width types in `<cstdint>` instead.

# Floating Point Numbers

Unfortunately, the standard library does not contain fixed width floating point types. Boost does, however.

# Boolean Values

C++ has a builtin boolean type; it has two data values: `true` and `false`. Boolean data types are of type `bool`, are declared as such.

```cpp
bool truth = true;
bool falsity = false;
```

Other than being built-in, boolean values are identical to those in C (including the annoying `true` evaluating to `1` and `false` evaluating to `0`).

# Characters and Strings

## Characters

You can use `std::cin` to get characters from the command line, but note that it will only store the first character you provided.

```cpp
std::cout << "Enter a letter: ";
char letter;
std::cin >> letter;
std::cout << "You entered " << letter << std::endl;
```

# Pointers and References

## Null Pointers

Instead of the macro `NULL`, the null pointer is represented by the keyword `nullptr`. It works exactly as null pointers usually work.

```cpp
int* x = nullptr;
if (x != nullptr) {
    x += 1;
}
```

## Unique Pointers



## References

Pointers are dangerous beasts. C++ has a safer feature called **references** that perform a similar task.

### Const References

What is a `const reference`? It is essentially a pointer to an object, but you cannot modify the object through that pointer. Note that this *does not mean the object itself is `const`*. The object itself can be `const` or non-`const`, and if the latter, could change at any time behind your back. So when you grab a value from a `const` reference, do some stuff, and then get another value, those two values could be different!

```cpp
void func(const std::vector<int>& x, std::vector<int>& y) {
    // Reference to the first element of x
    const auto& a = x[0];
    const auto b = x[0];
    y[0] = 3;
    // x and y could reference the same thing, so now it is
    // possible that a is different! But, b will still be the
    // same
}

int main() {
    const std::vector<int> b{1, 2, 3};
    func(b, b);
}
```

In this regard, `const` references aren't really constant. The reference is constant, but the value you point to is not. If you want to ensure a variable is truly constant, you need to make a copy and declare that `const`. Rust forbids schenanigans like this. Dang, references in C++ are still dangerous.

Now, the reference *itself* is still `const`, because you can't make it refer to a different object, for example. With pointers the rules are different.

## Return Values for Functions

```cpp
class Test final {
    private:
    std::vector<std::string> vec;
    public:
    explicit Test(const std::string& x) {
        vec.push_back(x);
    }

    // Returns a copy of the string that exists
    // outside the vector, so modifying the return
    // value of this function won't modify this object,
    // which is why we can declare this function const.
    std::string value(const size_t i) const {
        return vec.at(i);
    }

    // Prevents move semantics for objects, and the caller
    // should determine if they want the return value
    // to be constant or not, so this is pointless.
    const std::string const_value(const size_t i) const {
        return vec.at(i);
    }

    // With the reference functions, however, you need to
    // be careful about iterator invalidation. If a change
    // is made to the vector, like an insertion that reallocates
    // the vector, all the references to objects inside the
    // vector will become invalid. Ouch. This is one place in
    // C++ where unsafe memory access is very easy to do.

    // Return a reference to the string inside the vector
    // This function cannot be marked const, since modifying
    // the string outside this function can modify the vector
    std::string& reference(const size_t i) {
        return vec.at(i);
    }

    // Returns a const reference to the string inside the vector
    // The caller of the function must obey the const directive
    // of the reference, so it cannot modify the vector,
    // hence the const nature of the function
    const std::string& const_reference(const size_t i) const {
        return vec.at(i);
    }
};

int main() {
    Test x{"hello"};

    // Now this performs a copy of string reference returned, so
    // this string now lives outside the object
    auto str1 = x.reference(0);
    auto str2 = x.const_reference(0); // note doesn't have to be const
}
```

# The Standard Library

The standard library is defined within the namespace `std`. The standard library is written in C++ itself; it is not a basic language feature provided by the compiler.

# Container Objects

C has arrays. They are fast and simple, but they suck. C++ has better container objects within its standard library.

## std::array

This is an addition in C++11. It represents a fixed length array, and is essentially a wrapper around a static C-style array that knows its length. Like C-style arrays, you need to know the length of a `std::array` at compile time. This length is provided as a constant expression. Thus, the usability of a `std::array` is limited compared to that of a `std::vector`.

## std::vector

A `vector` is a wrapper around a dynamically allocated C-style array. Like `std::array`, it knows its length and can do bounds checking.

Note there is an important difference between `push_back` and `emplace_back`. You pass the object you want to add to `push_back`, while in `emplace_back` you pass the *arguments to the constructor of the object*, which will be constructed in the array without creating a temporary.

```cpp
std::vector<std::pair<int, int>> x
x.push_back({1, 2});    // creates an object, and then copies it inside the vector
x.emplace_back(1, 2);   // no intermediate copy
```

## std::string

The `std::string` type is a much needed improvement over C-style strings. They know their own length and have many overloaded operators that make life so much easier.

As usual, strings can be created in many different ways:

```cpp
std::string x = "hello";
std::string x {"hello"};
std::string x = {"hello"};
std::string x("hello");
```

Strings can also be copied (hooray!),

```cpp
std::string x = "howdy";
std::string y = x;
```

compared,

```cpp
bool comp = x == y
```

and concatonated,

```cpp
std::string x = std::string("y") + std::string("a");
```

Wow. How awesome is that?

# Control Flow

**Statements** are the most basic construct in C++. All statements are terminated by a semicolon.

```cpp
int x = 0;
std::cout << x;
```

Newlines are not important in C++.

## For Loop

C++ has *range* based for loops for iterating over containers. Note that this can only be used on sequences that know their own length, so it cannot be used for C arrays.

```cpp
std::vector<int> vec = {1, 2, 3};
for (int x : vec) {
    std::cout << x << std::endl;
}
```

## While Loop

A `while` loop has the following syntax.

```cpp
while (condition) {
    // stuff
}
```

The body will be executed as long as the `condition` is true. Everything you can do with a `for` loop you can do with a `while` loop, but in general, you should use a `for` loop for definite iteration and a `while` loop for indefinite iteration.

```cpp
int i = 0;
while (i < 5) {
    printf("\%d\n", numbers[i]);
    i += 1; }
```

## If, Else, and Else If

C does conditional evaluation using `if`, `else`, and `else if` statements. The general syntax looks like this

```cpp
if (condition) {
    /* body */ }
else if (condition) {
    /* body */ }
else {
    /* body */ }
```

`condition` is a Boolean expression, and if it returns any non-zero integer, the body of the block is executed. If it returns `0`, the block is not executed. As with Python, the `else` and `else if` blocks are optional.

```cpp
if (x == 1) {
    puts("Hello World.");
} else if (y == 2) {
    y = 3;
} else {
    puts("I don't know.");
}
```

C also has `switch` statements. Don't use them. They are incredibly easy to break and just crytic all around. Use `if-else` instead.

# Basic Data Structures

# Enumerated Classes

Old C-style enums are type unsafe and weakly scoped, they are just treated as `int`s. Instead, C++11 defines an **enum class** data type, which improve on C-style enums. For example,

```cpp
// This does not allocate any memory
// The enum objects are implemented as int32_t under the hood
enum class Color : int32_t {
    RED,
    ORANGE,
    YELLOW,
    GREEN,
    BLUE,
    INDIGO,
    VIOLET
};  // Don't forget the semicolon!

// Declare an instance of the Color type like this
// Memory allocation happens here
Color favourite_color = Color::RED;
```

By default, all enumerators are implemented as `ints` under the hood. However, it is best to explicitly state the enumeration base type in the declaration, as we did above.

`enum classes` are useful for code documentation purposes and representing a fixed number of states. For example, we can use `enum classes` to represent error codes.

```cpp
enum class parse_result : int32_t {
    success,
    error_opening_file,
    error_reading_file,
    error_parsing_file
};

parse_result parse_file() {
    if (!open_file()) {
        return parse_result::error_opening_file;
    }
    if (!read_file()) {
        return parse_result::error_reading_file;
    }
    if (!parse_file()) {
        return parse_result::error_parsing_file;
    }

    return parse_result::success;
}

int main() {
    if (parse_file() == parse_result::success) {
        // do stuff
    }
}
```

Enum classes are just special types of classes, so you can define operators and methods for it.

## Using

C++11 has an alternative way of declaring a `typedef`. It looks like this:

```cpp
using MyType = OldType;
```

For example,

```cpp
using Speed = int;
```

Unfortunately, this new way of declaring types is only weakly typed, just like a `typedef`. However, it is easier to read, and can be used to create template aliases.

```cpp
template <typename T>
using CustomVec = std::vector<T, Allocator<T>>
```

## Variable Scope

A **block** is a piece of code enclosed within braces. For example,

```cpp
int main() {
    int x = 1;
    std::cout << x << std::endl;
}
```

the statements between the `{` and the `}` are the block for the `main` function.

All variables are local to the block they are defined in. They can only be accessed from within that block and are destroyed when that block ends. For example,

```cpp
int main() {
    int x = 1;
    if (x == 1) {
        int y = 2;
    }  // y is local to the if statement, so it is destroyed here

    std::cout << y << std::endl;  // Nope! y is not defined
}
```

Variables within nested blocks can access variables in high ones. For example,

```cpp
int x = 1;
if (x == 1) {
    int y = 2;
    std::cout << x << y << std::endl;  // We can access x from here
}
```

Be careful not to shadow higher variables with variables in a lower scope, however. This is confusing and should be avoided.

```cpp
int x = 1;
if (x == 1) {
    x = 2;  // Reassing variable in higher scope
    int x = 2;  // New variable local to this scope
}
```

In general, variables should be declared in as local a scope as possible. This reduces complexity and cleans the variables up sooner. (In this respect, using the comma operator might be justified.)

**Global variables** are defined outside of any block (within the main body of the program). These variables have **program scope**, so they can be accessed anywhere within a program.

```cpp
int x = 1;  // This is a global variable

int main() {
    std::cout << x << std::endl;  // We can acces x from here
    return 0;
}
```

Global variables usually get a bad rap that they do not entirely deserve. The common complaint is that because any function can access them, any function can also change them, so it is difficult to know the value of a global variable without a detailed knowledge of the program. For example,

```cpp
int x = 1;  // x is a global variable

void do_something() {
    x = 2;  // It is not clear which x this will change until the function is called
}

int main() {
    x = 3;

    do_something();

    std::cout << x << std::endl;  // Uh, x is now 2, not 3!
    return 0;
}
```

The problem here is not with global variables, but with *impure functions*. The function `do_something` is impure; it has the side effect of changing x. If all functions are pure, there are no issues with global variables, because the functions cannot change them. Furthermore, the global variable would be provided as an argument to the function and not silently accessed within, so there are no issues if you, for example, decide to change the name of the global variable.

Note there is slight technicality when using variables defined inside one program in another. Say we are building a math library and want to have a file with some common mathematical constants.

```cpp
// constants.cpp
const double e = 2.71;
const double pi = 3.14;
```

```cpp
// constants.h
#ifndef CONSTANTS_H_INCLUDED
#define CONSTANTS_H_INCLUDED

extern const double e;
extern const double pi;

#endif
```

```cpp
// main.cpp
#include <iostream>

#include "constants.h"

int main() {
    double radius = 1;
    double area = pi * radius ** 2;

    std::cout << "The area is " << area << std::endl;

    return 0;
}
```

The `constants.h` header file needs to be included to let `main.cpp` know what type the variable `pi` is. Within that header file, the declaration of `pi` needs to be preceeded with the `extern` keyword. This is important. The statement

```cpp
const double pi;
```

is a **definition** of the variable `pi`; it allocates memory and gives it an initial value (which would be random). Adding the `extern` keyword makes the statement a **declaration** (of the variable's type) and *does not* allocate any memory. Thus, if we had simply said the first statement, the variable `pi` would have been allocated a random value, and we would have gotten a random value for the `area`. The second statement is only a declaration; no memory is allocated, so the compiler would know to look elsewhere to find the value of `pi` (in `constants.cpp` in this case).

By default, all global variables and functions have program scope, meaning they can be referenced by other files and included when they are linked together ("imported" in Python lingo). We saw this when we "imported" `pi`. You can change this by adding a `static` keyword before the variable definition. This changes the variable so it only has **file scope** and thus cannot be included into other files. For example

```cpp
static const double pi = 3.14;  // This variable can only be accessed within this file

static int area(double radius) {  // Same with this function
    return pi * radius ** 2;
}
```

The general idea is that all global variables are public by default, and adding `static` makes then private. Note that adding `static` should only be done to global variables; local variables within functions can be declared `static`, but this does something entirely different and should be avoided.

# Control Flow

## If Statements

If statements allow conditional execution. The code contained in an `if` statement will only execute if some condition evaluates to `true` (or `1`). General `if` statements look very similar to ones in other languages.

```cpp
if (condition) {
    // some code
} else if (other_condition) {
    // some more code
} else {
    // even more code}
```

Like other languages, you can include as many `else if` statements as you wish. Note that you should *always* include the braces around `if`, `else if`, and `else` blocks. This improves readability, reduces ambiguity, and avoids problems like the *dangling else*.

```cpp
if (x > 5) {
    std::cout << "x is greater than five" << std::endl;
} else if (x == 5) {
    std::cout << "x is equal to five" << std::endl;
} else {
    std::cout << "x is less than five" << std::endl;
}
```

## Switch Statements

Do not use switch statements. The are bug prone and unpleasant. Use `if-else` statements instead.

## Goto Statements

> Go To Statements Considered Harmful

'Nuff said.

## While Statements

A `while` statement will execute code as long as some condition is true.

```cpp
while (condition) {
    // code
}
```

Note that if `condition` evaluates to `false` the first time the `while` loop is encountered, the code in the body of the loop will not execute at all.

```cpp
int32_t num = 0;
while (num < 10) {
    std::cout << num;
    num += 1;
}
```

To use an infinite loop, use

```cpp
while (true) {
    // code
}
```

Some people use `for (;;) {}`, but I don't. Note that all variables local to the body of the while loop are destroyed at the end of each iteration and recreated at the beginning of the next one.

```cpp
int32_t i = 0;
while (i < 5) {
    int32_t j = 0;  // This variable is destroyed and then reinitialized at each iteration
    std::cout << j << " ";  // Will print 0 0 0 0 0
    i += 1;
}
```

## Do While Loops

Huzzah! C++ has `do while` loops, which are identical to `while` loops except the loop condition is checked at the end of the body, so the body always executes at least once!

```cpp
do {
    // code
} while (condition);
```

```cpp
int32_t number;  // number needs to be declared here
do {
    std::cout << "Enter the number 1 > ";
    std::cin >> number;
} while (number != 1);  // Because it needs to be accessed here!
```

When using a `do while` loop, make sure you do not declare within the loop body a variable used in the condition; a variable inside the block will be destroyed once the body exits, so referencing it within the `while` condition causes an error.

## For Loops

The `for` loop in C++ is essentially a glorified `while` loop. It just takes the initialization, condition, and increment statements of a `while` loop when you are doing definite iteration and places them at the top of the loop. The general syntax is as follows:

```cpp
for (initialization; condition; increment) {
    // Code
}
```

The *initialazition* statement is executed the first time the loop is entered. Any variables declared within this statement are local to the loop and are destroyed once the loop exits. If the *condition* statement is true, the loop body executes. If not, the loop is exited. Once the body is done, the *increment* statement is evaluated. Then it goes back to the condition statement, etc. For example,

```cpp
for (size_t i = 0; i < 10; i += 1) {
    std::cout << i << ' ' << std::endl;
}  // i is destroyed here
```

You can leave out any or all of the `for` loop expressions, but don't do this. You can also write `for` loops with an empty body.

```cpp
for (int32_t i = 0; i < 10; i += 1) {}
```

In some cases this might seem convenient, but don't do it. Just use a normal `while` loop: it is more explicit, simpler, and is obvious you didn't forget to write the body.

In general, avoid using an unsinged integer as a loop variable. It is rather error prone.

## Break and Continue

The `break` keyword is used to terminate a loop immediately.

```cpp
while (true) {
    int x;
    cin >> x;
    if (x == 0) {
        break;
    }
}
```

The `continue` keyword is used to skip the rest of a loop and go back the top for the next iteration. Be careful when using the `continue` keyword in a while loop, because it might skip updating the loop variable.

```cpp
for (int32_t i = 0; i < 10; i += 1) {
    if (i % 2 == 0) {
        continue;  // Skip the even numbers
    }
    std::cout << i;
}
```

```cpp
int32_t i = 0;
while (i < 10) {
    if (i == 5) {
        continue;  // Watch out!
    }

    std::cout << i;  // When i == 5, we get an infinite loop!
}
```

Be judicious when using `break` and `continue`. They disrupt the flow of execution and can make the code more difficult to reason with.

# Iterators

Remember iterating over an array using pointers? The good old

```c
for (int* p = a; p < a + len; ++p) {
    // stuff
}
```

It sucks. C++ has a much better and more powerful feature called **iterators**. They act like pointers to elements of an array (or container more generally) but are much better.

# Operator Overloading

Ahh, good old operator overloading. Unlike Java. C++ actually has operator overloading. Hooray! Here is the canonical way of implementing operator overloading for a basic class. In general, I implement most operator overloads as `friend` functions inside the class. This gives the function access to any private members of the class necessary to implement the operator properly, but is also not a method (so no dangerous `this` pointer). There are multiple ways to implement operator overloading, as usual.

# Conversion Operators


# Working With Types

## Type Conversion and Casting

C-style type casts are dangerous. Instead, you should use `static_cast`. The general syntax for a static cast is `static_cast<type>(variable)`. For example,

```cpp
int x = 10;
int y = 3;
double divide = static_cast<double>(x) / static_cast<double>(y);
```


# C Headers

C++ has two versions of every header in the C standard library. For example, it has `<stdio.h>` and `<cstdio>`. These two are identical, except the former puts all definitions in the global namespace, and possibly in the `std` namespace. The latter puts all in the `std` namespace, and possible in the global. In general, it is preferable to use the second, and preface C specific functions with `std` (note that macros ignore namespaces, and so are always present in the global namespace).

# Variadic Constructor

As said [here](https://www.reddit.com/r/cpp/comments/5h5vhw/fixing_stdinitializer_listt/), you should prefer variadic constructors to `std::initializer_list`.

# C++ Vs Java

Some will contend that the JVM JIT compiler has advantages over C++. It may have a few, but those can be easily mitigated if you care about performance.

- The JIT is a "HotSpot" compiler that will optimize the most run parts of your code. Well, use PGO for C++.
- The JIT can use processer specific instructions. Well, compile with -march=native.
- Java is better at heap allocation. Yep, it is, but in C++ you don't much heap allocation at all, so it doesn't matter.

Java just has a huge amount of overhead. Generics, boxing, and the metadata associated with each object makes it unsuitable for very-high performance code. Unlike C++ code, Java is very difficult to tweak. PLUS, there is the overhead of the HotSpot compiler itself, which needs to start from scratch every time you restart your program. Ridiculous. There is no way Java can seriously contend with C++ for performance. If you are very performance sensitive, use Rust or C++ instead.

# Unity Build

If you a making a single executable or shared library with no iteration (such as for a release), it is usually faster to concat all your build files into one massive one and just compile that. This way, independent object files don't need to be created, and templates don't need to be instantiated multiple times. For releases, this is the fastest way to go (and also helps with inter-procedural optimization).
