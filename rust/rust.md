# Rust

> Rust is a systems programming language focused on three goals: safety, speed, and concurrency.
>
> -- The Rust Book

Rust is the greatest advance in systems level programming since the development of C++. It is a game changer. Despite being so low-level, it is the safest language on the planet (after Haskell, of course). It's just so amazing. Let's dive in.

Also, Rust also comes with a copy of its own docs (!), so check them out using `rustup doc`.

# rustc

There are two types of rust code: executables and libraries. All executables must be named `main.rs`.

Compiling a Rust executable is easy; just go

```fish
rustc main.rs
./main
```

and you're done!

# Cargo

Cargo is Rust's build and dependency management tool. It is similar to make, but much better.

# Variable Bindings

Variable bindings in Rust look like this:

```rust
let x = 5;
```

Rust has type inference, so it is not necessary to specify the type of `x`. However, for integers it is best to explicitly state the type, which can be done like this:

```rust
let x: i32 = 5;
```

This makes `x` a 32-bit integer.

By default, all variable bindings are immutable (hooray!). This means you cannot reassign a variable or change any of its interval structure.

```rust
let x = 5;
x = 10;     // does not compile
```

If you want a binding to be mutable, use `mut`.

```rust
let mut x = 5;
x = 10;
```

Note that `mut` is part of a pattern, so you can do fancy things like

```rust
let (mut x, y) = (5, 4);
```

It is also possible for a variable to mutable for a short period of time.

```rust
let mut x = 3;
x = 4;
let x = x;  // x is now immutable
```

This uses *variable shadowing* to create a new variable with the same name as the old one.

Variables can also be declared without being given an initial value. Unlike other languages, these variables are *not* assigned a default value, and *cannot* be used without being initialized first.

```rust
let x: i32;
```

Note that a type signature is mandatory here, since there is nothing to infer.

A *constant* can be declared using the `const` keyword. These values are always immutable, must be declared with a type, and must have a value known at compile time. The are usually written in UPPER_CAPS case.

```rust
const MAX_POINTS: u32 = 100_000;
```

## println!

The `println!` macro

# Functions

Functions are declared with the `fn` keyword. Here are some examples

```rust
// function that takes no arguments and returns nothing
fn foo() {

}

// takes some arguments
fn print_number(x: i32, y: i32) {
    println!("x is {}, y is {}", x, y);
}

// returns a value
fn sum(x: i32, y: i32) -> i32 {
    x + y
}
```

Note that the type signature of a function must be included; it is not inferred.

Every executable starts with the `main` function.

```rust
fn main() {
    // your awesome code here
}
```

By default, all functions that do not explicitly return a value return the empty tuple `()`. Unlike say, Python, this is completely fine, because the compiler will warn you if the types don't match up.

```rust
fn hello() {
}

let x = hello();    // x: ()
```

However, some functions never return, like functions that print errors. These types of functions are called *diverging*, and they have return type `!`.

```rust
fn error(msg: String) -> ! {
    panic!(msg);
}
```

## Function Pointers

Rust has stupidly easy function pointers. For example,

```rust
let f: fn(i32) -> i32;
```

declares a variable that points to a function with the given signature. It can then be called just like a normal function.

```rust
fn plus_one(x: i32) -> i32 {
    x + 1
}

let f = plus_one;
let x = f(6);
```

# Statements and Expressions

# Booleans

Rust contains a built in boolean type `bool`. It has two values: `true` and `false`.

```rust
let x = true;
let y: bool = false;
```

# Characters

The `char` datatype represents a single UTF-8 character. You can create one by enclosing the symbol in single quotation marks.

```rust
let x = 'x';
let y: char = '2';
```

# Numbers

Rust has builtin fixed width integers (huzzah!). It has `u8 u16 u32 u64 u128` for unsigned integers, and `i8 i16 i32 i64 i128` for (two's complement) signed integers. Rust also has integers whose size is the size of the underlying machine architecture (so memory address size). They are `isize` and `usize`, which are the signed and unsigned versions.

Rust also has `f32` and `f64`, which represent the corresponding IEEE-754 floating point numbers.

All integer literals can be given a type suffix to indicate their type (hooray!), and I recommend using that in all circumstances.
If not specified explicitly, integer literals default to `i32` and floats to `f64`.

```rust
let x = 3   // x: i32
let y = 5.0 // y: f64
let x = 3u32;   // do this
let y = 126i64;
```

Integer literals can also use `_` to separate digits. One can also use the `0x`, `0o`, `0b` prefixes for hexademical, octal, and binary literals.

# Arrays

Arrays can be created like so.

```rust
let a = [1, 2, 3];
let mut b = [4, 5, 6];
```

Arrays have type `[T; N]`, where `T` is the type of the elements in the array and `N` is the length of the array. Note that the length is a compile time constant (like 3, for example). Thus, they are stack allocated, so it is impossible to change their size once the program is running.

There is a handy syntax for initializing each element of an array to the same value.

```rust
let a = [0; 20];    // each element is initialized to 0
```

You can get the size of an array using the `len` method.

```rust
println!("a has {} elements", a.len());
```

You can access elements of an array using subscript notation. Note that arrays are zero-indexed and bounds checked.

```rust
let names = ["Graydon", "Brian", "Niko"]; // names: [&str; 3]

println!("The second name is: {}", names[1]);
```

# Slices

A **slice** is a reference to a part of another data structure. It allows you to read or write to that data structure without copying (sorta like two iterators in C++, or two pointers in C).

```rust
let a = [0, 1, 2, 3, 4];
let complete = &a[..];  // all of a
let middle = &a[1..4];  // elements 1, 2, 3
```

Slices have type `&[T]`.

# Tuples

Tuples can be declared like this:

```rust
let x = (1, "howdy");   // x: (i32, &str)
```

Unlike Python, the parentheses around the tuple are mandantory. You can assign one tuple to another if they have the same type.

```rust
let mut x = (1, 2);
let y = (2, 3);

x = y;
```

You can *destructure* a tuple like so (whoo hoo!)

```rust
let (x, y, z) = (1, 2, 3);
```

A 1-tuple looks like this `(0,)`.

The elements of a tuple are indexed from 0, and can be accessed like so:

```rust
let tup = (1, 2, 3);

let x = tup.0;
let y = tup.1;
let z = tup.2;
```

It's identical to array indexing, except it uses a `.` instead of `[]`.

# Comments

```rust
// Line comments look like this. They are just like C++ comments

/// Doc comments look like this. They support markdown notation. They are meant
/// to document a function or code coming after them.
///
/// # Examples
///

//! These are special doc comments that document crates and modules, since they go inside
//! them.
```

You can generate the documentation for a project using the `rustdoc` command line tool.

# if

Rust has the standard `if`, `else if`, and `else` like most other languages.

```rust
let x = 5;

if x == 5 {
    println!("x is five!");
} else if x == 6 {
    println!("x is six!");
} else {
    println!("x is not five or six :(");
}
```

However, `if` is also an expression, so you can do something like this:

```rust
let y = if x == 5 { 10 } else { 15 };
```

Note that the braces are mandatory.

# Loops

Rust has three types of loops.

## loop

The `loop` keyword is used to create an infinite loop.

```rust
loop {
    // do stuff until we break the loop
}
```

Note that this is better than using the common `while true`, because it gives better control flow analysis for the compiler.

You can also return values from these loops, though I think using a function in this case is better.

## while

The `while` statement loops while a condition is true.

```rust
fn collatz(mut n: u64) -> u64 {
    let mut iters: u64 = 0;

    // note there are no parentheses
    while n != 1 {

        n = if n % 2 == 0 { n / 2 } else { 3 * n + 1 };

        iters += 1;
    }

    iters
}
```

## for

A `for` loop is rather abstract. It looks like this:

```rust
for var in expression {
    // stuff
}
```

where `expression` is an **iterator**. This is exactly how Python for loops work.

To generate an iterator of numbers, use the `..` syntax.

```rust
for x in 0..10 {
    println!("{}", x);
}
```

In true programming style, this iterates from 0 to 9, leaving off 10 (as usual).

To iterate through an array

```rust
let a = [10, 20, 30, 40, 50];

for elem in a.iter() {
    println!("the value is: {}", elem);
}
```

You can reverse an iterator using `.rev()`.

If you want to keep track of

## break and continue

Rust also has `break` and `continue`, which work as normal.

# Ownership

First, let's see what `=` means.

In C, saying `a = b` is simple: it just copies the stack bytes of `b` into `a`. In C++, you can overload `=`, so it can mean whatever you want it to be. Rust is different than both: `=` moves a value. However, this could lead to issues. If you have a struct that contains a pointer to heap memory, copying that struct will result in *two* pointers to that memory, which leads to endless problems about reallocating, concurrent access, etc. Rust solves this with ownership.

Variable bindings have *ownership* of what they're bound to. This means when a binding goes out of scope, Rust will free the bound resources. This is entirely similar to destructors in C++. For example,

```rust
fn foo() {
    let v = vec![1, 2, 3];
}
```

This creates a vector called `v`, which has some heap memory allocated to hold the elements. Once `v` goes out of scope, the allocated memory in the vector is automatically cleaned up.

Rust also ensures there is exactly one binding to any resource. For example,

```rust
let v1 = vec![1, 2, 3];
let v2 = v1;
```

The assignment `let v2 = v1` moves the value of `v1`, so we can't use `v1` after doing this.

```rust
let v1 = vec![1, 2, 3];
let v2 = v1;
println!("v1 is {}", v) // error!
```

This also means you can't do things like this.

```rust
fn take(v: Vec<i32>) {
    // some stuff
}

let v = vec![1, 2, 3];

take(v);

println!("v is {}", v);     // error!
```

These ownership rules prevent data races and allow move semantics.

## `Copy` Types

By default, reassignment transfers ownership of an object, since copying the stack value might be unsafe. However, you can give a type the `Copy` trait, which means that copying is safe. For example,

```rust
let x = 3;
let y = x;  // y has ownership of new value

println!("x is {}", x); // x retains ownership of its old value
```

All primitive types implement the `Copy` trait, so you can working with them is natural.

# Borrowing

Because of move semantics, it is impossible to get basic copy issues with vectors or whatever.

By default, `=` transfers ownership (unless the object you are reassigning implements `Copy`, I think). This is problematic for function, because what if you want to use the original resource after the function call? You have to do something like this.

```rust
fn foo(v1: Vec<i32>, v2: Vec<i32>) -> (Vec<i32>, Vec<i32>, i32) {
    // do some stuff
    // hand back ownership
    (v1, v2, 42)
}

fn main() {
    let v1 = vec![1, 2, 3];
    let v2 = vec![4, 5, 6];

    let (v1, v2, ans) = foo(v1, v2);
}
```

Very unelegant. You can get around this using *references*. A reference lets you look at an object without owning it, so the original bindings retain ownership and are still valid. You can create a reference using a `&`.

```rust
fn foo(v1: &Vec<i32>, v2: &Vec<i32>) -> i32 {
    // do stuff with v1 and v2
    42
}

fn main() {
    let v1 = vec![1, 2, 3];
    let v2 = vec![4, 5, 6];

    let ans = foo(&v1, &v2);
}
```

By default, all references are immutable. Hooray!

```rust
fn foo(v: Vec<i32>) {
    v.push(5);  // does not compile
}

fn main() {
    let v = vec![];
    foo(&v);
}
```

### &mut References

*Mutable* references allow you to mutate the resource you're borrowing. These references have type `&mut T`. Note this syntax is different than a normal reference: it's `&mut T`, not `mut &T` (the first is a reference to a mutable resource, the second is a mutable reference).

```rust
let mut x = 5;
{
    let y = &mut x;
    *y += 1;
}   // mutable reference ends
println!("{}", x);  // immutable reference begins
```

Three things to note:
- The original resource must also be marked `mut`, or else you would be taking a mutable reference to an immutable resource.
- The original is changed using a `*`.
- The printing of `x` cannot occur until the mutable reference of `y` has gone out of scope. In theory we could launch the `y` piece of code in another thread, which would lead to a data race with printing and updating `x`.

## Referencing Rules

- No reference can last for a scope greater than that of the owner (no dangling pointers).
- At any time you can have either one mutable reference or any number of immutable references.

This is to prevent data races. A data race occurs when you have mutiple threads accessing the same data, where at least one is writing. The rules above ensure you are only reading, or only writing from exactly one thread. Pretty darn awesome.

These rules prevent some common problems:

- Iterator Invalidation. It is impossible to modify an iterator while you are iterating over it.

```rust
let mut v = vec![1, 2, 3];

for i in &v {
    println!("{}", i);
    v.push(32);     // gahh! an immutable reference to v already occurs from the loop
                    // so we can't mutate v again
}
```

- Use after free. References can live no longer than the values they refer to

```rust
let y: i32;
{
    let x = 5;
    y = &x
}
println!("{}", y);  // uh oh, big error!
```

It is also illegal to declare a reference before declaring the variable it refers to.

```rust
let y: i32;
let x = 5;
y = &x;

println!("{}", y);  // error!
```

Variables are cleaned up in the opposite order they are declared, so `y` would live longer than `x`, which is not allowed.

# Structs

There are various ways to declare structs. The most basic is like this:

```rust
struct Point {
    x: i32,
    y: i32,
}
```

Note that structs are written in CamelCase, as is usual these days.

Structs can be created using a `key: value` syntax, with whatever order you like.

```rust
let origin = Point { x: 0, y: 0 };
```

You can access the entries of a struct using the dot notation.

```rust
let mut origin = Point { x: 0, y: 0};
origin.x = 3;

println!("({}, {})", origin.x, origin.y);
```

Individual entries of a struct cannot be made mutable, since mutability is the property of a binding and not the data structure.

## Update Syntax

A struct can include `..` to copy some values from a struct into another one. For example,

```rust
struct Point3D {
    x: i32,
    y: i32,
    z: i32,
}

let origin = Point3D { x: 0, y: 0, z: 0};
let point = Point3D { y: 1, .. origin};
// this copies the x and z values from origin to the point
```

## Tuple Structs

Tuple structs have names, but their fields don't.

```rust
struct Color(i32, i32, i32);
struct Point(i32, i32, i32);

let black = Color(0, 0, 0);
let origin = Point(0, 0, 0);
```

In most cases, it is better to use a struct rather than a tuple struct, since the fields are given names that way. Compare:

```rust
// red, blue, green
struct Color(i32, i32, i32);

// No need to document, the names are part of the code.
struct Color {
    red: i32,
    blue: i32,
    green: i32,
}
```

One common use case for a tuple struct is the `newtype` pattern. This is similar to a typedef in C, but is (of course) strongly typed.

```rust
struct Meters(i32);

let length = Meters(10);
// can use destructuring on a tuple struct, just like with a tuple
let Inches(integer_length) = length;
println!("length is {} meters", integer_length);
```

## Unit-like Structs

You can define a struct with no members at all:

```rust
struct Electron;

let x = Electron;
```

This is called *unit-like* because it resembles the empty tuple `()`, which has no members.

# Enums

An `enum` is a sum type of structs. You can use any kind of struct you want inside the enum.

```rust
enum Message {
    Quit,
    ChangeColor(i32, i32, i32),
    Move { x: i32, y: i32 },
    Write(String),
}
```

The variants inside the enum are scoped by the name of the enum. They can be accessed using `::`.

```rust
let x: Message = Message::Move { x: 3, y: 4 };

enum BoardGameTurn {
    Move { squares: i32 },
    Pass,
}

let y: BoardGameTurn = BoardGameTurn::Move { squares: 1 };
```

Since enums are scoped, the `Move` thing inside `Message` and `BoardGameTurn` don't conflict.

# Match

You can use the `match` keyword to pattern match on values. They look like this:

```rust
match expr {
    val => expression,
    ...
}
```

Match returns the `expression` of the first `val` that matches. Note this implies all the expressions must return the same type. For example,

```rust
let x = 5;
let number = match x {
    1 => "one",
    2 => "two",
    3 => "three",
    _ => "something else",
}
```

```rust
let x = 5;

match x {
    1 => println!("one"),
    2 => println!("two"),
    3 => println!("three"),
    _ => println!("something else"),
}
```

Note that all the `println!`s return `()`, so this is all good.

You can also do pattern matching on enums.

```rust
enum Message {
    Quit,
    ChangeColor(i32, i32, i32),
    Move { x: i32, y: i32 },
    Write(String),
}

fn quit() {}
fn change_color(r: i32, g: i32, b: i32) {}
fn move_cursor(x: i32, y: i32) {}

fn process_message(msg: Message) {
    match msg {
        Message::Quit => quit(),
        Message::ChangeColor(r, g, b) => change_color(r, g, b),
        Message::Move { x: x, y: y } => move_cursor(x, y),
        Message::Write(s) => println!("{}", s),
    }
}
```

Another note is that match statements are *exhaustive*. If matching against an enum, you need to have a match for every variant. As a "catch-all", you can always just put a `_` at the end of a match.

# Methods

You can use the `impl` keyword to define methods on structs.

```rust
struct Circle {
    x: f64,
    y: f64,
    r: f64,
}

impl Circle {
    fn area(&self) -> f64 {
        std::f64::consts::PI * self.r * self.r
    }
}

fn main() {
    let c = Circle { x: 0.0, y: 0.0, r: 2.0 };
    println!("{}", c.area());
}
```

All methods for a struct go inside an `impl` block. All methods also take a special first parameter, which refers to the object the method is being called on. This first parameter could be

- `self`, which moves the struct
- `&self`, which is a reference to the struct
- `&mut self`, which is a mutable reference (hence the struct itself must be mutable)

You should use `&self` by default.

```rust
impl Circle {
    fn grow(&self, increment: f64) -> Circle {
        Circle { ..self, r: self.r + increment }
    }
}

fn main() {
    let c = Circle { x: 0.0, y: 0.0, r: 2.0 };
    println!("{}", c.area());

    let d = c.grow(2.0).area();
    println!("{}", d);
}
```

You can also define associated functions that do not take a `self` parameter. These are similar to static methods.

```rust
impl Circle {
    fn new(x: f64, y: f64, r: f64) -> Circle {
        Circle {
            x: x,
            y: y,
            r: r,
        }
    }
}

fn main() {
    let c = Circle::new(0.0, 0.0, 2.0);
}
```

Note that associated functions are called with the `Struct::function()` syntax, rather than `ref.method()`.

## Builder Pattern

The builder patern is common when creating structs in rust.

```rust
struct Cirle {
    x: f64,
    y: f64,
    r: f64,
}

impl Circle {
    fn area(&self) -> f64 {
        std::f64::consts::PI * self.r * self.r
    }
}

struct CircleBuilder {
    x: f64,
    y: f64,
    r: f64,
}

impl CircleBuilder {
    fn new() -> CircleBuilder {
        // the default values
        CircleBuilder { x: 0.0, y: 0.0, r: 1.0 }
    }

    fn x(&mut self, coord: f64) -> &mut CircleBuilder {
        self.x = coord;
        self
    }

    fn y(&mut self, coord: f64) -> &mut CircleBuilder {
        self.y = coord;
        self
    }

    fn radius(&mut self, radius: f64) -> &mut CircleBuilder {
        self.radius = radius;
        self
    }

    fn finalize(&self) -> Circle {
        Circle { x: self.x, y: self.y, r: self.r }
    }
}

fn main() {
    let c = CircleBuilder::new()
                    .x(1.0)
                    .y(2.0)
                    .r(2.0)
                    .finalize();
}
```

# Strings

In Rust, all strings are sequences of valid UTF-8 characters. Also, strings are not null terminated and can contain null bytes.

Rust has two types of strings: `&str`, which are called string slices, and `String`, which is a mutable string object (like `std::string` in C++).

String literals are of type `&'static str`.

```rust
let greeting = "Hello world";   // greeting: &'static str
```

This string is statically allocated in the `data` segment of the program. String slices have a fixed size and cannot be mutated.

A `String` is heap-allocated and growable. It's like a UTF-8 vector, essentially. You can convert a `&str` to a `String` using the `to_string` method.

```rust
let mut s = "Hello".to_string();    // mut s: String
println!("{}", s);

// append to the end
s.push_str(", world.");
println!("{}", s);
```

You can convert a `String` to an `&str` with an `&`:

```rust
fn takes_slice(slice: &str) {
    println!("Got: {}", slice);
}

fn main() {
    let s = "Hello".to_string();
    takes_slice(&s);    // note this is a &str, not a &Strig
}
```

Viewing a `String` as a `&str` is cheap, but converting a `&str` to a `String` involves allocating memory.

## Indexing

Strings are encoded with UTF-8, which is a variable length encoding. Thus, you cannot simply index to a certain character, because you don't know how far along in memory that character is. Thus, you have to walk through the bytes of a string each time in order to get the `nth` one.

To iterate over the characters of a string, do something like this:

```rust
// Awww yeah! Unicode strings
let hachiko = "忠犬ハチ公";

for c in hachiko.chars() {
    println!("{}, ", c);
}
```

To get the `nth` character, use the `nth` method (recall, strings start with the 0th character).

```rust
let dog = hachiko.chars().nth(1);   // like hachiko[1]
```

# Generics

Generics, aka parametric polymorphism, allow you to use multiple types in your code. For example, here is how you create a generic enum.

## Generic Structs

```rust
struct Point<T> {
    x: T,
    y: T,
}

let a: Point<i64> = Point { x: 0, y: 0 };
let b: Point<f64> = Point { x: 0.0, y: 0.0 };

impl<T> Point<T> {
    fn swap(&mut self) {
        std::mem::swap(&mut self.x, &mut self.y);
    }
}
```

## Generic Enums

```rust
enum Option<T> {
    Some(T),
    None,
}

let x: Option<i32> = Some(5);

enum Result<T, E> {
    Ok(T),
    Err(E),
}

let y: Result<i64, String> = Ok(3);
```

## Generic Functions

```rust
fn id<T>(x: T) {
    x
}

fn other_stuff<T, U>(x: T, y: U) {
    // ...
}
```

# Traits

A **trait** is essentially a typeclass or interface. A trait describes a set of methods that a type must implement.


# Crates and Modules

A *crate* is a library or package of Rust code. Crates can produc an executable or library.

Each crate has an implicit *root module* that contains the code for the crate. You can then create a tree of sub-modules that branch out from the root module.

To create a

# Collections

## Vector

A vector `Vec<T>` is a dynamic array. Unlike an array, which is placed on the stack (if it's not too big), all vectors are allocated on the heap (hence their dynamic nature).

You can create a vector using the `vec!` macro.

```rust
let v = vec![1, 2, 3, 4];   // v: Vec<i32>
```

Note that unlike the `println!` macro, we use square brackets here. They are entirely equivalent, and are more convenient in this situation. There is another form that lets you initialize all elements of a vector.

```rust
let v = vec![0; 10];    // vector of 10 zeros
```

You can use the regular `[]` syntax to access elements of a vector. Note that this is bounds checked by default (Huzzah!).

```rust
let v = vec![1, 2, 3, 4, 5];
println!("v[2] = ", v[2]);
```

All indices must be of type `usize`.

You can iterate over a vector using `for` in several ways.

```rust
let mut v = vec![1, 2, 3, 4];

for i in &v {
    println!("reference to {}", i);
}

for i in &mut v {
    println!("mutable reference to {}", i);
}

for i in v {
    println!("take ownership of v and {}", i);
}
```

One thing I like about Rust is it doesn't specify field order. The compiler is free to reorder the fields as it sees fit for optimization purposes. This is much better than specifying them manually, because the sizes can often change from system to system and platform to platform.
