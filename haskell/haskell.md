# Introduction

Haskell is a **functional** programming language. It is different than other common programming languages, which are **imperative**. It lacks common programming constructs like `for` loops, `if` statements, and even variable reassignment. However, it makes up for this with its own powerful constructs.

The Glasgow Haskell Compiller is the de facto Haskell compiller. It comes with three components:

- **ghc**, a compiler
- **ghci**, an interative interpreter
- **runghc**, an interpreter that runs Haskell code as a script (without compiling)

When you start ghci, you get a prompt that looks like `Prelude>`. This indicates that the `Prelude` module is loaded, which is a standard library of common functions. When you load other modules or source files, they will show up in the prompt too. To change the prompt, use `:set prompt "my awesome prompt"`. The `Prelude` is analagous to Python's `builtins`: these functions and names are always avaliable. To load other modules, use `:module + MyModule`, e.g. `:module + Data.Ratio`.

## GHCi

In GHCi, you can use several commands

- `:load` or `:l` for loading files
- `:reload` or `:r` for reloading
- `:type` or `:t` for getting the type of an object

# General Constructs

## Variables

In Haskell, variables are names bound to expressions (not necessarily values). Once a variable is defined, it cannot change. You can define variables inside ghci using the `let` function. Eg.

```haskell
ghci> let e = exp 1
```

## Conditional Evaluation

Haskell has `if-then-else` construct like other languages, but an important note is that it is an *expression*, not a statement. It must always return a value, which means the `else` clause is mandatory. It is rather simple.

```haskell
ghci> if 1 > 2 then "Something is wrong" else "The world is as it should be"
"The world is as it should be"
```

The `it-then-else` has three parts: the **predicate**, which is an expression of type `Bool`, the expression that evaluates if the predicate is `True`, and then the expression that evaluates if the expression is `False`. The expressions after the predicat are known as **branches**. These must have the same type, and the entire `if` expression has the return type of the branches.

## Case

Pattern matching is not restricted to function definitions. The `case` construct lets us match patterns within an expression.

```haskell
a = case expression of
         pattern0 -> expression0
         ...
         patternN -> expressionN
```

The pattern match is performed on the value of `expression` in the order the patterns are given. The expression of the first pattern to match is evaluated. Note that the type of all the pattern expressions must be the same. As with pattern matching in functions, we can use underscores to match any value.

```haskell
fromMaybe :: a -> Maybe a -> a
fromMaybe defval wrapped = case wrapped of
                                Nothing -> defval
                                Just value -> value
```

# Atomic Data Types

## Numbers

There are several number types in Haskell. The `Integer` type represents signed integers of arbitrary size (rather like in Python, since under the hood `Integer` is a wrapper around GMP). The `Int` type represents signed machine-size integers, so 32 or 64 bit. The `Float` and `Double` types represent floating point numbers of 32 and 64 bit width respectively.

## Basic Mathematics

Haskell provides the basic mathematical operators that you expect `+ - * /`.

```haskell
ghci> 2 + 2
4
ghci> 31337 * 101
3165037
ghci> 7 / 2
3.5
```

Note that `/` performs floating point division. Integer division is done using the function `div`, and modulus with `mod`.

You can also write these expressions in **prefix** form. To do this, you must enclose the operator in parenthesis.

```haskell
ghci> (+) 2 2
4
```

Not too shabby.

Haskell also provides `^` for integer exponentiation, and `**` for floating point exponentiation.

```haskell
ghci> 313 ^ 15
27112218957718876716220410905036741257
```

Note that the minus sign `-` in Haskell is a unary operator, so when you write things like `-3`, you are not writing the literal "minus 3", you are saying "negate 3". Because of this, you must always enclose negative numbers in parenthesis, otherwise the unary operator will be provided as an argument to the funtion (along with the numbers).

```haskell
ghci> 2 * (-3)
-6
```

## Conversions

Haskell does not provide implicit integer converion (hallelujah). It must be done explicitly.

You can convert from an `Int` or `Integer` with `fromIntegral`. The type it should convert to is inferred from the environment.

```haskell
y :: Int
y = 4

x :: Double
x = fromIntegral y
```

You can convert a `Float` or a `Double` to an integral type using `round`, `floor`, and `ceiling`. Again, the explicit type to convert to will be inferred from the environment.

```haskell
x :: Double
x = 4.2

y :: Int
y = round x
```

## Boolean Values and Operators

The Boolean values in Haskell are `True` and `False`, and are of type `Bool`. Unlike other languages (notably Python), `0` is not equal to `False`, and non-zero objects are not equal to `True`. Hooray!

Haskell uses the Boolean operators `&&` for and, `||` for or, and `not` for not.

```haskell
ghci> True && False
False
ghci> not True
False
```

## Orderings

It also uses the common relational operators `== > < >= <=` and `/=` for not equal.

```haskell
ghci> 1 < 3
True
ghci> 2 == 2
True
ghci> 6 /= 4
True
```

## Operator Precedence

Haskell assigns a precedence value to operators, with 1 being the lowest precedence and 9 the highest. You can look at the precendence of an operator using the `:info` command. Eg. `:info (+)`. The output of info also tells us the associativity of the operator: infixl operators are evaluated from left to right, infixr operators are evaluated from right to left, and plain old infix operators are non-associative. The combination of precedence and associativity are know as the *fixity* rules.

# Composite Data Types

A **composite** data type is constructed from other types. The most common composite data types in Haskell are lists and tuples.

## Lists

A list is surrounded by brackets, with the elements seperated with commas. The empty list is written as `[]`.

```haskell
ghci> [1, 2, 3]
[1, 2, 3]
```

Lists can be of any length and any type, and all the elements must be of the same type. In the above example, the list is of type `[Integer]`. The list `[[True, False], [True]]` is of type `[[Bool]]`, because it consists of lists of Booleans. Because the elements of a list can be any type, we say the list type is **polymorphic**. To signify this in type signatures, we use a **type variable**, which represents any type. Type variables must start with a lowercase letter to distinguish them from type names, which must start with an uppercase letter. Thus, we symbolically say that lists are polymorphic by giving them the type signature `[a]`, which `a` is a type variable and represents any type.

You can use the enumeration notation `..` to fill in the contents of a list that can be enumerated. This will return a closed interval (that is, the list will contain both endpoints). In other words, the type must have a defined predecessor and successor.

```haskell
ghci> [1..10]
[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
```

You can also optionally specify the step by providing the first two elements of the enumeration.

```haskell
ghci> [10,8..2]  -- the step is mandatory for stepping down
[10,8,6,4,2]
ghci> [1,4..15]
[1,4,7,10,13]
```

You can also use enumerations with floating point numbers, though I would avoid this. Floating point numbers are always quirky.

If you want, you don't have to specify an upper bound, eg. `[1..]` will create an infinite list. This will create an endless list of numbers in the interpreter, but because the compiler is lazy, this will actually work in a program. This is a very powerful, because the program will only evaluate the elements of the list that it needs to. There are several common functions for creating infinite lists.

The `cycle :: [a] -> [a]` function cycles the given list around infinitely.

```haskell
ghci> cycle [1, 2, 3]
[1,2,3,1,2,3 ...]
ghci> cycle "LOL "
"LOL LOL LOL ..."
```

The `repeat :: a -> [a]` function returns an infinite list of the given element.

```haskell
ghci> repeat 5
[5, 5, 5, 5, ...]
```

The `replicate :: Int -> a -> [a]` will return a list of the given length of the given element.

```haskell
ghci> repeat 3 10
[10, 10, 10]
```

You can concatenate two lists using the `(++) :: [a] -> [a] -> [a]` operator.

```haskell
ghci> [] ++ [1, 2, 3] ++ [4]
[1, 2, 3, 4]
```

The `(:) :: a -> [a] -> [a]` operator, known as construct, adds an element to the front of a list.

```haskell
ghci> 2 : [3, 4]
[2, 3, 4]
```

Note that the bracket notation for lists is merely syntactic sugar for repeated application of the cons operator. For example, `[1, 2, 3]` is shorthand for `1 : 2 : 3 : []`.

Further note, lists in Haskell are linked-lists, *not* arrays. So if you want to do any intensive string or listy processing, you should use a vector or something.

The `(!!) :: [a] -> Int -> a` operator returns the element of a list at a particular index. Lists in Haskell are 0-indexed (as they should be!), so keep that in mind.

```haskell
ghci> [1,3,7] !! 2
7
ghci> "hello" !! 1
'e'
```

Lists of type `Ord a` can be compared in lexigraphical order using the relational operators (`>`, `<`, `==`, etc.). (No kidding right?)

The `length :: [a] -> Int` returns the length of a list.

```haskell
ghci> length [1, 3, 4]
3
ghci> length ""
0
```

The `null :: [a] -> Bool` function checks if a list is empty.

```haskell
ghci> null [1, 2]
False
ghci> null []
True
```

The `head :: [a] -> a` function returns the first element of a list.

```haskell
ghci> head [1, 2, 3, 4]
1
ghci> head "abcd"
'a'
```

The `tail :: [a] -> [a]` function returns all except the head of the list (within a list).

```haskell
ghci> tail [1, 2, 3, 4]
[2, 3, 4]
ghci> tail "abcd"
"bcd"
ghci> tail [True, False]
[False]
```

The `last :: [a] -> a` function returns the last element of a list.

```haskell
ghci> last [1, 2, 3, 4]
4
ghci> last "abcd"
'd'
```

The `init :: [a] -> [a]` function returns all except the last element of a list (within a list).

```haskell
ghci> init [1, 2, 3, 4]
[1, 2, 3, 4]
ghci> init "ab"
"a"
```

The `take :: Int -> [a] -> [a]` function takes the first `n` elements of a list (and return them in a list).

```haskell
ghci> take 2 [1, 2, 3, 4]
[1, 2]
```

The `drop :: Int -> [a] -> [a]` function returns all except the first `n` elements of a list.

```haskell
ghci> drop 3 [1, 2, 3, 4]
[4]
```

The `reverse :: [a] -> [a]` function reverses a list.

```haskell
ghci> reverse [1, 2, 3]
[3,2,1]
ghci> reverse "howdy"
"ydwoh"
```

The `maximum :: Ord a => [a] -> a` function returns the largest element of a list.

```haskell
ghci> maximum [1, 2, 3]
3
ghci> maximum "hello"
'o'
```

The `minimum :: Ord a => [a] -> a` function returns the smallest element of a list.

```haskell
ghci> minimum [1, 2, 3]
1
ghci> minimum "hello"
'e'
```

The `elem :: (Eq a, Foldable t) => a -> t a -> Bool` tells you if an element is within a list.

```haskell
ghci> elem 4 [1, 2, 3, 4]
True
ghci> 'a' `elem` "hello"
False
```

The `sum :: Num a => [a] -> a` returns the sum of a list.

The `product :: Num a => [a] -> a` returns the product of a list.

### List Comprehension

A **list comprehension** is used to create lists of objects that satisfy certain conditions. They are very similar to set comprehensions. For example

```haskell
> [2 * x | x <- [1..10]]
```

The identifier `x` is our dummy variable, and `x <- [1..10]` means that `x` is draw from `[1..10]`. We can add a predicate (Boolean function), to the comprehension. Predicates, as many as you want, go after the binding parts and are separated by commas.

```haskell
[2 * x | x <- [1..10], 2 * x >= 10]
```

```haskell
[ x | x <- [50..100], x `mod` 7 == 0]
```

The process of weeding out lists by predicates is called **filtering**. You can put any function at the front of the comprehension, even less obvious ones.

```haskell
boomBang xs = [ if x < 10 then "BOOM!" else "BANG!" | x <- xs, odd x]
```

## Characters and Strings

A **character** of type `Char` represents a single Unicode character. They are enclosed in single quotes, eg. `'a'`. As with other languages, you can escape whitespace characters, eg. `'\n'` is a newline and `'\t'` is a tab.

A **string** is simply a list of characters, and is thus of type `[Char]`. Strings are surrounded by double quotation marks, eg. `"this is a string"`, though this is equivalent to including the characters in a list.

```haskell
ghci> let a = ['o', 'n', 'e']
ghci> a
"one"
ghci> a == "one"
True
```

The empty string is written as `""`, and is the same as `[]`.

```haskell
ghci> "" == []
True
```

Since strings are just lists of characters, we can use the list operators on them.

```haskell
ghci> "one" ++ "two"
"onetwo"
ghci> 'a' : "bc"
"abc"
```

Cool, eh?

The `putStrLn` function prints a string to the screen.

```haskell
ghci> putStrLn "My amazing string"
"My amazing string"
```

The `lines` function splits a string on its line boundaries and returns the split strings in a list.

```haskell
ghci> lines "thequick\nbrownfox\njumps"
["thequick", "brownfox", "jumsp"]
```

## Tuples

A **tuple** is a collection of values, where each value can have a different type. You create a tuple by enclosing the values within parenthesis, separating them with commas.

```haskell
ghci> (2014, "Haskell is the best")
(2014, "Haskell is the best")
```

The type of tuple in the above example is `(Integer, [Char])`. The type of `((True, 2), "Bub")` is `((Bool, Integer), [Char])`. Note that the order and number of elements in a tuple are part of its type, so a tuple of type `(Bool, Integer)` has a different type than `(Integer, Bool)`. Furthermore, the length of a tuple is a part of its type; a tuple of type `(Integer, Integer)` is very different than a tuple of type `(Integer, Integer, Integer)`. This is different from lists, where the length of the list is not part of its type.

We use `()` to signify a tuple of zero elements. Haskell does not have 1-tuples, since `(a) iso a`. Tuples are often used to return multiple values from functions.

```haskell
fst :: (a, b) -> a
snd :: (a, b) -> b
```

# Functions

To apply (not call) a function in Haskell, write the name of the function followed by its arguments, separated by spaces.

```haskell
ghci> odd 3
True
ghci> compare 2 3
LT
```

Function application has higher precedence than operators, so `odd 3 == True` is equivalent to `(odd 3) == True`. Sometimes, you need to include parenthesis to indicate how an expression should be parsed.

```haskell
ghci> compare (sqrt 3) (sqrt 6)
LT
```

Like other objects, functions have types as well. The general format for the type of a function is

`param0Type -> ... -> paramNType -> returnTuple`

For example,

```haskell
ghci> :type lines
lines :: String -> [String]
```

It is considered good style to give all top level functions in your code an explicit type signature. Though Haskell usually does a fantastic job of inferring the types itself, occasionally it needs a little help.

Functions are called **polymorphic** if their signatures contains type variables, i.e. they accept arguments of any type.

In Haskell, (almost) all functions are **pure**. Their output can only depend on the arguments provided to the function, and they cannot modify any global state (that is, they do not have side effects). **Impure** functions are not pure. The only **impure** functions in Haskell are IO functions. You can identify these functions, because the type of the return value will begin with `IO`.

```haskell
ghci> :type readFile
readFile :: FilePath -> IO String
```

The above function returns a `String`, but because it is impure (the contents of the file might be different depending on when you read it), we indicate this with `IO`.

Functions are defined very simply. For example,

```haskell
add a b = a + b
```

Unlike imperative languages, functions are not a series of statements: they are a single expression.

You can continue the definition of a function by putting it on multiple lines. This is merely for readability purposes.

If a function takes two parameters, you can call it in infix notation by surrounding it in backticks. For example,

```haskell
ghci> div 92 10
9
ghci> 92 `div` 10
9
```

## Pattern Matching (Hallelujah!)

Functions can also be defined through **pattern matching**. This allows us to define the behaviour of a function for specific value constructors. For example,

```haskell
myNot True = False
myNot False = True
```

That was quite simple. Here is another example.

```haskell
sumList :: Num a => [a] -> a
sumList [] = 0
sumList (x : xs) = x + sumList xs
```

Let's step through evaluating `sumList [1, 2]`. `[1, 2]` is just shorthand for `1:2:[]`. This matches the pattern `x : xs`, so that branch of the function is called. This continues until we get down to `[]`. In this case, the first pattern matches, so 0 is returned. An important note is that the function patterns are checked in the order they are defined; only the first pattern that matches will be executed, and all others after it ignored.

You can also peek inside composite data types with pattern matching.

```haskell
third (a, b, c) = c
aFunction (one, x:y:xs, three) = (three, y, x)
```

There is no limit to how "deep" you can look inside a composite data type. You can also match literals in the pattern. That's what we did for the `myNot` function.

```haskell
select [True, "hello", four] = four ++ "hello"
```

## Errors

Haskell provides an error function, `error :: String -> a`, that you can call to report errors. It takes a string as a parameter, which is the error message, and immediately aborts evaluation to print that message. It has a polymorphic return type so that you can call this function anywhere and it will always have the right return type (to make the compiler happy).

```haskell
second :: [a] -> a
second xs = if null (tail xs)
            then error "list too short"
            else head (tail xs)
```

```haskell
ghci> second "xi"
'i'
ghci> second [2]
*** Exception: list too short
ghci> head (second [[9]])
*** Exception: list too short
```

One issue with the `error` function is that it crashes the program immediately (a bit like a `raise` statement in Python). This function should really only be used to indicate extreme errors that cannot be recovered from (in C, this would maybe like running out of memory to allocate, but I'm not sure of an equivalent thing in Haskell). In most cases, it would be better if we could recover from this error and (potentially) keep going. To do this, we can use the `Maybe` data type.

```haskell
data Maybe a = Just a
             | Nothing
```

If the operation fails, we can return `Nothing`. Otherwise if it succeeds, we can wrap the return value in `Just`. We can also use pattern matching to clean up the definition of the function.

```haskell
second :: [a] -> Maybe a
second (_:x:_) = Just x
second _ = Nothing
```

Another option is using the `Either` data type. It is defined as

```haskell
data Either a b = Left a
                | Right b
```

It is basically a binary data type that stores two values. The left type usually contains a string describing the error, and the right data type contains the return value if the function was successful. So beautiful.

## Local Variables

Local variables can be introduced into the body of a function using the `let-in` expression; `let` begins a block of variable declarations, and `in` ends it. Each line between the two keywords introduces a new variable.

```haskell
lend amount balance = let reserve = 100
                          newBalance = balance - amount
                      in if balance < reserve
                         then Nothing
                         else Just newBalance
```

The **scope** of a variable are the places where you can access that variable in your code. In this case, the scope of the *let-bound* variables is within the `let` block and within the expression that follows the `in` keyword.

You can also nest multiple `let` blocks within each other.

```haskell
foo = let a = 1
       in let b = 2
          in a + b  -- this is the return value

```

Each `let` block has its own subscope, so variables referenced within the block will be searched for within the block. Thus, it is legal to repeat variable names within a nested `let` expression. This is called **shadowing**.

```haskell
foo = let x = 1
       in ((let x = "hello" in x), x)

-- return value is ("hello", 1)
```

This allows you to shadow a function's parameters too.

```haskell
foo a = let a = "hello"
         in a ++ "world"
print foo 1  -- returns "helloworld"
```

Because the parameter `a` is shadowed by the let-bound `a`, it is never used in the function, so it can be any type.

```haskell
ghci> :type foo
foo :: t -> [Char]
```

You can also use the `where` clause to introduce local variables. The definitions in a `where` clause apply to the code that precedes it, so it must always go at the end of a function.

```haskell
lend :: Integer -> Integer -> Maybe Integer
lend amount balance = if amount < reserve * 0.5
                      then Just newBalance
                      else Nothing
                      where reserve = 100
                            newBalance = balance - amount
```

You can also define local functions within `let` and `where` blocks.

```haskell
pluralise :: String -> [Int] -> String
pluralise word counts = map plural counts
                        where plural 0 = "no " ++ word ++ "s"
                              plural 1 = "one " ++ word
                              plural n = show n ++ " " ++ word ++ "s"
```

Local functions can use variables from the scope that encloses them. This is how the `plural` function used the `word` variable from the `pluralise` function.

## Guards

Pattern matching allows us to match against specific value constructors. We can use `guards` for condition evaluation, like `if` statements. Here is the general syntax.

```haskell
myFunction pattern match
    | boolean expression = return expression
    -- etc...

```

Each pattern in a function can be followed by zero or more guards, each of which contains an expression of type `Bool` and a return expression. The return expression is the result of the function of the guard expression evaluates to `True`. As you expect, all the return expressions must be of the same type, and the guards expressions are evaluated in the order they are given.

```haskell
lend amount balance
    | amount <= 0 = Nothing
    | amount > reserve * 0.5 = Nothing
    | otherwise = Just newBalance
  where reserve = 100
        newBalance = balance - amount
```

The expression `otherwise` expression is defined to be equal to `True` to aid readability.

```haskell
myDrop _ [] = []
myDrop n xs
    | n <= 0 = xs
myDrop n (_:xs) = myDrop (n - 1) xs
```

Thus, guards are equivalent to a long `if-elif-else` chain in imperative languages.

You can combine guards and pattern matching:

```haskell
foo :: Integer -> Integer
foo 0 = 16
foo 1
    | "Haskell" > "Go" = 3
    | otherwise        = 4
foo n
    | n < 0            = 0
    | n `mod` 17 == 2  = (-42)
    | otherwise        = n + 3
```

## The Offside Rule and White Space

Like Python, whitespace is important in Haskell. First, you start off a line at some indentation. The *layout heralds* are `where`, `let`, `do`, `of`, and `case`. When a layout herald appears, GHC looks at the indentation of the next line. Any lines following it of the same indentation are part of that group, and anything to the left is part of a new group.

# Accumulator

Occassionally, we need to keep track of an accumulator as we iterate across some data structure. If you recall, we looked at this quite extensively in the Wizard Book. For example,

```haskell
sumTo20 :: [Integer] -> Integer
sumTo20 nums = go 0 nums
    where go acc [] = acc
          go acc (x : xs)
             | acc >= 20 = acc
             | otherwise = go (acc + x) xs
```

Writing Haskell feels like building things with Lego. You get a bunch of small pieces, and they all fit together cleanly into a nice structure.

# The Three Functional Functions

# Types

Every expression and variable has a certain type. Haskell has a strong, static, inferred type system. It is strong because Haskell will never accept arguments to functions with the wrong type. An expression that does obey the type rules is said to be **well typed**, and one that does not obey the rules is **ill typed** and will cause a **type error**. Furthermore, Haskell does not have type coercion; unlike some languages like C and Python, it will not automatically convert a `Float` to an `Int` for us. This must be done explicitly with a type casting function. It is static because the types of values are known at compile time, which lets Haskell catch type errors for you. It is also inferred, because Haskell can infer the types of almost all expressions for you; no need to declare them (take that Java!!!). Haskell requires type names to start with an uppercase letter, and variable names (including type variables) to start with a lower case letter. You can get ghci to print out the types of expressions using `:type`, eg `:type 'a'`. This command doesn't actually evaluate the expression, it just prints its types. Note that the value of the last expression is stored in the special variable `it`.

The **type signature** of a value is expressed using `::`. For example, the type signature of `3` is `3 :: Integer`. You can explicitly state the type signature of a variable,

```haskell
ghci> 'a' :: Char
'a'
```

or you can let the compiler infer it (which one usually does).

## Algebraic Data Types

You can define an **algebraic data type** using the `data` keyword. The general syntax is this:

```haskell
data TypeConstructor = ValueConstructor components ...
                       other information
```

For example,

```haskell
data BookInfo = Book Int String [String]
```

The type constructor and value constructor must both begin with uppercase letters. `BookInfo` is the name of the type, and `Book` is the name of the function we use to create a new book. For example,

```haskell
myBook = Book 1234 "My Fav Book" ["Albert Einstein", "George Washington"]
```

Note that the type constructor and value constructor are usually given the same name. This does not create a conflict, because type constructors can only exist within type declarations and type signatures, while value constructors can only appear within expressions.

Algebraic data types can have more than one constructor. The `Bool` data type, for example, has two:

```haskell
data Bool = False | True
```

In the above example, `False` and `True` are simply functions that take no arguments. Each of the different value constructors can accept multiple arguments.

```haskell
data Shape = Circle Double
           | Square Double Double

myCirle = Cirle 10  -- has type Shape
mySquare = Square 20 10  -- has type Shape

```

Algebraic data types can be **deconstructed** using pattern matching. This allows us to acces the values supplied to the constructor when the object was created. Using the `Shape` type we declare above, we can define a `perimiter` function.

```haskell
perimeter (Circle radius) = 1 / 2 * tau * radius ** 2
perimeter (Square length width) = 2 * (length + width)

perimiter myCircle
perimiter mySquare
```

We can use this to extract the components of an object.

```haskell
bookID (Book id title author) = id
bookTitle (Book id title author) = title
bookAuthor (Book id title author) = author
```

You can use an underscore `_` to indicate that you don't care about a particular component in a pattern match. For example,

```haskell
bookID (Book id _ _) = id
bookTitle (Book _ title _) = title
bookAuthor (Book _ _ author) = author
```

GHC will give a warning if a variable name is introduced in a pattern but not used in the function. Using an underscore avoids this. Incidentally, this allows you to use underscores for a wildcard pattern, where you don't care about the specific value constructors.

```haskell
goodExample (x : xs) = x + goodExample xs
goodExample _ = 0
```

It is important to consider all value constructors when using pattern matching, otherwise the function may be undefined for a particular input (e.g. forgetting to define the case for the empty list).

Algebraic data types can also be defined using **record syntax**. Instead of simply giving the types of the object's components, we also give them names. This automatically defines accessor methods, and provides additional structure for the type.

```haskell
data Customer = Customer { customerID :: CustomerID
                         , customerName :: String
                         , customerAddress :: Address
                         } deriving (Show)

lincoln = Customer 123 "Abraham Lincoln" ["White House"]
customerID lincoln  -- returns 123
customerName lincoln  -- returns "Abraham Lincoln"
customerAddress lincoln -- returns ["White House"]

-- We can also create a value like this
washington = Customer {
    customerID = 1234,
    customerName = "George Washington",
    customerAddress = ["White House"]
}

-- The printing is also different

print washington
--> Customer {customerID = 1234, customerName = "George Washington", customerAddress = ["White House"]}
```

## Polymorphic Algebraic Data Types

You can make types polymorphic by adding type variables into the type declaration.

```haskell
-- This data type mimics a two tuple
data TwoTuple a b = TwoTuple a b

myTuple = TwoTuple "hello" 1
-- myTuple has type TwoTuple [Char] Integer
```

The `TwoTuple a b` type constructor accepts two type variables, which it then uses to create a specific type, such as `TwoTuple Char Integer` or `TwoTuple [Bool] [Char]`.

You can nest parametrized types within each other, though you'll need to supply paranthesis for it to be parsed correctly.

```haskell
anotherTuple = TwoTuple True (TwoTuple 42 ["howdy"])
-- has type TwoTuple Bool (TwoTuple Integer [String])
```

## Recursive Types

Types can also be defined in terms of themselves. For example, we could define a `NaturalNumber` as

```haskell
data NaturalNumber = Succ NaturalNumber
                   | Zero

zero = Zero
one = Succ zero
two = Succ (Succ Zero)
```

We could use this to define an alternative `List` type.

```haskell
data List a = Empty
            | Cons a (List a)

myList1 = Cons 0 Empty
myList2 = Cons 32 (Cons 1 Empty)  -- has type List Integer
```

Here is an implementation of binary tree

```haskell
data Tree a = Node a (Tree a) (Tree a)
            | Empty

myTree = Node "the parent" (Node "left child" Empty Empty) (Node "right child" Empty Empty)
-- has type Tree String
```

## Type Synonyms

You can create a synonym for an existing type using the `type` keyword. Note that this is very different from `data`: while `data` creates a new type, `type` only creates a synonym for an existing type (perhaps to give it a more descriptive name). For example,

```haskell
type CustomerID = Int
type Reviewer = String

data Review = Review BookInfo CustomerID ReviewBody  -- note we can use BookInfo as a component

type BookRecord = (BookInfo, BookReview)
```

Type synonyms only create a new name for an existing type constructor; the name of the value constructor does not change. For example,

```haskell
type BookInfo2 = BookInfo

-- The name of the value constructor is the same
myBook2 = BookInfo 12 "Title" ["Author"]
```

# Typeclasses

A **typeclass** describes the behaviour of a type. It is very similar to an interface in other languages, but better of course. Types that are part of a certain typeclass support certain functions and operators.

You can specify that a type satisfy a certain typeclass using a `class constraint`. For example,

```haskell
elem :: (Eq a) => a -> [a] -> Bool
```

## Builtin Typeclasses

### Eq

### Ord

`min :: (Ord a) => a -> a -> a`
`max :: (Ord a) => a -> a -> a`

### Enum

The `succ :: (Enum a) => a -> a` returns the succesor of an element.
The `pred :: (Enum a) => a -> a` returns the predecessor.

### Bounded


### Show

Members of `show` can be

### Read

