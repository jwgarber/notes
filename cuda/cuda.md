# Introduction

Traditionally, all computers have run on a *central processing unit* (CPU). CPUs are general purpose processing units designed for sequential execution. They have advanced control-flow features such as branch prediction, pipelining, out of order execution, and large instruction and data caches. Most CPUs today are multicore, and have 4-8 cores. CPUs are latency oriented.

With the advent of computer graphics and high performance computation, another type of processor emerged, the *graphics processing unit* (GPU). A GPU is optimized for parallel execution. A GPU consists of hundreds or thousands of processors, each of which runs their own thread. GPUs are designed for (relatively simple) mathematical computation, and as such do not have the advanced control-flow features of a CPU. GPUs are throughput oriented.

# CUDA Introduction

CUDA is an extension of the C language used for general purpose graphics proccessing. CUDA only works on Nvidia chips. We shall look at OpenCL later.

Every computer consits of the *host* (the CPU) and one or more *devices* (GPUs). A general CUDA C program consists of a mixture of host and device code, which are marked by special keywords. When compiling, the NVCC compiler will split apart the program into the host and device code. The host code is compiled by the system compiler, and the device code by NVCC to PTX. This code is then run on CPU and GPU.

## Thread Organization

A *kernel* is a function that is the entry point for code run on the GPU. A kernel is invoked from the host, and runs on the GPU.

All threads in a kernel are organized into fixed-sized *blocks*. These blocks are then collected together and organized into a *grid*, which contains all threads run during the kernel execution.

In general, blocks and grids can be three dimensional objects. The dimensionality chosen for these objects is usually modelled after the dimensionality of the data.

The dimensions of a grid and block can be declared using the `dim3` class. This class has `x`, `y`, and `z` members (which are of type `uint`), which give the dimension of the grid or block.

```cu
dim3 blockDim(16, 16, 1);   // number of threads in each block
dim3 gridDim(32, 32);       // number of blocks in the grid
```

Note that trailing dimensions can be left out, in which case they will be default-initialized to 1. This is handy for declaring one and two dimensional blocks or grids.

Once grid and block dimensions have been defined, a kernel `my_kernel` can be invoked as follows (we will see how to define a kernel shortly).

```cu
my_kernel<<<gridDim, blockDim>>>(args ...);
```

Note that these dimensions are not passed as arguments to a kernel. However, all kernels can access them through the the builtin `gridDim` and `blockDim` variables.

Given a particular thread, it is possible to access its location within a block and grid using two builtin `uint3` variables. The `threadIdx` variable gives the index of the thread within its block, and the `blockIdx` variable gives the index of the block within the grid. Thus, for example, the index of the thread within the entire grid can be calculated as follows:

```cu
int idx = blockIdx.x * blockDim.x + threadIdx.x;    // x index
int idy = blockIdx.y * blockDim.y + threadIdx.y;    // y index
```

# Function Declarations

All functions in a CUDA file must be annotated with a special keyword, which specifies how the function will be called and where it will execute. This is needed so the compiler can decide which functions to pass to the host compiler, and which to compile onto the device.

A `__global__` function is called from the host, and executed on the device. This is used to declare kernels.

A `__device__` is called from the device, and executed on the device. These are generally utility functions that the kernel needs to use during its execution. Note that these functions are compiled by NVCC and not the host compiler, and so for C++ one will have to check which language constructs are supported in device code.

A `__host__` is called from the host, and executed on the host. Note that a function without a keyword declaration will default to host, but I think it is best to annotate all host functions. Note that it is also possible to annotate a function with `__host__` and `__device__`, which allows the function to be called from the host and the device.

A CUDA program generally has the following structure:

- Allocate host memory, and initialize them with the input data
- Allocate GPU memory
- Copy the host memory to the GPU
- Launch the kernel code
- Copy

## Error Handling

Almost all functions in CUDA C return an instance of the `cudaError_t` enum to indicate success or failure. There are multiple reasons why a function could fail (many many reasons), which I will not get into here now. The important thing is to always check each function call for errors, and then take appropriate action. For simple applications, the following error handler suffices.

```cu
#define CUDA_CHECK(ans) cuda_check(ans, __FILE__, __LINE__)

void cuda_check(const cudaError_t err, const char* const file, const int line) {
    if (err != cudaSuccess) {
        fprintf(stderr, "cuda_check: %s %s %d\n", cudaGetErrorString(err), file, line);
        exit(EXIT_FAILURE);
    }
}
```

All CUDA API calls should be wrapped in the `CHECK()` macro, which will exit the application if there is an error. For brevity, we will not include this macro in the following code samples, but of course actual code should use it.

Kernel errors can be checked as follows:

```cu
kernel<<<1, 1>>>(args);
CUDA_CHECK(cudaPeekAtLastError());
CUDA_CHECK(cudaDeviceSynchronize());
```

# Memory

- Register memory.
- Local memory. This memory can only be accessed by a single thread
- Shared memory. This memory can only be accessed by the threads in a block
- Global DRAM memory. All threads in the grid can access this memory
- Constant memory. This is global memory that is initialized to read-only memory before the execution of the kernel.

## Global Memory

There are three primary functions used to manage global memory on a GPU.

```cu
cudaError_t cudaMalloc(void** device_ptr, size_t size);
```

The `cudaMalloc()` function allocations a section of memory `size` bytes long in the global device memory. The address of the allocated memory is stored in `device_ptr`. Note that because of the `void**` type, device pointers will generally have to be cast to `void**`. This is undefined behaviour, but generally works on most computers. A better solution is to use the C++ function overload, which has templated pointers and avoids this problem. When writing CUDA code myself, I would use C++ anyway, so this doesn't bother me much.

```cu
float* device_array;
cudaMalloc((void**)&device_array, 128);
```

Memory allocated on a device can be released using `cudaFree()`.

```cu
cudaError_t cudaFree(void* device_ptr);
```

The `device_ptr` must point to GPU memory previously allocated by `cudaMalloc()`. Nothing is done if `device_ptr` is `NULL`.

Memory can be copied from the host to the device or device to the host using `cudaMemcpy()`.

```cu
cudaError_t cudaMemcpy(void* dest, const void* src, size_t size, enum cudaMemcpyKind kind);
```

This function copies `size` bytes from the `src` to the `dest`. The type of transfer is specified by the `kind` parameter, which can be any one of the following four values.

- `cudaMemcpyHostToHost`
- `cudaMemcpyHostToDevice`
- `cudaMemcpyDeviceToDevice`
- `cudaMemcpyDeviceToHost`

Generally, global memory is allocated as arrays, which are then passed as pointers into the kernel. However, it is also to declare variables as residing in global memory using the `__device__` keyword. These are global variables that can be accessed by all threads in a grid, and persist across kernel executions. Of course, writing to these variables needs to be synchronized, which is tricky to do. Generally speaking, constant variables are used instead.

### A Quick Note About Multidimensional Arrays

Fundamentally, all host and device memory is represented as a linear array. To work with this multidimensional arrays are linearized. CUDA C uses

### Memory Coalescing

Most CUDA applications process a large amount of data in a small amount of time. Access times for global memory are often very slow, so we want to speed up global memory access as much as possible. This can be done through *coalescing*. Recall that in the SIMD model, each warp executes a single instruction at once, with different arguments to that instruction. Suppose that all threads in a warp execute a load instruction at once. In this case, the hardware checks if the loads access consecutive locations in memory. In this case, the hardware will coalesce the memory accesses, and load all of them in one large DRAM *burst*. This allows for much faster memory loading than if each thread were to access a random piece of memory. To take advantage of this burst mechanism, it best when writing a kernel to have threadIdx.x 0 access memory location 0, and then threadIdx.x 1 access memory location 1, etc. In this way, when all those threads are executed at once in a warp, the loads will be coalesced.

### Channels and Banks

Generally speaking, global memory isn't lumped into one massive piece of DRAM. Instead, the processor has multiple channels, and each channel controls multiple DRAM *banks*. An important note is that each bank can serve only one memory request at one (bursting included). If multiple banks are connected to a channel, then each bank can serve a memory request independently. The DRAM bursts themselves must be serialized along the channel, but the latency of accessing the cells can overlap among the banks. A *bank conflict* is when multiple memory accesses are made to the same bank at once. The bank can only serve a single access at once, which prevents the other banks from distributing the cell latency.

To take advantage of this, it is important that warps executing at the same time (say on different SMs) access memory in different channels and banks. Thus, the parallel structure of the SMs is mirrored by the parallel structure of the DRAM memory.

## Constant Memory

A global variable can be declared to be in constant memory using the `__constant__` keyword. Constant variables must be initialized in host code before a kernel call, and must remain constant throughout the kernel execution. Because variables in constant memory remain constant during a kernel execution, they can be aggressively cached.

```cu
cudaError_t cudaMempyToSymbol();
```

This is used to copy to constant memory.

## Shared Memory

Shared memory is stored on-chip and has much lower latency than global memory.

Note that there are limits on the amount of shared memory avaliable in each SM. For example, suppose an SM can run at most 8 blocks at once, and has 16KiB of shared memory. In this case, each block can use at most 2KiB of memory, or else fewer blocks will be scheduled onto the SM.

## Local Memory

Local memory is thread-specific memory that is local to each thread. Generally speaking, only thread-local arrays are stored in local memory, eg. `int a[255]`. Generally speaking, all other small variables are stored in registers.

## Registers

Like a CPU, all non-array thread local variables are stored in registers. This allows very fast computation to happen withe variables, because they are stored directly on chip.

## Host Memory

You can use `cudaMallocHost()` to allocate pinned memory on the host. Since this memory is pinned, calls to `cudaMemcpy()` will automatically be accelerated and skip the copy to a pre-pinned buffer. Also, `cudaMemcpyAsync()` can only work with memory allocated by this function. Use `cudaFreeHost()` to free this memory. The `cudaHostAlloc()` function is a more optioned version of this.

# Streams

A stream is a queue of CUDA commands, which include kernel launches and memcpys. All operations submitted to a stream run in serial, while the streams themselves run in parallel.

Events allow streams to synchronize with each other.

The default stream (NULL) is used when no stream is specified.

```cu
cudaStream_t stream0, stream1;
cudaStreamCreate(&stream0, &stream1);

cudaMemcpyAsync(, stream0);
cudaMemcpyAsync(, stream0);

cudaMemcpyAsync(stream1);
cudaMemcpyAsync(stream1);
vecAdd<<<, stream0>>>();
vecAdd<<<, stream1>>>();

cudaMemcpyAsync(stream0);
cudaMemcpyAsync(stream1);
```

You can use `cudaMemcpyAsync()` to launch an asynchronious memcpy on a stream. Note that the host memory passed to this function must have been allocated with `cudaMallocHost()`, so it can be pinned.

`cudaStreamSynchronize()` waits until all operations in the stream have finished.
`cudaDeviceSynchronize()` waits until all operations on the current device have finished.

# A Basic Kernel and Striding

Consider the following simple kernel that adds two vectors together. In this kernel, each element of the vector addition is assigned to an individual thread in the grid.

```cu
__global__
void add_vector(float* c, const float* a, const float* b, const size_t len) {

    const size_t i = blockIdx.x * blockDim.x + threadIdx.x;

    if (i < len) {
        c[i] = a[i] + b[i];
    }

}
```

With this approach, there must be at least one thread for each element in the vector.

# GPU Architecture

A GPU consists of multiple *streaming multiprocessors* (SMs). Each SM contains multiple *streaming processors* which share instruction caches.

When a kernel is executed, the corresponding grid of threads is created. For execution, blocks within the grid are scheduled onto an SM, which can generally execute several blocks at once. Each SM then consists of multiple *streaming processors*, each of which executes an individual thread. As with blocks, there is a limit on the number of threads that an SM can run at a time.

For example, suppose we have a GPU with 32 SMs, each of which can execute at most 8 blocks and 1536 threads at once. When scheduling blocks onto an SM, it is necessary that both constraints be satisfied at once. For example, one could have 6 blocks of 256 threads each, 3 blocks of 516 threads each, etc.

Since all blocks run independently of each other, the order they are scheduled onto the SMs for execution doesn't matter. All scheduled blocks run until they are finished, and then new blocks are scheduled on.

## Warps

Once a block is scheduled onto an SM, it is further divided into *warps*. Warps are groups of 32 threads, grouped by threadIdx. For example, starting at 0, threadIdx.x from 0 to 31 will be the first warp, 32 to 63 will be the second, etc. For a multidimensional kernel, the warp allocation is done by the x values, then the y values, and finally the z values.

Once an SM has chosen a warp, it executes it according to SIMD (Single Instruction Multiple Data). One instruction is fetched for the entire warp, and then all threads in the warp execute that instruction on different pieces of data. For example, in a vector addition kernel, a warp might fetch the add instruction, and then each thread would run the add function on different vector elements.

Note that on an SM, only a small subset of the scheduled warps are running at any given time. This is because many warps need long-latency operations, such as accessing global memory. Once a warp requests memory access, it is descheduled from the SM and waits until the memory read is finished. Until then, another warp that is ready to execute is scheduled onto the SM and run. This is very similar to process scheduling on an operating system. Because of this design, most GPUs do not allocate much chip area for memory caches, and instead direct it toward instructions.

Note that because blocks are chopped up into warps, it is best to always have the block size a multiple of the warp size. If this is not the case, some threads in a warp will be left idling because they have nothing to do. The warp size can be accessed within a kernel using the builtin `warpSize` variable.

Now, in the SIMD model all threads in a warp execute the same instruction at once. However, what if different threads want to run different instructions, as in an `if-else` statement? This is called *divergence*, because there are different control flows for the threads in a warp. In this case, the hardware needs to make multiple passes, one for each control path in the warp. Threads that do not participate in the particular control path are left idle, which leads to suboptimal resource utilization.

# Synchronization

All threads in a particular block execute in parallel. Due to divergence and scheduling issues, it is possible that the threads in a block could become desynchronized. Generally, this isn't a problem, but in some situations it is needed to synchronize all threads in a block before they continue. This is generally done when a thread needs to access data written by another thread. Threads within a block can be synchronized with each other using the `__syncthreads()` function within a kernel. This is known as *barrier synchronization*, because all threads in a block wait at the barrier until the rest arrive, then they all proceed. When using `__syncthreads()`, it is important to ensure the threads all have roughly the same running time, else many threads could be waiting for one laggard to finish.

Note that while threads within a block can synchronize with each other, it is *not* possible to synchronize different blocks. This is very powerful, because it allows all blocks to run completely independent of each other, and can be allocated to system resources in any way. This is known as *transparent scalability*.

# Tiling

Often when running a parallel algorithm, each thread in a block must access a long strip of global memory. Having each thread in a block acess the global memory is somewhat repetive and slow: global memory access is very long and often the bottleneck on code. To speed this up, we can use tiling, where all threads in a block collaborate to load the necessary global memory into a shared array (usually each thread loads one element), and then all threads can access the memory they need from the shared memory, which will be much faster.

Loads from global memory can also be done in a coalesced manner, and then the threads can access the tiles in any way they want. This allows using the burst mechanism.

# Atomic Operations

Suppose we have an array in global memory, or a global variable. Recall that this memory is accessible (and writable) by all threads in the grid. Suppose we want to update this global memory. In a highly parallel environment, how can we do this without having race conditions? The key is to use *atomic operations*. These are operations that perform a read-update-write cycle *atomically*, that is, no other read-update-write cycle can occur on that memory address while this operation is happening. Thus, all atomic operations on the same variable are forced to be serialized.

For example,

```cu
int atomicAdd(int* address, int val);
```

This function atomically adds `val` to the integer stored at `address`. This function returns the old value of the address. Atomic operations are done on global or shared memory, since those are memory locations that can be accessed by multiple threads.

In general, an atomic operation will be faster on shared memory rather than global memory because of the reduced latency.

# Privatization

*Privatization* is a technique where private copies of an output are calculated for each block, and then the blocks merge their results together at the end. The private copies have much less contention and often operate at lower latency.


# Convolution

Convolution is an array operation where each output element is a weighted sum of a collection of neighbouring input elements. The weights used in the weighted sum are stored in an array called the *convolution mask*. The same mask is usually used for all elements of the array.

For example, for a 1D convolution

```
Given an input array of size N, a mask array of size M, and an output array of size N
for i = 0 .. N
    output[i] = dot product of input and mask, centered at i
end
```

Usually, the mask has an odd length, which makes the inner product symmetric around the element being calculated.

Note that elements close to the boundaries of the array will not have enough adjacent elements to do the inner product. In this case, we often fill in these missing cells, called *ghost cells*, with a default value (usually 0, which allows us to ignore those cells when calculating the inner product).

## 1D Convolution

Note that for a convolution, each element can be calculated independently. As such, we assign a single thread to the calculation of each output element. For a 1D array, we split it apart into 1D blocks.

```cu
__global__
void convolution_one_dim(const float* input, const float* mask, float* output,
                         int width, int mask_width) {

    const int i = blockIdx.x * blockDim.x + threadIdx.x;

    if (i < width) {

        float sum = 0;
        const int start = i - mask_width / 2;   // we assume the mask width is odd
        for (int j = 0; j < mask_width; ++j) {
            if (0 <= start + j && start + j < width) {  // check for ghost cells
                sum += input[start + j] * mask[j];
            }
        }

        // It is better to acculumate the product in a local variable, which is stored in
        // a register, rather than accmuluting into the array, which is stored in global memory
        output[i] = sum;
    }
}
```

Note that this kernel will have control divergence for calculating elements near the boundary of the array (those involving ghost cells). In general, the divergence will be small for large arrays and small masks.

As it stands, this kernel makes many reads to global memory, which will severly reduce the optimal performance of the kernel. One way of improving this is using constant memory. Note that the mask will generally be small, is unchanged during the course of the kernel, and needs to be accessed by all threads in the grid. This makes the mask an excellent candidate for constant memory.

## Tiled 1D Convolution

To reduce global memory access even further, we can use tiling to load parts of the array into shared memory. We will refer to the output elements processed by each block as an *output tile*, and the collection of elements needed to calculate the output tile as the *input tile*. As we shall see, the input tile will generally be larger than the output tile. However, note that since each element of the output tile is calculated by one thread, the output tile size will be equal to the block size.

For this algorithm, each block loads its input tile into shared memory. For an output tile of width `O_TILE_SIZE`, the input tile will have width `O_TILE_SIZE + MASK_SIZE - 1`. Note that the extra elements on each side of the input tile also need to be loaded into adjacent input tiles. These are called *halo cells*, and need to be loaded multiple times. Cells that are only loaded into one tile are called *internal cells*.

_ _ internal _ _ (the ones on the outside are halo cells)

```cu
__global__
void convolution_one_dim(const float* input, const float* mask, float* output,
                         int width, int mask_width) {

    __shared__ float local[TILE_WIDTH + MASK_WIDTH - 1];

    int i = blockIdx.x * blockDim.x + threadIdx.x;

    int n = MASK_WIDTH / 2;

    // load the left halo cells
    int halo_left = (blockIdx.x - 1)*blockDim.x + threadIdx.x;

    if (blockDim.x - n <= threadIdx.x) {
        local[]
        if (halo_left < 0) {
            local[threadIdx.x - (blockDim.x - n)] =
        }
    }

    // load the internal cells

    // load the right halo cells

    // loading the left and right halo cells will lead to divergence
    __syncthreads();

    // TODO do bounds checking on i here
    float sum = 0;
    for (int j = 0; j < MASK_WIDTH; ++j) {
        sum += local[threadIdx.x + j]*input[j];
    }

    output[i] = sum;
}
```

Assuming the mask is in constant memory, internal the old kernel does

Using this technique will, in the long run, reduce global memory access by `MASK_WIDTH` times.

# Reduction


