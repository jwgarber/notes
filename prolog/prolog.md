# Prolog my Dude

Prolog is a declarative programming language based on first order logic. Prolog is a declarative language. Rather than describing how to solve a problem, you declare information using logic, and then let Prolog attempt to find a solution.

There are three basic constructs in Prolog: facts, rules, and queries. A collection of facts and rules is called a *knowledge base*. We then can ask questions about the knowledge base using *queries*.

# SWI-Prolog

A knowledge base stored in a file can be loaded using the `consult`.

Using `listing` will list out all information in the knowledge base. You can use `listing(predicate)` to find everything you know about an predicate.

# Data Types

Prolog has a single type, called a *term*. There are various subtypes of terms.

A *number* is either an integer or floating-point number, which are written in the standard syntax. Floating point numbers aren't really used in Prolog, but integers certainly are.

An *atom* is a general-purpose name with no inherit meaning. It is composed of a sequence of characters beginning with a lowercase letter, or a string enclosed in single quotes. Eg. `dave`, `steve`, `'Donald Trump'`.

*Variables*: These are words beginning with an uppercase letter. A single underscore `_` denotes an anonymous variable.

*Structures*: Functions are also denoted with lowercase letters. In general, an n-ary function `f(t1, ..., tn)`, where each `ti` is a term and `f` is an atom. Note that atoms can be viewed as zero-ary functions.

A *predicate* has the form `p(t1, ..., tn)`, where `p` is an atom and each `ti` is a term. Note that predicates are allowed to have the same name but different arity. As such, we often refer to a predicate `p` as `p/n`, where n is its arity.

# Clauses.

All Prolog programs consist of *clauses*. There are two types of clauses, facts and rules.

A *fact* is a statement of an unconditonal truth. a rule where the body is `true`. For example,

```pl
Head.
```

```pl
cat(tom).

```

A *rule* has the following form:

```pl
Head :- Body.
```

Where `Head` and `Body` are both predicates. A rule can be interpreted as `Body => Head`, which is the opposite of the given notation. Note that facts can be viewed as rules with empty bodies.

The body can consist of predicates joined by conjunction `,/2` and disjunction `;/2`.

In this case, often omit the body and merely state the head.

A *query* is a body. There are two types of queries in Prolog:

- Boolean queries do not contain variables. They return either `true` or `false`. `true` signifies that the query can be deduced from the knowledge base, and `false` means that the query cannot be deduced from the knowledge base.
- It will search for all instantiations that make the body true. If the query contains variables, it will show all instantiations of the variables that make the body true.

# Pattern Matching

# Unification

