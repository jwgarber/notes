# Intoduction

Matplotlib is an extremely powerful 2D plotting library. For 3D, you'll want to use Mayavia (or however you spell it). Mpl has two ways of interacting with it. Under the hood, everything is done in an object oriented way using **artists**. However, you can also use the `pyplot` interface, which hides all these details and just works (though I would rather use the artists). To get started using `pyplot`, go

```python
import matplotlib.pyplot as plt
```

The basic workhorse plotting command is `plt.plot`. It takes a 1D array of `x` coordinates and a 1D array of `y` coordinates and plots a scatter plot, with a line too if you like.

```python
plt.plot([1, 2, 3], [3, 4, 5])
```

This would correspond to plotting the points `(1, 3), (2, 4), (3, 5)`. You can pass the `x` and `y` coordinates as any linear data type (lists, tuples, etc.), but numpy arrays are the most common and useful.

You can also pass a format string that specifies the color and line type of the plot. It consists of two characters: the first is the color, and the second is the line type. For example, `'ro'` will make a plot of red dots (with no line). The default format string is `'b-'`, which corresponds to a blue line.

You can make multiple plots on the same figure by calling `plt.plot` multiple times. The plots will be zordered based on when they are plotted.

```python
plt.plot(x1, y1, 'ro')
plt.plot(x2, y2, 'b-')  # This plot will overlay the other
```

You can change the axis of the figure using the `plt.axis` command. It takes a list of the form `[xmin, xmax, ymin, ymax]`

```python
plt.plot(x, y)
plt.axis([0, 4, 0, 10])
```

You can change other properties of a plot using keyword arguments in the plot command. For example,

```python
plt.plot(x, y, linewidth=2)
```

will set the line width to 2 (remember that `'b-'` is the default format string).

Now for some important terms. The **figure** is the canvas that all the plots go on, aka the white background. The **axes** are the particular plots that go on that figure. This allows you to create multiple subplots on the same figure. Pyplot also has the concept of **current figure** and **current axes** (aka subplot). All plotting commands are applied to the current axes on the current figure. For example

```python
plt.figure(1)  # Create a new figure called 1
# plt.figure() does the same, because the default number is 1
# calling this is usually unnecessary, because pyplot will do it for you.

plt.subplot(2, 1, 1)  # Create a 2 rows by 1 column plotting space, and put this subplot in position 1
# If you only want to make 1 subplot, calling plt.subplot(1, 1, 1) is unnecessary because pyplot will do it for you

plt.plot([1, 2, 3], [4, 5, 6])  # Goes on subplot 1 of figure 1

plt.subplot(2, 1, 2)  # Change to subplot 2 of figure 1
plt.plot(x1, y1)  # Goes on subplot 2 of figure 1

plt.figure(2)  # Create a second figure
plt.plot(x, y)  # Gets plotted on subplot 1 of figure 2 (subplot 1 is created automatically)

plt.figure(1)  # Switch back to figure 1
plt.subplot(2, 1, 1)  # Switch back to subplot 1
plt.title('Now this is awkward')  # Title of subplot 1
```

You can clear the current figure (delete everything on it) using `plt.clf()`. Similarly, you can clear the current axes (or subplot, same thing) using `plt.cla()`.

You can close the current figure (totally delete it) using `plt.close()`. If you are creating lots of figures, make sure you explicitly call this once you are done with a certain figure (or else it will be kept in memory).

You can use `plt.title()`, `plt.xlabel()`, and `plt.ylabel()` to label the current axes.

```python
plt.plot(x, y)
plt.title('Ma title')
plt.xlabel('Fo shizzles / minute')
plt.ylabel('Rad factor')
```

Like lines, the title and labels have kwargs that control their properties. Ex.

```python
plt.xlabel("This ain't yo grandmother's label", fontsize=14, color='red')
```

You can incorporate LaTeX into your text using raw strings and `$` signs. Mpl will automatically typeset it.

```python
plt.title(r'$E = mc^2$')
```

You can save the current figure using `plt.savefig('/path/to/save/location.ext')`. Mpl will automatically chose the correct renderer based on the extension you give the file.

You can customize the defaults of Mpl (renderer, font, etc.) in the `matplotlibrc` file. These settings are called *rc settings*. Matplotlib looks for a `matplotlibrc` in three locations in the following order.

- The current working directory. This is for specific customizations you don't want to apply elsewhere.
- `~/.matplotlib/matplotlibrc`. This is for user settings.
- some folder in the matplotlib install path. These are the default settings, and you don't want to touch them.

To see where the currently active `matplotlibrc` file was loaded from, call the function `matplotlib.matplotlib_fname()`.

You can dynamically change the rc settings in a Python script. All the rc settings are stored in a dictionary called `matplotlib.rcParams`, which you can modify directly.

```python
import matplotlib as mpl
mpl.rcParams['lines.linewidth'] = 2
mpl.rcParams['lines.color'] = 'r'
```

