HDF5 is a hierarchal data format (that's what the name stands for). An HDF5 file has a layout similar to a directory. It is composed of groups (which like folders hold other groups and datasets) and datasets (which hold the actual data). The root group is given by a simple slash /. To access the pizza group within the root, simply use /pizza. If the pizza_size dataset is within the pizza group, you can access it using /pizza/pizza_size.

PyTables provides a Python interface for opening and accessing the data in an HDF5 file. To open an HDF5 file, use the open_file function.

my_file = open_file('myhdf5_file.hdf5')

This function returns an instance of the File class. To create a file for writing, pass mode='w' to the function call. To modify an existing file, use the mode='a' option. The root of this file can be accessed by looking at the root attribute of my_file. Ex. my_file.root.

To create a group that branches off the root, use the create_group method of the file.

detector = my_file.create_group('/', 'detector')

This will create a group called 'detector' that branches of the root '/'. It also returns an instance of the Group class that is assigned to the variable detector, which we can use to manipulate it.

We can then create a table (tables store heterogenous data) that branches off the detector group using the create_table method.

my_table = my_file.create_table(detector, 'readout', Particle)

This creates a table called 'readout' that branches off the detector group. This table inherits its attributes from the particle class. An instance of the Table class referring to the readout table is returned and stored in the my_table variable.

To see the structure of my_file, simply print it.

As said before, all HDF5 files have a UNIXy like file directory system. For example, the readout table would have the pathname '/detector/readout'. To access this directory structure in Python, we simply use the dot notation. It would be my_file.root.detector.readout. We can save a reference to this table for later.

readout = my_file.root.detector.readout

Look into Table.where() for fast searching.

An array stores homogenous data (this is what we're interested in). To create one, simply use the create_array method. First, lets make a group to put it in.

array_group = my_file.create_group(detector, 'arrays')

my_array = my_file.create_array(array_group, 'my_array', array([1, 2, 4]))

This creates a Numpy array that is stored in the 'arrays' folder under the name 'my_array'. (It isn't actually stored as a Numpy array, but when we want to access the data this is what it will be returned as.) Use the my_file.close() command to close the file when you're done.

You can iterate over all the nodes of a file using a for loop. Ex.

for node in my_file:
	print(node)

will print the pathname and information of each node (group or dataset) within the file. You can use the walk_groups() method to iterate only over the groups.

for group in my_file.walk_groups():
	print(group)

The iter_nodes() method will iterate over all the children of a particular group. You can provide a classname=Class option that will only show the children of that particular class. Ex.

for group in my_file.walk_groups():
	for array in my_file.iter_nodes(group, classname='Array'):
		print(array)

We can combine both techniques in the walk_nodes() attribute of the File class.

for array in my_file.walk_nodes('/', 'Array'):
	print(array)

This will print all the arrays that are children of '/'. You can iterate over all the Leaf nodes of a group (the array and table objects) using the Group._f_walknodes() method.

for leaf in my_file.root.detector._f_walknodes('Leaf'):
	print(leaf)

You can set attributse of leaf nodes using the attrs attribute and using the _v_attrs for groups.

table = my_file.root.detector.readout
table.attrs.hello = 'Hello world!'
table.attrs.number = 1
table.attrs.list = [5, 'snth', (0, 4)]

detector = my_file.root.detector
detector._v_attrs.bub = 'Bub.'

You can get the information about an object using the help() function, as usual.

You can access a node in the file by using the File.get_node() method. You simply supply the path to the node you want to get to.

arrays = my_file.get_node('/detector/arrays')
arrays = my_file.root.detector.arrays # same thing

data_array = arrays.my_array

data_array points to the array object within the HDF5 file. To read the data inside this array, use the read() method.

data = data_array.read()

This will return an object of whatever type it was stored as.

