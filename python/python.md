There are three ways to define functions with a variable number of arguments. All three ways can be combined.

Default Argument Values

A simple way is to give a default value for one or more parameters. If these arguments are not supplied in the function call, their default values will be taken.

def myfunc(prompt, tries=4, complaint='A little help here!'):
	pass

You can call this function specifying only one, two, or three arguments.

myfunc('Hello')
myfunc('Hello', 5)
myfunc('Hello', 5, 'HEEEEEEELP!')

To keep things simple, don't give the default parameters as any other variable, and don't make them lists or dicts.

Keyword Arguments

You can also explicity specify the value of a parameter by specifying it in the function call.

myfunc(prompt='bub', complaint='Yolo swag')
myfunc('bub', complaint='Yolo swag', tries=2)

In the function call, keyword arguments must follow positional arguments. The order you supply the keyword arguments doesn't matter, because you are saying exactly what they are.

You can also define a *args parameter and a **kwargs parameter to allow the function to take an abritrary number of parameters. When defining a function, the order of the parameters must be

def myfunc(positional parameters, *args, **kwargs):
	pass

All positional arguments after the positional parameters are scooped up and put in the args parameter, which is a tuple. All keyword arguments are scooped up and put in the kwargs dictionary, where the keywords are the keys and the values the arguments.

def f(x, *args, **kwargs):
	print(x)
	for i in args:
		print(i)
	for key, value in kwargs:
		print(key, 'maps to', value)

f('hello', 1, 2, 3, apple='red', banana='yellow')

It is possible to add keyword only arguments after an *args parameter. This is kind of like the print function.

Unpacking Arguments

You can also use the * syntax to unpack a series of positional arguments in a list or a tuple. Ex. if hello = [1, 2, 3], then f(*hello) will unpack to f(1, 2, 3). This is a really handy property. You can also include this with other arguments. Ex. f('a', *hello).

The ** syntax can be used to provide keyword arguments to a function call. Ex. if mydict = {'hello': 1, 'orange': 'apple', 'yolo': ['swag', 2]}, then f(**mydict) will expand to

f(hello=1, orange='apple', yolo=['swag', 2])  # but not nesicarily in that order

If it so happens that there is an *args or **kwargs parameter in the function call, they will then scoop up the provided arguments.


