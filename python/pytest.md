# Testing Python

Testing your code is very important; you gotta make sure it works. Of particular importance are unit tests: these test one small aspect of your program, usually one function, and make sure it works before you move on.

In test driven development, you write your unit tests before you write any code. This has several advantages:

- it forces you to explicitly write out your requirements for the code
- they keep you from over coding. When the unit tests pass, you are done
- when refactoring or porting code, they help you make sure the code still works
- it makes sure code stays compatible when other people work on it

Writing test cases is very UNIXy (well, you should be writing code in general UNIXy): each test case answers a single question about the code it is testing. Furthermore, test cases should:

- run completely by themselves, without human input. Automation is one of the key goals of testing.
- determine itself whether the test passed or failed, without human interpretation
- run in isolation, separate from all other test cases (even if they test the same function)

The assert statement is commonly used for testing in Python. The

assert condition

syntax will raise a fatal AssertionError if condition evaluates to False. Ex.

```python
assert myfunc(10) == 8
```

You can assert that a certain exception is raised by using the raises function from the pytest module.

```python
from pytest import raises

def f():
	raise KeyError

def test_mytest():
	with raises(KeyError):
		f()
```

Once you begin to write multiple tests, it makes sense to organize them into classes. For example, you might have a TestNode class to accompany a Node class.

```python
class TestClass:

	def test_one():
		x = 'this'
		assert 'h' in x

	def test_two():
		x = 'hello'
		assert hasattr(x, 'check')
```

Note that you really will not use instantiation for these classes; they are only meant to group common functions together, so there is no need for an __init__ method (or any of the other special methods for that matter).

To use pytest, simply call `py.test` in the directory you want, or supply the path to begin in as an argument. Pytest will recursively search through all subdirectories and run tests if it finds any. Pytest will

- run on files with the name `test_*.py`
- classes that start with Test and do not have an `__init__` method
- execute functions or methods that start with `test_*`
