# Introduction

Sympy is a symbolic computation library written in Python.

# Symbols

A **symbol** must be defined in Sympy before it is used. They can be defined using the `symbols` function, which takes a space separated list of symbol names.

```python
x = symbols('x')
x, y, z = symbols('x y z')
rho, nu, theta = symbols('rho nu theta')
chi, tau = symbols('x y')   # the variable name has nothing to do with the underlying symbol
                            # but picking a different name is very confusing
                            # as always, variables just point to objects
x, y = symbols('x y', integer=True)
x, y = symbols('x y', real=True)
x, y = symbols('x y', positive=True)
# all symbols are assumed to be complex by default
```

You can also import symbols from `sympy.abc`, but this doesn't give you precise control over the properties of the symbol.

You can use `Eq` to set two expressions equal to each other to create an equation.

```python
eq = Eq(x + 1, 4)
```

In general, the `==` represents boolean equality from Python. To check if two expressions are equal to each other, simply subtract the two and see if you get 0.

```python
a = (x + 1)**2
b = x**2 + 2*x + 1
simplify(a - b) == 0    # this should work
```

There are many other relations you can use, such as `Ge`, `Uq`, and `Le`.

You can numerically check if two expressions are equal using the `equals` method.

```python
a = cos(x)**2 - sin(x)**2
b = cos(2*x)
a.equals(b)
```

# Printing

Run `sympy.init_printing()` to set up the best printer.

# Numbers

In reality, all numbers are represented as Symby objects. However, Sympy will automatically coerce Python ints into the corresponding `Integer` class automatically. Watch out for division especially.

```python
x = Integer(1) / Integer(3)
y = Rational(1, 3)  # same thing
```

Note that Sympy has symbolic versions of all common functions, including `sin`, `cos`, `log`, etc. To convert an expression to its numerical Numpy equivalent, use the `lambdify` function.

- subs: symbolic replacement
- evalf: evaluate a numerical expression. Can also use for substitution and then evaluation

```python
expr = sympy.sin(x)
f = lambdify(x, expr, "numpy")
f(3)
```

# Representation

- str: get string representation
- srepr: get the exact representation of an object
- sympify: use a string representation to get a sympy representation. The input can be from `str` or `srepr`

# Simplification

- simplify: performs general simplifications on an expression. Two disadvantages are that it's slow (it tries lots of simplifications), and it might not try a specific simplification you have in mind. It is best to only use this in general situations when you have a decent idea on the output.

## Polynomial and Rational Function Simplification

- expand: expand multiplied expressions
- factor: factor a polynomial
- collect: collect like polynomial powers
- cancel: rational functions
- apart: partial fractions

## Trig

- trigsimp: simplifies using trig identities. A general simplifier for trig functions, like `simplify`
- expand\_trig: apply the sum or double angle identities

# Solvers

- solve: solve an equation symbolically for a certain variable. By default, outputs a list of solutions. If it finds no solutions, it returns an empty list `[]` or a `NotImplementedError`. This doesn't mean that there are no solutions, but that it couldn't find them symbolically. In this case, you need to resort to numerical solutions.

```python
solve(x**2 - 1, x)  # implicitly sets x**2 - 1 == 0
[-1, 1]

solve(cos(x) - x, x) # raises an exception
solve([x - y + 2, x + y - 3], [x, y])   # can also do systems
```

This is a generic solver. It determines the type of input you give it, and then passes the hard work off to a more specific solver. For example, you can pass it an inequality, and then it will call the `univariate_inequality_solver`, for example.

- roots: finds roots of a polynomial equation, including multiplicities

We need to solve several nonlinear inequalities. We have continuous functions, so find the roots of the equation, and then evaluate the function between each root. Finding the roots is the hard part, though. In general, these equations cannot be solved symbolically (Sympy gives you a "can't convert expression to float" exception when this happens). So, we need to do this numerically. We need to find all roots of the function within (0, pi/2).

Solving roots of a polynomial function is much easier. You can apply a general purpose algorithm like Jenkins-Traub or Durand-Kerner that solve for all the roots at once, or you can find bounding intervals on the roots and then apply a general root solver. I think the latter might be better for us, because we only care about roots in some specific interval.

In our situation, we have a linear combination of sines. The polynomial we reduce it to can certainly have multiple roots.

# Floats and Numerical Accuracy

The best measure of numerical accuracy is to state that | calculated answer - actual answer | < some value.

The `evalf` method will evaluate a number to `n` digits of accuracy. Remember from physics that errors will propagate through calculations and accumulate over time. However, I really don't think this is much of an issue. To keep it simple, I'll just use the accuracy value wherever I find anything.
