# Numpy Notes

Numpy is a package that adds significant functionality to Python for numerical work. The fundamental numpy object is a homogenous multidimensional array. It is a table of elements all of the same datatype (unlike Python lists, you cannot mix types in an ndarray). Each element is indexed by a tuple of natural numbers that indicate its position in the array. In numpy, each of the dimensions is called an axis, and the number of axes in an array is called the rank. For example,

[1, 2, 1] is an array of rank 1 (1 dimensional) with length 3.

[[1, 2, 1],
 [2, 3, 0]] is an array of rank 2. The first axis has a length of 3, while the second has a length of 2.

Numpy's array class is called ndarray (it is also known as array). The most important attributes of an ndarray are as follows (assume N is an instance of ndarray):

N.ndim -> int. The number of axes (dimensions) of the array. This is also called rank for some stupid reason.

N.shape -> tuple of ints. The tuple indicates the length of each axis. Ex. (2, 3, 1) means the first axis has a length of 2, the second has a length of 3, and the third has a length of 1. Note that the length of this tuple is the rank of the array.

N.dtype -> object. The object indicates the type of the elements stored in the array. Ex. perhaps it is storing ints or floats. Note that numpy also provides its own datatypes: np.int32, np.int16, and np.float64 are some examples.

N.itemsize -> int. Returns the size in bytes (1 byte = 8 bits) of the elements stored in the array. For example, an array of int64s has itemsize 8 (=64/8), while one of complex32s has itemsize 4 =(32/8).

N.data -> buffer. Returns a buffer containing the elements of the array, but we won't use this.

# Array Creation

One way to create an array is to pass a Python list or tuple to the array function. The dtype of the array will be the type of the objects in the list or tuple.

```python
a = array([2, 3, 4])
a.dtype -> dtype('int32')

b = array([2.5, 4.2, 5.8])
b.dtype -> dtype('float64')
```

The array function will transform sequences of sequences into 2D arrays, sequences of sequences of sequences into 3D arrays, etc.

```python
b = array([[2, 3, 4], [5, 4, 2]])
b
array([[ 1.5,  2. ,  3. ],
       [ 4. ,  5. ,  6. ]])
```

The type of an array can also be passed as an argument to the array function.

```python
c = array([[2, 2], [2, 3]], dtype=complex64)
```

Sometimes, the exact content of an array is unknown, but the dimensionality is. In this case, you can create an array of the required dimension with placeholder content. This avoids making arrays larger, which is an expensive operation. Simply pass the shape of the array to the corresponding function. By default the dtype of the created array is float64, but you can change this.

```python
zeros((2, 3))  # creates a 2 by 3 array full of zeros.

ones((1, 4, 4), dtype=int64)  # creates an array of int64 ones.

empty((2, 4, 5))  # creates an array with some random numbers
```

To create sequences of numbers, numpy provides the arange function. It is similar to range, except it will also work with floats (YES!!!) and returns a 1D array.

```python
arange(10, 30, 5) -> array([10, 15, 20, 25])

arange(0, 2, 0.3) -> array([0., 0.3, 0.6, 0.9, 1.2, 1.5, 1.8])
```

When arange is used with floats, it is in general not possible to say how many elements it will create due to floating point precision. In these cases, it is better to use the function linspace, which will return a 1D array of evenly spaced numbers between two points (including the endpoints).

```python
linspace(0, 2, 9)  # return 9 numbers from 0 to 2 including 0 and 2
-> array([0, 0.25, 0.5, 0.75, 1., 1.25, 1.5, 1.75, 2.])

x = linspace(0, tau, 100)  # useful for creating points to evaluating functions
f = sin(x)
```

# Printing Arrays

Numpy prints arrays similar to nested lists, but with the following layout:

* the last axis is printed from left to right
* the second last axis is printed top to bottom
* the rest are also printed top to bottom, but eoch slice is separated from the next by an empty slice

```python
a = arange(5)
print(a)
[0 1 2 3 4]

b = arange(12).reshape(3, 4)
print(b)
[[ 0  1  2]
 [ 3  4  5]
 [ 6  7  8]
 [ 9 10 11]]

c = arange(8).reshape(2, 2, 2)
print(c)
[[[0 1]
  [2 3]]

 [[4 5]
  [6 7]]]
```

If the array is too large, numpy will skip the centre and only print the corners of the array. You can change this using the `set_printoptions` function.

# Basic Operations

Regular arithmetic operators are applied to arrays elementwise. That is, the operation is applied individually to each element.

```python
a = array([10, 20, 30, 40])
b = arange(4) -> array([0, 1, 2, 3])
c = a - b
print(c) -> [10 19 28 37]

a + 2 -> array([12, 22, 32, 42])j
b*2 -> array([0, 2, 4, 6])

b**2 -> array([0, 4, 16, 36])
```

Contrary to what you might think, the * operator DOES NOT do matrix multiplication. It simply multiplies corresponding elements of two arrays.

```python
a = array([[1, 2], [3, 4]])
b = array([[2, 3], [4, 5]])
print(a*b)
[[ 2  6]
 [12 20]]

c = array([2, 2])
print(a*c)
[[2 4]
 [6 8]]
 ```

The matrix product can be done using the dot function.

```python
c = dot(a, b)
print(c)
[[10 13]
 [22 29]]
 ```

You can use the some operations, such as `*=` and `+=`, to modify and existing array in place instead of creating a new one.

```python
a += b
print(a)
[[3 5]
 [7 9]]

b *= 3
print(b)
[[ 6  9]
 [12 15]]
```

When you operate on arrays with two different types, the type of the resulting array is the more general type of the originals. This is known as upcasting. In general, int -> float -> complex, and if they are of the same type, the higher bit type will be taken.

Many unary operators are implemented as methods of the ndarray class.

```python
a.sum()
a.min()
a.max()
a.cumsum()  # returns a 1D array showing the cumsum of the array
```


By default, these operations apply to the entire array. However, you can apply them along one axis by passing an extra argument. Note that the axes are indexed according to their position in N.shape. For example, if N.shape returns (2, 1, 8), the "first" axis is the one at index 0, the "second" at index 1, etc.

```python
b = arange(12).reshape(3, 4)

b.sum(axis=0)  # will sum each column
-> array([12, 15, 18, 21])

b.min(axis=1)  # find the min of each row
-> array([0, 4, 8])

b.cumsum(axis=1)  # cumulative sum of each row
-> array([[ 0,  1,  3,  6],
          [ 4,  9, 15, 22],
          [ 8, 17, 27, 38]])
```

# Universal Functions

Numpy provides several universal functions. These functions take an array as an argument, apply the operation to each element, and return an array back.

```python
b = arange(3)
exp(b)
sin(b)
cos(b)
sqrt(b)
```

# Indexing, Slicing, and Iterating

1D arrays can be indexed, sliced and diced, and iterated over just as if they were Python lists. Nothing new here. With multidimensional arrays, simply add the extra indices after commas for the other axes. Remember though, all axes have 0 based indexing.

```python
b = arange(12).reshape(3, 4)
[[ 0  1  2  3]
 [ 4  5  6  7]
 [ 8  9 10 11]]

b[2, 3] -> 11

b[:, 1] -> array([1, 5, 9])  # column 1 of the array

b[1:3, :] -> array([[ 4,  5,  6,  7],
                    [ 8,  9, 10, 11]])  # rows 1 and 2
```

You can use the ... notation to represent complete slices of larger arrays. For example, if you have a 5D array, then

a[1, 2, ...] is the same as a[1, 2, :, :, :]
a[..., 3] is the same as a[:, :, :, :, 3]
a[4, ..., 5] is the same as a[4, :, :, :, 5]

Note: a[i] will be treated as a[i,...].

Iterating over a multidimensional array is always done with respect to the first axis.

```python
for row in b:
	print(row)
->
[0 1 2 3]
[4 5 6 7]
[ 8  9 10 11]
```

If you want to iterate over each element of the array, use the flat method. It returns an iterator over the entire array.

```python
for element in b.flat:
	print(b)
-> 0 1 2 3 4 5 6 7 8 9 10 11
```

# Changing the Shape of an Array

The shape of an array can be changed using various methods.

a.ravel() -> 1D array. Will flatten the array into a 1D array. Similar to .flat, but .flat returns an iterator.
a.transpose() -> array. Will transpose an array.
a.reshape(l1, l2, ...) -> array. Will return the array reshaped to the specified shape, where the length of each axis is provided as an argument.
a.resize(l1, l2, ...) -> None. Will modify the shape of the existing array in place.

If a dimension of -1 is given as an argument, that dimension will automatically be calculated based off the other dimensions. Ex. if your array has 12 elements, array.reshape(3, -1) will have the -1 become 4.

# Stacking

Arrays can be stacked together along differet axes.

The vstack function takes a tuple of arrays and stacks them vertically (in general, along the 0 axis).

The hstack function takes a tuple of arrays and stacks them horizontally (in general, along the 1 axis).

To stack along an arbitrary axis, use the concatenate function. Ex.

```python
concatenate((a, b, c), 2)  # will stack a, b, and c along the 2 axis
```

You can also `row_stack` 1D arrays into rows in a 2D array. Ex.

```python
a = array([1, 2, 4])
b = array([2, 5, 4])
c = row_stack((a, b))
print(c)
[[1, 2, 4]
 [2, 5, 4]]
```

`column_stack` works analogously.

# Splitting

Arrays can also be split along axes. I will read this up when I need to.

# Copying

A view is a shallow copy of an array. It returns a new array that points at the same data as the old array. In particular, modifying the data in a view will modify the data in the original array.

To create a view, simply call the view() method on any array object. Ex.

```python
c = a.view()
c.resize(2, 6)  # Won't modify a
c[0, 4] = 1234  # But will change a's data.
# So really, a view gives you a malleable way to reshape and such an array while still referring to the original data. It does not "copy" the data over, just gives you another way of looking at it (a view).
```

In particular, slicing an array returns a view. Unlike Python, it will not return a copy. Ex.

```python
s = a[:, 1]
s[:] = 10  # Will change the corresponding entries in a

s = mylist[1:4]
s[:] = [10]*3  # Won't change mylist
```

Use the copy method to make a deep copy of an array. This will return an array that does not share data with the old one and is completely independent.

d = a.copy()  # modifying d will not modify a

# Fancy Indexing

In addition to being indexed by integers (a[5, 2, 4]) and slices (a[:, 0, 1:3]), arrays can be indexed by arrays of integers and arrays of booleans.

```python
a = arange(12)**2
i = array([1, 1, 3, 8, 5])
a[i] -> array([1, 1, 9, 64, 25])  # these are the elements of a at the positions of i
```

```python
j = array([[3, 4], [9, 7]])  # this is a 2D array
a[j] ->
[[9, 16]
 [81, 49]]
 ```

In essence, the integers in i and j are the indices, and each index gets replaced with the element of a at that index. You can also do this with lists of elements instead of arrays.

```python
a = arange(5)
a[[1, 3, 4]] = 0
a -> [0 0 2 0 0]
```

We can also do indexing with Boolean arrays. The Boolean array must have the same shape as the original array. The True values indicate we want to keep that element, and the False values mean we don't.

```python
a = arange(12).reshape(3, 4)
b = a > 4  # all the elements of a greater than 4. This is a Boolean array with the same shape as a
a[b] -> [5, 6, 7, 8, 9, 10, 11]  # 1D array with wanted elements
```

You can also use this for assignments.

```python
a[b] = 0
print(a)
[[0 1 2 3]
 [4 0 0 0]
 [0 0 0 0]]
 ```

