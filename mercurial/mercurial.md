# Mercurial

Mercurial is a **version control system**. It has two main purposes:

- it remembers every version of code that you've made (sort of like a time machine)
- it merges different versions of code, so people can work independently and then merge their changes

Mercurial is used through the `hg` command in the command line. Typing `hg` without any arguments will give you a list of the most common commands. Typing `hg help` will give you a complete list of commands. To get help on a specific command, simple type `hg help <command>`, eg. `hg help clone`. To get even more information, pass the `-v` flag for *verbose*.

To see the version of Mercurial you are using, use the `hg version` command.

All the old versions of your code are stored in a **repository**. The repository, or repo for short, doesn't store complete copies of the old versions, just a list of changes you've made. To create a repository for your code, simply change into the directory storing your code and type `hg init`. This will create a repository in the current directory. The repository itself is stored in the .hg file, but don't mess around with this. The files and directories you actually work with and modify are called the `working directory`. They live in the same folder as the repository.

To add files in the current directory to your repository, simply use `hg add`.

Now we need to **commit** our changes to the repository. Use the `hg commit` command, and an editor will pop up. Type in the commit message here. When you close the editor, the changes will be commited. If you close the editor without saving, the commit will be aborted. When you make a commit, you save a **changeset** to the repository, which remembers what changes you have made since the last commit.

You can use `hg log` to see your changeset history. Each changeset has two identifiers: the first is a revision number that is unique to this version of the repository. The first commit has a revision number of 0, the second 1, etc. Note this revision number will vary among clones of the repository; in someone else's clone, it is very likely that a certain revision number will point to a different revision than it does in yours. This is what the hexadecimal identifier is for: it uniquely identifies a revision across *all* clones of a repository. Thus when discussing a particular revision with other people, use the hexadecimal identifier.

- The **user** field says who created that particular changeset.
- The **date** field says when the changeset was created (in the local time of the person who created it).
- The **summary** field contains the first line of the commit message for that changeset.

Some changesets have a **tag** field. This is a way of labelling that changeset. The most recent changeset always has the `tip` tag. You can use the `hg tip` command to see the output of `hg log tip`. `hg tip` accepts most of the same flags as `hg log`.

You can pass the `-r` option to `hg log` to see only a specific revision: this option accepts both the revision number and the hexadecimal identifier. You can also supply a range as an argument to `-r` to see all revisions between and including those numbers. Note that the order you specify the range matters; `2:4` will display in a different order than `4:2`. You can also supply negative numbers to range (to go backwords), and leave a number out (and it will be filled in with a sensible default, like Python). You can also pass a `-v` flag to see more detail in the logs (the opposite of `-v` is `-q`). If you want to see the changes as well, pass the `-p` flag.

The `hg revert` command will revert files in the directory back to the state they were at the time of the last commit. You can use the `--all` flag to revert all the files. When you revert something, Mercurial keeps your uncommited changes in a file called filename.ext.orig. That way you can go back to them if you want to.

The `hg status` command gives you an overview of the files that have changed since the last commit. It displays each file that has been changed along with a little letter describing the change.

- A - added
- M - modified
- R - removed
- ! - missing (`hg revert` is your friend)
- ? - untracked

The `hg diff` command tells you how a file has changed since the last commit. You can also pass the `-r` flag to see how it has changed between two revisions. Ex. `hg diff -r 0:1 hello.txt` will print the changes between revisions 0 and 1.

To keep mercurial up to date when moving or removing files, always use `hg mv` and `hg rm`. Otherwise, it won't know what's going on.

The `hg cat` command is used to view a file from any revision. Use it with a filename to see the file at the time of the last commit, eg `hg cat hello.txt`. You can also pass the `-r` flag to show the file at a particular revision number.

The `hg update` command moves the entire repository back to a certain revision. Without any arguments, it will update the repo to the latest revision (if you are working with other people, for example), but you can also go back in time. Eg, `hg update 2` will move the working directory back to revision 2.

## Collaboration

The most common way to collaborate using Mercurial is to set up a central repository where everyone merges their code.

The `hg serve` command creates a web server of the current repository. To acces it on the web, simply go to `http://hostname:8000/`.

To copy a repository from a server onto your own computer, use the `hg clone` command. It's quite simple: simply provide the URL to the server, and it will clone that repository into the current directory. You can also use this command to clone a preexisting repository on your computer to another one, say for development work. Eg. `hg clone myrepo myrepodev`. When you clone a repository, a path called "default" is set up that points to the repository you cloned from. When yuo `hg push` changes, they will by "default" get pushed back to that repo. You can view the paths of a repository using the `hg paths` command.

The `hg push` command pushes the changes in your repository back to the central repository you cloned it from. This will almost certainly require some sort of security verification, so you'll need to set that up.

The `hg outgoing` command lists the changes you made to your repository that would be sent by `hg push`.

If changes have been made to the central repository, running `hg push` won't work. You first have to merge the changes in the central repo and your repo, and then push them.

The first thing is to `hg pull` the changes from the central repo back to your own. By default, these changesets will be added as parents to the last changeset your repository had in common with the other one. If you have made commits since that changeset, this will create another **head** in your repo with the pulled in changesets. (A head is a changeset that has no children.) Otherwise, if you have made no changes, the pulled in changsets will neatly stack on the changeset you have in common. To merge the heads of the changesets, just use `hg merge` (make sure you have commited any changes in the working directory first). This puts the merged files in your working directory. If there are conflicts in the merge that Mercurial cannot resolve automatically, it will launch a diff tool (vimdiff in my case) where you can resolve the conflicts manually. Note that the changes you make will be reflected in the working directory; that is the state of the files when you commit the merge. You can use `hg resolve` to retry a merge if it goes badly. Next, commit the merge, and push it back to the central repo. Note that the merge changeset will have two parents in this case: the one the head of your changes, and the other the head the changes you pulled in. Note that `hg pull` accepts a `-r` flag that accepts a hexadecimal identifier that will only pull in revisions up to and including that identifier.

The `hg fetch` command automates the pull, merge, commit process. It is a handy tool to have.

The `hg incoming` command lists the changes made to the central repository that would be pulled by `hg pull`.

You can also push/pull incoming/outgoing directly to a repository by providing a path to it on the command line.

Make sure that after using `hg pull` that you `hg update` to the latest revision (this will not happen by default).

The `hg parents` command lists what revision your working directory is currently at.

When you clone a repository, Mercurial automatically remembers where the new repository was cloned from, and sets up a **default** path pointing to that repo (this is stored in .hg/hgrc in your repo). If you don't provide a repository to hg push/pull incoming/outgoing, the default one will be used. You can add a defualt-push and default-pull path too.

## Changing the History

If you have made a commit you want to undo, you can use `hg rollback`. It will only undo one commit, but isn't really that useful if you have already pushed that commit elsewhere.

You can also "undo" a changeset from the past in the current working directory using the `hg backout` command. For example, if you made some changes in revision 2 that you want to undo, `hg backout -r 2` will create a new head with those changes undone. Then you can merge these two heads together and commit the changes.

## Merging

Pulling in changes will create a new head in your repo if you have made commits that aren't reflected in the pull. Use the `hg heads` command to view the heads you have. To merge these heads together, use `hg merge`. However, sometimes the merging doesn't go smoothly, and you get conflicts. These have to be dealt with manually. If this is the case, some sort of conflict resolution tool will pop up for you to use.

You can **tag** a certain changeset with a name to identify it later. To tag the current changeset you're working on, simple go `hg tag v1.0`. The act of tagging is itself a changeset and gets commited automatically for you (so it's easy to `hg rollback` if you like).

You can use tag names to `hg update` back to the revision with the tag. Say for example that you have made several commits, but now want to go back and fix a bug in a previous version. You can `hg update` back to that revision, make your changes, and then make a new commit. But this will create a new head, because you have already made other changes. Then, simply merge these changes together.

When you want to work on different branches of code (say a stable one and a dev one), you can clone the dev one from the stable one. Then, you can continue to apply bugfixes to stable, work on new features in dev, and then pull the changes from dev into stable and merge them in.

# Workflow

```fish
hg branch new-feature


Use `hg update` to swit
