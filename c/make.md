# Introduction

GNU Make is a tool used to automate compiling and linking of programs. Though it can be used with any language, the most common are C and C++. The compilation commands are placed in a Makefile, which make then uses to build your program.

# Rules

The fundamental construct in a Makefile is a **rule**. The basic format for a rule is this:

```make
target: dependencies
    shell commands
```

The dependencies and target are files within the directory of the Makefile. If the dependencies have newer timestamps than the target, the target is rebuilt from the dependencies using the shell command. For example,

```make
draw.o: draw.c draw.h
    clang -c draw.c
```

To use this rule, execute the following shell command

```
$ make draw.o
```

You can define multiple rules.

```make
draw: draw.o
    clang draw.o -o draw
```

If a dependency of a rule is itself a target, the rule of that dependency is invoked first (to keep everything up to date). For example, the rule of `draw.o` will be invoked first if the `draw` rule is used.

If you provide no target to the make command (that is, just plain old `$ make`), it will just invoke the first rule in the Makefile.

# Variables

You can declare variables using the following syntax.

```make
variable = values
```

This is very handy for storing common values. For example, we can store the compiler flags in the `CFLAGS` variable.

```make
CFLAGS = -Weverything -std=c11 -g
```

You can use a variable with `$(VARIABLE)`. For example,

```make
clang $(CFLAGS) -c draw1.c
```

There are also automatic built in variables that are only one character long. For example, the `$@` represents the target of a rule, `$^` represents the dependencies, and `$<` represents the first dependency.

# Pattern Rules

You can create a **pattern rule** that matches a particular rule. For example,

```make
%.o: %.c
    clang -c $(CFLAGS) $<

# This matches the above generic rule, so we do not need to write one down
draw1a.o: draw1a.c draw1a.h
```

# Implicit Rules

Make has some implicit pattern rules built in. The most common one is

```make
%.o: %.c
    $(CC) -c $(CFLAGS) -o $@ $<
```

Thus, you only have to specify the `CC` and `CFLAGS` variables, and maybe some rules. While very handy for hacking around, I would not use these rules in a real Makefile.

# Phony Targets

You can also create rules with no dependencies. These are called **phony targets**, and are used for executing shell commands. To make a rule phony, you need to add it as a dependency to a special `.PHONY` target.

```make
.PHONY: tar clean submit

tar:
    tar cvf submit.tar draw1.c draw1.h Makefile README

clean:
    -rm -f *.o

submit:
    astep -c c201 -p asn1 submit.tar
```

