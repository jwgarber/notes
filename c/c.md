# Introduction

C is a **systems language**. It is compiled, low-level, and does not hide any details from you.

# Making Executables

**Preprocessing** evaluates the macro expressions within a program file and translates them into legitimate C code.

**Compiling** translates the file into machine code the computer can run. Note that this *only* compiles code within the given file. Things that are used within the code but not defined within there are not compiled. This happens later in the linking step.

To compile a file called `test.c`, run this

```
$ clang -c test.c
```

This produces an **object file** (a file containing unlinked machine code) called `test.o`. This file contains the machine code of the code within that file. It is considered good practice to create object files for each program file, because then your program can be relinked without rebuilding each file (you only rebuild the object files that changed).

Suppose you have several object files `main.o`, `matrix.o`, and `func.o` that you want to link into one executable (in this case, assume main `#include ""` those two other files). To do this, run this

```
$ clang main.o matrix.o func.o -o main
```

This links together those files and any included standard library object files into an executable called `main`.

Libraries are a way of packaging object files so other programs can link to them.

## Static Libraries

## Dynamic Libraries


# Variables

A **variable** is a name for a piece of memory in the RAM. In a variable, you can store information and retrieve it later. C is a statically typed language, so all variables have a specific type that cannot be changed. You specify the type of a variable in a **declaration statement**, which looks like this

```c
type variable_name;
```

When you write the statement

```c
int x;
```

a piece of memory in the RAM is set aside for the variable `x`. This piece of memory will be referenced whenever we access the variable `x`. By default, the value of a declared variable will be whatever the that piece of memory was when the variable was declared. This value is essentially random. Thus, you need to **initialize** your variable with a value before actually using it.

```c
int x = 0;
```

It is very important to initialize your variables. A good practice is to always initialize your variables at declaration.

It is possible to declare and assign variables on the same line, but avoid doing this: it decreases readability and is error prone.

It is best to declare variables only when the are needed, and within a small a scope as possible.

Because all information stored in a computer is just a sequence of zeros and ones, we give that information a **data type** to allow us to interpret it the way we want.

## Identifiers

The name of a variable, funtion, struct, or other object in C is called a **indentifier**. There are several rules for naming identifiers.

- The identifier cannot be a keyword.
- The identifier can only be composed of letters, numbers, and the underscore character. However, identifiers cannot begin with numbers. Furthermore, it is best not to begin an identifier with an underscore.
- Identifiers are case sensitive, so `Value` is different than `value`.

You can use the `sizeof` operator to determine the size of a variable or data type in bytes. The `sizeof` operator returns an unsigned integer of type `size_t`. The size of this integer is machine dependent, so it must be printed using `%zu` to be safe.

```c
printf("The size of an int is %zu\n", sizeof(int));  // Returns 4 bytes
double x;
printf("The size of x is %zu\n", sizeof(x));  // Returns 8 bytes
```

## Constants

**Constants** are variables that cannot change. Constants can be `#defined` using the preprocessor, but this is a bad idea. In general, it is best to use the preprocessor as little as possible. A much better idea is to use the `const` keyword. Simply preface your variable declaration with `const`, and your variable will be not reassignable. It is a good idea to make all variables constant that do not need to be modified.

```c
const int exchange_rate = 122;
exchange_rate = 123;  // ERROR!
```

All constants must be initialized at declaration time.

## Operators

The order in which operators are evaluated in a compound expression is called **operator precedence**. Those with a higher operator precedence are evaluated before those with a lower. For operators with the same precendence, such as `*` and `/`, the **associativity rules** govern in what order the expression is evaluated.

The **comma** operator and the **ternary if** operator should not be used. Things are just clearer if you do not.

### Relational Operators

C has the standard relational operators: `<`, `>`, `<=`, `>=`, `==`, and `!=`. All these operators return a boolean value, either `true` (`1`) or `false` (`0`). Note that using these operators with floating point values is dangerous: you need to take special measures.

# Whitespace and Comments

C is extremely liberal about how you format your code. Newlines mean nothing. You could delete all the newlines in your code, and it would still compile.

There are two ways to comment in C. The first is to use C style commenting, which includes everything between `/*` and `*/` as a comment (including newlines). C style comments are bad: do not use them. Use C++ style comments instead, which were added in C99. With these comments, everything from a `//` to the end of the line is a comment.

# Numbers

There are two broad types of numbers in C: integers and floating point numbers. Integers store integers, and floating point numders store

## Integers

The native integer data types in C should be avoided. The sizes of these data types depend on the platform, which means they are not very portable. Wherever possible, the fixed width integers in `<stdint.h>` should be used instead.

A variable of size n bits can hold 2^n different values (because each bit is either a 0 or a 1). Thus, an integer with n bits can store 2^n different numbers. All the possible numbers a Thus, each type has a maximum and minimum value it can hold. When you add a number outside that variables range, you get **overflow**, and the number wraps around to the other side of the range. In general, it is best to always use signed integers.

The `intptr_t` and `uintprt_t` are signed an unsigned integers of the system platform size. If you really need a system dependent int, use this, not `int`.

There is a special unsigned integer type called `size_t` specifically designed for storing the sizes of objects. It is guaranteed to be able to hold the largest possible size of an object on the current system (including the size of an array). Thus, you should always use `size_t` when dealing the sizes of objects and array indexing, because it is the only one guaranteed to be big enough. `size_t` is defined within `<stddef.h>`, `<stdio.h>`, `<string.h>`, and `<time.h>`.

```c
const size_t size = 10000;
for (size_t i = 0; i < size; i += 1) {
    // do some array stuff
}
```

## Floating Point Numbers

**Floating point numbers** store

The basic number types are `int` (32 bit), `long` (64 bit), `float` (32 bit), `double` (64 bit).
When declaring `longs` and `floats`, you need to append an `L` or an `f` to the end of the number.

```c
int age = 5;
long height = 5L;  // note the L
double length = 2.35;
float area = 28.20f;  // note the f
```

You can use the standard `+`, `-`, `*`, and `/` operators for dealing with numbers, as well as

## Arithmetic Operators

There are two **unary** arithmetic operators: `+` and `-`. They do as you expect.

```c
int x = 1;
printf("%d\n", +x);  // Prints 1
printf("%d\n", -x);  // Prints -1
```

The are five **binary** arithmetic operators: `+`, `-`, `*`, `/` and `%`. They do as expected.

```c
int x = 10;
int y = 5;
printf("%d\n", x + y);  // Prints 15
printf("%d\n", x - y);  // Prints 5
printf("%d\n", x * y);  // Prints 50
printf("%d\n", x / y);  // Prints 2
printf("%d\n", x % y);  // Prints 0
```

Note that division operator has one subtlety. If both arguments to the operator are integer types, such as in `3 / 4`, the division operator will perform **integer division** and drop the fraction of the answer. So in the above case, `3 / 4 == 0`. If one or both of the arguments are floating point, then the operator will perform **floating point division** and return a floating point value (with the decimal). So we have `3.0 / 4.0 == 0.75`.

Note that C also has a **remainder** operator `%`. This is not modulus operator: it does not follow the Euclidean algorithm (what the crap!). The **remainder** (not modulus) operator only works with integer arguments and returns the remainder after doing integer division. For example, `20 % 3 == 2`.

All of the above binary operators have assignment equivalents, that is `+=`, `-=`, `*=`, `/=`, and `%=`.

There are also the increment `++` and decrement `--` operators. These operators have **side effects**, which are results of a function that persist even after the function has finished executing. Side effects are **bad**, so do not use these operaters! In general, all operators with side effects should be placed on their own line, not as a part of a compound statement.


# Boolean Values

C doesn't really have boolean values. Instead, `0` is false and any other integer is true. To clean things up a bit, you can access boolean values within the `<stdbool.h>` header. This defines the values `true` and `false` as well as the data type `bool`.

```c
bool truth = true;
bool falsity = false;
```

When boolean values are evaluated, they **do not** remain `true` and `false`. Instead, `true` evaluates to `1` and `false` evaluates to `0`. Keep that in mind. Boolean values are really just integers.

```c
bool is_equal(int x, int y) {
    return x == y;
}
```

The standard boolean operators are `!` for *not*, `&&` for *and*, and `||` for *or*. For example,

An important note is that `!` has very high operator precendence, even higher than the relational operators (which is horrendous and does not make sense, but such is the world). Thus, one must do `!(x == y)` rather than `! x == y`, which in the second case is equivalent to `(!x) == y`. Thus, you need to surround all negated compound expressions with parenthesis to ensure the order of evaluation is correct.

The `&&` and `||` operators have lower precendence than the relational operators, so `x < 5 || x == 3` will do as you expect. However, for clarity and to again ensure proper order of evaluation, it is just easier to write `(x < 5) || (x == 3)`.

Further note that `&&` has a higher order of precedence than `||`. You can use this, but again, it is easier just to be explicit and use parenthesis.

The `&&` and `||` operators use **short circuit evaluation**. That is, each operator will only evaluate as many of its arguments as needed to determine the output. For example,

```c
if ((x == 1) && (y++ == 2)) {
    // do something
}
```

if `x == 1` is false, the entire expression will return false, and so `y++ == 2` will not be evaluated, hence `y` will not be incremented. On the other hand, if `x == 1` is true, `y++ == 2` will be evaluated, so `y` will be incremented. This is just another example of why it is a *terrible* idea to use operators, methods, and functions with side effects. Just DON'T DO IT.

# Characters

Characters are of type `char`, and they are written between single quotation marks.

```c
char letter = 'a';
```

Each character is really just an 8 bit integer with a little sugar on top. In Clang (and GCC too I believe), `chars` are signed, so they have a range of `-128` to `127`. The positive numbers have a direct mapping onto the ASCII codes (in fact, I believe this is where the ASCII codes came from). The above example is entirely equivalent to saying

```c
char letter = 97;
```

because 97 is the ASCII code for `'a'`.

The function `putchar` will print `char` values as letters instead of numbers. Thus

```c
char letter = 97;
putchar(letter);  // will print a
```

In all other regards though, `chars` are just plain numbers, so you can do things like

```c
char letter = 'a' + 1;
putchar(letter);
```

which will print `b`. In fact, this is how the `<` operator works when comparing strings: it just compares the ASCII codes to see which one is smaller.

```c
char uno = 'a';
char dos = 'b';
printf("%u\n", uno < dos);  // will print 1 for true
```

In general, you should avoid doing math and other such fishy things on characters.

There is a special character called the **null character**. It has ASCII code 0, and is thus denoted by `'\0'`, and is the empty character (it doesn't print out anything).

**Escape sequences** are characters with special meaning. They all start with a backslash `\\` and are then followed by a character. You know what the escape sequences are.

# Macros

**Macros** are used to write sort of meta-code. Macros are written using **directives**. These directives start with a `#` symbol and end with a newline (*not* a semicolon). There are several directives the preprocessor uses.

The `#include` directive is used to "import" other files. It inserts the contents of the given file at the spot of the `#include` statement. To include a file from the runtime library, use the `#include <filename>` syntax. For example, `#include <stdio.h>`. To include a file relative to the current directory of the source file, use the `#include "path/to/file"` syntax. For example, `#include "deps/math/add.h"`.

**Macro** directives can be created using `#define identifier replacement`. Whenever the preprocessor finds `identifier` in the code, it replaces it with `replacement`. For example,

```c
#define NAME "Jacob"
printf("My name is %s\n", NAME);
```

is converted by the preprocessor to

```c
printf("My name is %s\n", "Jacob");
```

Note that this does a straight `s/identifier/replacement`, so the quotation marks around `"Jacob"` are very important. It is convention to write the `identifiers` entirely in uppercase, using underscores in the place of spaces, `LIKE_THIS`.

You can undefine a previously defined identifier using `#undef identifier`. For example,

```c
#define NAME "Jacob"
printf("My name is %s\n", NAME);
#undef NAME
printf("My name is %s\n", NAME);
```

will not compile because the second `NAME` is no longer defined within the program.

You can conditionally compile code using `#ifdef`, `#ifndef`, and `#endif`. The `#ifdef indentifier` directive checks whether `identifier` has been previously defined. If so, the code between the `#ifdef` and `#endif` is left in the source code and compiled. If the identifier has not been defined, the code between the `#ifdef` and `#endif` will be removed from the source code and not compiled.

```c
#define JOE

#ifdef JOE
puts("Hello Joe");
#endif

#ifdef BOB
puts("Hello Bob");
#endif
```

The above code will print out `Hello Joe` and not `Hello Bob`, because the `BOB` identifier has not been defined. The `#ifndef` directive is similar to `#ifdef`, but it checks if an identifier has not been defined.

In general, you should only use macros for `#including` files and header guards. For everything else, there's a better way.

# Functions

Functions are defined like so:

```c
return_type function_name(paramtype0 param0, ..., paramtypeN paramN) {
    // Body
}
```

For example:

```c
int min(int a, int b) {
    if a < b {
        return a;
    } else {
        return b;
    }
}
```

All functions can have only *one* return value. It sucks, I know, but that's life. If the function has no return value, it returns `void` (and hence that is the type of the return value). For example,

```c
void print_number(int a) {
    printf("a is %d\n", a);
}
```

Functions that take no parameters take `void` as a parameter. If you simple define no parameters at all, the function can take any number of arguments in a function call (which will be ignored).

```c
void print0(void) {
    puts("Hello world!");
}

void print1() {
    puts("Hello world!");
}

print0();  // just fine
print1();  // works
print1(3); // also works
print1(3, "shebang", 3.14);  // also works
```

## Function Prototypes

C has a funny rule about functions. All functions must be defined before they are called in the code, even in a function definition. This is different than in Python, where you can define your functions in any order, as long as they are defined before the code is called. Because it is often inconvenient to put your functions in a certain order to make the compiler happy, you can provide **function prototypes** at the beginning of the file. These statements, called **forward declarations**, simply given the type signature of the function. The function prototype for our `min` function would look like.

```c
int min(int, int);
```

It is considered good practice to give a function prototype to each function defined in your code (except the main function). These prototypes are usually placed in a header file that is included at the top of the file.

If you declare a function prototype, never define the function, but call the function, it will compile, but not link, because the linker cannot find the function definition. However, if the function definition is contained within another file, you can provide that file as an argument to the compiler, which will find the function definition and link them together.

## Main

Every executable needs a `main` function; this is what gets executed when the program runs. A proper `main` functican be look like one of two things.

```c
int main(void) {
    // Code here
}
```

```c
int main(int argc, char* argv[]) {
    // Code here
}
```

This function takes several command line arguments. All `main` functions must return an `int`, which is the exit status of the program. Return `0` to indicate successful execution, and return any other number to indicate an error. If you leave out a return statement, the main function will implicitly return `0`.


# Header Files

Header files contain type declarations for variables, structs, enums, and functions. That is all. They do not contain runnable code, nor code that uses memory. They simply contain type declarations.

All header files should start with a **header guard**, which uses the conditional compilation directives of the preprocessor to prevent the header from being included twice. All header guards should look like this.

Headers often introduce several new types that are not included in the basic file. All these types should be included from the necessary header files. So make sure the header file compiles on its own.

```c
#ifndef PATH_TO_HEADER_H
#define PATH_TO_HEADER_H

// header contents

#endif
```

For example, suppose we have two files `math.c` and `main.c`.

```c
// math.c

int add(int x, int y) {
    return x + y;
}
```

```c
// math.h

#ifndef MATH_H
#define MATH_H

int add(int, int);

#endif
```

```c
#include <stdio.h>
#include "add.h"

int main() {
    printf("The result is %d\n", add(1, 2));
}
```

Compile with `clang main.c math.c -o main`.

On modern compilers, you should use `#pragma once` at the start of the file rather than header guards.


# Pointers

The smallest unit of memory is a **bit**, which is a `0` or a `1`. Computer memory is divided into **bytes**, where each byte contains eight bits. A byte is the smallest unit of memory that you can work with. Each byte has a unique **address**, which is a number that distinguishes it from other bytes.

Each variable in a program is stored in one or more sequential bytes of memory. The address of the variable is defined to be the address of the first byte. For example, an `int` occupies 4 bytes of memory. If those 4 bytes have addresses 10011, 10012, 10013, and 10014, the address of the `int` itself is 10011.

A **pointer** is a variable that stores an address of memory. Depending on the processor of your computer, this number will either be 32 or 64 bits wide. Each pointer has a specific type, which is the same type as the value stored at the address of memory. A pointer can point to an object of any type, including other pointers.

Pointers are declared in the same way as ordinary variables, except that an asterisk follows the type.

```c
int* p;
```

Pointer syntax is pretty flexible (well, isn't all syntax in C?), so there is an equivalent way to declare pointers:

```c
int *p;
```

This is very common, though I try to avoid it.

As usual, this assigns some garbage memory address to the pointer. To give the pointer a meaningful value, we need to assign it the address of a variable. The **address** operator `&` returns the address in memory of a variable. Using the address operator, we can assign our pointer a value.

```c
int x;
int* p = &x;  // p now stores the address of x
```

Once a pointer stores a memory address, we can **dereference** the pointer using `*` to access the value it points to.

```c
printf("%d\n", *p);
```

The `*` and `&` operators are inverses of each other. For example,

```c
j = *&i;
```

is equivalent to just `j = i`.

You can use pointers to directly modify the value of a variable, because they directly access the memory the variable points to.

```c
int i = 0;
int* p = &i;

*p = 2;  // i is now 2
printf("%d\n", *p);
printf("%d\n", i);
```

Working with an uninitialized pointer variable is undefined behaviour. The garbage memory address assigned to the pointer may point anywhere, including memory within your program or memory protected by the OS. Attempting to read or write to the memory location referred to by the pointer will likely cause your program to crash (with a segfault usually).

```c
int* p;
printf("%d\n", *p);  // likely crash
*p = 2;  // even more likely to crash
```

You can copy the value of one pointer to another, as long as they have the same type.

```c
int i;
int* p = &i;
int* q = p;  // q now points to i as well
*q = 1; // i is now 1
```

## Pointers as Arguments

In C, all variable assignments are done by value. For example, when we say

```c
x = y;
```

a complete copy of `y` is made and put into `x`. Changing the value of `x` has no effect on the value of `y`, because these variables are now stored in different locations of memory. This is very different from Python, where all assignments are done by reference. This is also very different than Haskell, where it doesn't matter.

This applies to parameters of functions too. When you pass arguments to a function, the parameters are complete copies of the arguments. They are completely independent of the arguments, so changing them will not change the arguments. But what if you want to change an argument, like a struct? In this case you need to pass a pointer, which allows you to directly modify the contents of memory.

Note that with pointers you can directly change objects of any type, including ints, doubles, structs, arrays, you name it. However, it is generally unnecessary to have pointers to atomic data types, and should be only done with complex data typse, such as structs and arrays.

```c
int var = 3;

void modify(int* int_ptr) {
    *int_ptr = 4;
}

modify(&var);

printf("%d\n", var); // var is now 4
```

It is sometimes useful to append `ptr` to the name of a pointer variable to reinforce it is a pointer.

### Const Pointers

Suppose you want to pass a pointer to a struct for efficiency, but don't actually want to modify the struct. You can guarantee this won't happen by making the pointer `const`. While you can change the pointer itself, this guarantees you cannot change the object it points to.

```c
void print(const Node* node) {
   printf("%d\n", node->size); // fine
   node->size = 3;             // not ok!
}

void main(void) {
    int i = 0;
    const int* int_ptr = &i;

    *p = 1; // NOT OK

    int y = 1;

    p = &y; // Ok
    *p = 1; // NOT OK
}
```

Thus, the address the pointer points to can change, but it cannot change whatever it points to. If you wanted to make the pointer const and the object it points to readonly, you would need to do something like `const int* const p`.

Note that a `const` pointer is only a *read-only* pointer. It is very possible that other pointers to that object could modify it. You can tell the compiler to trust you and then make the pointer `restrict`, which means the memory it points to will only be accessed through that pointer. So a `const restrict` pointer means the object you point to is constant, though this is not enforced by the compiler.

## Pointers as Return Values

You can also return a pointer from a function.

```c
Image* index(Image array[], int len, char name[]) {
    // Do some stuff
    return image_ptr;
}
```

However, you must be careful not to return a pointer to a variable declared within the scope of the function. The variable will be popped off the stack when the function returns and may be later erased when other function calls are made. Always return pointers to variables in some higher scope.

```c
int* f(void) {
    int i = 0;
    return &i; // Whoa Nelly! Not good!
}
```

# Pointers and Arrays

## Address Arithmetic

In C, you can do **address arithmetic** on pointers to manipulate the addresses they point to. This ties in very closely to array indexing.

The most basic operation is adding an integer to a pointer. Suppose you have a pointer `int* p` that points to an address of memory. When we add an integer `i` to the pointer, the address gets incremented by `i * sizeof(int)`.

```c
int* p;
p = p + 3;
// p now is 3 * sizeof(int) addresses greater than before
```

In other words, `p` now points 3 integers ahead of where it did before. Whether there is allocated or initialized memory there is another matter, but `p` now points to that location.

Subtraction of integers is similar, execpt we now subtract rather than add.

```c
int* p;
p = p - 3;
// p is now 3 * sizeof(int) addresses smaller than before
```

This works for all data types, not just integers. For example,

```c
Node* p;
p = p + 3;
// p is now 3 * sizeof(Node) addresses larger
// p points to the 3rd next Node
```

You can also subtract pointers of the same type. The result is the distance between the pointers in `sizeof(types)`. That is, it subtracts the pointers as addresses, and then scales the difference down by the size of the type. It returns a signed integer of type `ptrdiff_t`.

```c
long* p;
long* q;
ptrdiff_t i = p - q;
// i is then number of longs separating p and q
// that is (p - q)/sizeof(long)
// p and q are both long aligned, so this division will always work
```

Pointers of the same type can be compared using the relational operators `< > <= >= == !=`. This compares the pointers as addresses. Do not do this with pointers of different types.

```c
int* p;
int* q;
bool b = p == q; // do p and q point to the same location?
```

## Arrays and Pointers

The distinction between an array type and pointer type in C is minimal. In almost all cases, an array variable when used will decay to a pointer to its first element. That's right. It's not an array any more, it's a pointer.

```c
int a[10];

*a = 7;
*(a + 1) = 12;
*(a + 2) = 16;
sizeof(a);  // a is NOT a pointer in this case

int c[10];

bool b = a == c;  // compares the addresses of the first elements. Will always be FALSE
c = a; // HAHA. You make me laugh. This is illegal, since C has no idea how large an array is
```

In particular, this means you cannot use the assignment operator to copy arrays or the equality operator to check if two arrays are equal. You need to do it by hand buddy! In the few cases where this actually works (when you pass an array to a function), it will do pointer assignment and comparison, and the compiler won't warn you in this case.

Because array elements are stored sequentially in memory, array subscripting is equivalent to using pointer arithmetic. In general

- `a[i]` is the same as `*(a + i)`
- `&a[i]` is the same as `a + i`

Using this, we can iterate over an array using pointers instead of indexing.

```c
int a[10];

for (int* p = a; p < a + 10; p += 1) {
    *p = 0;
}
```

However, indexing is almost always more readable, so use that.

Because of the above equivalence, indexing can also be done on normal pointers.

```c
int* p;
int i = p[0];  // equivalent to i = *p;
int j = p[1];  // equivalent to j = *(p + 1);
```

When passed to a function, an array name is always treated as a pointer.

```c
int find_largest(int a[], int len) {

    if (len <= 0) {
        // do some error handling
    }

    int max = a[0];
    for (size_t i = 1; i < len; i += 1) {
        if (a[i] > max) {
            max = a[i];
        }
    }

    return max;
}
```

In the above function definition, `int a[]` is translated by the compiler into `int* a`. Because this is just a pointer, you can make it `const` to prevent the array from being modified. This also lets you pass "slices" of arrays to functions.

```c
int array[20];
find_largest(array + 5, 10);  // find the largest element in array[5:15]
```

## Multidimensional Arrays and Pointers

C has some tricky rules when it comes to pointers to multidimensional arrays. First of all, suppose we have this

```c
int a[10][10];
```

Using our rule, what we actually get is that `a` is an `int**`. Yep, a pointer to a pointer to an int. If we were to pass it to a function, we would get something like

```c
void my_func(int** a)
```

In general, `a[i]` would give us a pointer to the ith row of the matrix, *not* the ith element in the first row (since a is an `int**`).

So to get the address of the first element, you have to do something like `&a[0][0]`, which is equivalent to just `&a`. Moral of the story: just use the `&` operator on indexing rather than trying to figure everything out using addition and such. The compiler is much better at doing this than you are.

When passing an array to a function, I think it is better to use the `int a[]` syntax, rather than the `int* a` syntax. The first implies that you are passing an array, not just a pointer to an object.

# Dynamically Allocating Arrays

An example for dynamically allocating an array. The simplest thing is to just allocate one massive array, and then treat the indexing like it's multidimensional.

```c
// allocate an array with LEN * LEN ints
// you have to manually index this to treat is as a matrix and not a giant array
int* a = malloc(LEN * LEN * sizeof(int));

for (size_t i = 0; i < LEN; i += 1) {
    for (size_t j = 0; j < LEN; j += 1) {
        // it is just an array of ints, but you have to index it manually
        printf("%d\n", a[i * LEN + j]);
    }
}
```

A more sophisticated thing is to create an array of pointers, and then make each pointer point to a dynamically allocated 1D array. This has the advantage that the individual arrays can all be different lengths.

```c
// malloc an array of pointers to ints
int** a = malloc(LEN * sizeof(int*));
for (size_t i = 0; i < LEN; i += 1) {
    // make each pointer then point to another array
    a[i] = malloc(LEN * sizeof(int));
}

for (size_t i = 0; i < LEN; i += 1) {
    for (size_t j = 0; j < LEN; j += 1) {
        // indexing now works as normally
        printf("%d\n", a[i][j]);
    }
}
```


# I/O

Input and output in C is done using the functions within `<stdio.h>`.

## Stream

A **stream** is any source of input or destination for output. Streams can be many things, such as input from the keyboard, printing characters to the screen, files, USB sticks, network ports, and printers.

The `<stdio.h>` header provides three standard streams that are always avaliable; they are never opened and never closed, they are ready to use. They are

- `stdin` is standard input. By default, this is keystrokes from the keyboard.
- `stdout` is standard output. By default, this prints to the screen.
- `stderr` is standar error. By default, this also prints to the screen.

Most common IO functions, like `printf`, `scanf`, `puts`, and `gets`, get their input from `stdin` and send output to `stdout`. Note that we can use **input redirection** and **output redirection** to change the above standard streams. For example, using the `<` and `>` things you can change standard input from the keyboard to a file, and standard output from the screen to a file.

## File IO

A file stream can be opened using the `fopen` function, which has the following type signature:

```c
FILE* fopen(const char* restrict filename, const char* restrict mode);
```

where `filename` is a string containing the path to the file, and `mode` is a string specifying in what mode to open the file, such as `"r"` for reading or `"w"` for writing. `fopen` returns a pointer to a file of type `FILE`, which can then be used later to perform operations on the file. If `fopen` cannot open the file, it will return a null pointer. You should always test the return value of `fopen` to see if it is a null pointer and act accordingly.

```c
FILE* fp = fopen("documents/info.txt", "r");
if (fp == NULL) {
    perror("fopen");
    exit(EXIT_FAILURE);
}
```

There are various modes for opening a text file. Here are the three most common.

- `"r"`     open for reading
- `"w"`     open for writing
- `"a"`     open for appending

Note there is an important distinction between *writing* data and *appending* data. Writing data will create a new file that overwrites any file of the same name that was there before. Appending data will write data to the end of a file that already exists (or create a new one if it does not exist).

A file can be closed using the `fclose` function, which looks like this:

```c
int fclose(FILE* stream);
```

Simply pass a pointer that points to an open file to `fclose`, and it will close that file. `fopen` returns `0` if the file closed successfully or the error code `EOF` (a macro defined in `<stdio.h>`) if unsuccessfull.

```c
if (fclose(fp) == EOF) {
    perror("fclose");
    exit(EXIT_FAILURE);
}
```

Writing data to a disk is slow work. If you are writing to a file, the data you write is not written to the file immediately. Instead, it is kept in a **buffer** in memory. When the buffer gets full or the file stream is closed, the data is then written (aka *flushed*) to the file all at once. However, you can use the `fflush` function to flush data to an output stream immediately. This function has type signature

```c
int fflush(FILE* stream);
```

where `stream` is a pointer to the output stream. For example,

```c
fflush(fp);
```

flushes the data written to an output file. You can also flush standard output streams

```c
fflush(stdout);
```

or pass a null pointer to flush all output streams at once.

```c
fflush(NULL);
```

`fflush` returns `0` if it is successful and `EOF` if it is not.

## Character I/O

### Output

There are three functions used to print characters to a stream.

```c
int putchar(int ch);            // writes ch to stdout
int putc(int ch, FILE* stream); // usually implemented as macro
int fputc(int ch, FILE* stream);// as above, except a function, so preferable
```

### Input

There are three functions used to read characters from a stream.

```c
int getchar(void);          // reads from stdin
int getc(FILE* stream);     // usually implemented as a macro
int fgetc(FILE* stream);    // must be a function, so preferable
```

Each of these functions returns the character it reads as an `int` (*not* as a `char`), and returns `EOF` if it reaches the end of the stream or a read error occurs. These last two conditions can be differentiated using `feof` and `ferror`.

```c
// Read all the characters in a file, and print them out one at a time

FILE* infile = fopen("test.txt", "r");
if (infile == NULL) {
    exit(EXIT_FAILURE);
}

int ch;
while (ch = fgetc(infile), ch != EOF) {
    fputc(ch, stdout);
}

if (ferror(infile)) {
    printf("Error reading file\n");
} else if (feof(infile)) {
    printf("End of file reached\n");
}

fclose(infile);
```

## Line I/O

### Output

There are two functions for writing a string to a stream.

```c
int puts(const char* str);      // writes to stdout
int fputs(const char* restrict str, FILE* restrict stream); // writes to arbitrary stream
```

These functions write a string to the given stream. `puts` automatically appends a newline character to the end of the string, while `fputs` does not. Both functions return `EOF` if an error occurs.

```c
puts("Hello, how are you?");
fputs("S'up pal?\n", outputfile);
```

### Input

There is one function used to read lines from a stream. Reading strings is rather tricky, because you have to know the maximum length of a string you could read so you can set aside a buffer large enough. Again, C has no idea what size an array is, so it will happily write characters past the end of a buffer if there are more than you thought. An array only exists within the mind, my friend.

```c
char* fgets(char* restrict str, int buf_len, FILE* restrict stream);
```

`fgets` will read at most `buf_len - 1` characters from the given stream and store them in `str`. The reading will stop early if `fgets` encounters a newline character, in which case it will store the newline in the string. After which, `fgets` will null-terminate the string in `str`.

`fgets` returns a null pointer if there is a read error or it encounters the end of the stream before any characters are read. As usual, these two situations can be differentiated by using `ferror` and `feof`.

```c
#define SIZE 100
char string[SIZE + 1];
while (fgets(string, SIZE + 1, stdin) != NULL) {
    puts(string);
}
```

## Formatted Strings

These following functions allow us to parse and format strings.

### Formatting Strings

These functions can be used to get the string representation of a data type.

```c
int sprintf(char* restrict buffer, const char* restrict format, ...);
int snprintf(char* restrict buffer, int bufsz, const char* restrict format);
```

The `sprintf` function is potentially insecure, because it can write an arbitrary number of characters to the given string array. Use `snprintf` instead.

The `snprintf` function writes at most `bufsz` characters into the array pointed to by `buffer`, the last of which is always a null character. The format string specifies the formats for the following arguments.

`snprintf` returns the number of characters that would have been written to the buffer (not including the null character) had there been no length restriction. If an encoding error occurs, `snprintf` returns a negative number. We can check if `snprintf` had room to write all the necessary characters by seeing if its return value is non negative and less than `bufsz`.

```c
#define MAX_STR_LEN 100
char string[MAX_STR_LEN + 1];
int num_chars = snprintf(string, MAX_STR_LEN + 1, "(%d, %d, %d)", x, y, z);
if (num_chars < 0) {
    // Encoding error
    exit(EXIT_FAILURE);
} else if (num_chars > MAX_STR_LEN) {
    // Buffer was not big enough, so maybe malloc a new string and
    // do something else
}
```

### Parsing Strings

The `sscanf` function can be used to parse a string.

```c
int sscanf(const char* restrict buffer, const char* restrict format, ...)
```

The first argument `buffer` is the string to read from, and `format` is the format string. `sscanf` returns the number of items successfully parsed from the `buffer`, so this is handy for checking if you parsed the proper number of items.

```c
// Unpacking a tuple
sscanf(str, "(%d, %d, %d)", &x, &y, &z);
```

Parsing a substring from `sscanf` can be dangerous, because you may potentially read a larger substring than the buffer you set aside.

## Formatted I/O

Formatted I/O lets you combine the formatting and printing steps into one function. For example, suppose you want to format a string and then print it. This could be done using a call to `snprintf` and then `puts`, or it can be combined into `printf`.

### Output

There are two functions for printing formatted output.

```c
int printf(const char* restrict format, ...);
int fprintf(FILE* restrict stream, const char* restrict format, ...);
```

The above functions are nearly identical, except that `printf` prints to `stdout` by default, while `fprintf` can print to any stream. These functions will format a string and write it to the given stream directly.

```c
printf("Coords are (%d, %d, %d)\n", x, y, z);
fprintf(stderr, "Uh oh, name is %s\n", name);
```

- `%c` is for printing characters. You can format an `char`, `short`, or `int`, in which case it will % 127 that integer and print the corresponding ASCII character.
- `%s` is for printing strings.
- `%d` is for printing `char`, `short`, and `int` as a signed integer.
- `%u` is for printing unsigned integers
- `%ld` is for printing `long` as a signed integer.
- `%lu` is for printing `unsigned long`
- `%p` is for printing memory addresses (pointers)
- `%zu` is for printing `size_t`
- `%f` is for printing `float`
- `%lf` is for printing `double`
- `%ju` is for `uintmax_t`
- `%ji` is for `intmax_t`

### Input

There are also two functions for scanning and parsing from a stream.

```c
int scanf(const char* restrict format, ...);
int fscanf(FILE* restrict stream, const char* restrict format, ...);
```

These two functions read data from an input stream, using a format string to indicate the layout of the input. After the format string, any number of pointers to variables indicate where to store the parsed items. `scanf` reads from `stdin`, while `fscanf` reads from the given stream.

Both functions return the number of items successfully read. If one of these functions does not return the proper number of items, then one of three things could have happened:

- End of File
- Read Error
- Matching Failure

You can determine what kind of error occured using `ferror` and `feof`.

As with `sscanf`, reading strings is dangerous. Make sure you have buffer sizes taken care of.

```c
// Parses a CSV file
int x;
int y;
int z;
// reads a line of the file
while (fscanf(infile, "%d,%d,%d", &x, &y, &z) == 3) {
    printf("%d\n", x + y + z);
}

// Three possibilities
if (ferror(infile)) {
    printf("Encoding Error\n");
    exit(EXIT_FAILURE);
}

if (!feof(infile)) {
    // if we haven't reached the end of the file
    // there was a matching error
    printf("Matching Error\n");
    exit(EXIT_FAILURE);
}

// Otherwise, we reached the end of the file, so life is good
```


# Arrays

An **array** is a way of initializing a contiguous block of memory. An array contains a number of data values, all of the same type. When declaring an array, you must specify the type of the array and its length. This allows it to reserve the necessary amount of memory.

```c
type array_name[length];
```

For example,

```c
int a[10];
```

declares an array of `ints` of length 10. Note that at compile time, the array length must be an integer literal, *not* a variable. That literal can be provided by a macro or an enum, but it cannot be provided by a variable, even if that variable is a constant.

```c
#define LENGTH 10
enum {
    SAFE_LENGTH = 10,
};
const int length = 10;

int a[LENGTH]; // Good stuff
int a[SAFE_LENGTH]; // Even better
int a[length]; // Not happening
```

Thus, you *must* know the length of an array at compile time, like know an actual number. (Note that some compilers support variable length arrays, but that's an advanced and nonstandard topic.) Also note that the length of an array is fixed: you cannot change its length after declaration.

## Array Indexing

You can access elements of an array using indices. Like all good arrays, an array of length *n* is indexed from 0 to *n* - 1.

```c
int a[10];
for (size_t i = 0; i < 10; i += 1) {
    a[i] = i;
}

for (size_t i = 0; i < 10; i += 1) {
    printf("%d\n", a[i]);
}
```

## Initializer Lists

Declaring an array allocates memory for each of the entries. As usual, each of these entries are garbage without initialization. To initialize an array *at declaration*, you can use an initializer list.

```c
int a[5] = {0, 1, 2, 3, 4};
```

If the initializer list is shorter than the length of the array, the remaining elements are given the value `0` for the specific type of the array.

```c
int a[10] = {0, 1, 2, 3, 4};
// equivalent to {0, 1, 2, 3, 4, 0, 0, 0, 0, 0}
```

You can use this to initialize an array to all `0`.

```c
int a[100] = {0};
```

It is illegal for the initializer to be empty, and it is also illegal for the initializer to be longer than the array.

If an initializer is present, you can omit the length of the array from the declaration. It will be inferred from the initializer.

```c
int a[] = {0, 1, 2, 3, 4}; // array has length 5
```

### Designated Initializers

To initialize certain elements of an array and set the rest to `0`, you can use a **designated initializer**, which was added in C99. For example,

```c
int a[50] = {[2] = 5, [8] = 10, [28] = 3, [42] = 2}
```

The numbers within the brackets are called the **designators**, and they can be given in any order. All values not given a designator are initialized to the default value of `0`.

Note that the designators specify indices of the array, so if the array has length *n*, the designators must be between 0 and *n* - 1. However, if the length of the array is not given, it will inferred from the largest designator.

```c
int a[] = {[10] = 5, [3] = 80, [41] = 10};
// a is given length 42
```

You can also mix initializer lists with designated initializers.

## Sizeof

The `sizeof` operator returns the size of an array in bytes. Determing the size of an array is easy. If you have an array declared as `type a[length]`, then `sizeof(a) == sizeof(type) * length`. For example,

```c
int a[10];
```

then `sizeof(a) == 40` bytes.

## Multidimensional Arrays

An array can have any number of dimensions. For example, the following code creates a matrix:

```c
int matrix[ROWS][COLUMNS];
```

You can also create an array with more than two dimensions, such as `int array[5][6][8]`, but these are less common.

Nested `for` loops are the usual way of iterating over an array. For example, the following code initializes an array to the identity matrix.

```c
#define LEN 10
double matrix[LEN][LEN];

for (size_t row = 0; row < LEN; row += 1) {
    for (size_t col = 0; col < LEN; col += 1) {
        if (row == col) {
            matrix[row][col] = 1.0;
        } else {
            matrix[row][col] = 0.0;
        }
    }
}
```

### Initializing Multidimensional Array

You can initialize a multidimensional array by nesting one dimensional initializers.

```c
int m[2][3] = {{1, 2, 3},
               {3, 4, 5}};
```

Each initializer provides values for one row of the matrix.

As with 1D arrays, if any of the initializers are left out, they will be initialized to 0.

```c
int m[3][3] = {{1, 2},      // third element initialized to 0
               {3, 4, 5}};
               // the last row initialized to 0

int m[3][3] = {{0}}; // initializes entire array to 0
```

Arrays are multidimensional objects, but memory is linear, so how do we store arrays in memory? C stores arrays in *row-major order*, which means the rows are stored sequentially. For example, the above matrix is stored as

```c
1, 2, 0, 3, 4, 5, 0, 0, 0
```

exactly as it is initialized. In general, arrays are stored in memory exactly as the initializer gives them. This is why you can also omit the inner braces within the initializer list (since the elements are stored within the memory exactly as you lay them out), but this is bad.


# Strings

Character arrays, commonly called strings. Arrays are simply ways of allocating consecutive bytes of memory. It is up to you to remember how long the array is, with maybe a macro or other variable. Strings are slightly different. A properly formatted string is terminated with a null character, so when iterating over an array you can tell when you get to the end. Thus, it is not necessary to remember the length of a string. However, it is so easy for the null character to be overwritten that remembering the length of a string is almost always a good idea. The null character has an ASCII code of 0, so it is represented by the octal sequence `'\0'`.

## Basic Formatting

A **string literal** is a sequence of characters enclosed within double quotes:

```c
"How are you today?"
```

You can use **escape sequences** to put special characters in strings. This is most commonly done with whitespace characters, such as `'\n'` and `'\t'`.

```c
"Count:\t53\n"
```

You can also use octal or hexadecimal numerical escapes to directly state the ASCII code of the character you want. Octal escapes can be a maximum of three digits long, like `\12`, and hexadecimal escapes must be in the range `\x00` - `\xFF`.

In general, when two or more string literals are adjacent (separated only by whitespace), the compiler concatenates them into a single string. This is handy when you have a very long string that you want to split up over several lines.

```c
printf("This is a rather long string. I think so, but wait, hold on, we can"
       " split it up over several lines.");
```

```c
// A very useful function for converting a number between 0 and 15 to hex
// poor man's hash table
char digit_to_hex(int digit) {
    return "0123456789ABCDEF"[digit];
}
```

## Declaring Strings

A string can be declared as such:

```c
#define MAX_STR_LEN 80
char char_array[MAX_STR_LEN + 1];
```

This sets aside 80 bytes of memory for the string, and then 1 extra byte for the null character. Remembering to leave an extra byte for the null character is extremely important. Don't forget it. The above variable can store up to 80 characters.

Character arrays can be initialized at declaration time:

```c
char char_array[MAX_STR_LEN + 1] = {'a', 'b', 'c'};
```

As usual, the rest of the array will be padded with zero bytes, which in this case corresponds to the null character. Thus, as long as there are fewer characters in the initializer than the length of the array, the character array will be a properly formatted string. If there are exactly the same number of characters as the length of the array, a null character will not be stored at the end, so this will not be a properly formatted string.

```c
char char_array[3] = {'a', 'b', 'c'};  // Watch out!
```

Writing initializers of characters is tedious, so you can equivalently use a string literal.

```c
char char_array[10] = "abc";
```

If you leave the length of the array out, the compiler will compute it from the initializer and add a null character to the end for you (the one place where the compiler is smart).

```c
char str[] = "Hello how are you?";
// has length 18 (17 for string + 1 null character)
```

This is especially handy when initializing long strings, where calculating the length by hand is tedious and error prone.

C also allows you to declare strings like this:

```c
char* p = "Hello";
```

These strings are read-only and are stored in the static data segment of a program. Attempting to modify them is undefined behaivour, so make these strings `const` if you ever decide to use them.

As usual, strings can be declared constant by appending `const` to the front.

```c
const char str[] = "abc";
```

Because strings are arrays of characters, we can iterate over them using indexing or pointers.

```c
// Using indexes
int count_spaces(const char s[]) {
    int count = 0;

    for (size_t i = 0; s[i] != '\0'; i += 1) {
        if (s[i] == ' ') {
            count += 1;
        }
    }

    return count;
}
```

```c
// Using pointers
int count_spaces(const char* s) {
    int count = 0;

    while (*s != '\0') {
        if (*s == ' ') {
            count += 1;
        }
        s += 1;
    }

    return count;
}
```

## C String Library

C strings are arrays, so the assignment and equality operators are essentially useless for working with strings (if they work at all, the pointer operations will be used). So things like

```c
str1 = "abc"; // nope, initializer list only allowed at declaration
str2 = str1; // nuuh, arrays can't be reassigned
bool b = str1 == str2;  // works, but compares them as pointers
```

are useless. Instead, you have to use the functions in `<string.h>`.

### Copying Strings

We can't do things like `str1 = str2`. Instead, use `strncpy`. It has the following prototype:

```c
char* strncpy(char* restrict dest, const char* restrict src, size_t count)
```

This copies the first `count` characters in `scr` into `dest`, or stops once it copies a null character. To avoid buffer overflows, make sure that `count` is always less than the length of `dest`.

```c
strncpy(str1, "abc", sizeof(str1));
strncpy(str1, str2, sizeof(str1));
// This is a bit dangerous, because str1 might not be null terminated
strncpy(str1, str2, sizeof(str1) - 1);
str1[sizeof(str1) - 1] = '\0';  // Safer
```

The `strncpy` function returns a pointer to the `dest` string.

A safer alternative to `strncpy` is `strlcpy`. This is nonstandard, so you need to include it from somewhere else.

### Length of a String

The only way to determine the length of a string is to iterate over it until we find the null character. The `strnlen` function does exactly that. It has prototype:

```c
size_t strnlen(const char* s, size_t maxlen);
```

This function looks at the first `maxlen` characters of `s`. If it encounters a null character in those characters, it returns the length of the string (not including the null character). If it does not encounter a null character, it just returns `maxlen`. Unlike the other C string functions, this is not part of the C standard library, so you'll need to `#define _POSIX_C_SOURCE 200809L` to use it.

```c
char* str = "Hello how are You?";
size_t len = strnlen(str, 100);
printf("%zu\n", len);  // prints 18
```

Note that `strnlen` does *not* return the length of the array the string is stored in (use `sizeof` for that); it only returns the length of the string within the array.

### Concatenating Strings

The `strncat` function concatenates two strings. It has prototype:

```c
char* strncat(char* dest, const char* src, size_t count);
```

This appends the first `count` characters of `src` to `dest`, or the first characters before a null character. A null character is always appended at the end of the appention, so make sure you leave room for that when you concat.

```c
strncat(str1, str2, sizeof(str1) - strnlen(str, sizeof(str1)) - 1);
// leave room for null character
```

### Comparing Strings

Because we can't test for string equality using `==`, we have to use `strncmp`.

```c
int strncmp(const char* lhs, const char* rhs, size_t count);
```

This compares at most `count` characters in the two strings. The strings are compared in lexicographical order using their ASCII codes. A negative value is returned if `lhs` comes before `rhs`, 0 is returned if the two strings are equal, and a positive value if `lhs` comes after `rhs`.

```c
int strncmp("abc", "abcd", 4);
```

If you want to legitimately compare the strings, make sure that `count` is greater or equal to the lengths of each.

# Working With Types

## Type Conversion and Casting

**Type conversion** is the process of converting a value of one type into a value of another type. This involves reshuffling the bits of the variable so they are comprehensible to the new type. There are two types of type conversion: implicit and explicit.

**Implicit type conversion** happens to numeric types. When numbers of different types are mixed, the compiler will implicitly convert the types of some of the numbers so they work out. There

**Explicit type conversion** is done using a **type conversion** function, which looks like `(type)variable`.

```c
int x = 10;
int y = 3;
double div = (double)x / (double)y   // Need to do explicit cast to force floating point division
```

## Typedefs

A `typedef` is used to create an alias for a type. The general syntax is:

```c
typedef type_name alias_name;
```

For example,

```c
typedef int64_t meters;  // create the alias meters for int64_t

meters distance;
int64_t distance;  // completely equivalent to the above
```

Note that typedefs are weakly typed; they do not create new types, but are simply aliases to old ones. Hence, you can mix two type aliases as much as you want as long as they refer to the same underlying type.

```c
typedef int distance;
typedef int speed;

distance dist = 13;
speed cheetah = 100;

dist = cheetah;  // Not what we wanted, but the compiler doesn't complain!
```

Thus, the power of typedefs is greatly reduced because of their weak typing. Their prime use is then mostly for documentation purposes.

# Structs and Enums

## Structs

A `struct`, short for *structure*, allows us to group multiple variables or **members** under one name. Here is an example struct declaration.

```c
struct {
    int number;
    char name[NAME_LEN + 1];
    int on_hand;
} part1;
```

Each structure has a separate name space for its members, so any names declared within the struct will not conflict with other names defined within the program.

The previous code declared a structure variable. However, it is rather limited, because we cannot reuse the struct to declare other variables later. Furthermore, we cannot pass the variable as an argument to a function, because it doesn't have a type. We can fix this by declaring a *struct tag*.

```c
struct part {
    int number;
    char name[NAME_LEN + 1];
    bool on_hand;
};

struct part part1 = {5, "yello", true};
```

Now variables of the type can be declared and passed around like normal, albeit the type must be prefixed by `struct`. This can be avoided by using `typedef` to give the struct its own type name. For example,

```c
typedef struct {
    int number;
    char name[NAME_LEN + 1];
    int on_hand;
} part;
```

Declaring a `part` variable is now done like this.

```c
part part1;
```

Passing a struct to a function and returning a struct are now done like any other types.

```c
void print_part(part p) {
    printf("number = %d\n", p.number);
    printf("name = %s\n", p.name);
    printf("on_hand = %d\n", p.on_hand);
}
```

```c
part build_part(int number, const char* name, int on_hand) {
    part p;
    p.number = number;
    strcpy(p.name, p);
    p.on_hand = on_hand;

    return p;
}
```

Because structures can often be quite large, copying them when passing them to a function as arguments can be expensive. Thus, it is very common, in fact probaply universal, to always pass a pointer to a struct when passing it to a function. Furthermore, if you want a function to modify a struct in place, you have to pass a pointer. If you don't want the function to modify the pointer, just make it a `const`.

### Initializing Struct Variables

As usual, garbage values are stored in struct member variables when a struct is created. Like arrays however, we can initialize the members at declaration using an initializer list. The values in the initializer must be in the same order as the members in the struct.

```c
struct part part1 = {5, "wrench", false};
```

Note, however, that if the entries in the struct are reordered (say, to reduce padding), initializer lists for the struct variables may then initialize the wrong elements! Thus, when initializing a struct it is always best to use a *designated initializer*. Never, never use a plain initializer list.

```c
struct part part1 = {.number = 5, .name = "wrench", .on_hand = false};
```

# Variable Scope

A **block** is a piece of code enclosed within braces. For example,

```c
int main() {
    int x = 1;
    printf("%d\n", x);
}
```

the statements between the `{` and the `}` are the block for the `main` function.

All variables are local to the block they are defined in. They can only be accessed from within that block and are destroyed when that block ends. For example,

```c
int main() {
    int x = 1;
    if (x == 1) {
        int y = 2;
    }  // y is local to the if statement, so it is destroyed here

    printf("%d\n", y);  // Nope! y is not defined
}
```

Variables within nested blocks can access variables in high ones. For example,

```c
int x = 1;
if (x == 1) {
    int y = 2;
    print("%d %d\n", x, y);  // We can access x from here
}
```

Be careful not to shadow higher variables with variables in a lower scope, however. This is confusing and should be avoided.

```c
int x = 1;
if (x == 1) {
    x = 2;  // Reassing variable in higher scope
    int x = 2;  // New variable local to this scope
}
```

In general, variables should be declared in as local a scope as possible. This reduces complexity and cleans the variables up sooner. (In this respect, using the comma operator might be justified.)

**Global variables** are defined outside of any block (within the main body of the program). These variables have **program scope**, so they can be accessed anywhere within a program.

```c
int x = 1;  // This is a global variable

int main() {
    printf("%d\n", x);  // We can acces x from here
    return 0;
}
```

Global variables usually get a bad rap that they do not entirely deserve. The common complaint is that because any function can access them, any function can also change them, so it is difficult to know the value of a global variable without a detailed knowledge of the program. For example,

```c
int x = 1;  // x is a global variable

void do_something() {
    x = 2;  // It is not clear which x this will change until the function is called
}

int main(void) {
    x = 3;

    do_something();

    printf("%d\n", x);  // Uh, x is now 2, not 3!
}
```

The problem here is not with global variables, but with *impure functions*. The function `do_something` is impure; it has the side effect of changing x. If all functions are pure, there are no issues with global variables, because the functions cannot change them. Furthermore, the global variable would be provided as an argument to the function and not silently accessed within, so there are no issues if you, for example, decide to change the name of the global variable.

# Extern

There is slight technicality when using variables defined inside one program in another. Say we are building a math library and want to have a file with some common mathematical constants.

```c
// constants.c
const double e = 2.71;
const double pi = 3.14;
```

```c
// constants.h
#pragma once

extern const double e;
extern const double pi;
```

```c
// main.c
#include <stdio.h>

#include "constants.h"

int main(void) {
    double radius = 1;
    double area = pi * radius ** 2;

    printf("The area is %f\n", area);
}
```

Compiled with

```
clang constants.o main.o -o main
```

The `constants.h` header file needs to be included to let `main.c` know what type the variable `pi` is. Within that header file, the declaration of `pi` needs to be preceeded with the `extern` keyword. This is important. The statement

```c
const double pi;
```

is a **definition** of the variable `pi`; it allocates memory and gives it an initial value (which would be random). Adding the `extern` keyword makes the statement a **declaration** (of the variables type) and *does not* allocate any memory. Thus, if we had simply said the first statement, the variable `pi` would have been allocated a random value, and we would have gotten a random value for the `area`. The second statement is only a declaration; no memory is allocated, so the compiler would know to look elsewhere to find the value of `pi` (in `constants.c` in this case).

By default, all global variables and functions have program scope, meaning their symbols are visible during linking. We saw this when we used `pi`. You can change this by adding a `static` keyword before a global variable or function definition. This changes the variable so it only has **file scope** and thus will not be visible to other files during linking. For example

```c
static const double pi = 3.14;  // This variable can only be accessed within this file

static int area(double radius) {  // Same with this function
    return pi * radius ** 2;
}
```

The general idea is that all global variables are public by default, and adding `static` makes then private. Note that adding `static` should only be done to global variables; local variables within functions can be declared `static`, but this does something entirely different and should be avoided.

# Control Flow

**Statements** are the most basic construct in C. All statements are terminated by a semicolon.

```c
int x = 0;
printf("%d\n", x);
```

Note that statements can span several lines; all that matters is where you put the semicolon.

```c
int
  x
      =
 0; // Totally fine, but never do this.
```

## If Statements

If statements allow conditional execution. The code contained in an `if` statement will only execute if some condition evaluates to `true` (or `1`). General `if` statements look very similar to ones in other languages.

```c
if (condition) {
    // some code
} else if (other_condition) {
    // some more code
} else {
    // even more code}
```

Like other languages, you can include as many `else if` statements as you wish. Note that you should *always* include the braces around `if`, `else if`, and `else` blocks. This improves readability, reduces ambiguity, and avoids problems like the *dangling else*.

```c
if (x > 5) {
    puts("x is greater than five");
} else if (x == 5) {
    puts("x is equal to five");
} else {
    puts("x is less than five");
}
```

## Switch Statements

Do not use switch statements. The are bug prone and unpleasant. Use `if-else` statements instead.

## Goto Statements

> Go To Statements Considered Harmful

Nuff said.

## While Statements

A `while` statement will execute code as long as some condition is true.

```c
while (condition) {
    // code
}
```

Note that if `condition` evaluates to `false` the first time the `while` loop is encountered, the code in the body of the loop will not execute at all.

```c
int num = 0;
while (num < 10) {
    printf("%d ", num);
    num += 1;
}
```

To use an infinite loop, use

```c
while (true) {
    // code
}
```

Some people use `for (;;) {}`, but I do not. Note that all variables local to the body of the while loop are destroyed at the end of each iteration and recreated at the beginning of the next one.

```c
int i = 0;
while (i < 5) {
    int j = 0;  // This variable is destroyed and then reinitialized at each iteration
    printf("%d ", j);  // Will print 0 0 0 0 0
    i += 1;
}
```

## Do While Loops

Huzzah! C has `do while` loops, which are identical to `while` loops except the loop condition is checked at the end of the body, so the body always executes at least once!

```c
do {
    // code
} while (condition);
```

```c
int number;  // number needs to be declared here
do {
    puts("Enter the number 1 > ");
    scanf("%d", &number);
} while (number != 1);  // Because it needs to be accessed here!
```

When using a `do while` loop, make sure you do not declare within the loop body a variable used in the condition; a variable inside the block will be destroyed once the body exits, so referencing it within the `while` condition causes an error.

## For Loops

The `for` loop in C is essentially a glorified `while` loop. It just takes the initialization, condition, and increment statements of a `while` loop when you are doing definite iteration and places them at the top of the loop. The general syntax is as follows:

```c
for (initialization; condition; increment) {
    // Code
}
```

The *initialazition* statement is executed the first time the loop is entered. Any variables declared within this statement are local to the loop and are destroyed once the loop exits. If the *condition* statement is true, the loop body executes. If not, the loop is exited. Once the body is done, the *increment* statement is evaluated. Then it goes back to the condition statement, etc. For example,

```c
for (size_t i = 0; i < 10; i += 1) {
    printf("%d ", i);
}  // i is destroyed here
```

You can leave out any or all of the `for` loop expressions, but do not do this. You can also write `for` loops with an empty body.

```c
for (int i = 0; i < 10; i += 1) {}
```

In some cases this might seem convenient, but do not do it. Just use a normal `while` loop: it is more explicit, simpler, and is obvious you did not forget to write the body.

In general, avoid using an unsinged integer as a loop variable. It is rather error prone.

## Break and Continue

The `break` keyword is used to terminate a loop immediately.

```c
while (true) {
    int x;
    printf("%d\n", x);
    if (x == 0) {
        break;
    }
}
```

The `continue` keyword is used to skip the rest of a loop and go back the top for the next iteration. Be careful when using the `continue` keyword in a while loop, because it might skip updating the loop variable.

```c
for (int i = 0; i < 10; i += 1) {
    if (i % 2 == 0) {
        continue;  // Skip the even numbers
    }
    printf("%d ", i);
}
```

```c
int i = 0;
while (i < 10) {
    if (i == 5) {
        continue;  // Watch out!
    }

    printf("%d ", i);  // When i == 5, we get an infinite loop!
}
```

Be judicious when using `break` and `continue`. They disrupt the flow of execution and can make the code more difficult to reason with.

# Dynamic Memory

The memory of a computer is organized into several segments, which are placed in this order (from lowest addresses to highest).

- OS - Contains OS memory. This memory is protected, so attempting to read or write to it will cause a segfault.
- Text - The machine code for the program. This is the assembly text
- Data - This segment contains all data that we know exactly how much space we need. It contains global data declared outside the main function, static variables declared within functions, and constants used within the program itself (like numbers 130 and string constants "Hello"). Program constants are placed in the read only data segment, which explains why trying to modify a string literal usually creates a segfault or some other error.
- Heap - dynamically allocated data. Only data allocated with `malloc` will be put here
- Stack - statically allocated data. All variables declared within functions (including the main function), are allocated in the stack

All data structures within the stack are statically allocated: their size is not allowed to change once they are created. Thus, you must know the exact size of these items at compile time. However, there are many cases when you do not know the exact of a data structure that you need to create. However, you can use dynamic memory, where you can dynamically do stuff. All functions needed to work with dynamic memory are within `<stdlib.h>`.

## Void Pointers

A `void* ` pointer is a pointer that can point to any data type. Unlike other pointers, a void pointer does not know what type of data it points to. Because of this, you cannot derefence void pointers. Instead, you must cast it to another data type, and then dereference it. It is up to you to make sure you cast it to the appropriate type. Void pointers will be implicitly cast to another pointer type.

```c
void* p = malloc(10);
char* a = p;    // implicit casting to char*
```

## Null Pointers

A null pointer is a special pointer represented by the macro `NULL`. This represents a pointer to nothing, so you cannot derefence a pointer with this value. In most systems, a null pointer points to the address `0x0`, which is within the OS segment of memory. Thus, dereferencing a null pointer will usually result in a segfault, but not always.

```c
int* p = NULL;
*p; // undefined behaviour
```

The NULL macro is defined within `<locale.h>`, `<stddef.h>`, `<stdio.h>`, `<stdlib.h>`, `<string.h>`, and `<time.h>`, so you can use it as long as one of these headers is included.

## Allocating Dynamic Memory

There are three functions used for dynamically allocating memory.

### Malloc

```c
void* malloc(size_t size);
```

The `malloc` function allocates `size` bytes of memory in the heap. It returns a `void*` pointer to the first byte of that memory, which then must be cast in to a pointer to a real data type.

```c
// allocate space for 10 ints
// void* implicitly cast to int*
int* arr = malloc(10 * sizeof(int));
```

It is possible for the memory of a program to run out, so `malloc` may be unable to allocate the requested amount of memory. In this case, it will return a NULL pointer. Though it is unlikely this will happen, you should always check for this when allocating memory.

```c
int* arr = malloc(10 * sizeof(int));
assert(arr != NULL);
```

### Calloc

```c
void* calloc(size_t num, size_t size);
```

`calloc` is similar to `malloc`. It allocates memory for an array of `num` objects each of `size` bytes, and then sets each of the bits within the allocated memory to 0. Again, `calloc` returns NULL if it cannot allocate the memory.

```c
int* arr = calloc(10, sizeof(int));
assert(arr != NULL);
```

### Realloc

The `realloc` function allows us to adjust the size of already allocated memory.

```c
void* realloc(void* ptr, size_t newsize);
```

`ptr` points to the beginning of a block of already allocated memory, and `newsize` is the desired new size of the block in bytes. `realloc` returns a pointer to the start of the newly resized block of memory. This is necessary because `realloc` may be unable to change the size of the block in place, if your expanding it for example, so it may have to copy the already allocated memory to a new location and expand it there. Thus, make sure you update all pointers that point to the memory block, because it may have moved after reallocation.

If `realloc` cannot enlarge the piece of memory, it returns a null pointer; the original piece of memory is left unchanged and *not* deallocated.

```c
int* arr = malloc(10 * sizeof(int));
arr = realloc(arr, 20 * sizeof(int));
// This could result in a memory leak, because if realloc fails, the value in
// gets set to NULL. The original memory is not freed, so now we have a leak.
// In this case we are doing an assertion though, so it's not too bad.
assert(arr != NULL);
```

If `ptr` is a null pointer, then `realloc(ptr, size)` is equivalent to `malloc(size)`.

## Deallocating Memory

You are responsible for deallocating dynamic memory once you no longer need it. This can be done using the `free` function:

```c
void free(void* ptr);
```

where `ptr` points to a block of dynamically allocated memory. Calling `free` on a null pointer has no effect. On other pointers that do not point to dynamic memory, the result is undefined, and will usually result in a crash.

```c
int* arr = malloc(10 * sizeof(int))
free(arr);
```

Once a pointer is freed, it becomes *dangling*. It no longer has control over the memory it points to, so dereferencing it is undefined.

```c
free(arr);
arr = NULL;
```

Another potential issue is **memory leak**. This occurs when allocated memory is not freed before the end of the program. This can especially be an issue when a pointer that points to allocated memory goes out of scope, because you than cannot free that memory.

```c
if (x == 1) {
    int* p = malloc(10 * sizeof(int));
}
// Uh oh, now we can't access p anymore
// We have a memory leak
```

# Pointers to Functions

Because functions are stored within the text section of memory during program execution, you can also make pointers to functions. This allows you to pass a function to another function. For example:

```c
double f(double);

double integrate(double f(double), double a, double b) {
    double x = (a + b) / 2;
    return f(x) * (b - a);
}
```

Here is the function prototype for `qsort`.

```c
void qsort(void* ptr, size_t count, size_t size, int (*comp)(const void* a, const void* b));
```

where
  - `ptr` is a pointer to the start of the array you want to sort
  - `count` is the number of elements in the array
  - `size` is the size of each element in the array in bytes
  - `comp` is a comparison function. Pointers to elements of `ptr` are passed to this function. In this case, the `void` in the function declaration has to match the `void` in `ptr`. If `*a < *b`, then return a negative number. If `*a == *b`, rturn 0. If `*a > *b`, return a positive number.

Function pointers are dangerous, because you could point the function pointer to an arbitrary piece of memory and begin to execute code there.

# File Scoping

Static variables are stored in the data segment because of the `static` modifier.

# Error Handling

Most standard library functions return a value to indicate if an error occured. In this case, the easiest way to deal with it is

```c
if (rval != success_val) {
    perror("function name");
    exit(EXIT_FAILURE);
}
```

`perror` prints an error message based on the current `errno`.


# Program Organization

Usually, each module ( a .c file and usually one .h file) contains one ADT. Debugging and testing can be put in another .c file (or use a testing framework). Put all global type information in the header file, all information that will be used in other files. This is usually the ADT definition (struct, whatever), function prototypes, and maybe some #definitions. Do not include anything that runs code or allocates memory.

Functions and global variables that are not exported in the header file should be declared `static`. This makes those identifiers private to the translation unit; other units cannot access them by manually declaring the type signatures and then linking to them.

Segmentation Fault results when you try to access memory you are not allowed to. Sometimes, you can overwrite memory unintentionally, but a segfault won't happen because that memory isn't protected. In general, accessing uninitialized or unallocated memory will not crash your program. It will only crash if you try to access protected memory.

# Volatile


# Type Punning

How to reinterpret one variable as another one:

https://stackoverflow.com/questions/17789928/whats-a-proper-way-of-type-punning-a-float-to-an-int-and-vice-versa
https://gustedt.wordpress.com/2014/04/02/dont-use-casts-i/

In C, there are two techniques:

```c

```

We are going to get deep into the standardese here, but this first one has a subtle problem. The C standard allows for the resulting value to be a *trap representation*, a bit pattern that fits into that type, but is UB if used as a value of the type. For example, a signaling NaN is a trap representation for floating point types. Also, be careful, because this behaviour is undefined in C++.

```c
static_assert(sizeof(double) == sizeof(uint64_t));
double a = 0.0;
uint64_t x;
memcpy(&x, &a, sizeof(a));
```

I recommend using the second technique. The first has several subtle problems, and the second is shorter anyway.

# Exiting

A program can exit in several ways.

The simplest way to exit a program is to call the `_Exit()` function (identical to `_exit` in POSIX).

```c
#include <stdlib.h>
noreturn void _Exit(int exit_code);
```

The `exit_code` is an integer returned to the calling process indicating the termination status of the program. There are many different exit values, but the C standard defines only two: `EXIT_SUCCESS` and `EXIT_FAILURE`.

A more sophisticated way is to call `exit()`, which performs various cleanup actions before exiting:

```c
#include <stdlib.h>
noreturn void exit(int exit_code);
```

- exit handles registered with `atexit()` are called, in reverse order of their registration.
- All streams are flushed and closed
- `_Exit()` is called with the supplied `exit_code`

## Exit Handlers

A function that is called when a program `exit()`s or returns from `main()`is called an *exit handler*. Exit handlers can be registered using the `atexit()` function.

```c
#include <stdlib.h>
int atexit(void func(void));
```

This function takes a pointer to a function that takes no arguments and returns no value, and registers it as an exit handler. It returns 0 on success, and non-zero value on failure.

Calling `exit()` inside an exit handler is undefined behaviour, so don't do it.

A child process created by `fork()` inherits its parent's exit handlers. However, an `exec()` removes exit handlers.

# Signals

A *signal* is a software interrupt. It can be delivered to a program as a result of a runtime error or hardware message. Signals are delivered to a program asynchronously; there is no special place where they appear, they just show up. When a signal is generated, we say it has been *raised*.

The `<signal.h>` header defines the following signals:

- `SIGABRT`: abnormal program termination, possibly caused by a call to `abort()`
- `SIGFPE`: error during a math operation (such as division by zero)
- `SIGILL`: invalid instruction
- `SIGINT`: interrupt
- `SIGSEGV`: invalid memory access (a segfault)
- `SIGTERM`: termination request

A *signal handler*, which is a function that is invoked when a signal is delivered, can be installed using the `signal` function.

```c
typedef void sighandler_t(int);
sighandler_t signal(int sig, sighandler_t handler);
```

The `signal` function takes the given signal, and installs the given handler. On success, it returns the previous signal handler, and on error it returns `SIG_ERR` and sets the `errno`. When a signal is raised, the corresponding signal code is given to the handler as an argument.

```c
void handler(const int sig) {
    // UB in most cases, actually, since not async-signal-safe
    printf("recieved signal %d\n", sig);
}

int main(void) {
    if (signal(SIGTERM, handler) == SIG_ERR) {
        puts("signal");
        exit(EXIT_FAILURE);
    }
}
```

Note that signal handlers are very limited in what they can do. Unless they are invoked by a call to `raise()` or `abort()` (a so-called *synchronous signal*), signal handlers cannot call most library functions or access static variables. See the C reference page for more.

Once a signal handler returns, the program continues execution at the place where it was interrupted, except in two cases: a program will abort after the handler for a `SIGABRT` signal returns, and returning from a handler for `SIGFPE`, `SIGILL`, or `SIGSEGV` is undefined (so don't do it, call `exit()` or `abort()` before returning).

There are also two predefined signal handlers:

- `SIG_DFL`: handles a signal in the default way, which is implementation defined, but usually causes program termination
- `SIG_IGN`: ignores the signal

At program startup, the signal handler for each signal is initialized to `SIG_DFL` or `SIG_IGN`.

What happens if a signal handler raises a signal itself? To prevent infinite recursion, an implementation may do two things:
- reset the signal handler to `SIG_DFL` before calling the handler
- block the signal while the handler is called

In this case, it may be necessary to a signal handler to reinstall itself as the signal handler before it returns (bleh).

The `raise()` function can be used to raise a signal manually (this is a synchronous signal).

```c
int raise(int sig);
```

This function returns zero on success, and non-zero on failure.

```c
#include <signal.h>
#include <stdio.h>

volatile sig_atomic_t status = 0;

void handler(const int signal) {
    status = signal;
}

int main(void) {
    if (signal(SIGINT, handler) == SIG_ERR) {
        perror("signal");
        exit(EXIT_FAILURE);
    }

    printf("sending signal: %d\n", SIGINT);
    if (raise(SIGINT) != 0) {
        fputs("unable to raise signal", stderr);
        exit(EXIT_FAILURE);
    }
    printf("signal value: %d\n", status);
}
```

## setjmp and longjmp

Normally, a `goto` statement can only jump to labels within the same function. The `setjmp` macro and `longjmp` function makes it possible to jump between arbitrary locations in a program.

```c
int setjmp(jmp_buf env);
noreturn void longjmp(jmp_buf env, int val);
```

First, we need to mark a position to jump to using `setjmp`. This takes a `jmp_buf` environment variable, where it stores the location to jump to. It then returns zero. Then, we return to the saved location using `longjmp`, which takes the `jmp_buf` variable that was initialized by `setjmp` and some other value. Then, this transfers control back to `setjmp` and returns the `longjmp` value (which is set to 1 if it is 0). Thus, `setjmp` returns twice. For example,

```c
#include <setjmp.h>
#include <stdio.h>

int main(void) {

    jmp_buf env;

    const int val = setjmp(env);
    if (val == 0) {
        puts("setjmp return first time");
        longjmp(env, 10);
    } else {
        printf("longjmp val: %d\n", val);
    }
}
```

Be careful! Ensure that the argument to `longjmp` was previously initialized by a call to `setjmp`, and that `longjmp` doesn't try to jump to a function that has already returned (so, you should only jump higher up the call stack).

```c
jmp_buf env;

void jump_here(void) {
    setjmp(env);
}

int main(void) {
    jump_here();
    longjmp(env, 1);    // Undefined behaviour!
}
```

# Bitwise Operations

Only do bitwise ops on unsigned (preferabbly fixed width) integers. We want to treat them as just bit patterns, which unsigned integers are best at (no sign bit to mess things up).
