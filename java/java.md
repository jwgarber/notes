# Introduction

Java is a medium-level programming language. It has static typing like C++, but has no direct access to memory. It is also has automatic memory management. Java is entirely object oriented. Everything, even the main function, occurs within a class. Unlike other compiled languages, Java code is not compiled into native machine code. Instead, is is converted to bytecode, which is then run by a Java Virtual Machine on your environment.


# The JVM

All Java source code is written in plain text files ending with the `.java` extension. These files are then compiled into `.class` files by `javac`, the Java compiler. Unlike other compiled languages, these `.class` files do not contain native machine code. Instead, they contain **bytecode** that is specific to the Java Virtual Machine. To run the code, the bytecode is loaded into a JVM specific to your platform, which then converts it to machine code.

Because of the bytecode is native to the JVM, the same program can run on essentially any platform, as long as you have a JVM for that platform. Thus, Java is often touted as being **platform independent**.

Java is entirely object oriented.


# Variables

All variables are statically typed, and are declared using the following syntax.

```java
Type variableName;
```

A variable can also be explicitly initialized at declaration time.

```java
int age = 1;
```

Variables that are not explicitly initialized will be assigned a zero or null value at declaration time.

```java
int age;  // This will be assigned 0
```

However, relying on the implicit initialization is considered bad programming style. In addition, local variables *are not* implicitly initialized, so accesing an uninitialized local variable will result in a compiler error. Thus, it is best to always explicitly initialize a variable.

# Primitive Data Types

Java has eight primitive data types. These data types are special: they are built into a language and are not implemented as classes. Thus, you do not use the `new` keyword when creating a variable of these types.

## Integers

Java has `byte`, `short`, `int`, and `long` to represent 8, 16, 32, and 64 bit signed integers respectively. There are also unsinged versions, but I will use them if I need to.

All integer literals, such as `1000` are of type `int` implicitly. You can make a literal of type long by appending an `L` to the end of the literal.

```java
byte length = 100;  // The literal is an int, but we can save it as a byte
long size = 10000;  // This literal is also an int, but we can save it as a long
long bigger = 1000000000L  // This literal is a long
```

Just to emphasize that you are creating a `long` variable, always assign a `long` literal to a `long` variable.

## Floating Point

Java has `float` and `double` to represent 32 and 64 bit floating point numbers. `float` literals end with an `f` or `F`. `double` literals can optionally end with a `d` or `D`, but this is usually omitted. You can also use an `e` or `E` to write a literal in scientific notation.

```java
double d1 = 123.4;
double d2 = 1.234e2;  // Scientific notation
float f1 = 123.4f;
```

## Booleans

The `boolean` type represents boolean values. There are only two boolean values: `true` and `false`.

```java
boolean result;  // Implicitly initialized to false
boolean result = false;  // Much better
```

## Characters

The `char` data type represents a single UTF-16 character. All characters are enclosed within single quotes.

```java
char favChar;  // Implicitly initialized to \u0000
char favChar = 'J';  // Much better
```

# Control Flow

## Conditional Evaluation

An `if-else if-else` statement will execute a one of several blocks of code depending if a certain condition evaluates to `true`.

```java
if (condition) {
    // code
} else if (condition) {
    // more code
} else {
    // yet more code
}
```

For example,

```java
int age = 50;

if (age > 65) {
    System.out.println("Senior");
} else if (age > 18) {
    System.out.println("Adult");
} else {
    System.out.println("Minor");
}
```

Note that you can include as many `else if` statements as you want (including none), and that the final `else` statement is optional.

## While Loops

A while loop executes a block of code as long as some condition evaluates to `true`.

```java
while (condition) {
    // code
}
```

For example,

```java
int i = 0;
while (i < 10) {
    System.out.println(i);
    i += 1;
}
```

You can express an infinite loop using

```java
while (true) {
    // code
}
```

## Do While

A `do while` loop is very similar to a while loop, except that the code within the loop is always evaluated at least once.

```java
do {
    // code
} while (condition);
```

For example,

```java
int i = 0;
do {
    System.out.println("Hello world!");
    i += 1;
} while (i < 10);
```

## For Loop

There are two forms of a `for` loop. The first is just a glorified version of a `while` loop.

```java
for (initializition; condition; increment) {
    // code
}
```

This simply collects the init, condition, and increment statements of a `while` loop and puts them at the top of the loop.

```java
for (int i = 0; i < 10; i += 1) {
    System.out.println("Hello World!");
}
```

Any variables declared within the init statement have loop scope; they can be accessed within the loop, but are destroyed once the loop exits.

There is another version of the `for` loop used for iterating through the values in Collections and arrays. This version of the for loop is very similar to the one in Python. The general syntax is this:

```java
for (type variable : array) {
    // code
}
```

For example,

```java
int[] numbers = {0, 1, 2, 3, 4};
for (int number : numbers) {
    System.out.println(number);
}
```

Use this form of the for loop whenever natural. It cleaner than messing with indices.

## Branching Statements

There are two (three?) branching statements in Java: `break`, `continue`, and `return`.

The `break` statement has two forms: labelled and unlabelled. The unlabelled form will break out of the switch statement or loop it is in.

```java
class BreakDemo {
    public static void main(String[] args) {

        int[] arrayOfInts = {32, 87, 3, 589, 12, 1076, 2000, 8, 622, 127};
        int searchFor = 12;

        boolean foundIt = false;
        for (int integer : arrayOfInts) {
            if (integer == searchFor) {
                fountIt = true;
                break;
            }
        }

        if (foundIt) {
            System.out.println("Found " + searchFor);
        } else {
            System.out.println(searchFor + " not in array");
        }
    }
}
```

You can also use labelled break statements to specify which loop a break statement should break out of. However, this really disrupts the flow of the code, and should be avoided.

The `continue` statement skips the current iteration of a loop. An unlabelled `continue` statement will skip to the next iteration of the innermost loop.

```java
class ContinueDemo {
    public static void main(String[] args) {
        String searchMe = "peter piper pick a peck of pickled peppers";
        int numPs = 0;

        for (char letter : searchMe) {
            if (letter != 'p'){
                continue;
            }

            numPs += 1;
        }

        System.out.println("Fount " + numPs + " p's in the string.");
    }
}
```

A labelled `continue` statement will skip the iteration of the given loop. Like the labelled `break` statement however, I would avoid using it.

In general though, I prefer to avoid using `break` and `continue` statements. They do simplify code in a few select cases, but can easily degenerate into a mess. Usually when I need to use a `break` or `continue` statement, I wrap that code in a function and replace the `break` or `continue` with a `return` instead. Helps minimize the jump statement's effect.


# Composite Data Types

## Arrays

An **array** is an ordered container that contains a fixed number of elements of a certain type. The length of an array is declared when the array is created, and this length does not change. Arrays are 0-indexed, like other programming languages. Arrays can be declared using the `type[] arrayName` syntax (you can also put the brackets after the variable name, but this is discourged). For example, to declare an array of ints,

```java
int[] myArray;
```

Note that the above code only *declares* the array. It does not allocate any memory. To allocate memory for your array, use the `new` keyword.

```java
myArray = new int[10];  // An array of length 10
```

You can combine the above two statements to declare and initialize an array at the same time.

```java
int[] myArray = new int[10];
```

Instead of using the `new` keyword, you can also explicitly initialize the array using a comma separated list. In this case, the length of the array is determined by the number of initializers provided.

```java
int[] myArray = {1, 2, 3, 4};  // An array of length 4
```

As usual, the elements of the array are initialized to their default values when the memory is allocated. You can combine As with other languages, the elements of an array can be assigned to and referenced using their indices (which are ints).

```java
myArray[0] = 100;
myArray[9] = 900;
System.out.println(myArray[0]);
```

You can declare a *multidimensional array* using multiple brackets:

```java
int[][] numbers = new int[10][20];  // Create a 10 x 20 array
```

Unlike in C, the elements of a multidimensional array are themselves arrays, so these arrays within arrays can have different lengths. You can also use the same syntax to provide an initializer list to these arrays.

```java
String[][] names = {
    {"Mr", "Mrs", "Ms"},  // Length 3
    {"Smith", "Jones"}  // Length 2
    };

System.out.println(names[0][0]);
System.out.println(names[1][1]);
```

You can use the `length` property of an array to determine its size.

```java
int len = myArray.length;
```

### Manipulating Arrays

The `java.util.Arrays` class has many useful methods for working with arrays. For example, the `copyOfRange` method can be used to copy an array (like slicing in Python).

```java
int[] myArray = {1, 2, 3, 4};
int[] newArray = java.util.Arrays.copyOfRange(myArray, 0, 3);
```

In the above method, the elements from indices 0 to 3 (inclusive to exclusive, like in Python) are copied. Other handy methods within the class include `binarySearch` for searching, `copyOf` for copying an entire array, `eqauls` for checking if two arrays are equal to each other, `fill` for filling an array with values, and `sort` and `parallelSort` for sorting into ascending order.

## Strings

A `String` contains a sequence of Unicode characters. Strings are enclosed within double quotes, and they are created like so:

```java
String name = "jacob";
```

Strings can be concatenated using the `+` operator:

```java
String firstName = "Jacob";
String lastName = "Garber";
String fullName = firstName + " " + lastName;
```

# Operators

Here is a precedence table for the Java operators. Note that all binary operators with equal precedence are evaluated from left to right, except the assignment operators, which is evaluated from right to left.

| Category | Operators |
|:-------- |:---------:|
| expr++ expr--
| ++expr

When using expressions with many operators in them, it is best to use parenthesis generously to be explicit about the order of evaluation.

## Assignment

The **assignment operator** evaluates the expression on its left and assigns it to the variable on its left.

```java
int number = 1;
char letter = 'J';
boolean correct = false;
```

You can also combine the assignment operator with the arithmetic operators to get the **compound assignment** operators `+= -= *= /= %=`.

## Arithmetic

The `+ - * / %` operators work as you expect. There is also the unary negation `-` operator.

## Relational Operators

Java has the standard relation operators `== != < > <= >=`.

## Boolean Operators

The `!`, `&&`, `||` operators represent Boolean *not*, *and*, *or*. Unlike C++, the negation operator has very high precedence, so one does not have to worry so much about it being evaluated properly. Also like C++, the `&&` and `||` operators have **short-circuit evaluation**; the right hand side of the expression will not be evaluated unless necessary. Thus, keep in mind the warnings about passing expressions with side effects to these operators.

## Bitwise Operators

The `~ & | ^ << >> >>>` are bitwise operators. I doubt I will need these.

## Instanceof

The `instanceof` operator returns a boolean indicating if an object is an instance of a class, or if it extends an interface. For example,

```java
class Parent {}
interface MyInterface {}
class Child extends Parent implements MyInterface {}

class InstanceOfDemo {
    public static void main(String[] args) {
        Parent obj1 = new Parent();
        Parent obj2 = new Child();

        obj1 instanceof Parent;  // true
        obj1 instanceof Child;  // false
        obj1 instanceof MyInterface; // false

        obj2 instanceof Parent; // true
        obj2 instanceof Child; // true
        obj2 instanceof MyInterface; // true

    }
}
```


# Identifiers

**Identifiers** are the names of variables, classes, methods, and packages. Identifiers are case sensitive, and can be made of any combination of letters, digits, the dollar sign `$` and the underscore `_`. It is convention to begin all identifiers with a letter and not use the dollar sign at all.

- Identifiers are case sensitive; Java will treat `myName` differently than `myname`.
- Identifiers can be made of any combination of letters, digits, the dollar sign `$` and the underscore `_`. However, identifiers can begin with any of the allowed characters except digits. While using the dollar sign is allowed, it is convention not to use it.

- Variable and function names are written in `lowerCamelCase`.
- Constants are written in `SNAKE_CASE'.` It is convention not to use the underscore anywhere else.
- Class names are written in `UpperCamelCase`.

# Comments

There are three types of comments in Java: C style comments, doc comments, and C++ style comments.

```java
/*
 * This is a C style comment. Everything between the delimiters, including
 * newlines, is treated as a comment. I don't like these very much.
 */

/**
 * This is a doc comment. It is identical to a C style comment, except the
 * first delimiter begins with two stars. The javadoc program uses these
 * comments to automatically generate documentation for your code.
 */

// This is a C++ style comment. It includes everything up to the end of
// the line. I prefer using these instead of C style comments.
```

## Main

The main function of every executable is encapsulated as a method within a class. The generic template is

```java
class MyProgramName {
    public static void main(String[] args) {
        // Your code
    }
}
```

The command line arguments to your code are stored in the `args` variable (which is an array of strings). Note that the first element of this array the first command line argument (not the name of the program, unlike in C).

# Classes

Classes are declared using a **class declaration**. Trying to explain everything separately just doesn't work in this case, because everything is so interconnected. Here is one big example, and I will just explain as I go.

```java
class Bicycle {

    // These are the fields of the class. They hold the internal state of the objects of the class.
    // The private keyword indicates that these fields cannot be accessed directly outside of the
    // class, so things like myBicycle.cadence = 10 won't work. The principle of encapsulation says
    // that all objects should only be accessed through their methods. Declaring these fields public
    // would have the opposite effect.
    private int cadence;
    private int gear;
    private int speed;


    // This is the constructor of the class. It creates new instances of the class. Constructors
    // are different than methods in that they use the same name as the class and do not have a
    // return type. You will want to declare a constructor public, otherwise it's kind of pointless.

    // Note that unlike Python, there is no self parameter you pass to a method. The fields of the
    // object are just modified directly. You can declare other local variables within a method,
    // but if those variables are not declared as fields as above, they will not be treated as such.
    // When you reference the cadence variable, it cannot find it in the scope of the function, so
    // it looks one scope up and finds it in the field declarations.
    public Bicycle(int startCadence, int startGear, int startGear) {
        cadence = startCadence;
        gear = startGear;
        speed = startSpeed;
    }

    // This is a method of the class. In Java, you do not have functions, only methods. All methods
    // have only one return type, which is specified before the name of the method. A method that
    // does not return any value has a return type of void. All parameters of the method are named
    // and have their types specified before their names. The public keyword before the methods
    // indicates these methods can be accessed outside the class, like myBicycle.getCadence().
    // You can also make methods private, in which case they can only be accessed from within the
    // class.

    // Methods traditionally begin with a verb and are spelt in camelCase.
    public int getCadence() {
        return cadence;
    }

    public void setCadence(int newValue) {
        cadence = newValue;
    }

    public int getGear() {
        return gear;
    }

    public void setGear(int newValue) {
        gear = newValue;
    }

    public int getSpeed() {
        return speed;
    }

    public int setSpeed(int newValue) {
        speed = newValue;
    }

}
```

Within the declaration, there are usually three components: fields, a constructor, and methods.

## Fields

**Fields** are the variables that store information about the class and the objects instantiated from it. It is customary to declare all fields at the start of your class. In the above example, `cadence`, `gear`, and `speed` are the fields of the class. Fields can be of any data type and can be initialized or uninitialized.

```java
int length;
int width = 0;
Point p1;
Point p2 = new Point(0, 0);
```

## Constructors

A **constructor** is a function that creates and returns a new instance of the class. It is similar to a method, except it is called the same name as the class and has no return type. Here is the constructor for the above class.

```java
public Bicycle(int startCadence, int startGear, int startGear) {
    cadence = startCadence;
    gear = startGear;
    speed = startSpeed;
}
```

To create an instance of a class, use the `new` keyword followed by the constructor of the class. For example,

```java
Bicycle myBike = new Bicycle(0, 20, 5);
```

If you do not explicitly define a constructor, Java will automatically provide a default zero-argument constructor that calls the zero-argument constructor of the superclass. The compiler will complain if the superclass does not have a zero-argument constructor. Note that all classes without an explicit superclass implicitly inherit from the `Object` class, which does have a zero-argument constructor (and I assume it just returns an empty object), in which case you are good.

## Methods

**Methods** are functions that access the internal state of the class or instances of the class. It is considered good form to make all mutable fields private and only access them through methods. Here is the generic form of a method:

```java
modifier keywords returnType methodName(arguments) {
    // code
}
```

You should only define getters and setters if necessary. Usually, it is best just to directly define methods that do what you want to do. If you really need to define getters and setters without any error checking, you might as well skip the boilerplate and make the field public.

For each mutable field, you should define *getter* and *setter* public methods to access and mutate that field. For example, here are the getters and setters for the `speed` field.

```java
public int getGear() {
    return gear;
}

public void setGear(int newValue) {
    gear = newValue;
}
```

Methods can take any type of parameters and return any type of arguments. For example,

```java
public Polyon pointsToPolygon(Point[] points) {
    // Code
}
```

creates a `Polygon` from an array of `Points`. Note that methods can only return one object. Ridiculous, I know, but that's life. Methods that do not return any value have the special return type `void`. Any empty `return` statement will have this return type.

Note that all instances of subclasses are also instances of the superclass. For example, suppose the `Square` class extends the `Polygon` class. All instances of the `Square` class are also instances of the `Polygon` class. This means that if the return type of your method is a superclass, any subclass can be returned, because they are all instances of the superclass.

Furthermore, you can use an interface as a return type. In this case, the object returned must implement that interface.

Methods can be **overloaded**. That is, you can have multiple methods defined in a class with the same name as long as the types and number of the parameters of the methods are different. When you call a certain method, Java will pick the correct one method to use based on the types and number of the arguments. Overloading methods is bad. It is far better to use generics, which are more powerful, cleaner, and elegant. Constructors can be overloaded as well, but just avoid it.

You can use *varargs* to pass an arbitrary number of values to a method. This is similar to tuple packing in Python. To use varargs, follow the type of the last parameter with an ellipsis `...`. All arguments passed to the method after the mandatory arguments (including none) will be collected together into an array.

```java
public Polygon pointsToPolygon(int numberOfSides, Point... points) {
    // Code. points is now an array of Points. That is, it is of type Point[]
    int length = points.length;
    point1 = points[0];
    // etc
}
```

When using varargs, you can pass either an array to the function or a sequence of elements. They will be treated the same.

```java
Point a = new Point();
Point b = new Point();
Point c = new Point();
Point[] points = {a, b, c};

pointsToPolygon(3, a, b, c);
pointsToPolygon(3, points);  // same as above
```

## Scoping and this

In Java, you can access the fields and methods of a class or object within a method directly. There is no need for a `self` object as in Python. When you call these names, Java will use the normal scoping rules; if they are found within the current method scope, Java will use those identifiers, otherwise, it will look one level up and find them in the class scope.

```java
int height;
int width;
int length;

public int getArea() {
    return height * width;
}

public int getVoluem() {
    return getArea() * length;  // We can access all these identifiers unqualified
}
```

As with other variables though, the fields and class identifiers can be shadowed by local variables in a method. To access these fields, use the `this` keyword, which references the current instance of the class. This keyword is rather similar to `self` in Python.

```java
public Box(int height, int width, int length) {
    this.height = height;
    this.width = width;
    this.length = length;
}

public int getVolume() {
    // Using this is unnecessary in this case, but is still perfectly valid
    return this.getArea() * this.length;
}
```

You can also use `this` to call one constructor of a class within another constructor of the same class (using overloading). I find this confusing though and don't intend to use it.

## Access Control

Access control in Java is used to restrict which parts of a program can access or use other parts. There is no equivalent concept in Python, because in Python everything is public. Access control for a class, field, or method is done by preficing the declaration for that thing by a modifier.

There are two levels of access control - class level and member level.

Class level access control restricts the visibility of classes within files. That is, can other packages import and use the classes I define in a particular file? There are two levels of class access control - public and package-private. Classes preficed with `public` are visible to all classes everywhere. Each package can only have one `public` class. Clases with no explicit modifier are known as *package-private*. These clases can be accessed only within that particular package, and no where else. You can think of these classes as being auxillary helper classes for the `public` class.

```java
package sll;
class Node {}  // This class can only be accessed within the sll package
public class Sll {}  // This class is the front of the package that you use
```

Member level access control restricts the visibility of members of classes within other classes (remember, everything happens in classes). Member level access control has four levels.

- `private` members can only be accessed within their own class
- package-private members in addition can be accessed within the package
- `protected` members can in addition be accessed within subclasses of the class outside the package
- `public` members can be accessed from within any class


| Modifier | Class | Package | Subclass | World |
|:-------- |:-----:|:-------:|:--------:|:-----:|
| public   |   Y   |    Y    |    Y     |   Y   |
| protected|   Y   |    Y    |    Y     |   N   |
| none     |   Y   |    Y    |    N     |   N   |
| private  |   Y   |    N    |    N     |   N   |

In general, use the most restrictive modifier for each class and member.
- There is no point in declaring `protected` members unless the class is `public`
- There is also no point in declaring `public` members unless the class is `public`
- Helper functions that are only used within a class should be declared `private`
- All mutable fields should be made `private` with a public getter or setter to access it

## Class Members

By default, all fields and methods declared in a class are attribuetes of instances of that class. However, you can use the `static` keyword to declare a field or method as being a member of the class itself.

Normally, each instance object has its own copy of the class fields. **Static fields** on the other hand are members of the class. Each instance of the class shares these fields and can modify them. However, because the variables are members of the class, there is only one.

```java
// Each instance gets its own personal copy of this variable
private int length;

// However, this variable is a member of the class and is shared by all instances
// We should initialize it here.
static int numberOfBoxes = 0;

public Box() {
    // Yada yada yada
    numberOfBoxes += 1;
}
```

You can reference nonprivate `static` fields using the name of the class, such as `Box.numberOfBoxes`. You can also reference them from the instance variables, such as `myBox.numberOfBoxes`, but this is discouraged. Also note that it is best to initialize static fields within the class definition, because unlike instance instance fields they have no constructor to initialize them.

You can also declare **static methods** that are members of the class. Static methods are commonly used for accessing static fields.

```java
public void getNumberOfBoxes() {
    return numberOfBoxes;
}
```

Like static fields, you can access static methods using the class name, `Box.getNumberOfBoxes`, or an instance name, `myBox.getNumberOfBoxes`, though this is discouraged.

Instance variables can have sophisticated initializations within the constructor of a class, ie using for loops and exceptions and such. To provide similar functionality for static variables we can write static methods. For example,

```java
private static int length = initializeLength();

private static int initializeLength() {
    // Code here
}
```

There are alternatives for initializing static and member fields, such as static and member initialization blocks and final methods, but they are inferior to the approaches described above.

## Constants

The `final` modifier creates a constant. It prevents whatever variable you have from being reassinged to another value. That value itself might change through internal methods, but the variable that points to it cannot be reassigned. Used in conjunction with the `static` modifier, you can use this to define constants. Recall that constants are spelt in `UPPER_CASE`.

```java
static final double PI = 3.14159;
```

## Null

The `null` keyword represents the abscence of an object. Object references of any type can point to `null`, and all object references are implicitly initialized to `null` when they are declared. This is often used as an return value of a function to indicate the non-existence of an object. This is bad, however. It is much better to use the `Optional` type instead. Dereferencing a `null` object leads to a `NullPointerException`, which is a nasty thing.

# Variables and References

In Java, variables are not distinct little boxes that hold values and objects. Instead, they are references that point to values. When you say something like `x = y`, a new reference `x` is created that points to the same object that `y` points to.

```java
int x = 3;
int y = x;  // y points to the same object as x
y = 4;  // y now points to a new 4 object
System.out.println(x);  // x is still 3

int[] list1 = {1, 2, 3};
int[] list2 = list1;  // list2 now points to the same array is list1
list2 = {1, 2, 4};  // list2 now points to a new array, with list1 unchanged
```

When you have multiple variables that point to the same object, you need to be careful about mutation, because the object can be changed through any reference. This is not an issue for the eight primitive data types because they have no internal structure to be mutated. However, keep this in mind for reference types (objects createed with `new`), because they can be mutated.

```java
int[] xlist = {1, 2, 3};
int[] ylist = xlist;  // ylist points to the same object as xlist
ylist[1] = 4;  // this mutates xlist as well
System.out.println(xlist[1]);  // this is now 4
```

## Creating Objects

Instantiation of an object in Java has three steps (wow, this is such crap): declaration, allocation, and initialization.

A variable **declaration** for a reference type is of the form

```java
className variableName;
```

This notifies the compiler that `variableName` is of type `className`. Unlike declarations for primitive types, reference type declarations do not allocate any memory. For example,

```java
Point p1;
```

The `new` operator allocates memory for an object and returns a reference to that object. The object to create is provided by the constructor of the class. This constructor initializes the object.

```java
p1 = new Point(1, 2);
```

You can combine declaration and initialazation into one statement.

```java
Point p1 = new Point(1, 2);
```

## Nested Classes

A **nested class** is a class defined within another class. There are two types of nested classes: static and non-static. Nested classes that are declared `static` are *static nested classes*. Nested classes that are not static are known as *inner classes*.


```java
class OuterClass {
    // Code
    class InnerClass {
        // Code
    }

    static class StaticClass {
        // Code
    }
}
```

A nested class is a member of its enclosing class. Consequently, all the normal modifiers apply to nested classes, so they can be declared `private`, `protected`, `public`, and package private like other memberrs.

### Static Nested Classes

Like static class methods, a static nested class cannot refer directly to non-static members of the enclosing class. The only way it can access these is through an object reference. Static nested classes are members of the enclosing class and are accessed as such.

```java
OuterClass.StaticNestedClass
```

To create an instance of a static nested class, use the following syntax

```java
OuterClass.StaticNestedClass object = new OuterClass.StaticNestedClass();
```

Static nested classes are primarily helper classes for the outer class. They are only placed inside the outer class for encapsulation. For example, I could place the `Node` class for the single linked list inside the `SLL` class as a static nested class. However, I will not for simplicity.

### Inner Classes

Like instance methods and variables, an inner class is associated with an instance of its enclosing class and has direct access to that instance's methods and fields. Also, because an inner class is associated with an instance, it cannot define any static members itself. Inner classes have access to all other members of the enclosing class, even if they are declared private.

To instantiate an inner class, you must first instantiate the outer class, then instantiate the inner class from the instance of the outer class.

```java
OuterClass outerObject = new OuterClass();
OuterClass.InnerClass innerObject = outerObject.new InnerClass();
```

An inner class example.

```java
class DataStructure {

    // Create an array
    private static final int SIZE = 15;
    private int[] array;

    // Constructor
    public DataStructure() {
        // fill an array with integers
        this.array = new int[SIZE];
        for (int i = 0; i < SIZE; i += 1) {
            array[i] = i;
        }
    }

    // Why?
    interface DataStructureIterator extends java.util.Iterator<Integer> {};

    private class EvenIterator implements DataStructureIterator {

        private int nextIndex = 0;

        public boolean hasNext() {
            return nextIndex < SIZE;
        }

        public Integer next() {
            // See? We diretly access the array variable
            // Black magic
            Integer retValue = Integer.valueof(array[nextIndex]);

            nextIndex += 2;
            return retValue;
        }
    }
}

public class Main {
    public static void main(String[] args) {

        DataStructure ds = new DataStructure();

        // Print out values at even indices
        // We create an iterator that directly accesses the array variable in the object it was created from
        DataStructureIterator iterator = this.new EvenIterator();
        while (iterator.hasNext()) {
            System.out.print(iterator.next() + " ");
        }
        System.out.println();


    }
}
```

### Local Classes

**Local** classes are inner classes defined within a block, such as a loop, if statement, or method.

```java
public class LocalClassExample {

    static String regularExpression = "[^0-9]";

    public static void validatePhoneNumber(String phoneNumber1, String phoneNumber2) {

        final int numberLength = 10;

        class PhoneNumber {

            String formattedPhoneNumber;

            // Constructor
            PhoneNumber(String phoneNumber) {

                String currentNumber = phoneNumber.replaceAll(regularExpression, "");
                if (currentNumber.length() == numberLength) {
                    formattedPhoneNumber = currentNumber;
                } else {
                    // Better to use optional type
                    formattedPhoneNumber = null;
                }
            }

            public String getNumber() {
                return formattedPhoneNumber;
            }

            public void printOriginalNumbers() {
                System.out.println("Original numbers are " + phoneNumber1 + " and " + phoneNumber2);
            }
        }

        PhoneNumber myNumber1 = new PhoneNumber(phoneNumber1);
        PhoneNumber myNumber2 = new PhoneNumber(phoneNumber2);

        myNumber1.printOriginalNumbers();

        if (myNumber1.getNumber() == null) {
            System.out.println("First number is invalid");
        } else {
            System.out.println("First number is " + myNumber1.getNumber());
        }


    }
}
```

I do not see why it is ever a good idea to use local classes, so I won't go any deeper into them.

### Anonymous Classes

An anonymous class is a class instance that you define on the spot. The class you instantiate it from has no name, hence the anonymous part. Because it has no name, it also has no type, so you must indicate a class that it extends or an interface that it implements. This will be used as the type of the instance. Anonymous classes are expressions, so you can bind them to variables or (more commonly) pass them as function arguments. For example

```java
HelloWorld englishGreeting = new HelloWorld() {
    String name = "hello";
    public void greet() {
        greetSomeone(name);
    }

    public void greetSomeone(String someone) {
        System.out.println("Hello " + someone);
    }
}; // don't forget

englishGreeting.greetSomeone("Jim");
```

In this case, the anonymous class is extending the `HelloWorld` superclass. Any arguments you want to pass to the `HelloWorld` constructor go inside the braces. This syntax is identical for implementing an interface, but you don't put any arguments inside the braces, since an interface has no constructor. An anonymous class is part of an expression, so don't forget to put a semicolon at the end.

Anonymous classes have access to variables in their enclosing scope. However, they can only access final or effectively final variables (ones that don't change since declaration). This restriction means they cannot mutate any variables in the higeher scope. Again, hideous locality issues.

Anonymous classes have several restrictions:

- You cannot declare any `static` members. This makes sense, since you can only work with an instance of the class and not the class itself
- You cannot declare a constructor, since again the class has no name.

Anonymous classes are most useful for making an instance of a class with your own custom changes (like overriding a method or two). Here is an example from JavaFX:

```java
final TextField sum = new TextField() {
    @Override
    public void replaceText(int start, int end, String text) {
        if (!text.matches("[a-z, A-Z]")) {
            super.replaceText(start, end, text);
        }
        label.setText("Enter a numeric value");
    }

    @Override
    public void replaceSelection(String text) {
        if (!text.matches("[a-z, A-Z]")) {
            super.replaceSelection(text);
        }
    }
};
```

# Functional Programming

Wherein we divulge the only aspects of Java that are actually worth using.

## Lambda Functions

All decent languages have first class functions. Do we have those in Java? No. However, we can sort of "goof" the system to sort of implement them (you will notice that is a common theme for the functional aspecs of Java: they're all "sort of" implemented). What you can do is define an interface that contains your function signature, and then create an anonymous class that implements the interface, thus providing the definition. For example,

```java
// an interface for ordering objects of type T
interface Ord<T> {
    boolean compare(T a, T b);
}

// a specific function for comparing ints
Ord<Integer> ordFunc = new Ord<>() {
    public boolean compare(Integer a, Integer b) {
        return a < b;
    }
};

boolean x = ordFunc.compare(3, 4);
```

An interface that has only one abstract method, like `Ord<T>` above, is called a **functional interface** in Java parlance. This indicates that the interface is meant to act as a first class function wrapper. You can give your interface the `@FunctionalInterface` annotation to let the compiler check you've implemented it properly.

You can also emulate higher order functions using functional interfaces. Just pass around instances of the interface that have the desired function as a method.

That's really all there is to it. However, Java provides a little syntactic sugar on top of this to make working with first class functions slightly more pleasant.

A **lambda function** is syntactic sugar for an anonymous class that implements a functional interface. As you noticed above, the only really import part of the function is the `a < b` line. We can wrap this in a lambda function.

```java
Ord<Integer> ordFunc = (a, b) -> a < b;
boolean x = ordFunc.compare(3, 4);
```

Note that to actually use the function, you need to call it with `compare` as above. Also note that it is unnecessary to declare the types of `a` and `b`. The compiler infers them from `Ord<Integer>`.

Consider the folloing multiline function.

```java
Ord<Integer> ordFunc = new Ord<>() {
    public int compare(Integer a, Integer b) {
        if (a < b) {
            return -1;
        } else if (a == b) {
            return 0;
        } else {
            return 1;
        }
    }
}
```

You can turn this into a multiline lambda function:

```java
Ord<Integer> ordFunc = (a, b) -> {
     if (a < b) {
        return -1;
    } else if (a == b) {
        return 0;
    } else {
        return 1;
    }
}
```

Again, it's just syntactic sugar.

Note that the arguments must be enclosed within parentheses, unless there is only one argument, in which case you can omit them. If the function does not take any arguments, just use an empty pair of parenthesis `()`.

## Method Reference

Suppose you have the following lambda function.

```java
list.forEach(x -> System.out.println(x));
```

This lambda function does nothing but forward its arguments to another function. There is a syntactic sugar we can use called a *method reference* that does exactly this.

```java
list.forEach(System.out::println);
```

We simply pass a function that has the correct type signature, and the arguments will be passed along. Pretty handy, eh?

# Scoping

## Shadowing

To access an identifier in an outer scope that is shadowed in a local scope, you will have to preface the identifier.

```java
public class ShadowTest {

    public int x = 0;

    class FirstLevel {

        public int x = 1;

        void method(int x) {
            System.out.println("x = " + x);
            System.out.println("this.x = " + this.x);
            // Get the x variable in the object this is an instance of
            System.out.println("ShadowTest.this.x = " + ShadowTest.this.x);
        }
    }

    public static void main(String[] args) {
        ShadowTest st = new ShadowTest();
        ShadowTest.FirstLevel fl = st.new FirstLevel();
        fl.method(23);
    }
}
```

# Annotations

**Annotations** are metadata that provide extra information about your code. They look something like this:

```java
@MyAnnotation
```

For example,

```java
@Override
void mySuperMethod( ... ) { }
```

An annotation can include *elements*, which you can give values in a comma separated list:

```java
@Author(
    name = "Babe Ruth",
    date = "2003/3/27"
)
class MyClass { }
```

```java
@SuppressWarnings(value = "unchecked")
void myMethod() { }
```

If an annotation has just one element, it's name can be ommitted:

```java
@SuppressWarnings("unchecked")
```

If the annotation has no elements, the parenthesis can be ommitted, as the `@Override` shows.

You can use multiple annotations on the same object, and even repeat the same one.

```java
@Author(name = "Bill Joe")
@EBook
class MyClass { }

@Author(name = "Billy Bob")
@Author(name = "Joe Fresh")
class MyClass { }
```

There are predefined annotations in the `java.lang` and `java.lang.annotations` packages. In addition, you can define your own annotation types.

Annotations can be applied to declarations of classes, fields, and methods. By convention, each annotation appears on its own line.

Annotations can also be applied when using types. These are called **type annotations**. For example:

```java
// instantiation
new @Interned MyObject();

// type cast
myString = (@NonNull String) str;

// implements
class ImmutableList<T> implements @ReadOnly List<@ReadOnly T> { }

// exception
void monitor throws @Critical TemperatureException { }
```

## Declaring an Annotation

An annotation declaration is similar to an interface declaration, except that `interface` is preceded by an `@`. The general syntax looks like this:

```java
@Interface AnnotationName {
    // stuff
}
```

Within the annotation you can declare elements of any type, optionally with default values. Here is an example:

```java
@interface Preamble {
    String author();
    String date();
    int currentRevision() default 1;
    String lastModified() default "N/A";
    String[] reviewers();
}
```

After declaring an annotation, you can use one of that type.

```java
@Preamble(
    author = "Billy Bob",
    date = "somedate",
    currentRevision = 6,
    lastModified = "some date",
    reviewers = {"Euclid", "Euler", "Gauss"}
)
```

## Predefined Annotations

Java has two classes of built in annotations. The first are the regular annotations contained in `java.lang`. They are

- `@Deprecated` indicates the thing it annotates is deprecated and should not be used
- `@Override` indicates the thing it annotates is meant to override an element declared in a superclass
- `@SuppressWarnings` tells the compiler to suppress certain warnings
- `@SafeVarargs` suppresses unchecked warnings related to varargs
- `@FunctionalInterface` indicates a type declaration is intended to be a functional interface

There are also the meta annotations, which are applied to annotation declarations. They are defined within `java.lang.annotation`.

# Enum Types

An **enum type** allows a variable to be one of several predefined constants. An enum is declared using the `enum` keyword. For example,

```java
enum Day {
    SUNDAY, MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY, SUNDAY
}
```

The members of an enum are constants, so their names are in uppercase letters.

```java
public class EnumTest {
    public static void main(String[] args) {
        Day day = Day.MONDAY;
        if (day == Day.MONDAY) {
            System.out.println("It's monday");
        }
    }
}
```

Note that Java enums are scoped, unlike enums in C and like enum classes in C++.

The enum declaration is actually a class definition, so you can define other fields and methods within your enum. The enum values are the "instances" of the enum, and you can initialize them with values if you provide a constructor.

```java
enum Planet {
    // these must be defined first
    MERCURY (3.303e+23, 2.4397e6),  // these values are passed to the constructor
    VENUS   (4.869e+24, 6.0518e6),
    EARTH   (5.976e+24, 6.3714e6),
    MARS    (6.421e+23, 3.3972e6),
    JUPITER (1.900e+27, 7.1492e7),
    SATURN  (5.688e+26, 6.0268e7),
    URANUS  (8.686e+25, 2.5559e7),
    NEPTUNE (1.024e+26, 2.4746e7); // ends with semicolon if there are fields or methods

    // gravitational constant
    public static final double G = 6.67300E-11;

    // the fields each enum gets
    public final double mass;   // in kilograms
    public final double radius; // in meters

    // you cannot invoke this constructor outside the enum
    private Planet(double mass, double radius) {
        this.mass = mass;
        this.radius = radius;
    }

    public double surfaceGravity() {
        return G * this.mass / (this.radius * this.radius);
    }

    public double surfaceWeight(double otherMass) {
        return otherMass * this.surfaceGravity();
    }
}

public class PlanetTest {
    public static void main(String[] args) {
        if (args.length != 1) {
            System.err.println("Usage: java Planet <earth_weight>");
            System.exit(-1);
        }

        double earthWeight = Double.parseDouble(args[0]);
        double mass = earthWeight/Planet.EARTH.surfaceGravity();

        for (Planet p : Planet.values())
           System.out.printf("Your weight on %s is %f%n",
                             p, p.surfaceWeight(mass));
        }
    }
}
```

The compiler automatically adds some special methods to an enum. One useful static method is `values`. It returns an array of the values of the enum in the order they were declared. For example,

```java
for (Day d : Day.values()) {
    System.out.format("%s%n", d);
}
```

Note that all enums implicitly extend `java.lang.Enum`, so enums cannot extend from anything else.

# Numbers

The `Number` class in `java.lang` provides object wrappers for each of the primitive number types.

# Interfaces

An **interface** is a reference type that defines an Application Programming Interface (API). It contains a list of method signatures, and any class that *implements* the interface must provide definitions for those methods. Interfaces are similar to typeclasses in Haskell. Here is an example interface:

```java
interface MyInterface {
    // Code
}
```

All methods of an interface are `public` and `abstract`. It is considered bad form to explicitly say this though.

Like classes, interfaces can be declared public or package private.

An interface can also be used as a type. This allows subtype polymorphism. However, I discourage using it.

## Interface Fields

An interface can also include fields. These fields are then available to all classes thet implement the interface. These fields are assumed to be `static`, `final`, and `public`.

```java
interface GolfClub {
    int DRIVER = 1;
    int SPOON = 2;
    int NIBLICK = 3;
    int MASHIE = 4;
}
```

## Extending Interfaces

You can extend an interface from another using the `extends` keyword. This essentially copies all methods and fields from the superinterface into the subinterface. It's pretty simple.

```java
interface Set {
    int getCardinality();
}

interface VectorSpace extends Set {
    int getDimension();
}

class Matrices implements VectorSpace {
    // must define getCardinality and getDimension
}
```

## Default Methods

You can also define default methods, but I won't need these.

## Static Methods


# Type Casting

In Java, upcasting from a subclass to a superclass is done implicitly. For example,

```java
String x = "abc";
Object y = x;
```

This is always safe, since ever instance of a subclass is also an instance of the superclass, and hence has all the properties of that superclass. Type casting is used to go the other way. It is used to move an object down the inheritance hierarchy. For example,

```java
Object x = "howdy doo";
String y = (String)x;
```

This indicates that the object `x` is actually a String, and so should be treated as such. Note that type casting is different than in C and Python, because it does not change the internal representation of an object. It just indicates to the compiler that this particular object actually has a more specific type. Because all objects are represented as pointers, doing typecasts on objects is equivalent to doing a `void*` thing. For primitive types, you use casts to transform values. For example,

```java
double x = 1.0;
int y = (int)x;
```

converts a double to an int. For objects, however, the similar code would cause a compile error.

```java
Double x = new Double(5.0);
Integer y = (Integer)x;
```

This says that a Double is actually an Integer, which is of course wrong. To get the Integer value, you have to do something like

```java
Integer y = x.intValue();
```

Type casting is dangerous however, because what if you try to cast a value that doesn't actually work?

```java
Number o = new Integer(5);
Double n = (Double)o; // Whoah!
```

In this case, a `ClassCastException` is thrown at runtime. This was particularly dangerous in versions of Java before generics.

# Generics

Java and has long been able to write functions and collections that work on multiple types. This could be done by casting all objects to `Object`, or implementing an interface (cough, cough Go). For example, a Tree data type might look like this:

```java
class Tree {
    public Object data;

    public Tree(Object data) {
        this.data = data;
    }
}

class TestTree {
    public static void main(String[] args) {
        Tree tree = new Tree("a string");
        String data = (String)tree.data;
    }
}
```

This however, has a large problem. Notice the explicit cast to `String`. Once we put an object inside our `Tree`, we loose whatever type it was originally; it is now an `Object`, or perhaps some interface. The only way to get the type back is with an explicit cast, which puts the onus on the programmer to remember what the proper data type is. Luckily, Java raises a `ClassCastException` when you do an inappropriate cast, so these problems won't go unnoticed, but it vastly reduces type safety. Remembering types is now a run time issue than a compile time issue. Generics allow you to implement polymorphic functions and data types, but in a *type safe* manner.

We can make our tree generic as follows:

```java
class Tree<T> {
    public T data;

    public Tree(T data) {
        this.data = data;
    }
}
```

`T` is called a **type parameter**. It represents an arbitrary reference type (*not* a primitive type), and allows us to remember the type of the object we put in the tree. That is really all the type parameter does.

You can use as many type parameters as you want. For example:

```java
class TwoTuple<T, S> {
    public T fst;
    public S snd;

    public TwoTuple(T fst, S snd) {
        this.fst = fst;
        this.snd = snd;
    }
}
```

Generic interfaces are defined in a similar fashion:

```java
interface Eq<T> {
    boolean isEqual(T a);
}

class Number<T> implements Eq<T> {
    // etc
}
```

A generic type can be declared and instantiated by providing a type argument.

```java
Box<Integer> integerBox;    // Integer is the type argument
```

As usual for reference types, the above declaration does not allocate memory, but merely states that `integerBox` is of type `Box<Integer>`. Instantiation is done using the `new` keyword.

```java
Box<Integer> integerBox = new Box<Integer>();
```

However, when Java can infer the type of the object, which it should always to able to, you can omit the type arguments to the constructor and simply leave a diamond.

```java
Box<Integer> integerBox = new Box<>();
TwoTuple<String, Integer> t1 = new TwoTuple<>("hello", 8);
```

# Raw Types and Type Erasure

For compatability with pre-generic code, Java allows you to use **raw types**. These are generic classes and interfaces that do not have type arguments. For example, an instance of the box class can be created as

```java
Box<Integer> intBox = Box<>();
```

If you omit the type argument, you create a raw type

```java
Box intBox = Box();
```

This acts just like pre-generic code. Using raw types gives you warnings, so don't use them.

At compilation however, once the type checks have succeeded, the compiler replaces all generics with raw types and inserts the necessary typecasts to get the code to work properly. Because of this, generic types are not known at run time, which prevents doing many useful things with them. Dude, Java sucks.

# Generic Methods

Generic methods can also be done. To use them, indicate the type parameters before the method return type.

```java
public class Util {
    public static <K, V> boolean compare(Pair<K, V> p1, Pair<K, V> p2) {
        return p1.getKey().equals(p2.getKey()) && p1.getValue().equals(p2.getValue());
    }
}
```

For invoking this method, you would use

```java
Pair<Integer, String> p1 = new Pair<>(1, "apple");
Pair<Integer, String> p2 = new Pair<>(2, "pear");
boolean same = Util.compare(p1, p2);
```

# Bounded Type Parameters

You can restrict the types that will be accepted for a generic type. This is rather similar to using typclasses in the type signature of a polymorphic function in Haskell. In your type bracket, you simply go `<T extends Type>`, where `Type` is a class or an interface. If it is a class, `T` must be a subclass, and if it is an interface, `T` must implement the interface. For example, suppose you only want to write a generic function that only works on numbers. You could do something like this

```java
public <T extends Number> boolean isEqual(T a, T b) {
    return a == b;
}
```

You can also have multiple bounds for a type parameter. Simply separate the bounds with ampersands.

```java
class D<T extends A & B & C> {}
```

If one of the bounds is a class, it must be specified first.


## Wildcards

You can use wildcards to allow subtyping of generic types. However, this is stupid.

By convention, type parameters are single uppercase letters. Some common names are

- E for Element
- K for Key
- N for Number
- T for Type
- V for Value
- S, U, V for additional types


# Packages

A **package** is a bundle of related types. Packages provide a new namespace for the types within the package to avoid naming conflicts.

To create a package, choose a name for it, and then place `package packageName` at the top of every source file that you want to include in the package. If you do not include this statement, the file becomes part of an unnamed package and thus cannot be imported into other source files. The general convention is to place one package in each folder, where the folder is the name of the package.

Each source file can only contain *one* public level type, which must be the same as the name of the source file. For example,

```java
// in Monad.java
public class Monad {
    // code
}
```

You can include package-private types in the same file as a public type, but this is generally discouraged.

A type within a package can be referenced by `packageName.typeName` in other source code files.

By convention, all packages use lower case letters.

When you compile a source file, the compiler creates a different `.class` file for each type defined within the package (including non public types). For example, compiling

```java
interface Hello {}

class Apple {}
```

would create the `Hello.class` and `Apple.class` files.

Importing Other Classes

You can access any public class within your classpath using its fully qualified type name. For example, to use the `Rectangle` class in the `graphics` package, you would use

```java
graphics.Rectangle rect = new graphics.Rectangle();
```

To use the unqualified name of a class, you can import it first.

```java
import graphics.Rectangle;

Rectangle rect = new Rectangle();
```

You can use a wildcard `*` to import all classes within a package. However, this is discouraged. However, Java automatically imports all classes in the `java.lang` package and the classes of the current package, so you can use the unqualified names for these.

You can also use a `static` import to import the fields and methods of a class into the file, so you do not have to prefix them with the name of the class. For example,

```java
import static java.lang.Math.PI;

double tau = 2 * PI;
```

Again though, this is discouraged.

The `CLASSPATH` shell variable contains a list of directories to the `.class` files. All package names are resolved relative to it. For example, if my class path is `~/classes`, then

```java
import com.example.graphics;
```

would search for the `.class` files in `~/classes/com/example/graphics`. By default, the current directory and the Java system library are added to the classpath.

# Exceptions

An **exception** is an object that indicates some sort of error occured in the program. When a method wants to indicate an error occured, it creates an exception object and hands it off to the runtime system. This is called *throwing an exception*. Once an exception is throw, the runtime system searches through the call stack to find a method with an *exception handler* for that method. This handler *catches* the exception and then deals with it. If no exception handler is found, the method bubbles up to the top of the call stack and crashes your program.

Something looks generally like this.

```java
try {

} catch (ExceptionType varName) {

} finally {
    // always execute this code
}
```

# Concurrency

**Concurrency** is the ability of a program to do multiple things (called *tasks*) at once. **Parallelism** is a special type of concurrency, where each task is executed on a different processor. Parallelism leads to faster performance, concurrency in general does not. However, concurrency allows

A **thread** is the smallest unit of execution on a computer. Many threads are linked together to form a **process**, which roughly represents a single program. All the threads in a process share the same memory space, so they can access the same variables in a program (which often leads to a lot of trouble).

Every Java program starts on one thread, the *main thread*. This thread can then create other threads to run along-side it.

## Threads

Each thread is associated with an instance of the `Thread` class. To create a thread, you need to pass an object that implements the `Runnable` functional interface to a `Thread` constructor.

```java
@FunctionalInterface
interface Runnable {
    void run();
}
```

```java
final Thread thread = new Thread(() -> System.out.println("Hello from a thread!"));
thread.start();
```

To start a thread, simply call the `start` method.

The `Thread.sleep` method suspends execution of the current thread for a certain amount of time. This might be to free resources so other threads can run.

```java
final int[] arr = {1, 2, 3, 4};
for (final int i : arr) {
    Thread.sleep(4000);     // pause the main thread for 4 seconds
    System.out.println(i);
}
```

You can call the `join` method to pause the current thread while the execution of another thread finishes. This is very important, because it is possible for the main thread to finish before any of its spawned threads do.

```java
final Thread thread = new Thread(() -> computationIntensiveFunction());
thread.start();
thread.join();
```

## Interrupts

You can **interrupt** a thread to send an indication that it should do something else (usually terminate). You can send an iterrupt by invoking the `interrupt` method on the thread object to be interrupted. To respond to a possible interrupt call, you need to explicitly check inside the thread if it has been interrupted, and take the appropriate action. This *does not* stop or cancel the thread. It is merely a nice indicator of "hey, should should stop now", which the thread may ignore.

```java
for (final Data input : inputs) {
    // Check if the thread we are running on has been interrupted
    if (Thread.interrupted()) {
        // We've been interrupted: no more crunching.
        return;
    }
    heavyCrunch(input);
}
```

An


## Synchronization

Threads can communicate by accessing shared mutable variables. This is rather dangerous however, because it can lead to two problems: thread interference and memory consistency errors. These errors can be prevented using *synchronization*, but this can itself lead to *thread contention*.

*Thread interference* is when two threads try to mutate a shared variable, and the mutation instructions interleave with each other. This can lead to the shared variable having an unpredictable value.

You can make a method synchronized by adding the `synchronized` keyword to its declaration. This guarantees that any shared variable mutations that happen within the method don't interleave.

### Atomic Access

An *atomic* action is one that happens all at once. An atomic action cannot stop in the middle: it either happens completely, or it doesn't happen at all. No side effects of an atomic action are visible until the action is complete, so if the action fails it won't leave any "partially done" effects.

### Liveness

- deadlock
- starvation
- livelock

## Immutable Objects

All the issues with memory corruption and thread deadlock can be solved by one simple concept (which also solves a plethora of other programming issues): immutability. Immutable classes make your code far, far easier to work with. Here are some guidelines to making an immutable class

- Declare the class `final`
- Make all fields `private` and `final`
- Don't allow mutable instance fields to be changed. This includes providing no `setter` methods, and do not have references to external mutable objects.
- Use immutable classes, like from Guava

## High Level Concurrency

The `java.util.concurrent` package provides many high level concurrency abstractions that make writing concurrent code much easier.

### Locks

The `Lock` object in `java.util.concurrent.locks` can be used like so.

```java
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.Random;

final class Friend {
    public final String name;
    private final Lock lock = new ReentrantLock();

    public Friend(final String name) {
        this.name = name;
    }

}
```

## Executor

The `Executor` interface provides a single method `execute`, which is a drop in replacement for creating a thread. Instead of calling `new Thread(r).start()`, simply use `e.execute(r)`, where `e` is an `Executor` object. Note that the exact behaviour of `execute` will vary. It might run `r` on an existing thread, or place it in a queue of objects to be run when a thread becomes avaliable.

The `ExecutorService` interface supplements `execute` with a `submit` method, which can also accept `Callable` objects. This allows a task to return a value. The `submit` method returns a `Future`, which is used to retrieve the calculated return value.

`ScheduledExecutorService` has extra methods for scheduling when things get submitted.

A **thread pool** consists of several *worker threads*, which can be reused once the specified `Callable` object they are running is finished. This improves performance, because allocating new thread objects is rather memory intensive. Instead, preexisting threads can be reused.

## Fork/Join

The *fork/join* framework is a special `ExecutorService`. Like a standard one, it distributes tasks to a thread pool. However, it uses work-stealing, so any thread that runs out of things to do can steal tasks from other threads. This is particularily useful for divide-and-conquer algorithms.

This is implemented using the `ForkJoinPool` class.

Some fork/join methods are implemented in Java already, such as `parallelSort` and with streams.

## Concurrent Collections

- `BlockingQueue`
- `ConcurrentMap`
- `ConcurrentNavigableMap`
- `Vector`

### Wrappers


# Equals

The `equals` method inherited from `Object` is used to check if two objects are equal to each other. This is the equals you want to use in almost all situations. For reference types, the `==` operator checks if two references point to the same object. It is analagous to `is` in Python. The `==` operator checks for normal equality for primitive types, but not for reference types. So be careful!

The default implementation of `equals` simply defaults to `==`: it checks if two objects point to the same thing. This is almost certainly not what you want when placing those objects into a set or map. So, for your own classes, always override `hashCode` and `equals` when those objects will be placed into a `Set` or `Map`.

When adding a custom class to a `Set` or using it as a key for a `Map`:
- Always make your class immutable. That is, once an object has been constructed, it should be impossible to mutate it. This ensures the behaviour of `equals` and `hashCode` is consistent.
- Always override `equals` and `hashCode` (by default these will check if two objects are the same, which is likely not what you want to do).

# Collections

The Java collections are related through a hierarchy of interfaces. There are two hierarchies:

- Collection
  - List
  - Queue
  - Deque
  - Set
    - SortedSet
- Map
  - SortedMap

All the collections are generic with one parametric type. Here is a quick overview

- List
  - ArrayList:
  - LinkedList:
  - CopyOnWriteArrayList:
- Set
  - HashSet:
  - TreeSet:
  - LinkedHashSet:
  - EnumSet:
  - CopyOnWriteArraySet:
- Map
  - HashMap:
  - TreeMap:
  - LinkedHashMap
  - EnumMap
  - WeakHashMap
  - IdentityHashMap
- Queue
  - PriorityQueue
- Deque
  - ArrayDeque


