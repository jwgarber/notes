# Introduction

JavaFX is a library for creating GUIs. It is the successor to Swing, and available on all platfroms with Java 8.

All JavaFX stuff is located within the `javafx` package. A basic JavaFX program looks like this:

```java
import javafx.application.Application;
import javafx.stage.Stage;

// must be public
public class ProgramName extends Application {

    public static void main(final String[] args) {
        launch(args);
    }

    @Override
    public void start(final Stage primaryStage) {
        // code here
        primaryStage.setTitle("My awesome program");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
```

All programs extend from the `Application` class, which is responsible for the lifecycle of a JavaFX application. It consists of the following steps:

- Call the `launch` method, which is a static method of the `Application` class. This doesn't return until the application has closed. This method begins the lifecycle. The `args` argument do nothing by default, but you can access them in other functions later on.
- Create an instance of the `Application` class
- Call the `init` method. By default this does nothing, but you can override it to perform processing before the user interface is displayed.
- Call the `start` method. The `start` method is an abstract method, which means there is no default implementation within `Application`. You must provide your own implementation, which builds and displays the user interface.
- Wait for the program to end (when the user clicks the exit button).
- Call the `stop` method. Like `init`, this does nothing by default, but you can override it to do cleanup.

A `Scene` is a content pane that can hold `Node` objects. The `Scene` represents the content inside the window. To create a scene,

# Main JavaFX Packages

- `javafx.application`
- `javafx.stage` This contains classe for basic windows
- `javafx.scene`
- `javafx.scene.control`
- `javafx.scene.layout`
- `javafx.geometry`
- `javafx.collections`

# Node

A `Node` represents an object that can be added to a JavaFX scene. Thus, all objects that you will add to a scene inherit from `Node`, including controls, layout panes, and shapes. The `Node` class is contain in `javafx.scene`.

As we have seen, nodes can be nested within other nodes. Thus, all nodes within a particular scene form a tree structure called a *scene graph*. The root of the scene graph is used when creating a scene. A sample scene graph might look like

- BorderPane
  - HBox
    - Text
  - VBox
    - RadioButton

The `Node` class is abstract, so you can't directly instantiate it. The `Node` class is responsible for event handling.

`Canvas` inherits from the `Node` class, and looks interesting for what I want to do. Or perhaps not.

# Parent

The `Parent` class subclasses `Node`, and represents nodes that can have children. For example, all layout panes are also instances of `Parent`. The children of a `Parent` object can be accessed using the `getChildren` method, which returns an `ObservableList` that you can use to manipulate the children of the parent.

```java
HBox hbox = new HBox();
hbox.getChildren().addAll(lblAddress, txtAddress);
```

# Group

A `Group` is like a `Pane`, except it doesn't provide any layout for its child nodes.

```java
Group group = new Group(node1, node2, node3);
```

# Region

The `Region` class defines a visible area of a scene that has a physical size (a height and a width). The size of the region depends on many things, and you can set some guidlines, but JavaFX will determine it for you based on the contents of the region.

```java
lbl.setMinWidth(100);
lbl.setPrefWidth(200);
lbl.setMaxWidth(300);
```

If you don't specify the min, max, and preferred values for the dimensions, JavaFX will compute them for you. If you want an exact value for a region's width or height, set all three values to the same number.

You can also set the padding of a region using the `setPadding` method.

# Control

The `Control` class represents a control. It inherits from `Region`, `Parent`, and `Node`. You can add a tool tip to a control, which is a little window that pops up when you hover over it that explains what it does.

```java
btnSave.setTooltip(new Tooltip("Save the file"));
```

# Stage

The `Stage` represents the window in which the scene is displayed.

```java
primaryStage.setTitle("A nice title");  // title for the window
primaryStage.setScene(scene); // sets the scene
primaryStage.show();    // displays the window, this should be the last command
```

A stage has many methods that control the size and appearance of the window.

## Quitting the Stage

The proper way to close a stage is to call the `stage.close()` method. However, this can be overriden by clicking the exit button on the window, which closes the application immediately without doing any cleanup work.

You can add a Close button like so:

```java
Button btnClose = new Button("Close");
btnClose.setOnAction(event -> primaryStage.close());
```

You can wrap this into a function if there is cleanup work to be done.

```java
btnClose.setOnAction(event -> closeStage(primaryStage));

void closeStage(Stage stage) {
    // perform cleanup tasks, like closing files
    stage.close();
}
```

When the user clicks the close button on the window, a `CloseRequest` event is generated by the stage. You can provide an event handler for this event using the `setOnCloseRequest` of the stage.

```java
primaryStage.setOnCloseRequest(event -> closeStage(primaryStage));
```

This is good, but JavaFX will close the stage once the Close Request handler has finished (if the handler didn't close it already). To prevent this, call the `consume` method of the event, which stops it within the handler.

```java
btnClose.setOnAction(event -> {
    event.consume();
    closeStage(primaryStage);
    });
```

# Scene

A `Scene` contains the root node of a scene graph. A `Scene` can be created as follows

```java
Scene scene = new Scene(pane, 300, 250);
```

The last two arguments are the width and height of the screen in pixels. These can be omitted, in which case the size of the screen is determined automatically.

A stage can only display one scene at a time, but you can switch between different scenes to display.

# Buttons

The `javafx.scene.control.Button` class is used to create a button.

# Labels

These display text. The can be created like this

```java
Label label = new Label("This is the text to display");
```

Intended for labelling controls.

# Text

Similar to label, except

# Text Fields

A `TextField` is a box where the user can type a single line of text.

```java
TextField text1 = new TextField("Initial value");
String value = text1.getText();
text1.setText("Got the info");

// width is in pixels
text1.setMinWidth(150);
text1.setMaxWidth(250);
text1.setPrefWidth(200);

// preferred widith in characters
text1.setPrefColumnCount(50);
```

Ultimately, JavaFX will determine the size of the text field, but you can give it some pointers.

Putting a label beside a text field is very common.

```java
Label lblName = new Label("Name:");
lblName.setMinWidth(75);
TextField txtName = new TextField();
txtName.setMinWidth(200);
HBox pane = new HBox(10, lblname, txtName);
```

You can also display a prompt inside a text field that disappears when the user enters text.

```java
TextField txtName = new TextField();
txtName.setPromptText("Search");
```

# Check Boxes

The `CheckBox` class is used to create check boxes.

```java
// all check boxes are initially unchecked
CheckBox chkPepperoni = new CheckBox("Pepperoni");
CheckBox chkMushrooms = new CheckBox("Mushrooms");
CheckBox chkAnchovies = new CheckBox("Anchovies");

HBox hbox = new HBox(chkPepproni, chkMushrooms, chkAnchovies);
```

```java
public void btnOKHandler() {
    String msg = "";

    if (chkPepperoni.isSelected()) {
        msg += "Pepperoni\n";
    }
    if (chkMushrooms.isSelected()) {
        msg += "Mushrooms\n";
    }
    if (chkAnchovies.isSelected()) {
        msg += "Anchovies\n";
    }
    if (msg.equals("")) {
        msg = "You didn't order any toppings.";
    }
    else {
        msg = "You ordered these toppings:\n"
                + msg;
    }

    System.out.println(msg);
    chkPepperoni.setSelected(false);
    chkMushrooms.setSelected(false);
    chkAnchovies.setSelected(false);
}
```

You can also add event listeners to each check box, but this isn't usually done unless you need to provide immediate feedback to the user.

# Radio Buttons

Radio buttons are similar to check boxes, but they travel in groups, and only one button in a group can be selected at any time.

To use radio buttons, you first create them with the `RadioButton` class. Then you create a `ToggleGroup` class and add the radio buttons to the toggle group. Note that the `ToggleGroup` is only a way of grouping buttons together; it is not a control and is not added to the layout pane.

```java
RadioButton rdoSmall = new RadioButton("Small");
RadioButton rdoMedium = new RadioButton("Medium");
RadioButton rdoLarge = new RadioButton("Large");

ToggleGroup sizeGroup = new ToggleGroup();
rdoSmall.setToggleGroup(sizeGroup);
rdoMedium.setToggleGroup(sizeGroup);
rdoLarge.setToggleGroup(sizeGroup);
```

# Dialog Boxes

JavaFX 8u40 added simple dialog boxes to Java. Previously, you had to roll your own. Here are some examples

# Events and Event Handlers

An *event* is an object generated when the user interacts with the program An *event handler* is a piece of code that responds to the event, liking changing the text when someone clicks a button. All events are subclasses of `javafx.event.Event`. The most common include `ActionEvent`, `InputEvent`, `KeyEvent`, and `MouseEvent`.

An event handler must implement the `EventHandler` interface, which looks like this:

```java
interface EventHandler<T extends Event> {
    void handle(T event);
}
```

As you can see, this only contains one method, so you can pass lambda functions instead of an anonymous class.

All event sources have methods that allow you to specify the event handler, like the `setOnAction` method for buttons. For this method you pass an object that implements the `EventHandler` interface, and thus contains the `handle` method.

There are at least four ways of working with event handlers. The best is just to use lambda functions and method references. With these, your basic lambda function looks like:

```java
event -> stuff
```

Beautiful. If your lambda function is rather long, simply wrap that in another function, and just call that function within the lambda function. Awesome.

# Layout Panes

The *layout panes* in `javafx.scene.layout` determine the organization of user interface elements. There are eight types of layout panes.

To use a layout pane, you create an instance of one. Then, you add the user interface elements, providing details about how they are to be displayed. The exact spacing and arrangement of the elements is automatically determined for you.

```java
BorderPane pane = new BorderPane();
pane.setCenter(btn);
```

Each region in a pane contains a Node. Layout panes are themselves Nodes, so you can nest layout panes within each other.

- `HBox`: arranges nodes horizontally in a row
- `VBox`: arranges nodes vertically in a row
- `FlowPane`: arranges nodes horizontally or vertically, and then wraps around when it runs out of room
- `BorderPane`: Divides region into five regions: Top, Left, Center, Right, and Bottom

## Creating Layout Panes

- Create the nodes you want to add to the pane.

```java
Button btnOK = new Button("OK");
btnOK.setOnAction(event -> btnOKHandler());
Button btnCancel = new Button("Cancel");
btnCancel.setOnAction(event -> btnCancelHandler());
```

- Create the layout pane by calling its constructor.

```java
HBox pane = new HBox();
```

- Fine tune any settings of the pane.

```java
HBox.setSpacing(10);
```

- Add the nodes to the pane.

```java
pane.getChildren().addAll(btnOK, btnCancel);
```

- Create the scene by adding the pane as the root.

```java
Scene scene = new Scene(pane);
```

Recall that layout panes are also nodes, so you can nest panes within each other. Very useful.

## HBox

An `HBox` arranges Nodes horizontally in a row. The easiest way to create one is to first create the nodes you want in the `HBox`, and then pass them as arguments to the constructor.

```java
Button btn1 = new Button("Button One");
Button btn2 = new Button("Button Two");
HBox hbox = new HBox(btn1, btn2);
```

## VBox

A VBox is similar to an HBox, but it arranges nodes vertically in a column instead of horizontally in a row.

```java
Button btn1 = new Button("One");
Button btn2 = new Button("Two");
VBox vbox = now VBox(btn1, btn2);
```

## FlowPane

The `FlowPane` layout arranges nodes in rows or columns, and then wraps the nodes around when they get to specified limit.

```java
FlowPane pane = new FlowPane(Orientation.HORIZONTAL, 10, 10, btn1, btn2, btn3);
pane.setPrefWrapLength(300);
```

## BorderPane

The `BorderPane` is separated into five regions: top, left, center, right, and bottom. Each of these regions contains one node. Since each layout pane is also a node, you can nest other panes within the border pane. It is very common to first create a `BorderPane` as the top level pane, and then place other panes and nodes within it.

```java
VBox vbox = new VBox(btn1, btn2, btn3);

BorderPane pane = new BorderPane();
pane.setCenter(vbox);
```

- If you don't add a node to a region, that region is not rendered
- The border regions are sized according to their content
- If the user resizes the window along a dimension, the center region and the two side regions along that dimension are stretched. The other two regions are not.

## StackPane

## TilePane

## GridPane

This is a more powerful version of `VBox` and `HBox`. Instead of nesting those to create layouts, you can just use a `GridPane`.

## ScrollPane

You can place a layout pane within a `ScrollPane` to provide scroll bars (and panning if desired). For example,

```java
ScrollPane spane = new ScrollPane(tile1);
```


## Aligning Nodes in a Pane

The VBox and HBox layout panes have a `setAlignment` method that lets you control how nodes are aligned within the pane. Pass a constant in the `Pos` enumeration to control the alignment. For example,

```java
vbox.setAlignment(Pos.CENTERED);
```

The `Pos` class is contained in `javafx.geometry`.

## Same Width

You can set several nodes to all have the same width as follows. This is especially useful for buttons.

```java
// A button
Button btn1 = new Button("Number One");
Button btn2 = new Button("Two");
Button btn3 = new Button("The Third Button");

// Set the max width as big as possible
btn1.setMaxWidth(Double.MAX_VALUE);
btn2.setMaxWidth(Double.MAX_VALUE);
btn3.setMaxWidth(Double.MAX_VALUE);

// Now create a VBox
VBox vbox = new VBox(10, btn1, btn2, btn3);
vbox.setPadding(new Insets(10));
vbox.setAlignment(Pos.CENTERED);
```

## Spacing

By default, child nodes in a layout pane are arranged immediately next to each other, with no space in between. If you want to add space between the nodes in a pane, you can do so in four ways. Note that all spacing is done in terms of pixels. That is the only measurement that matters within computer graphics.

### Adding spacing between elements within the pane

The default spacing between elements of a pane can be set when calling the constructor of the pane, or by using the `setSpacing` method.

```java
HBox hbox = new HBox(10);   // 10 pixels
hbox.setSpacing(20);
```

Spacing adds space between the nodes of a pane, but it doesn't add space between the nodes and the edges of the pane itself.To add space around the perimeter of the pane, use the `setPadding` method. This method takes an `Insets` object, which stores the padding in pixels inside the perimeter of the pane.

```java
hbox.setPadding(new Insets(10));    // 10 pixels all around
hbox.setPadding(new Insets(20, 10, 20, 10));    // specific numbers for each side
```

The `Insets` enumeration is contained within the `javafx.geometry` package.

## Margins

Another way to add space is to create margins around the individual nodes. To do this, call the `setMargin` method with the node you want to add a margin to and an `Insets` object that defines the margins.

```java
HBox.setMargin(btn1, new Insets(10));
HBox.setMargin(btn2, new Insets(10, 15, 20, 10));
```

Oddly enough, `setMargin` is a static method of the layout pane class, not an instance method of a node.

## Spacers

You can add a spacer node between nodes of a layout pane, and configure this node to grow when the user stretches the screen. The easiest way to do this is to use a `Region` object, which is the base class for `Control` and `Pane`. Configure the `Region` object using the static `setHgrow` method and an element of the `Priority` enumeration, which is defined in `javafx.scene.layout`.

- `Priority.NEVER` The width of the node is not resized to fill available space. This is the default for all nodes in a pane.
- `Priority.ALWAYS` The width of the node is always resized to fill available space.
- `Priority.SOMETIMES` The width of the node is resized if there are not other nodes with `Priority.ALWAYS`.

```java
// Create the buttons
Button btn1 = new Button("One");
Button btn2 = new Button("Two");
Button btn3 = new Button("Three");

// Set the margins
HBox.setMargin(btn1, new Insets(10));
HBox.setMargin(btn2, new Insets(10));
HBox.setMargin(btn3, new Insets(10));

// Create the spacer
Regieo spacer = new Region();

// Set the Hgrow for the spacer
HBox.setHgrow(spacer, Priority.ALWAYS);

// Create the HBox layout pane
HBox hbox = new HBox(10, btn1, btn2, spacer, btn3);
```


# Transformations

By default, the top left corner of a window has the coordinate (0, 0), so all absolute coordinates are measured relative to that. However, you can translate the origin for each node individually using the `setTranslateX` and `setTranslateY` methods.

```java
group.setTranslateX(100);
group.setTranslateY(100);
```

This moves the origin for the `group` node and all its children. You can also scale a group.

```java
group.setScaleX(2);
group.setScaleY(2);
```

Note that all changes you make to a node are inherited by each child node. Thus, applying these transformations to the root node applies it to the whole scene.

You can also use the transformation class too.

# Shapes

The `Shape` class represents geometric shapes. You often add shapes to a group.

## Lines

A line is an instance of `javafx.scene.shape.Line`. It has the following constructor:

```java
Line(double startX, double startY, double endX, double endY)
// example
Line line = new Line(100, 10, 10, 110);
```

As usual, this has getters and setters for all the coordinates. `Line` nodes have a default stroke width of 1.0, and a default color of `Color.BLACK`.

## Rectangles


## Path

The `javafx.scene.shape.Path` class is used to create paths of various lines.

# Sliders

# Properties

A **property** is an attribute of a class that you can link together with other properties. All properties must have three methods associated with them. For example, if your class has the `firstName` property, it must also have the methods

- `getFirstName`
- `setFirstName` (if the property is not read-only)
- `firstNameProperty`

Almost every JavaFX class has many properties. For example, the  `TextField` class has a `text` property, with corresponding `getText`, `setText`, and `textProperty` methods.

## Creating a Property

There are two types of properties: read-only and read-write. Each property encapsulates a base class, and JavaFX provides wrappers for ten different classes.

Here is an implementation of a read-write string property.

```java
public class Customer {

    // this is the object that has the property
    // firstName is the name of the property
    // "" is the default value
    private final StringProperty firstName = new SimpleStringProperty(this, "firstName", "");

    // getter method
    // what boilerplate
    public final String getFirstName() {
        return this.firstName.get();
    }

    // setter
    public final void setFirstName(String value) {
        this.firstName.set(value);
    }

    // accessor
    public final StringProperty firstNameProperty() {
        return this.firstName;
    }

}
```

You can also implement a read-only property, but that has an incredible amount of boilerplate that I don't want to think about. In fact, I think implementing your own properties is a waste of time.

## Listeners

JavaFX properties have an `addListener` method that allows you to add event handlers that are called whenever the value of a property changes. There are two types of property event handlers:

- A **change listener** is called whenever the value of a property has been changed The change listener is passed three arguments: the property that has changed, the old value, and the new value.
- An **invalidation listener** is called whenever the value of the property becomes invalid, but has not been recalculated yet. This listener is only passed the property object.

These two listeners are defined by the functional interfaces `ChangeListener` and `InvalidationListener`. Because these are functional interfaces, you can pass lambda expressions, and the proper type of listener will be inferred from the signature.

Here is an example of adding a listener to the `text` property of a `TextField` named `textField`.

```java
textField.textProperty().addListener((textProperty, oldValue, newValue) -> some stuff); // change listener
textField.textProperty().addListener(textProperty -> some stuff); // invalidation listener
```

Here is another example:

```java
Rectangle rect = new Rectangle(100, 100);

StackPane pane = new StackPane(rect);

pane.widthProperty().addListener((observable, oldValue, newValue) ->
                                    rect.setWidth(newValue * 0.5));

pane.heightProperty().addListener((observable, oldValue, nowValue) ->
                                    rect.setHeight(newValue * 0.5));
```

Pretty neat, eh?

## Binding

In some cases, you might want to make one property always have the same value as another. You can implement this using listeners, but there is an easier way using **bindings**. Property bindings synchronize the values of two properties, so that whenever one changes the other is automatically updated. There are two types of property bindings.

- **Unidirectional bindings**, which just work in one direction (woot!). When you bind A to B, the value of A is changed when B changes, but not vice versa.
- **Bidirectional bindings**, which work in both directions. If either property changes, the other is changed as well.

You can use the `bind` and `bindBidirectional` methods of a property to implement the above bindings. For example,

```java
// The text of the label is bound to the text of the text field
label1.textProperty().bind(text1.textProperty());
// The text of both fields are bound to each other
text1.textProperty().bindBidirectional(text2.textProperty());
```

# Working With Pixels

You can use a `WritableImage` to write pixels directly to the screen. Maybe use a canvas too?

The z order of nodes is determined by their order in the children list. Things at the front are at the bottom, and things at the end are on the top.

# Concurrency

By default, the nodes in a JavaFX UI are not thread-safe; as such, modifying those nodes from multiple threads can introduce data races and illegal state. To prevent this, JavaFX enforces that objects in the scene graph can only be modified by the JavaFX Application Thread. Attempts to modify the UI from any other thread will result in an IllegalStateException.

This introduces a problem. Running long tasks on the Application Thread will prevent it from processing UI events and updates, causing the program to become unresponsive. To fix this, long tasks need to be run on separate threads. But, these threads cannot modify nodes in the scene graph, so how do we update it on our progress? One way is with `Platform.runLater`. This function takes a `Runnable`, which you can pass some work to do and it will run it on the Application Thread.

# javafx.concurrent

The `javafx.concurrent` package builds on the `java.util.concurrent` package to provide concurrency features for JavaFX. The package consists of these things:

- The `Worker` interface represents a task that needs to be performed on one or more background threads. The state of the task can be safely observed from the Application Thread.
- The `Task` abstract class represents a one-shot task. It cannot be reused.
- The `Service` abstract class represents a reusable task.
- The `ScheduledService` abstract crass represents a reusable task that can be restarted automatically after a certain time period.
- The `Worker.State` enum represents different states of a `Workr`.
- The `WorkerStateEvent` class represents an event that occurs as the state of a `Worker` changes. You can add event handlers to all `Worker` tasks to listen to changes in their state.

Both `Task` and `Service` implement the `Worker` interface.

# Worker

A `Worker<V>` is a task that is performed on a background thread. The paramter `V` represents the return type of the worker. The state of a `Worker` is observable from the JavaFX Application Thread,

## State Transitions for a Worker

A `Worker` can transition through different states, which are represented in the `Worker.State` enum.

When a `Worker` is created, it is in the `READY` state. It transitions to the `SCHEDULED` state before it starts executing, and is in the `RUNNING` state when it starts to run. If it completes successfully, it transitions to the `SUCCEDDED` state. If a `Worker` throws an exception during its execution, it transitions to the `FAILED` state. A `Worker` can be cancelled using the `cancel()` method, at which point it is in the `CANCELLED` state. A reusable `Worker` can then transition from one of these states to the `READY` state when it is restarted.

## Properties of a Worker

The `Worker` interface contains nine read-only properties that represent the internal state of the task. You can specify these properties when you create the `Worker`, and they will be updated as the task progresses.

- The `title` property represents the title of the task.
- The `message` property represents a string message that you can set as the worker progresses.
- The `running` proprety tells whether the `Worker` is running. It is true in the `SCHEDULED` or `RUNNING` states, and falses otherwise.
- The `state` property is one of the constants in the `Worker.State` enum.
- The `totalWork`, `workDone`, and `progress` properties represent the progress of the task. You can `totalWork` and `workDone` as the worker progresses, and `progress` is the ratio of `workDone` to `totalWork`.
- The `value` property represents the result of the task. Its value is non-null when the `Worker` reaches the `SUCCEEDED` state. For tasks that do not produce a result, the generic parameter `V` will be `Void`, and the `value` property will always be `null`.
- If a worker fails by throwing an exception, the `exception` property represents the exception that was throw. It is non-null only when the state of the worker is `FAILED`.

The properties of a `Worker` are updated on the JavaFX Application Thread, so you can bind properties of UI elements to these properties. You can also add `Invalidation` and `ChangeListener` to these properties.

If you are using concurrency for performance reasons, then use the basic stuff in java.util.concurrent. The ones in JavaFX have lots of extra stuff for interacting with the GUI.

# The Task Class

An instance of the `Task<V>` class represents a one-time task. Once the task is completed, cancelled, or failed, it cannot be restarted. The `Task` class inherets from `FutureTask`, which implements the `Future` interface.

Since `Task` is an abstract class, you cannot override the `call` method using a lambda. You have to explicitly subclass `Task` and provide the implementation there. The only method you explicitly need to override is `call()`, which does the computation and returns the result.

```java
public final PrimeFinderTask extends Task<ObservableList<Long>> {
    @Override
    protected ObservableList<Long>> call() {
        // implement the logic here
    }
}
```

## Updating Task Properties

Typically, you want to update a task as it is running. You can do this using the `updateProperty` methods within the task. All of these methods are executed on the JavaFX Application Thread, but can be called safely from within the `call()` function of a task.

## Listening to Task Transition Events

You can set event handlers to listen to state transitions of a task. They are of the form `setOnState`, where `State` is one of the values in `Worker.State`. These are implemented using properties.

```java
final Task<ObservableList<Long>> task = ...;
task.setOnSucceeded(event -> System.out.println("Success!"));
```

## Cancelling a Task

You can cancel a task using the `cancel()` or `cancel(boolean)` methods. The first removes the task from the execution queue or stops its execution. The second lets you specify whether the thread running the task is to be interrupted. Make sure you handle the `InterruptedException` inside the `call()` method, so you can finish the method quickly.

The following methods of the `Task` are called when it reaches a specified state.

- `scheduled()`
- `running()`
- `succeeded()`
- `cancelled()`
- `failed()`

You override these methods within the subclasses.

## Running a Task

You can use a background thread or `ExecutorService` to run a task.

```java
final Thread thread = new Thread(task);
thread.setDeamon(true);
thread.start();

executor.submit(task);
```
