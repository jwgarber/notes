# Git

Git is a distributed version control system, similar to Mercurial. It is more complicated than Mercurial above and under the hood, but is also faster. Git is the C++ of VCS. It's really complicated, but everyone uses it and there aren't many alternatives.

# Config

Git stores configuration information in three places:

- `/etc/gitconfig`: system configuration for every user. Pass the `--system` flag to modify this.
- `~/.config/git/config`: user configuration. Pass the `--global` flag to modify this.
- `./.git/config`: configuration for a specific repository. `git config` will modify this information by default (so, for example you can set different usernames or email addresses for different projects), but you can also pass the `--local` flag.

Use `git config` to set configuration information.

```fish
git config --global user.name "My Name"
git config --global user.email "awesome@email.com"
git config --global core.editor nvim
git config --list           # list all current information
git config user.name        # show the username
```

# Help

To get help on any Git command, just run `git help <command>`.

# Init

To create a repository within a directory, use `git init`.

```fish
git init
```

This creates a `.git` directory that contains the repository information.

# Clone

You can use `git clone` to create a complete copy of another git repository. You can use it on directories or urls.

```fish
git clone documents/programming newprogramming
git clone https://github.com/libgit2/libgit2
```

## Status

Use `git status` to check the status of the files in the repository. It will spit out information about what is tracked, modified, missing, etc.

## Add

To add a file to the repository, use `git add`. From now on, that file will be under source control.

```fish
git add test.txt
git add --all   # add all files
```

# Diff

You can use `git diff` to view the changes you've made to your repository. By default, `git diff` shows the changes that are not yet staged. To show the staged changes, use `git diff --staged`.

# Moving and Removing

You can use `git mv` to rename files, and `git rm` to remove them.

# Undoing Things

## Amending

Suppose you have just made a commit, but then realize you've made a mistake. Perhaps there was a file you forgot to add, or messed up a few lines, or whatever. There is a way of overwriting the last commit to fix this. For the next commit, modify the stage to the state you wanted it to be in on the previous commit, and then run `git commit --amend`. This will drop you into the editor with the message of the last commit, which you can edit as necessary. (Note that this is also a handy way of fixing a commit message. Simple amend without changing anything.)

## Unstaging

Use `git reset HEAD $file` to unstage.

## Discarding changes

Use `git checkout -- $file` to discard the changes to a file.

# Remotes

*Remotes* are copies of your repository stored somewhere else, usually on a server, but also perhaps on your own computer.

To view all configured remotes, use the `git remote` command. You can pass the `-v` option to see the URLs associated with each remote. Note that cloning a repository will be default add the `origin` remote to the repository you cloned from.

To add a remote, use the `git remote add <name> <url>`. For example,

```
git remote add https://...
```

All remotes that you add must somehow be related to your own repository.

You can now fetch and push to remotes

```
git fetch upstream
```

This will fetch all new changes from the `upstream` remote, and you can access a remote branch under the name `<remote>/<branch>`.

You can push to a remote using `git push <remote> <branch>`. This will push your local branch to the corresponding branch on the remote.

```
git remote show origin
git remote rename oldname newname
git remote rm name
```

# Tagging

To view your tags, use

```
git tag     # view tag
git tag -a v1.4     # create annotated tag
git tag v1.4        # create lightweight tag
git show v1.4       # show tag
git push origin tagname
git push origin --tags      # push all tags
```

A lightweight tag is like a branch; it is just a pointer to a commit. An annotated tag has much more information, including a message, recording the name of the committing, checksums, etc. It is generally recommended to create annotated tags. By default, a tag will be created pointing to the current HEAD.

```
git checkout tagname        # detached head state generall bad
git checkout -b branchname tagname  # create a new branch for the tag, say for editing
```

# Fetch

```
git fetch origin    # update references from remote
```

# Push

```
git push origin bname  # push a branch to the remote (not done explicitly)
```

# Pull

Do not use pull. Git pull --rebase.

# Merge

Use `git merge --no-ff`.

```
git checkout -b fix
make changes
git checkout master
git merge fix   # merge the fix back into master. Now the master HEAD points to the same commit. Use --no-ff
git branch -d fix   # now delete the branch pointer
```

If fast-forwarding is not possible, then Git will perform a three-way merge (both branch pointers and their common ancestor), and will create a new merge commit pointing to the merged branches. If there is a merge conflict, then you will need to resolve the conflicts, mark them as resolved, and then commit your changes as the merge.

# Rebase

Rebase will physically move a branch and its commits in the commit tree. Do not use rebase. Well, I could see the point of rebasing origin/master onto master, that would make sense, but it should not be done for separate branches. Uhhh, actually if there is a rebasing conflict (which is the important case), you will need to make a resolving commit anyway. Certainly if it just layers on top you shouldn't need a merge commit (for merging remote work on the same branch). Ehh, actually rebase might be Ok in some cases.

```
git rebase remote/master        # rebase master onto the remote master branch
```

Merging feature branches (even if it can be fast-forwarded), should always be done with a merge commit, so we can see exactly what happened in history. That is much better and less confusing.

Rebasing is problematic, because when you are creating new commits none of the precommit checks are run.

git rebase interactive is very powerful. If you are working on a branch that you want to get merged, I do think it might be good to rebase as you go to clean things up a bit. That is good.

# Branching

In Git, branches are implemented as pointers to commits. The default branch is called `master`.

```
git branch testing      # create new branch to current commit
git checkout testing    # move HEAD to the testing branch
git checkout -b testing # create and switch to branch
```

There is a special pointer called `HEAD` that points to the current branch you're working on. It is important to note that HEAD will only point to branches. Unlike Mercurial, you cannot update to an old commit and start hacking on an anonymous branch. That's a big no-no, because it puts you in detached HEAD state. You always need to create a new branch (sigh).

By default, any changes you make on the testing branch will layer on top of the `master` branch in the linear fashion. However, if you checkout back to the master branch and start making commits, then the history will diverge at this point.

```
git branch      # list all branches, * means HEAD
git branch -v   # list branches with last commit message
git branch --merged     # list branches merged into current branch, these are safe to delete
git branch --no-merged  # list branches not merged into current branch
```

# Remote References

Generally speaking, a *reference* is a pointer to a commit (such as a tag, branch, etc.). A *remote reference* is a reference on a remote. A *remote-tracking branch* is a branch that points to a branch on a remote. They are of the form `<remote>/<branch>`. These branches are maintained by Git, and are automagically updated whenever you fetch or full from the server.

# Workflow

Use GitHub flow. It is very simple, and makes a lot of sense. The `master` branch is the

```fish
git checkout -b "new-feature"   # create and go to the feature branch
# make changes and commits
git branch master               # switch to master
git merge --no-ff ...           # merge the branches
git branch -d "new-feature"     # delete the pointer
```

https://www.atlassian.com/git/tutorials/merging-vs-rebasing
https://github.com/mockito/mockito/wiki/Using-git-to-prepare-your-PR-to-have-a-clean-history
https://disjoint.ca/til/2017/10/04/signing-your-git-commits-using-your-gpg-key/

# Encryption and Signing

sign
- commits
- merges
- tags
- pushes

git config commit.gpgsign true

https://mikegerwitz.com/papers/git-horror-story
