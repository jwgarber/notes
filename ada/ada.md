Learning some Ada. Ada is targeted at low level and realtime systems. The entire language was designed for the purpose of high-reliability software. As such, while being a general purpose language, it isn't really used in places other than these original ones.

Comments are done with `--`.

# Modules

Use `with` to import a module. You can `use` to release the package name.

```ada
with Ada.Text_IO; use Ada.Text_IO;
```

# Procedures

A procedure is a function that does not return a value. It can be declared as follows:

```ada
procedure MyProcedure is
begin
    ...
end MyProcedure;
```

# If

```ada
if condition then
    ...
elsif condition then
    ...
else
    ...
end if;
```

You can also use *if expressions*. They are look like this:

```ada
S : String := (if N > 0 then "is positive" else "is not");
Put_Line (if I mod 2 = 0 then "Even" else "odd");
```

# Loops

## For Loops

```ada
for I in 1 .. 5 loop
    Put_Line ("Hello World!" & Integer'Image (I));
end loop;
```

A `for` loop increments over a range. 

The `..` syntax creates an inclusive range. You can use the `reverse` keyword to reverse a range.

```ada
for I in reverse 1 .. 5 loop
    Put_Line ("Hello World!" & Integer'Image (I));
end loop;
```

Note that if the upper bound is less than the lower bound, the loop does not execute at all (even if you use `reverse`).

## While Loops

A `while` loop is done as usual.

```ada
   while I <= 5 loop
      Put_Line ("Hello, World!" & Integer'Image (I));
      I := I + 1;
   end loop;
```

## Bare Loops

```ada
procedure Greet is
   I : Integer := 1; -- Variable declaration
begin
   loop
      Put_Line ("Hello, World!" & Integer'Image (I));
      exit when I = 5; --  Exit statement
      --        ^ Boolean condition
      I := I + 1;
   end loop;
end Greet;
```

This is an infinite loop, and we can break out of it using `exit`.

# Case

A `case` statement is basically like a souped up `switch`.

```ada
case expression is
    when condition =>
        ...
    when condition =>
        ....
    when others =>
        ...
end case;
```

For example,

```ada
case N is
    when 0 | 360 =>
        Put_Line (" is due east");
    when 1 .. 89 | 90 =>
        Put_Line ("is in northeast");
    when others =>
        Put_line ("this is for everything else");
end case;
```

Note that every possible of the case expression must be covered by a unique branch of the case statement. Also, once the appropriate branch has been executed, the program continues to the `end case` statement - there is no fallthrough.

There are also *case expressions*. For example,

```ada
Put_Line (case I is 
          when 1 | 3 | 5 | 7 | 9 => "Odd",
          when 2 | 4 | 6 | 8 | 10 => "Even");
```

# Declarative Regions

Declarations can only appear in special *declarative regions* - in these spots you can declare variables, constants, types, inner subprograms, and other things. One declarative region is between the `is` and `begin` statements in a subprogram. For example,

```ada
procedure Main is
    procedure Nested is
    begin
        Put_Line ("Hello World");
    end Nested;
begin
    Nested;     -- call Nested
end Main;
```

If you need to declare new things within the statements, you can do so with a `declare` block:

```ada
procedure Greet is
begin
    loop
        Put_Line ("Enter your name: ");

        declare
            Name : String := Get_Line;  -- funcion call
        begin
            exit when Name = "";
            Put_Line ("Hi " & Name & "!");
        end;

    end loop

    Put_Line ("Bye!");
end Greet;
```

# Types


