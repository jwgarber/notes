# Introduction

OpenCL is a cross-platform API for GPU programminging. It is similar to CUDA, but more general, is it must work across over a variety of devices.

An OpenCL program consists of kernels that execute on devices, and a host program that manages the kernels.

When a kernel is launched, the code is run by *work items*, which correspond to CUDA threads. Work items are organized into work groups (similar to blocks), which are then organized into an index space (like a grid).

Every work item has its own global index value within the index space, which can be accessed using `get_global_id()` function. The index within a particular dimension is indicated by passing an integer value. 0 means x, 1 means y, 2 means z, etc.

The index of a work item in its group can be accessed using `get_local_id()`, with the same argument as above. This is similar to the `blockIdx` variable in CUDA.

The `get_global_size()` function obtains the number of work items along a particular dimension.
The `get_local_size()` function obtains the number of work items in a work group.

An OpenCL system consists of a host, and one or more devices. Each device consists of several *compute units* (analogous to streaming multiprocessors). Each CU consists of many *processing elements* (which are similar to streaming processors).

OpenCL also has a memory hierarchy.

global - global
constant - constant
local - shared
private - local and registers

## Kernels

All OpenCL kernels must be prefixed by the `__kernel` keyword (which corresponds to `__global__` in CUDA). For example, here is a simple CUDA kernel that adds two vectors together.

```cl
__kernel void add(__global const float* a, __global const float* b, __global float* c, const size_t len) {

    size_t i = get_global_id(0);
    if (i < len) {
        c[i] = a[i] + b[i];
    }
}
```

Note that in this example, pointers to global memory are prefixed by the `__global` keyword.

## Device Management

OpenCL device management is more complicated than that of CUDA, because it needs to support many more hardware platforms.

OpenCL devices are managed through *contexts*. A context can be created using `clCreateContext()`. The details of using this API I won't get into, because it is very complicated.

Each device has a *command queue*, where commands are submitted for execution. A command queue can be created using `clCreateCommandQueue()`.

The arguments to a kernel can be set using `clSetKernelArg()`.

clCreateBuffer() - cudaMalloc()
clEnqueueReadBuffer() - cudaMemcpy() from device to host
clEnqueueWriteBuffer() - cudaMemcpy() from host to device
clReleaseMemObject() - decreases reference count for the object. Sorta-like cudaFree()

Each allocated buffer is associated with a context, not a specific device.
