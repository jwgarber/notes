application program <-> DBMS <-> database

A *schema* is a description of the structure of a database. Notably, SQL databases have a fixed schema. Most NoSQL databases, like MongoDB, do not.

external schema - the view of the database provided to application programs
conceptual schema - logical organization of the database
physical schema - physical organization of data on disk

Relational database. Data is organized into a bunch of tables. Each table is a set of rows (really a set of tuples). Eg.

sid | name | age
5366  Bub    15
8025  Jim    17
1805  Bill   18

schema = Students(sid: string, name: login, age: integer), contains name of relation, name and type of each column, and maybe integrity constraints

cardinality of table = number of rows
degree = number of columns

All rows are distinct, no duplicates. In real life SQL allows duplicate rows though.

## Sqlite Dot Commands

- `.tables`: list the tables in the database
- `.schema table`: show the schema for `table`
- `.read filename.sqlite`: Execute the commands in the given file.

# SQL

Sqlite uses dynamic typing. It does not enforce the type constraints on the attributes that other databases do.

```sqlite
```

## Alter Table

```sqlite
alter table movie
    add year integer;

alter table movie
    rename to reels;
```

To perform operations not supported by `alter table`, like renaming a column, rename your table to a temporary table, create a new table with the original name with the schema you want, copy into the new table from the old, and then delete the old.

## Update

```sqlite
update movie set
    year = 2002, runtime = 121
    where title = 'Spiderman';
```

```sqlite
update deposit
    set balance = 1.05 * balance
    where
```

## Select

Select things from the rows of a table.

```sqlite
select title, runtime
    from movie
    where year > 2005;
```

The syntax for `select` is a little funky. Conceptually, it works like this:

```sqlite
select c.cname, c.city
    from customer c, deposit d
    where c.cname = d.cname
```

What this does is create the cartesian product of customer and deposit. The c and d refer to a row (tuple) within each relation.

So we get the set {(c, d) | c in customer, d in deposit}. Then we take every tuple (c, d) and filter it through the predicate c.cname = d.dname, and then for the tuples that work, we select the fields c.cname and c.city.

The syntax order for this query is rather strange. It should rather be something like

```
# it's functional!
cross customer deposit |> filter (c, d) -> c.cname = d.dname |> map (c, d) -> (c.cname, c.city)
```

You can also return expressions in a select statement.

```sqlite
select b.bname || '--' || b.address
    from branch b;
```

The output from select is itself a table, and you can give an expression a column name in the new table.

```sqlite
select b.bname || '--' || b.address as name_add
    from branch b;
```

```sqlite
select c.city
    from customer c;
```

By default, the tuple of results returned from a select statement is a multiset. To instead get a set, use the `distinct` keyword.

```sqlite
select distinct c.city
    from customer c;
```

You can use `like` to do string pattern matching.

You can use `order by` to order the tuples of the select. The default is ascending, though you can add `desc` for descending.

```sqlite
select bname, assets
    from branch
    where assets > 250
    order by assets;
```

### Set Operations

Assume you have two select statements, both of which produce the same tuples (so they have the same number of columns and types for each column, even though the semantics of each field might be different). Then we can perform set operations on the result.

```sqlite
select cname from deposits
except/union/intersect
select cname from loan;
```

except is set difference.

```sqlite
-- Every city that has a branch but no customer
(select city from branch) except (select city from customer);
```

By default, set operations remove duplicates by default. You can use the `all` keyword to keep duplicates.

```sqlite
-- Find the names and assets of any branch that has deposit holders living in Jasper
-- Get the customers living in Jasper
-- Get the deposits for all those customers
-- Then get the branch name for that
select b.bname, b.assets
from branch b, customer c, deposit d
where c.city = 'Jasper' -- get the customers with city being Jasper
and d.cname = c.cname -- get the deposit for that customer
and d.bname = b.bname -- get the branch for that deposit
-- I think it would be simpler to use some sort of hierarchal data structure,
-- like a map, with the primary keys being the keys
```

### Subqueries

```sqlite
-- select every customer who has a deposit in some branch where John Doe has a
-- deposit
select cname
    from deposit
    where bname in (select bname from deposit where cname = 'John Doe');

-- This seems more obtuse than it needs to be
-- Once to iterate through to get the branches where John Doe
-- has a deposit, and then once again to get the customers who have
-- deposits at those branches
select d1.cname
    from deposit d1, deposit d2
    where d1.bname = d2.bname
        and d2.cname = 'John Doe';
```

branches = cross deposit
    | filter d -> d.cname = 'John Doe'
    | map d -> d.bname

cross deposit
    | filter d -> d.bname in branches
    | map d -> d.cname

`exists` evaluates to true iff the result of a subquery contains at least one row.

```sqlite
-- Find the names of customers who live in a city that has a bank branch
select cname
    from customer
    where exists(select 1 from branch where customer.city = branch.city);

-- This is equivalent to the above, but we need to add a distinct since
-- there could be multiple branches in the same city
select distinct customer.cname
    from customer, branch
    where customer.city = branch.city;
```

Correlated and Uncorrelated subquery.

```sqlite
-- uncorrelated
-- the subquery does not reference any variables in the outer query,
-- so it only needs to be executed once
select sname
    from sailors
    where sid in (select sid from reserves)

-- correlated
-- this subquery refers to each row in sailors, so it needs to repeat
-- the subquery for each iteration. Not as performant.
select sname
    from sailors s
    where exists (select 1 from reserves r where s.sid = r.sid)
```

Abstraction
- set operations
- in from, where, etc

- I think uncorrelated subqueries are ok

## Delete

Deletes rows from a table.

```sqlite
delete from movie
    where number = 1;
```

```sqlite
-- Careful! This deletes all the rows from the movie
delete from movie;
```


## Drop

Deletes a table.

```sqlite
drop table movie
```

## master

The `sqlite_master` table contains information about the tables in the database.

```sqlite
select name from sqlite_master
    where type = 'table';
```

# Aggregate Functions

These are functions that operate on sets. They are accumulate functions, essentially.

```sqlite
-- total assets of branches in Edmonton
select sum(assets)
    from branch
    where city = 'Edmonton'

-- JD could have multiple deposits at one branch
select count(distinct bname)
    from deposit
    where cname = 'John Doe'

-- Number of customers who have deposit accounts in at least 3 different branches
select count(*)
    from customer c
    where 3 <= (select count(distinct bname)
                    from deposit d
                    where c.cname = d.cname);

-- find the names of branches which have assets greater than the average assets of all branches
-- find the average asset first as a nested query, and then compare with that
select bname
    from branch
    where assets > (select avg(assets) from branch);
```

When designing a database, I think it's better to not create a DSL. They invariably involve some sort of procedural programming, which in most DSLs will suck. It's better to just create bindings from the database to your language of choice, with basic query, insert, and update stuff, and leave the procedural parts to the programming language.

## Grouping

```sqlite
-- the count is only applied to those rows where the cities match
select city, count(cname)
    from customer
    group by city;
```

So `group by` partitions the table into several partitions, where each tuple in a partition has the same value for the `group by`. The rest of the things you select from must be aggregates (so those aggregates are applied to the values in each partition).

You can then use the `having` clause to filter out groups (*not* tuples within the groups).

```sqlite
-- find cities with more than two bank branches
select city
    from branch
    group by city
    having count(*) > 2;

-- with no aggregates
select b1.city
    from branch b1, branch b2, branch b3
    where b1.city = b2.city and b2.city = b3.city   -- check that we are talking about the same city
        -- but we have different names for each city (3 different names)
        and b1.bname != b2.bname and b1.bname != b3.bname and b2.bname != b3.bname;
```

```sqlite
-- name and number of loans of each customer who has more than 2 loans, each over 100 000
loan(accno, cname, bname, amount)
select bname, cname, count(*)
    from loan
    -- filter out the loans with <= 100 000
    where amount > 100000   -- where is applied to individual rows
    group by bname, cname
    having count(*) > 2;

select bname, cname, count(*)
    from loan
    -- filter out the loans with <= 100 000
    group by bname, cname
    having count(*) > 2 and sum(amount) > 200 000
```

Order of evaluation is from, where, group by, having, and then select. So you create cross product, filter out the rows, group them, filter out groups, and then select.

Do some examples by hand.

```sqlite
-- group by exercises
branch(bname, address, city, assets)
deposit(accno, cname, bname, balance)

select c.cname, c.city
    from customer c, deposit d
    where c.cname = d.cname -- find the deposits for that customer
    group by c.cname, c.city
    having count(accno) > 2;

-- equivalent
select cname, city
    from customer
    where 2 < (select count(*) from deposit where customer.cname = deposit.cname);

-- find branches where average account balance is > 1200
-- these are correlated nested queries, and are best to avoid
select bname
    from branch
    where 1200 < (select avg(balance) from deposit where branch.bname = deposit.bname);

-- simpler solution
select bname
    from deposit
    group by bname
    having 1200 < avg(balance);

-- what if you also want the city
select b.bname, b.city
    from deposit d, branch b
    where b.bname = d.dname
    group by b.bname, b.city
    having avg(d.balance) > 1200;

-- 3
select bname
    from deposit
    group by bname
    -- uncorrelated subquery, so this is ok
    having avg(balance) > (select avg(balance) from deposit);
```

## SQLite Datatypes

SQLite has the following datatypes:

- `null`: The value `null` has this type
- `integer`: a signed 64 bit integer
- `real`: a 64 bit IEEE float
- `text`: arbitrary length string, stored using the database encoding
- `blob`: a sequence of bytes

### Type Affinity

Each column in a table has a declared type affinity, which is the preferred type for storing values in that column. SQLite is dynamically typed, so it does not enforce that the values of the column match the type of the column. If possible, it will convert the value to the affinity, but will keep it as-is if not. The affinities are

- `text`. Stores all data as a `null`, `text`, or `blob`. Integers inserted into this column will be converted into strings before being stored.
- `numeric`. Kinda pointless
- `integer`
- `real`
- `blob`

As you can guess, using type affinity can lead to all sorts of problems. So, we can enforce strict type affinity by adding a type check constraint. For example,

```sqlite
create table students(
    sid text check(typeof(sid) = 'text'),
    name text check(typeof(name) = 'text'),
    login text check(typeof(login) = 'text'),
    age integer check(typeof(age) = 'integer'),
    gpa real check(typeof(gpa) = 'real')
);
```

Note that this will also disallow `null` values, since those are of type `null`.

Because of SQLite's very simple type system, it does not have many of the types other SQL systems have.

### Null

Nulls are used when you don't know a column value.

```sqlite
select cname from customer where city is not null;
```

SQL has three-valued logic: `true`, `false`, and `unknown`. In short, any operator that can short circuit will evaluate to the correct thing, regardless if the other value is `unknown`.

Eg
```sql
true or unknown = true
unknown or true = true
false and unknown = unknown and false = false

true and unknown = unknown
```

# Joins

left outer join, right outer join, full outer join

You can simulate a right outer join using a left outer join. You can simulate a full outer join by taking a left and a right outer join, and then unioning the two results.

```sqlite
select a, b, c from R left outer join S using (B)
union
select a, b, c from S left outer join R using (B);

# Table

Use the `create table` command to create a table. When doing this, you specify the name of the table, the name of each attribute and its type.

```sqlite
create table students(sid text, name text, login text, age integer, gpa real);
```

## Insert

You can insert a tuple into a relation using the `insert` statement.

```sqlite
insert into students(sid, name, login, age, gpa)
       values (58042, 'Smith', 10);
```

You can select from one table and insert directly into another.

```sqlite
insert into move(title, number, runtime, year)
    select title, number, duration, year from temp;
```

## Integrity Constraints

Integrity constraints are conditions that must be true for data within a relation. They are defined within the schema, and are checked when relations are modified. They are restrictions on the domain. Tuples must satisfy these constraints into order to be inserted into a relation.

A `check` constraint is applied to each row

```sqlite
create table branch(
    bname text not null,
    address text,
    bname text,
    balance real default 0.0
        check (cname = 'Bill Clinton' or balance > 100)
);
```

### Unique

### Primary Key

## Keys

Keys of relations.

A set of fields is a *key* of a relation if it satisfies:

- *uniqueness*: for all tuples in the relation, if you select the fields in the key and form a tuple, then they form a set (no duplicates)
- *minimality*: no subset of the key fields form a key

The *candidate keys* are all the keys of the relation, and of these, one is selected to be the *primary key*.

A *superkey* satisfies just the first condition, but not the second (so combining some keys would be a superkey).

{sid} is a key, and {sid, gpa} is a superkey

You can declare a primary key using `primary key`, and then other keys are specified using `unique`.

```sqlite
create table enrolled1(
    sid text,
    cid text,
    grade text,
    primary key (sid, cid)
);
```

In order for your relation to form a set (and not a multiset, which sql defines by default), you must declare at least one key (the primary key). Most generally, the primary key could simply be the set of all fields, but usually it will be some smaller set of attributes that depend on the semantics of the data you are storing (for example, you often can make an id number the primary key).

### Foreign Keys

A *foreign key* allows you to enforce that values you insert into one table, a *child table*, are already present within another, the *parent table*. For example, consider the following tables.

```sqlite
create table artist(
    artistid    integer,
    artistname  text,
    primary key (artistid)
);

create table track(
    trackid     integer,
    trackname   text,
    trackartist integer,
    primary key (trackid),
);
```

In the track table, we don't want the artists to be any random person; we want each artist on a track to already be in the artist table. To do this, we can add a `foreign key` constraint to the table declaration.

```sqlite
foreign key (trackartist) references artist(artistid)
```

This means whenever we insert a row into the `track` table, there must already exist a row in the `artist` table such that `track_row.trackartist = artist_row.artistid`. One must be the subset of the other. Normally, the foreign keys in the parent table is the primary key.

Note that you can also use composite (multifield) keys within a foreign key.

```sqlite
create table student(
    age integer,
    name text,
    major text,
    minor text,
    primary key (age, name)
);

create table undergrad(
    student_age integer,
    student_name text,
    year integer,
    primary key (student_age, student_name),
    foreign key (student_age, student_name) references student(age, name)
);
```

If the fields of the child table match those in the parent table, the syntax is a little simpler.

```sqlite
create table student(
    studentid integer,
    name text,
    major text,
    minor text,
    primary key (age, name)
);

create table undergrad(
    studentid integer,
    year integer,
    foreign key (studentid) references student
);
```

#### Modifying Foreign Keys

map(key, child table) is a subset of map(key, parent table)

You can declare a foreign key for a table. That means when you insert a tuple into the table, they key for that tuple should already exist in the table you refer to (the parent table). The key you specify is usually the primary key of the parent table. Eg, you have a table of students in a university, and then you have a table of people enrolled within a class. You want everyone enrolled in the class to already be in the university.

But, what if you have two tables that use foreign keys to reference each other? You can create a deferred foreign key, which will insert all the stuff into the table, and only check that the constraints are satisfied at the end of the transaction.

# Views

A *view* is a presentation of an existing table. Only the definition of the view is stored, not the view itself. Whenever you access a view, the query that defined the view is executed. So a view changes as the underlying table changes.

```sqlite
create view young_active_students(student_name)
    as select s.name
    from students s, enrolled e
    where s.sid = e.sid and s.age < 21;
```

```sqlite
customer(cname, street, city)
create view jasper_customers
    as select *
        from customer
        where city = 'Jasper';

deposit(accno, cname, bname, balance)
create view cust_info(name, city, num, total)
    as select c.cname, city, count(accno), sum(balance)
    from deposit d, customer c
    where d.cname = c.cname
    group by c.cname, city;

create view deposit_holders(name, city)
as select distinct c.cname, city
    from deposit d, customer c
    where d.cname = c.cname;
```

# ER Model

Essentially you have a bunch of sets, and you have relations between the sets.

# Mapping ER to Tables

Mapping entities and attributes are the easiest ones to do. Simple create a table with the entity name, with all the attributes, and then identity some (or usually one) as the primary key. The underlined ones are the primary keys.

```sqlite
create table employee(
    sin integer,
    name text,
    primary key (sin)
);

create table project(
    pid integer,
    desc text,
    primary key (pid)
);

create table department(
    did integer,
    dname text,
    budget integer,
    primary key (did)
);
```

Now consider the relation `employee - works_in - department`. For a relation like this, with no constraints on either side, then we create a separate table that includes the primary keys of the things within the relationship.

```sqlite
create table works_on(
    employee_id integer,
    project_id integer,
    since date,
    primary key (employee_id, project_id),
    foreign key (employee_id) references employee(sin),
    foreign key (project_id) references project(pid)
);
```

The combination of the primary keys of the entities form the primary key of the relation table.

For relationships, you also create a table, but this time with foreign keys. The foreign keys must refer to the primary keys of the entity sets you are related to. The primary key of the table is the combination of the primary keys of the entities.

```sqlite
create table works_on(
    sin text,
    pid integer,
    since date,
    primary key (sin, pid),
    foreign key (sin) references employees,     -- all entries in the sin column must also be entries of the sin column in employees
    foreign key (pid) references projects
);
```

You also need to add foreign keys for the primary keys of the entities you are related to.

This is with no constraints. What if you have arrows and stuff?

Each employee works in at most one department.

```sqlite
create tabel works_in(
    sin text,
    did text,
    since date,
    primary key (sin),
    foreign key (sin) references employees,
    foreign key (did) references departments
);
```

More generally, you simply add a unique constraint for each arrow in your thing (and then choose one of these as the primary key). Eg, if you had two arrows, you would add a unique constraint for the primary keys of each. Or, you simply combine them together into a table.

What about a participation constraint. Then we need to add a foreign key from the entity table to the relationship table. (Or you combine the tables and allow them to be null). What if you have an arrow and a participation? Then you can combine part of the relationship table with with the entity table, and specify not null.

```sqlite
create table dept(
    did text,
    dname,
    budget,
    mgr text,
    since date,
    primary key (did),
    foreign key (mgr) references employees (sin)
);
```

What about two participation constraints? Then you can't model this using a table.

## Set Valued Attributes

employees
  - sin (primary key)
  - name
  - hobby (set valued)

What do we do? The simplest thing is to just use a list type, but what if we can't do that? Then we need to create a whole bunch of rows in the table, a copy of each one with a different hobby. This is very ackward and wastes space.

```sqlite
create table employees(
    sin text,
    name text,
    hobby text,
    primary key (sin, hobby)
);
```

## Weak Entities

Consider `employees - `

```sqlite
create table dep_policy(
    dname text,
    age integer,
    cost real,
    sin text not null,
    primary key (dname, sin),
    foreign key (sin) references employee
        on delete cascade
);
```

When an employee is deleted, we want all the dependents of the employee to be removed as well.

## ISA

In this case, we have one super table, and then for each entry in the super table we can specialize it and add extra data.

employee
  hourly employee
  contract employee

```sqlite
create table hourly_emp(
    sin text not null,
    contract_id text,
    primary key (sin)
);
```

```sqlite
create table employee(
    sin text,
    name text,
    primary key (sin)
);

create table policy_purchaser(
    policyid text,
    cost integer,
    sin text not null, -- employee who purchased
    primary key (policyid),
    foreign key (sin) references employee
);

create table dep_ben(
    dname text, -- a dname does not uniquely identify a dependent
    age text,
    policyid text not null,
    primary key (policyid, dname), -- only a combination of the two will uniquely identify a row
    foreign key (sin) references policy_purchaser
        on delete cascade   -- this is one of the differences between a weak entity and a normal <=
);
```

Arrow means merge, bold means not null. Each entity is its own table, and with an arrow the relation can be merged into an entity table.

Domain Key Normal Form - The best
