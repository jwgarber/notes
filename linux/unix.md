When learning a new technology, the best thing to do is *take it slow*. I know you just want to dive right in and start to code, but slow down, and take the time to read the documentation, do exercises, and really learn.

You need to become at peace with backwards compatability. Always strive to make things better, but it is more of a converging towards better programs and programming languages.

# Standards

## C

The current C standard is C11. Pass the `-std=c11` flag to the compiler to use this standard. A good place to go is [cppreference](http://en.cppreference.com/w/c).

## Single Unix Specification

The Single UNIX Specification (SUS) is a standardization of the UNIX operating system. It is a consolidation of many prexisting standards, and specifies standard UNIX tools and interfaces. It is maintained today by The Open Group, which holds the UNIX trademark. The standard consists of two parts:

- POSIX: Standing for *Portable Operating System Interface*, this is the mandatory interfaces that an implementation must provide.
- XSI: This is the *X/Open System Interface*, which consists of optional extensions.

The current standard at the time of writing (2016) is SUSv4, which contains POSIX.1-2008. For a system to be certified as SUSv4 compliant by The Open Group, it must provide both the base POSIX interface and the XSI extension. Note that when Poettering is complaining about POSIX compliance, this is what he's talking about. Linux is not officially SUSv4 compliant, since doing so would take much time and money, but it has *de facto* compliance.

Note that the SUS contains features that have been deprecated and should not be used anymore. These are identified as LEGACY features in the standard.

You can read the SUSv4 standard [here](http://pubs.opengroup.org/onlinepubs/9699919799/).

# C Tools

To check what a binary links against, use `ldd`.

```shell
$ ldd bash
    # prints out shared objects
```

# Basic Concepts

The *kernel* is the piece of software that manages and and allocates computer resources, such as processors, memory, storage disks, and I/O devices. It provides an abstraction around the hardware so it is usable to the user.

The kernel performs the following tasks:

- *Process scheduling*: The allocation of CPUs to processes
- *Memory management*: The allocation of RAM to processes
- *File system*: Management of the hard drive
- *External devices*: Provides an interface for processes to access external devices, such as mice, monitors, and keyboards, and hard disks.
- *Networking*: The transmission of packets between your computer and others
- *User accounts*: allowing multiple users to access a system independent of each other.

The kernel provides these services through *system calls*. These are functions processes can use to request that the kernel perform some task for them.

## User and Kernel Mode

Modern CPUs can operate in two modes: *user* mode and *kernel* mode. Only kernel mode can access kernel space memory, execute the instruction to halt the system, accessing the memory-management hardware, and conducting device I/O operations. Attempts to do these things in user mode result in a hardware exception. This is a safety feature that prevents user processes from tampering with the kernel or performing operations adverse to the system.

A kernel actually reminds me of Hobbes' Leviathan, which has ultimate power and control over all the system. It mediates the interactions of all processes, and has absolute control over everything.

## System Calls

A *system call* allows a user-land program to request the kernel to do something on its behalf. The invocation of a system call in userland looks something like this (this is for x86_64 Linux).

```nasm
mov     rax, 1      ; system call 1 is write
mov     rdi, 1      ; file descriptor 1 is stdout
mov     rsi, msg    ; string to output
mov     rdx, 13     ; number of bytes
syscall
```

Each system call has a number from 0 to 331 (at the moment), which is placed in the `rax` register, and further arguments in registers after that. The `syscall` instruction switches to kernel mode, and invokes the appropriate system call service routine. After the procedure is finished, and error code is (usually) returned, and the processor switches back to user mode. Now, using assembly is rather inconvenient, so the system calls have wrappers in the C library.

You can use the `strace` command to trace the system calls made by a program.

Functions in the C standard library, such as `fopen` use these system calls in their implementation.

# Users and Groups

Each user of a system has the following attributes defined in the password file `/etc/passwd`:

- login name
- user ID
- group ID
- home directory
- login shell

Users can be organized into *groups*. This is useful for administrative purposes, because certain privileges can given to all members of that group (such as accessing I/O devices, certain files, or who have root access).

Each group is identified by a line in the group file `/etc/group`.

- group name
- group ID
- user list: list of users in the group

There is usually one *superuser* who has special system privileges. The superuser account has user ID 0 and login name `root`. This user is mostly for administrative purposes, and can access all files on the system.

# File Types

On UNIX, everything is a file. This includes what we would normally describe as "regular" files, and includes devices, pipes, sockets, directories, and symlinks. A directory is a special file that contains a list of file names in the directory, and pointers to the locations of those files.

Filenames can be up to 255 characters long and contain any character except for `/` and `\0`. However, it is better in practice to restrict yourself to the *portable filename character set*, which is `[-._a-zA-Z0-9]`.

A relative pathname is specified relative to the cwd of the process. A process inherits its cwd from its parent.

# `/proc` Filesystem

The `/proc` directory is a virtual file system that provides an interface

## Error Handling

Almost every system call and library function returns a value to indicate if the call was successful or not. These return values should always be checked to see if the call succeeded, and appropriate action be taken if not. Please, please, please always check these return values, even for functions that "obviously will never fail".

Look at the documentation or man page of function to determine how it reports errors.

When some functions fail, they set the global integer variable `errno` to some positive value that identifies the error that occured. Including the `<errno.h>` header allows access to `errno`, various constants identifying errors, and the `perror` and `strerror` functions.

```c
char* strerror(int errnum);
void perror(const char *s);
```

The `strerror` function returns a pointer to a string describing the provided `errno`. Be careful! This string may be overwritten by future calls to `strerror` and is not thread-safe (who designed this crap?).

The `perror` function is essentially just `fprintf(stderr, "%s: %s\n", s, strerror(errno))`.

Successful calls never reset `errno` to 0, but they *can* set it to a non-zero value. Thus, it is not sufficient to simply check if `errno` is non-zero to see if the function failed. Always check if the function return value indicates an error, and then inspect `errno` to determine the source of the error.

# Feature Test Macros

When compiling in strict C11 mode, (done by passing the flag `-std=c11`), none of the SUS definitions are avaliable in the header files. To access these definitions, we need to define some macros.

- The `_POSIX_C_SOURCE` macro exposes the definitions in the POSIX base standard of SUS. Define it to be 200809L to access the definitions in POSIX.1-2008.
- The `_XOPEN_SOURCE` macro exposes the POSIX base and XSI extensions (aka all of SUS). Define it to be 700 to access the definitions in SUSv4. This macro will also include the definitions from `_POSIX_C_SOURCE`, so it is only necessary to define one of the two.

These macros can be defined in the source code, before the inclusion of any headers. However, I think it is best to define them as arguments to the compiler. For example,

```
-D_POSIX_C_SOURCE=200809L
-D_XOPEN_SOURCE=700
```

There are also three glibc specific macros that expose historical interfaces: `_BSD_SOURCE`, `_SVID_SOURCE`, and `_GNU_SOURCE`. These macros are non-standard and should be avoided.

For writing cross-platform applications, you should avoid relying on definitons outside the C standard library. However, since we are going to be explicitly using UNIX definitions, the thing to do is define `_XOPEN_SOURCE=700`.

# Portability Considerations

UNIX specifies many of its own datatypes, which are all typedefs of existing C types. For example, `pid_t` is often a typedef of `int`. However, the typedefs won't be the same on all systems, so we can't assume they correspond to a particular C type. For example,

```c
pid_t mypid;
printf("The PID is %d", mypid);
// This assumes mypid is an int, but that might not always be the case
```

So how do we print these types? The safest thing to do is cast them to `intmax_t` or `uintmax_t` (depending on the signedness), which are guaranteed to be able to hold any integer type on the system, and use `%ji` or `%ju` (`i` for signed, `u` for unsigned).

```c
printf("The PID is %ji", (intmax_t) mypid); // Don't forget the cast, or this is UB!
```

Double check the UNIX spec to see the numeric constraints on these types.

## Initializing Structures

In general, you can't assume a particular order for the fields defined in a struct. For example, consider `sembuf`.

```c
struct sembuf {
    unsigned short sem_num;
    short sem_op;
    short sem_flg;
};
```

We can't assume the fields are defined in this order, and some implementations may add extra fields to the struct. So for example, it is not portable to define a struct like this:

```c
struct sembuf s = { 3, -1, SEM_UNDO };
```

It is better to use designated initializers (and indeed, it is best to use these when defining all structs). When in doubt, do what Rust does.

```c
struct sembuf s = { .sem_num = 3, .sem_op = -1, .sem_flg = SEM_UNDO };
```

# File I/O

Don't copy and paste the book. Read what it says, and rewrite it in your own words.

A *file descriptor* is an unsigned integer that refers to an open file (note that a file here is used in the generalized UNIX sense, so this could include things like pipes, sockets, and terminals). By default, every process inherits three standard file descriptors from the shell.

FD      POSIX macro       stdio `FILE*`
0       `STDIN_FILENO`    stdin
1       `STDOUT_FILENO`   stdout
2       `STDERR_FILENO`   stderr

The POSIX names described above are defined in `<unistd.h>`. Note that it is possible to change the file descriptor of a stream using `freopen`.

## open

The `open` system call is used to open or create a new file.

```c
#include <sys/stat.h>
#include <fcntl.h>
int open(const char* path, int flags);
int open(const char* path, int flags, mode_t mode);
```

This call returns a file descriptor on success, and -1 on error. SUS guarantees that the returned file descriptor will be the lowest unused one on the process.

The `oflag` argument is a bitmask that describes how the file should be opened. It must contain one, and only one, of the following five *access mode flags* (whose meaning is rather straightforward.)

- `O_EXEC`
- `O_RDONLY`
- `O_RDWR`
- `O_SEARCH`
- `O_WRONLY`

You can also bitwise-or `|` the above one value with as many of the following as you like:

- `O_APPEND`: set the file offset to the end of the file prior to each write.

- `O_CREAT`: creates the given file if it does not exist. If this option is set, you can specify the permission options of the new file by `|`ing some of the following values (defined in `<sys/stat.h>`).

Early implementations of UNIX also included a `creat` system call specifically for creating files. This function is now obsolete; `open` should be used instead.

Note that opening a file outside the cwd is susceptible to race conditions. It is possible that part of the directory path could change concurrently with the call to open. In these cases, it is safer to open a fd to that directory, and then use `openat` to open the file within the directory.

## read

```c
#include <unistd.h>
ssize_t read(int fd, void* buffer, size_t count);
```

The `read` system call reads at most `count` bytes from the `fd` file descriptor into the given `buffer`. It returns the number of bytes read on success, `0` for end-of-file, or `-1` on failure.

## write

```c
#include <unistd.h>
ssize_t write(int fd, const void* buffer, size_t count);
```

The `write` system call writes at most `count` bytes from the given `buffer` into the `fd` file descriptor. It returns the number of bytes actually written on success, and `-1` on error.


## close

```c
#include <unistd.h>
int close(int fd);
```

The `close` system call closes the file associated with the `fd` file descriptor. It returns `0` on success and `-1` on error. Once closed, a file descriptor can be reused in further calls to `open`.

## lseek

For each open file, the kernel records a *file offset*, which is the position that subsequent `read` and `write` calls will commence at. The file offset counts the number of bytes since the beginning of the file, which is at position 0. The file offset is automatically adjusted in response to calls to `read` or `write` so it progresses through the file.

The `lseek` system call adjusts the file offset of the file descriptor `fd` according to the values of `offset` and `whence`.

```c
#include <unistd.h>
off_t lseek(int fd, off_t offset, int whence);
```

This call returns the new file offset if successful, or `-1` on error.

The `offset` argument is a signed integer type that specifies a byte offset, which is interpreted in the following ways according to `whence`.

- `SEEK_SET`: new offset = offset
- `SEEK_CUR`: new offset = current offset + offset
- `SEEK_END`: new offset = length of file + offset

Note that the length of file offset points to one byte past the end of the file.

## File Holes

Reading past the end of a file will return `0` to indicate EOF, but it is possible to write bytes past the end of a file to extend it. The space between the previous end of the file and the newly written bytes is referred to as a *file hole*. Reading from a file hole will return `0`, but disk space isn't actually allocated for the hole until bytes are written into it. This allows the file's size on disk to be much smaller than its "official" size.

You can ensure space is allocated for a byte range in a file using the `posix_fallocate` function.

# Processes

A *program* is an executable file that contains various information describing how to run it. This includes:

- binary format identification: the format of the executable, such as ELF, COFF, or MachO
- program entry point address
- machine code
- symbol and relocation tables
- data
- shared library and dynamic-linking information

A *process* is an instance of an executing program. It consists of user-space memory containing program code and variables. The kernel maintains various data structures that record information about the state of the process, such as identification numbers, virtual memory tables, a table of open file descriptors, signal delivery and handling, and much other information.

The memory allocated to a process consists of several *segments*:

- The *stack* contains stack frames for functions.
- The *heap* contains memory dynamically allocated at runtime.
- The *data segment* contains global and static variables, which persist for the entire execution of the program. This segment is usually split into the *user-initialized* data segment and the *unititialized data segment* (which the OS sets to 0 before program execution).
- The *text segment* contains the machine code instructions of the program run by the process. The text segment is usually marked read-only to prevent accidental or malicious corruption.

The `size(1)` command displays the size of the segments in an executable.

## PID and PPID

Every process has a *process ID*, a global positive integer that uniquely indentifies the process on the system. When a process begins, it is given a PID, and when it ends, the PID becomes free for other new processes to use. However, most OSes have a delay between the termination of a process and when its PID becomes avaliable again to minimize the chance that new processes will be mistaken for old ones. The `getpid()` system call returns the PID of the current process.

```c
#include <unistd.h>
pid_t getpid(void);
```

This call is always successful.

When a new process is created, it is assigned the next sequentially avaliable process ID. On Linux, once this number reaches 32767, the PID is reset to 300 and continues from there.

Processes on a system are organized in a tree. Every process is created by another one, called that processes parent. Every process has a parent - the process that created it. A process can find out the PID of its parent using the `getppid()` system call.

```c
#include <unistd.h>
pid_t getppid(void);
```

This call is always successful.

# Process Resources

## Resource Usage

Usage of system resources by a process or its children can be obtained using the `getrusage()` system call.

```c
#include <sys/resource.h>
int getrusage(int who, struct rusage* usage);
```

On success, this returns 0, and on failure -1.

The `who` argument specifies the processes for which the usage is retrieved. It can take one of two values:

- `RUSAGE_SELF`: The resource usage for the calling process
- `RUSAGE_CHILDREN`: The resource usage of all children of the calling process that have been terminated and waited on

Resource information is then stored in the `rusage` struct, whose contents will depend on the platform.

## Resource Limits

Every process has resource limits. These might be set, for example, to prevent a process from running too long, or consuming too much memory, or spawning too many other processes.

The resource limits for a process can be viewed and modified using `getrlimit` and `setrlimit`.

```c
#include <sys/resource.h>
int getrlimit(int resource, struct rlimit* rlim);
int setrlimit(int resource, const struct rlimit* rlim);
```

On success these functions return 0. On failure, they return -1 and set `errno`.

The `resource` argument specifies the resource to be retrieved or changed. Possible resource values include:

- `RLIMIT_AS`: The total memory (stack and heap) avaliable to the process, in bytes.
- `RLIMIT_CORE`: The maximum size of a core file, in bytes.
- `RLIMIT_CPU`: The maximum amount of CPU time the process can run for, in seconds.
- `RLIMIT_DATA`: The maximum size of the data segment, in bytes.
- `RLIMIT_FSIZE`: The largest size of a file that can be created, in bytes.
- `RLIMIT_NOFILE`: One greater than the maximum file descriptor number that can be created.
- `RLIMIT_STACK`: Maximum size of the stack, in bytes.

The `rlim` variable points to an `rlimit` structure, which is defined as follows:

```c
struct rlimit {
    rlim_t rlim_cur;
    rlim_t rlim_max;
};
```

This struct contains two limits for each resource. The `rlim_cur` field is the *soft limit*, and `rlim_max` is the *hard limit*. The soft limit is the actual limit for the process. If the process exceeds the soft limit, it will be sent a signal, or various system functions will begin returning error values (eg. malloc will fail if the memory requirements have been exceeded). The soft limit can be set to anywhere between 0 and the hard limit, which sets the upper bound on the soft limit. A privileged process can change the hard limit to anything greater than the soft limit, but an unprivileged process can only lower it.

There is a special value called `RLIM_INFINITY` used to specify that the resource has no limit. There are also the values `RLIM_SAVED_CUR` and `RLIM_SAVED_MAX`.

Resource limits are inherited by child processes created using `fork()` and are preserved after an `exec()`.

## Enivornment List

Every process has a list of strings called the *environment list*. Every string in this list is of the form `name=value`. The names in this list are referred to as the *environment variables*.

When a process is created, it inherits a copy of its parent's environment list. After this initial inheritance, both environment lists are completely independent; modifying one will not change the other.

Environment variables can be set in the shell. Programs often read from these variables to change their behaviour (eg, setting `CC` when executing make).

The value for a particular environment variable can be retrieved using `getenv`.

```c
#include <stdlib.h>
char* getenv(const char* name);
```

This function returns a pointer to the environment value, or `NULL` if the variable is not defined. Note that the `getenv` function is not thread-safe, and may be overwritting by subsequent calls to `getenv()` or the other similar environment calls. Be careful.

```c
const char* compiler = getenv("CC");
if (compiler == NULL) {
    // etc.
}
```

## Current Working Directory

Every process has a *current working directory*. It is relative to this directory that relative pathnames the process refers to are resolved.

The cwd can be retrieved using `getcwd`.

```c
#include <unistd.h>
char* getcwd(char* buf, size_t size);
```

This function places a zero-terminated string containing the absolute pathname of the cwd into `buf`, which must be `size` long. On success, `getcwd` returns `buf`. If the buffer is not long enough to contain the path name string, then the function returns `NULL` and sets `errno` to `ERANGE`.

```c
char cwd[PATH_MAX]; // PATH_MAX is the longest name of a path, including the nul byte
if (getcwd(cwd, sizeof(cwd)) == NULL) {
    // handle error
}
```

There are several non-standard extensions to this function that simplify its usage somewhat, but I will not describe them here. On Linux, the cwd of a process can also be determined by reading (`readlink()`) the `proc/PID/cwd` file.

The cwd can be changed using `chdir`.

```c
#include <unistd.h>
int chdir(const char* path);
```

This function changes the current working directory to that specified by `path`, which may be an absolute or relative pathname (in which case it is resolved relative to the current working directory). This function returns `0` on success and `-1` on failure (and then sets `errno`).

The `fchdir` function is similar, except it takes a file descriptor pointing to a directory (created with `open()`) instead of a pathname.

```c
#include <unistd.h>
int fchdir(int fd);
```

It also returns `0` on success and `-1` on failure.

## File Mode Creation Mask

Normally, programs can specify the permissions of files they create (such as passing extra arguments to `open()` or `mkdir()`). However, these permissions are all subject to the process *umask*, which describes which permissions should always be turned off.

The `umask` shell command changes the umask of the shell, which is then normally inherited by processes.

For example, most shells set the default umask to 022 (`----w--w-`), which specifies that write permissions are always turned off for group and other. These settings will override any specific permissions masks. For example, if a file is created with a mode of 0666 (read and write for all users), the owner will get read and write privileges, but everyone else will get read privileges.

The umask can be changed using the `umask()` system call.

```c
#include <sys/stat.h>
mode_t umask(mode_t mask);
```

This takes the given `mask`, which consists of `mode_t` constants OR'd together, and returns the previous process umask. This call is always sucessful.

```c
// This function reads the current umask value. Unfortunately, its
// implementation allows for race conditions
mode_t get_umask(void) {
    const mode_t mask = umask(0);
    umask(mask);
    return mask;
}
```

## Fork

An existing process can create a new one by calling `fork()`.

```c
#include <unistd.h>
pid_t fork(void);
```

This creates a child process that is an (almost) complete copy of the parent process. That is, all of the memory, including the stack, heap, and data, is completely copied over to the child process, so modifying any variables in the child process will have *no effect* on the parent (and vice versa). After the call to fork, both the parent and child will continue executing whatever code is left in the program.

You can differentiate which process you are in from the return value of fork. In the parent process, the returned `pid_t` is the PID of the child process. In the child process the returned PID is 0. On error, a value of `-1` is returned to the parent, no child process is created, and `errno` is set.

```c
pid_t pid = fork();

if (pid == -1) {
    // error! No child process created
} else if (pid == 0) {
    // perform operations in the child
} else {
    // perform operations in the parent
}
```

After forking, the parent and the child can execute in any order: the order of execution is determined by the scheduler, and it is possible that the parent finishes before the child. In this case, the child becomes *orphaned*, and is adopted by the `init` process.

## Wait

A parent process can use `wait` to get the exit status of a child process. It looks like this:

```c
#include <sys/wait.h>
pid_t wait(int* status);
```

The `wait()` function blocks until one of the child processes terminates (or returns immediately if a child process has already terminated), and returns the PID of that child process. If `status` is not `NULL`, the exit status of the child process is stored in `*status`. You can look at the termination status using macros in `<sys/wait.h>`. On error, `wait()` returns -1 and sets `errno`. One possible error is that calling process has no children, in which case `errno` is set to `ECHILD`. For example,

```c
// This waits for all the children to finish
pid_t child_pid = 0;
while ((child_pid = wait(NULL)) != -1) {
    continue;
}

if (errno != ECHILD) {
    // there was an unexpected error
}
```

## Waitpid

Note that `wait()` waits for any child process to end. To wait for a specific child process, use `waitpid()`.

```c
// statloc - variable to hold child return status
// options
//      0, which blocks until the child process terminates
//      or a bunch of macro craziness
//
// it returns the pid of the child process that terminated
#include <sys/wait.h>
pid_t waitpid(pid_t pid, int* status, int options);
```

The `pid` argument determines the behaviour as follows:
    - If `pid > 0`, wait for the child whose PID is `pid`
    - If `pid == 0`, wait for any child in the same process group as the parent
    - If `pid < -1`, wait for any child whos process group identifier is the same as abs(pid)
    - If `pid == -1`, wait for any child

The `status` argument is exactly the same as with `wait()`.

The `options` argument is a bitmask of zero or more of the following constants (which are declared in `<sys/wait.h>`):
    - `WUNTRACED`
    - `WCONTINUED`
    - `WNOHANG` (usefull for polling and not blocking)

On success, `waitpid()` returns the PID of the child process or -1 on error.

Note that `waitpid(-1, &status, 0)` is equivalent to `wait(&status)`.

## Wait Status

The `status` value stored by `wait()` and `waitpid()` contains information about the termination status of the child. This information is stored in a bitmask, which can be accessed using macros defined in `<sys/wait.h>`. There are four main macros used to classify the wait status. Of these four macros, only one will return true.

- `WIFEXITED(status)` returns true if the child process exited normally. In this case, `WEXITSTATUS(status)` returns the exit status of the child.
- `WIFSIGNALED(status)` returns true if the child was terminated by a signal. In this case, `WTERMSIG(status)` returns the number of the signal that killed the process.
- `WIFSTOPPED(status)` returns true if the child was stopped by a signal. In this case, `WSTOPSIG(status)` returns the number of the signal that stopped the process.
- `WIFCONTINUED(status)` returns true if the child was resumed by the delivery of `SIGCONT`.

Note information about the last two conditions are only returned for `waitpid()` when it is called with options `WUNTRACED` or `WCONTINUED` respectively.

## Orphans and Zombies

What happens to a child when its parent terminates before it does? In that case, the child becomes an *orphan*, and is adopted by the *init* process. Because of this, a child can check if it has become orphaned by checking if `getppid()` returns 1 (the PID of init).

When a child terminates, the kernel releases most of the child's resources and turns it into a *zombie*. This zombie persists until it is `wait()`ed on by the parent, and the zombie is then removed. If the parent terminates without doing a `wait()`, then the zombie is adopted by init, which then `wait()`s on the zombie to remove it. Note: be careful about creating zombies. If not removed, zombies will persist in the process table and needlessly use up resources. Thus, every call to `fork()` should be accompanied by an associated `wait()`, which will prevent the child from becoming a zombie.

## Execve

The `execve()` system call completely replaces the current process with another one. The old stack, data, and heap are thrown out and replaced with a new one. `execve()` is most commonly used in conjunction with `fork()` to spawn a new process.

```c
#include <unistd.h>
int execve(const char* pathname, char* const argv[], char* const envp[]);
```

- The `pathname` argument contains the path of the new program to execute. This pathname can be relative or absolute.
- The `argv` argument contains the command-line arguments that are passed to the new program. This array is passed as the `argv` parameter to `main()`, and has exactly the same semantics (i.e. it is a `NULL`-terminated list of strings, with `argv[0]` being the command name).
- The `envp` argument contains the environment list for the new program. This list is stored in the `environ` array of the new program. Thus, `envp` is a `NULL`-terminated list of strings of the form `name=value`.

Note that the PID of the process remains the same after an `execve`, since the process is not terminated, merely overwritten.

On success, `execve()` doesn't return, because the old process is overwritten. On failure, it returns -1 and sets `errno`. In this case, the current process remains.

```c

```

There are various other `exec()` functions that are layered on top of `execve`. All of these functions have the same semantics; they only differ in how the program name, argument list, and environment list are specified.

```c

```

In theory, one could combine `fork()` and `exec()` to create a `spawn()` function.

Note here that there is an important difference between concurrency and parallelism. *Concurrency* is having multiple units of execution that all run together. For example, when a client attempts to connect to a server, spawning a new process for each connection is concurrency. On its own, concurrency does not lead to improved speed (well, it might in some cases if one process blocks and then another takes over). *Parallelism* on the other hand, is when multiple concurrent processes are running on multiple cores. This does lead to a speedup.

# Threading

A thread is a unit of execution that exists within a process. By default, each process has one thread, the main thread. Other threads can be launched that coexist within the process.

Unlike a child process, threads share part of the same address space. All threads created in the same process share the same text, data, and heap segments. Each thread has its own stack segment (and register variables and program counter). Also, each thread gets a copy of all thread-local variables (such as `errno`) and their own signal mask.

The same observations about concurrency and parallelism apply to threads. For example, even on a system with one core, it is still possible to get speed ups running multiple threads. For example, one thread might need to block waiting for user input, and while it's doing so other threads can run instead.

## Thread Identification

Every thread has a thread ID, which is unique to that thread for the process. A thread ID is represented by the `pthread_t` data type. This data type can be implemented using a structure (rather than an integer), so we must compare TIDs using `pthread_equal()`.

```c
#include <pthread.h>
int pthread_equal(pthread_t t1, pthread_t t2);
```

This returns a nonzero value if equal and 0 otherwise.

A thread can obtain its own TID using `pthread_self()`.

```c
#include <pthread.h>
pthread_t pthread_self(void);
```

This returns the TID of the calling thread.

## Thread Creation

Additional threads within a process can be created using the `pthread_create()` function.

```c
#include <pthread.h>
int pthread_create(pthread_t* restrict tid, const pthread_attr_t* restrict attr, void* start_rtn(void*), void* restrict arg);
```

This function returns 0 on success, and an error number on failure.
- `tid` is a pointer to a TID that is set to the created thread on success.
- `attr` is a pointer to a thread attribute object. Setting this to `NULL` creates a thread with the default attributes.

Upon creation, the thread begins executing the `start_rtn` function which is passed the `arg` parameter.

As noted above, the created thread has access to the heap and data segments of the process. However, the new thread has its own signal mask, which is cleared on creation.

```c
int main(void) {
    pthread_t tid;
    // Note that we cannot pass the tid as an argument to the function
    // The problem is the function may begin executing before the value
    // is stored, and so the function reads garbage. Even if its
    // pre-initialized, this is a data race. This is made explicit
    // by the restrict pointers
    int err = pthread_create(&tid, NULL, thread_fn, NULL);
    if (err != 0) {
        errno = err;
        perror("pthread_create");
        exit(EXIT_FAILURE)
    }
}
```

## Termination

If any thread within a process calls `exit()`, `_Exit()`, or `_exit()`, the entire process terminates. This is why it is important to `join` on the created threads to prevent the main thread from returning, which will then end the entire process. Likewise, a nasty signal sent to any thread will terminate the entire process (like `SIGKILL`).

A thread can exit without killing the entire process in three ways:
1. The thread can return from the start routine. The start routine returns a pointer to its return value.
2. The thread can be cancelled by another thread in the same process
3. The thread can call `pthread_exit()`

The `pthread_exit()` function is a specialization of `exit()` that only cancels the current thread.

```c
#include <pthread.h>
void pthread_exit(void* rval)
// Call this inside a thread
```

This function always returns. The `rval` is a pointer to the return value of the thread.

The return value of a thread can be obtained using the `pthread_join()` function.

```c
#include <pthread.h>
int pthread_join(pthread_t thread, void** rval);
// Call this outside the thread
```

This function returns 0 on success and an error number on failure. The thread calling this function will block until the `thread` exits. The location pointed to by `rval` will contain the threads return pointer, or the special value `PTHREAD_CANCELED` if it was cancelled. The `rval` pointer can also be `NULL`, which will wait for the thread to cancel but ignore its return value.

One thread can request that another thread be cancelled using `pthread_cancel()`.

```c
int pthread_cancel(pthread_t thread);
```

This returns 0 on success and an error number on failure. This is equivalent to `thread` calling `pthread_exit(PTHREAD_CANCELED)`. Note that it is possible for a thread to disable cancellation in this manner using the `pthread_setcancelstate()` function.

## Synchronization

One must be very careful about data races when using threads to modify shared data. This can be dealt with using mutexes, which we do not need to get into now.

Note that when a multithreaded program calls `fork()`, only a single thread is created in the child process (the thread that called `fork()`). At this point, it is only safe to call async-signal-safe functions until `exec()` is executed.

The `pthread_kill()` function allows delivery of a signal to a specific thread.

## Sysconf

System limits can be obtained at runtime using the `sysconf()` function.

```c
#include <unistd.h>
long sysconf(int name);
```

The `name` argument is one of the `_SC_` constants, and the return value is the value of the limit. If the limit can't be determined, `sysconf()` returns -1. If an error occurred, then it returns `-1` and sets `errno`. To distinguish between these cases, we need to set `errno` to 0 before the call, and then check if it is non-zero after.

```c
errno = 0;
const long limit = sysconf(_SC_ARG_MAX);
if (limit == -1) {
    if (errno == 0) {
        puts("indeterminate limit");
    } else {
        perror("sysconf");
    }
} else {
    printf("limit is %ld\n", limit);
}
```

The value of the limits returned by `sysconf()` are constant for the lifetime of the calling process.

## Kill

A process can send a signal to another process using the `kill()` system call.

```c
#include <signal.h>
int kill(pid_t pid, int sig);
```

The `pid` argument specifies the process(es) the signal is sent to, in the following manner:
    - If the `pid > 0`, then the signal is sent to the process with the given PID.
    -

This function returns `0` on success. On failure, it returns `-1` and sets `errno`.

# Time

## Times

*Process time* is the amount of CPU time used by a process since it was created. It is separated into two components:
    - *User time* is the amount of time spent executing in user mode. This is the time that normal functions run on the CPU.
    - *System time* is the amount of time spent executing in kernel mode. This is the time the kernel spends executing system calls or performing other tasks on behalf of the program.

Process time information can be retrieved using the `times()` system call.

```c
#include <sys/times.h>
clock_t times(struct tms* buf);
```

The `tms` struct is defined as follows:

```c
struct tms {
    clock_t tms_utime;  // User time used by the calling process
    clock_t tms_stime;  // System time used by the calling process
    clock_t tms_cutime; // User time of all waited-for children (this information is only updated for a child once it has been waited for)
    clock_t tms_cstime; // System time of all waited-for children
}
```

On success, `times()` returns the real (wall) time since some arbitrary point in the past. This point is arbitrary, but fixed throughout the duration of the program. Thus, the only meaningful way to use this value is to measure durations, by subtracting a later real time from an earlier one. On failure, it returns `(clock_t)-1`.

The `clock_t` datatype measures time in units called *clock ticks*. We can call `sysconf(_SC_CLK_TCK)` to determine the number of clock ticks per second.

# Signals

POSIX expands on C signals extensively. Processes can send signals to each other, but most commonly the kernel sends a signal to a process. This can be caused in the following ways.

- A hardware exception occurred (such as execution of an illegal instruction, unprivileged access to memory, or division by 0), which the kernel delivers to the guilty process
- The user typed a special character that generate signals, such as Ctrl-C.
- A software event occurred, such as a timer went off or the child of a process terminated

All signals are defined as small positive integers, with macros in `<signal.h>`.

A signal is
- *generated* if the event that causes the signal has occurred
- *delivered* if the process has recieved the signal
- *pending* if it has been generated but not delivered
- *blocked* if it is pending because the target process does not want it delivered

Normally, a signal is delivered to the target process as soon as possible. However, processes can also add signals to their *signal mask*, which blocks delivery of those signals to the process.

Every signal has a default action that occurs when it is generated:
- *ignore*: the signal is never delivered to the process
- *terminate*: the recieving process is terminated. Sometimes a *core dump* is also generated, which is the image of the virtual memory of the process when it was terminated
- *stop*: the process is suspended
- *resume*: the process is resumed

## Signal Descriptions

Like error numbers, every signal has a string description. You can use the `strsignal()` function to obtain the string description for a given signal.

```c
char* strsignal(int sig);
```

The `psignal()` function prints to standard error the description of a signal.

```c
void psignal(int sig, const char* msg);
```

## Signal Sets



# IPC

## Pipes

*Pipes* allow processes to communicate with each other. A pipe has two ends: the write end, and the read end. Usually, the write end is connected to the file descriptor of one process, and the read end hooked up to the descriptor of another.

Pipes are essentially byte queues. The writer writes bytes to the pipe, and the reader then reads them at their leisure. Note that pipes have a limited capacity: they are stored using a buffer. Once that buffer is full, writing further bytes will block until the reader reads, and hence removes, some bytes. Also, if the pipe is empty, attemps to read from the pipe will block until data is written to it. If the write end of the pipe is closed, then the read end will return EOF once it has finished reading all the data from the pipe.

Note that multiple processes can write to the same pipe. In this case, it is possible that bytes written from the different processes could mix with each other. However, writes of up to length `PIPE_BUF` are guaranteed to be atomic. However, multiple processe reading from the same pipe can lead to data races, and should be avoided.

The `pipe()` system call creates a new pipe.

```c
#include <unistd.h>
int pipe(int fds[2]);
```

On success, this function returns 0. On failure it returns -1 and sets `errno`. When successful, this function creates a pipe, and stores a file descriptor to the read end of the pipe in `fds[0]`, and a file descriptor for the write end of the pipe in `fds[1]` (this order is the opposite of what one might expect). As with other file descriptors, we can `write()` and `read()` to the pipe as normal.

The `stdio` functions can also be used to access the pipe by converting the file descriptor to a stream using `fdopen`.

Pipes are usually not very useful within a single process (they are designed for IPC after all), so a call to `pipe()` is almost immediately follewed by a call to `fork()`. The child process created inherits copies of its parents file descriptors as usual. Afterwards, the process intended for reading closes its `write()` fd, and the process intended for writing closes its `read()` fd.

```c
int fds[2];
if (pipe(fds) == -1) {
    perror("pipe");
    exit(EXIT_FAILURE);
}

const int pid = fork();
if (pid == -1) {
    perror("fork");
    exit(EXIT_FAILURE);
} else if (pid == 0) {
    // child is reader

    // close unused write end
    if (close(fds[1]) == -1) {
        perror("close");
        exit(EXIT_FAILURE);
    }

    // now do reading
} else {
    // parent is writer

    // close unused read end
    if (close(fds[0]) == -1) {
        perror("close");
        exit(EXIT_FAILURE);
    }

    // now do writing
}
```

A pipe can be used for communication between any two processes, as long as that pipe was created by a common ancestor of those processes.

Note that it is in fact essential that processes close their unused read and write file descriptors of a pipe. For example, if the reading process keeps its write file descriptor open, then it will not see EOF when the writing process closes its write fd, since a write fd (the one for the reading process) is still open. Instead, the `read()` call will block, waiting for data that will never be written.

When a process tries to write to a pipe that has no open read descriptor, the kernel sends SIGPIPE to the writing process. If the writer does not close its reading fd, then this will never happen. The writer will send data happily down the pipe that never gets read, and then block once the pipe is filled.

A pipe is only destroyed and its resources released once all file descriptors of all processes pointing to the pipe are closed.

### Synchronization Using Pipes

Pipes can be used to synchronize a parent and multiple children.

### Redirecting Input and Output

Consider piping on the command line:

```c
$ ls * | grep mydir
```

In this example, the `output` for the `ls` command is redirected into the reading end of the pipe, and the `stdin` for `grep` is connected to the writing end of the pipe. This can be emulated by duplicating file descriptors using the `dup2` command.

```c
int pfd[2];

pipe(pfd);

// other steps, including fork()

// It is possible that stdout has been closed prior to this call, and that pfd[1] may then point to it
// So, only dup if it doesn't already point to stdout
if (pfd[1] != STDOUT_FILENO) {
    dup2(pfd[1], STDOUT_FILENO);    // close STDOUT_FILENO, and reopen pointing to the write end of the pipe
    close(pfd[1]);  // close the unused writer
}

// Now, all programs that print to stdout will instead print to the pipe
write(STDOUT_FILENO, "hello", 5);   // gets written to the pipe instead
```

A very common thing to do is run a shell command and then read its open or write to its input. The `popen()` and `pclose()` provide an abstraction around this process.

```c
#include <stdio.h>
FILE* popen(const char* command, const char* mode);
int pclose(FILE* stream);
```

The `popen()` function creates a pipe, and then forks a child process that execs a shell, which in turn forks and execs the given `command`. The given `mode` specifies how the calling process will access the pipe.
- "r". The calling process will read from the `stdout` of the command
- "w". The calling process will write to the `stdin` of the command

On success, `popen()` returns a file stream to the pipe. On failure, it returns `NULL` and sets `errno`. Note that `popen()` returns a file pointer, and so the given stream can be used with any of the `stdio` functions. Note that because of this, normal `stdio` buffering occurs on write calls. In that case, don't forget to `fflush`.

When finished with the pipe, use `pclose()` to close the stream. On success, `pclose()` returns the termination status of the shell command. On failure, it returns `-1` and sets `errno`.

### FIFOs

A FIFO, also known as a *named pipe*, is a pipe that is represented by a file in the file system. Unlike a pipe, which is tied to the lifetime of the processes that use it, a FIFO exists on the file system. This allows them to be opened and manipulated by any processes on the system. Like pipes, when all file descriptors referring to a FIFO are closed, any outstanding data in the FIFO is discarded. Just like normal files, FIFOs can be manipulated using `open()`, `read()`, `write()`, and `close()`.

A FIFO can be created in the shell using the `mkfifo` command:

```shell
$ mkfifo [ -m mode ] pathname
```

The `pathname` is the name of the FIFO to be created, and the `mode` specifies the file permissions.

You can also use the `mkfifo()` system call.

```c
#include <sys/stat.h>
int mkfifo(const char* pathname, mode_t mode);
```

The `pathname` argument specifies the name of the FIFO to be created, and `mode` gives the file permissions. On success, the `mkfifo()` function returns 0, and on failure, it returns -1 and sets `errno`.

Opening FIFOs is different than regular files. Generally, one process opens a FIFO to reading, and another process opens it for writing. Opening a FIFO for reading (the `open() O_RDONLY` flag) blocks until another process opens the FIFO for writing (the `open() O_WRONLY` flag). Likewise, opening a FIFO for writing blocks until another process opens it for reading. Some systems support passing the `O_RDWR` flag that will open the pipe for reading and writing (and hence not block). This should be avoided, because it is non-standard, and causes problems, because a single process will always have the pipe open for reading and for writing, so all the blocking issues we had with extra descriptors come back. Instead, pass the `O_NONBLOCK` flag if you don't want it to block.

### I/O Multiplexing

I/O multiplexing allows us to simultaneously monitor multiple file descriptors to see if I/O is possible on any of them.

The `select()` system call blocks until one or more of a set of file descriptors becomes ready.

```c
#include <sys/select.h>
int select(int nfds, fd_set* readfds, fd_set* writefds, fd_set* exceptfds, struct timeval* timeout);
```

The `poll()` system call is similar to select, but has a simpler interface.

```c
#include <poll.h>
int poll(struct pollfd fds[], nfds_t nfds, int timeout);
```

The

# Networking

As we have seen, there are a variety of IPC techniques for communicating between processes on the same computer. However, what if you want to communicate with a process on a different computer? For that, we need networking and sockets.

A socket is an IPC mechanism that allows processes to communicate with each other, either on the same host, or on another host connected by a network. Every socket has an address that identifies it on the network. In client-server communication for example, the server creates a socket, binds it to a well known address, and then waits for incoming client connections.

Every socket has three attributes: domain, type, and protocol.

The *domain* of the socket specifies the type of communication allowed on the socket. On UNIX, there are three standard domains.
- The UNIX domain allows communication between processes on the same host.
- The IPv4 domain allows communication between processes on the network using the IPv4 protocol.
- The IPv6 domain allows communication between procceses on a network using the IPv6 protocol.

The *type* of a socket specifies the communication format. All of the above domains support the following two types:
- *Stream sockets* provide a reliable, bidirectional, byte-stream communication. That is, the socket on the other end of the connection will recieve the data as transmitted by the sender, or an error message. As with pipes, this type of connction is a byte-stream. There is no concept of messages. Stream sockets always operate in pairs, and the socket on the other end of the connection is known as the *remote*. The socket on the this end of the connection is know as the *local* end.
- *Datagram sockets* send messages using datagrams. Datagrams preserve message boundaries, but their transmission is not reliable. Messages may arrive out of order, corrupted, or not arrive at all. These sockets are *connectionless*.

The *protocol* of a socket specifies.

## Socket

A socket can be created using the `socket()` function.

```c
#include <sys/socket.h>
int socket(int domain, int type, int protocol);
```

On success, this creates a socket and returns a file descriptor referring to that socket. On failure, it returns -1 and sets `errno`.

The `domain` argument specifies the type of communication over the socket, and can be any of the following values:

- `AF_UNIX` - UNIX domain
- `AF_INET` - IPv4
- `AF_INET6` - IPv6
- `AF_UNSPEC` - unspecified

The `type` argument specifies the type of the socket, and can be any of the following:

- `SOCK_DGRAM`
- `SOCK_SEQPACKET`
- `SOCK_STREAM`

The `protocol` argument can be any one of the following:

- `IPPROTO_IP` - IPv4 Internet Protocol
- `IPPROTO_IPV6` - IPv6 Internet Protocol
- `IPPROTO_ICMP` - Internet Control Message Protocol
- `IPPROTO_RAW` - Raw IP Packets
- `IPPROTO_TCP` - Transmission Control Protocol
- `IPPROTO_UDP` - User Datagram Protocol

You can set the protocol argument to 0 to choose the default protocol for the given domain and type. For example, the default protocol for `AF_INET` and `SOCK_STREAM` and is TCP, and the default protocol for `AF_INET` and `SOCK_DGRAM` is UDP.

UDP is implemented as a connectionless service. No connection with the other peer needs to exist, you just sends the data to the other socket and hope it gets there.

TCP is a connection-based service. It establishes a connection with the peer socket and then talks back and forth.

Don't forget to `close()` the socket when you are finished with it. Many functions that work on general file descriptors have unspecified behaviour or do not work on socket descriptors. It is possible to `read()` and `write()` to a socket, but it is perhaps better to use the standard `recv()` and `send()`.

## Bind

The `bind()` system call binds a socket to an address.

```c
#include <sys/socket.h>
int bind(int sockfd, const struct sockaddr* addr, socklen_t addrlen);
```

On success, this returns 0, and on failure, returns -1 and sets `errno`.

The `addr` parameter is pointer to the address the socket is to be bound to. Each domain has a different sThis binds the socket file descriptor to the address struct. The `addrlen` parameter gives the length of the `addr` struct.

## Addresses

Every socket has an address, and the format of this address depends on the domain of the socket. For example, in IPv4, the address is composed of a 32 bit *IP address* (such as 129.128.5.145), and 16-bit *port number*. Since the socket functions must work for each type of address, every domain address struct is cast to a generic `sockaddr` struct first (yes, this is undefined behaviour), which is then passed to the functions.

```c
#include <netinet/in.h>

struct sockaddr {
    sa_family_t sa_family;  // address family type (AF_*)
    char        sa_data[];
    ... // other stuff
};
```

## Stream Sockets

Client-server communication using stream sockets is often done using the following techniques:

The server does the following:
- `socket()` to create a socket
- `bind()` the socket to an address
- `listen()` to notify the kernel that it will accept incoming connections
- `accept()` to wait and block for incoming connections
- `read()` and `write()` back and forth
- `close()` when done

The client does
- `socket()` to create a socket
- `connect()` to connect to the server address
- `read()` and `write()` again as normal
- `close()` when done

A socket created using `socket()` is *active*. An active socket can use `connect()` to connect to a passive socket.
A *passive* socket is one that has been passed to `listen()`. These sockets listen for incoming connections using `accept()`.

## Listen

The `listen()` system call marks a socket as passive, which can then be used to accept connections from other active sockets.

```c
#include <sys/socket.h>
int listen(int sockfd, int backlog);
```

This returns 0 on success and -1 on error. The `sockfd` argument gives the file descriptor of the socket. The `backlog` argument.


## Accept

The `accept()` system call accepts an incoming connection on a passive socket. This call blocks until a connection is established.

```c
#include <sys/socket.h>
int accept(int sockfd, struct sockaddr* addr, socklen_t* addrlen);
```

- `sockfd` is the file descriptor of the passive socket that will accept a connection.
- `addr` is set to the address of the remote socket. As with `bind()`, this needs to be a domain-specific struct that is cast to a `sockaddr`.
- `addrlen` is a pointer to an integer that contains the size of the `addr` buffer. On return, this integer is set to the number of bytes actually copied into the buffer.

The last two parameters can be set to `NULL` to ignore the remote address. The remote address can be retrieved later using `getpeername()` if desired.

This function returns a file descriptor to a *new* socket that is connected to the remote socket. This is very important. The passive socket is *not* changed and is *not* connected to the remote. This is very handy, because it allows the old socket to continue listening for incoming connections. On error, this function returns -1.

## Connect

The `connect()` system call attempts to connect an active socket to a passive socket located at the given address.

```c
#include <sys/socket.h>
int connect(int sockfd, const struct sockaddr* addr, socklen_t addrlen);
```

This returns 0 on success and -1 on failure. The `addr` and `addrlen` arguments are provided in the same ways as those given to `bind()`. If a connection attempt fails, the thing to do is to close the socket, create a new socket, and reattempt with the new one.

## Stream Socket I/O

I/O on sockets is similar to that for pipes. Sockets are bi-directional, so you can `read()` or `write()` to both ends of a socket. Note that like pipes, these calls will block until the desired number of bytes have been read or written.

Attempts to read from a closed socket will return 0 to indicate end-of-file (once all remaining data has been read). Attempts to write to a closed socket will generate the `SIGPIPE` signal and sets the `EPIPE` error. Usually, one ignores the `SIGPIPE` signal to prevent crashing in this situation, and then disconnects the peer upon recieving `EPIPE`.

A socket can be closed using `close()`. A socket is only terminated once all file descriptors referring to the socket are closed.

## Shutdown

Sockets by default allow bi-directional I/O. However, it is possible to change this using the `shutdown()` function.

```c
#include <sys/socket.h>
int shutdown(int sockfd, int how);
```

The `how` variable can be any of `SHUT_RD` to disable reading, `SHUT_WR` to disable writing, and `SHUT_RDWR` to disable both. This returns 0 on success and -1 on error.

## Byte Ordering

Computers can be either big-endian or little-endian, and it is important not to get these two mixed up when transmitting protocol information between different computers. Thus, there is a preset *network order* (which is big-endian, incidentally) that all protocol information must be converted to and from on the host computers. IP addresses and port numbers are both integers, so they must be converted to this network orderbefore being stored in the socket address structures.

```c
#include <arpa/inet.h>
uint32_t htonl(uint32_t hostlong);  // convert host byte order to network byte order for a long
uint16_t htons(uint16_t hostshort); // the rest are similar
uint32_t ntohl(uint32_t netlong);
uint16_t ntohs(uint16_t netshort);
```

Note that as a corollary of this, all information written or read from a socket must be encoded using a common representation. With pipes, it is very common to write the byte representation of objects directly to the pipe, and then read them back on the other end. But for sockets, *this won't work*, because different hosts can have different data representations. For example, a `size_t` may be 32 bits on one computer and 64 bits on another, structs may have different padding alignments or field orders, and so on. The sender must encode data into a common representation, and the reciever must decode it. This is called *marshalling*. There are several encoding standards that one can use, including XDR, CORBA, and XML. However, these are all rather complicated, and a simpler alternative exists: simply encode all data as strings, with appropriate delimiters as necessary. This also allows us to use the `telnet` application.

## Socket Addresses

For IPv4 (`AF_INET`), the domain address struct is defined as follows.

```c
struct in_addr {
    in_addr_t   s_addr;     // 32 bit IPv4 address
};

struct sockaddr_in {
    sa_family_t sin_family; // address family type, AF_INET in this case
    in_port_t   sin_port;   // 16 bit port number
    struct in_addr sin_addr;
};
```

Both the IP address and port number must be stored in network byte order.

## Address Presentation Format

IP addresses and port numbers are both represented as integers. However, integers are a little difficult to work with, so IP addresses are often represented in dotted-decimal notation. The IP address is split apart into bytes, and each byte is written in decimal, separated by dots. For example, 127.0.0.1. One can use the `inet_pton()` and `inet_ntop()` functions to convert back-and-forth between integers and presentation format for IPv4 and IPv6.

```c
#include <arpa/inet.h>
int inet_pton(int domain, const char* restrict str, void* restrict addr);
```

This function converts the presentation string in `str` to an address in network byte order. The `domain` parameter indicates the type of address, which should be `AF_INET` or `AF_INET6`. The `addr` parameter points to an `in_addr` or `in6_addr` struct, where the network address is stored. This function returns 1 on success, 0 if the format is invalid, and -1 on error.

The `inet_ntop()` function converts a network binary address to a presentation string.

```c
#include <arpa/inet.h>
const char* inet_ntop(int domain, const void* restrict addr, char* restrict dst, socklen_t size);
```

The `domain` should be `AP_INET` or `AP_INET6`, and `addr` is `in_addr` or `in6_addr` as above. The `addr` is converted to a null-terminated presentation string that is stored in the `dst` buffer, which is of size `size`. On success, this function returns `dst`, and on failure returns `NULL` (such as when the buffer is too small). To ensure the buffer is large enough, one can set its length to the `INET_ADDRSTRLEN` or `INET6_ADDRSTRLEN` constants defined in `<netinet/in.h>`, which contain the maximum possible length of the presentation string (including the null terminator).

## Hostnames and Service Names

It is also convenient to give IP addresses and port numbers names. A *hostname* is a string for an IP address, and a *service name* is a string for a port number.

The `getaddrinfo()` function converts a hostname and service name a network address. Note that it is possible that multiple addresses could satisfy the given input information (such as a datagram and stream domain), so this function returns a list of the possibilities.

```c
#include <sys/socket.h>
#include <netdb.h>
int getaddrinfo(const char* host, const char* service, const struct addrinfo* hints, struct addrinfo** result);
```

- `host` is a string containing a hostname or presentation string
- `service` contains a service name or decimal port number
- `hints`

As output, this function allocates a linked-list of `addrinfo` structures and sets `result` to the beginning of the list. The `addrinfo` struct has the following form:

```c
struct addrinfo {
    int     ai_flags;           // Input flags, only used for hints
    int     ai_family;          // Socket domain, aka address family
    int     ai_socktype;        // Socket type, such as SOCK_DGRAM or SOCK_STREAM
    int     ai_protocol;        // Socket protocol
    socklen_t ai_addrlen;       // Size of structure pointed to by ai_addr
    char*   ai_canonname;       // Canonical name of host
    struct sockaddr* ai_addr;   // Pointer to socket address structure
    struct addrinfo* ai_next;   // Next addrinfo in the linked list
}
```

The `hints` argument contains information for selecting the appropriate socket address structs. When used in this way, only the first four fields of the `addrinfo` struct are used.

- `ai_family`

This function returns 0 on success, and a non-zero error code on error. These errors are special constants of the form `EIA_*`. You can obtain a description of the error code using `gai_strerror()`.

```c
#include <netdb.h>
const char* gai_strerror(int errcode);
```

The `getaddrinfo()` function dynamically allocates memory for the linked-list stored in `result`. The `freeaddrinfo()` structure will deallocate this list when no longer needed. If you want to save an `addrinfo` or `sockaddr` struct, you must copy it before calling this function.

```c
#include <sys/socket.h>
#include <netdb.h>
void freeaddrinfo(struct addrinfo* result);
```

The `getnameinfo()` function converts and IP address and port number to a hostname and service name.

```c
```

There are several obsolete functions that also do this stuff for IPv4 only, but those shouldn't be used.
