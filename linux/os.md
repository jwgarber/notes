# Introduction

An **operating system** provides an abstraction around the computer hardware so it is useful to the user. It provides resource management of the hardware, such as processors, memory, I/O devices, and storage disks, so user programs can operate in a convenient and efficient manner.

The **kernel** is a part of the OS that runs all the time. Everything else is either a system program (part of the OS, but not the kernel), or an application program (run by the user).

A usual computer consists of one or more CPUs and device controllers connected by a common bus to the main memory. The device controllers are connected to hard disks, displays, or audio ports. A memory controller ensures that access to the memory by the CPU and controllers is synchronized.

When the computer is booted, **firmware** written into the hardware bootstraps it. The firmware initializes the hardware of the system and loads the kernel into memory.

Hardware -- OS -- User applications

**Multiprogramming**: storing multiple processes in memory at once, and select the process to run based on CPU and I/O usage.

# Operating System Services

- *Filesystem manipulation*
- *Communication and networking*
- *Error detection*: Errors may accur in the CPU,
- *Resource allocation*: Needs to manage CPU cycles, memory, file storage, and I/O between different processes.
- *Accounting*: Keep track of which users use how much memory and what kind of resources
- *Security*:

The command line interface allows you to give commands to the computer. A *shell* is the program run in the CLI.

## Sizes

A KiB is 1024 bytes (2^10).
A MiB is 1024^2 bytes
A GiB is 1024^3 bytes, etc.

There is an important difference between a kilobyte kB and a kibibyte KiB. A kilobyte is 1000 bytes, and is almost never actually used. What you really mean is a KiB.

## Interrupts

An **interrupt** is a notification of an external event, such as I/O. It is asynchronous with the current activity of the processor (can happen in the middle of executing an instruction), and they are not predictable. When an interrupt occurs, the OS takes control of the CPU again (from a user program perhaps) to deal with it (like `ktext`). This has three stages:

- The OS preserves the state of the CPU by storing registers and the program counter.
- The CPU uses an interrut vector to determine what segment of code to execute for the interrupt (then execute that code).
- Restore the previous state of the CPU by restoring registers.

An interrupt vector contains addresses to the code to be executed for each type of interrupt.

An **exception** is a notification of an interval event, such as a system call, illegal instruction/access violation, or page fault. An exception occurs synchronously with machine instruction executions of the CPU.

**Virtual memory** allows execution of processes that are larger than the physical memory. The job scheduler switches between current processes.

# System Calls

The OS provides services to user programs. User programs access these services through **system calls** (things the OS does on behalf of the user program). For safety and efficiency reasons, the OS has control of these things. These services include

- **Program execution**: the OS must load user programs into main memory and run it
- **I/O operations**: For security and efficiency reasons, the OS alone has direct access to I/O devices. If a user program wants to read or write to a file, it must request the OS to do it.
- **File system access**: The OS manages the file system structure and the permissions of files
- Communication between processes
- Error detection. The OS needs to be aware of errors and recover from them if possible. Errors could occur in the CPU (invalid instruction), memory (access to restricted memory), I/O devices (your keyboard broke!), or in user programs (divide by zero, integer overflow).
- Allocate system resources betwen processes, such as CPU cycles, memory, and disk storage.
- Protection and security. Processes cannot interfere with each other or the kernel, and resources must have access management in play, such as requests to access the file system and the network.

There are two OS modes: **user mode** and **kernel mode**. The **mode bit** specifies which mode is in usage. Certain privileged instructions can only run in kernel mode. The mode bit is 0 in kernel mode, and 1 in user mode. System calls can only be executed in kernel mode.

These are usually accessed by user programs from a high-level API rather than a direct system call, such as Win32/WinRT, POSIX, or Java API for the JVM. One example system call is `read()` in POSIX.

Usually, each system call has a number. The system-call interface maintains a table indexed according to these numbers. In MIPS for example, each `syscall` is given a number of the syscall.

System call parameter passing

- simplest: put the arguments in registers. This limits the number of arguments however.
- parameters stored in a table in memory, and address of table is passed in a register (used by Linux)
- parameters are pushed onto the stack by the program and popped off by the OS once the syscall is done
- the last two methods have no limit on the number of arguments.

System calls usually fall into one of six categories

TODO finish this list of stuff
- process control
  - create and terminate processes (load, execute / end, abort)
  -
- file manipulation
- device manipulation
- information maintenance
- communications
  -
- protection
  - control access to resources

## Microkernels

Microkernels have the minimum amount of code in the kernel as possible, and have everything else implemented as modules that plug into the kernel. These modules talk with the kernel through message passing. These kernels have higher security (less code needs to be run in kernel mode) and are simpler (less code in the kernel), but have worse performance (message passing has a high overhead). Monolithic kernels, like Linux, just throw everything inside the kernel. They are uglier, but have better performance.

Most modern OSes have loadable kernel modules (like udev in Linux!). These are modules for hardware and such, and they interact with other modules through the kernel.

## Timers

A timer is sometimes set to prevent infinite loops and processes hogging resources. The timer is set to interrupt the computer after some time period. The OS initializes the clock, and decrements it based off the physical clock. When the clock reaches 0, an interrupt is generated, which allows the OS to terminate the program that overran its time.

## Processes

Time-sharing is sharing the same processor among multiple processes. This is done so rapidly so the user can interact with all running processes.

A **process** is a program in execution. The memory of every process consists of several segments:

- stack
- heap
- data
- text

A process also consists of the *program counter* and the contents of its registers.

A process can be in one of several states.

- *new*: The process is being created
- *ready*: The process is on hold, waiting to be assigned to a processor
- *running*: The process is running by executing instructions
- *waiting*: The process is waiting for some event to occur (such as blocking on some I/O, or waiting on a child process)
- *terminated*: the process has exited

Only one process can be running on a single processor at a time.

Each process is represented in the OS by a *process control block*, which contains the information associated with the process. It includes:

- *process state*: The state of the process, as described above
- *program counter*: The address of the next instruction to be executed by the process
- *registers*: The values of the CPU registers.
- *scheduling information*: including process priority and other information
- *memory info*: Information about the process memory segments, such as page tables and the like
- *process limits*: Things like the CPU and user time the process has used, time limits, child creation limits, and such
- *I/O*: The list of open file descriptors, I/O devices used by the process, and other info

Most operating systems today allow each process to run multiple threads. In these cases, the PCB is expanded to include information about each thread.

A *context switch* is when the scheduler removes one process from a CPU and replaces it with another (this can happen when, for example, a hardware interrupt occurs or the process's time slice is over). When this happens, the state of the old process is saved from the CPU into its PCB, and the state of the new process is loaded from its PCB into the CPU. Context switches must be very fast.

## Process Scheduling

Each processor can only have one running process at a time. While that process is running, any other processes on the system have to wait until it is their turn to run. As such, we need a *scheduler* to schedule processes onto the processors in a fair and efficient manner.

As processes enter the system, they are put into a *job queue*, which consists of all processes on the system. The processes that are in the ready state (and are ready to be executed next), are kept in the *ready queue*, which is usually represented by a linked list, in the order they are to be executed.

Processes on a system also attempt to access I/O devices. Many processes may attempt to access the same device at the same time, so each device has a *device queue*, which records the processes waiting to use the device.

Consider a process waiting in the ready queue. Once it reaches the front of the queue, it is *dispatched* by the scheduler to one of the CPUs, where it starts executing. Once in this state, several things could happen:

- The process could issue an I/O request and be placed in a device queue. This puts the process in a waiting state.
- The process could create a subprocess and wait for its termination.
- The process could be removed from the CPU by a hardware interrupt and be put back in the ready queue.
- The time slice for the process could expire, and it could be put back in the ready queue by the scheduler.

The job queue is maintained by the *job scheduler*, or long-term scheduler. The ready queue is maintained by the *CPU scheduler*, or short-term scheduler.  The job scheduler is only invoked when processes are created or terminated, which may happen once every few minutes. Thus, the long-term scheduler can afford to take some time when deciding which processes to run. The CPU scheduler runs much more often than the job scheduler, multiple times a second, and so also must be much faster.

In general, all processes can be split into two groups: I/O bound and CPU bound. *I/O bound* processes spend most of their time performing I/O operations. *CPU bound* processes spend most of their time running on the CPU rather than doing I/O. It is important that the long-term scheduler select a good mix of I/O bound and CPU bound processes. If mostly I/O bound processes are selected, the ready queue will be very small and CPU time will be wasted, since most of the processes will be in the waiting state blocking on I/O. On the other hand, if mostly CPU bound processes are selected, the device queues will be nearly empty, and little I/O work will be done. The best thing to do is have a good combination of both. (Note that some systems like Windows and UNIX do not have a long-term scheduler; they simply put all processes in the ready queue.)

Some systems also have a *medium-term scheduler*, which may remove or insert processes into the ready queue in between the short and long term schedulers. This is called *swapping*.

## Operations on Processes and IPC

See the relevant section in the Unix notes for this.

# Threading

A thread is a basic unit of CPU utilization. It comprises of a thread ID, a program counter, register variables, and a stack. Everything else it shares with other threads in its process.

Like processes, threads allow for concurrency and (depending on the hardware) parallelism. However, threads are much more lightweight than processes, and their ability to share memory makes them very suited for servers dealing with client requests.

Threads have several major advantages:
- Responsiveness: Running several program components under different threads allows other threads to progress while one is blocked. This makes GUIs responsive, because the user can still interact with the program while it is running something that is computationally expensive.
- Resource Sharing: threads share the same address space and resources, so they do not require IPC
- Economy: Threads are much cheaper than processes to spawn and switch
- Scalabitily: threads can make use of multiple processors, which improves performance

## Thread Models

Fundamentally, every thread is implemented at the kernel level. These are *kernel threads*, and the kernel is responsible for scheduling these threads onto the processor cores. However, kernel threads are tricky to get right, so many languages provide abstractions around them. These are called *user threads*. Fundamentally, these user threads only exist conceptually at the language or library level. Each user thread must be mapped onto a corresponding kernel thread for its actual execution.

There are several models for mapping user threads onto kernel threads.

The *many-to-one* model maps many user threads onto one kernel thread. The decision of which user thread to run at any point is decided by the library, but if that thread blocks, the entire application will block. Also, because only one kernel thread is active at one point, this model is unable to use parallel processes.

The *one-to-one* model maps each user thread to a unique kernel thread. There is no thread management by the library; it is all done by the kernel. This provides more concurrency than the many-to-one model, because other threads can run if one of them blocks. It also allows greater parallelism, because each kernel thread can run on a different processor. However, creating a kernel threads has a relatively high overhead, so this limits the threads to user can create (which may be inconvenient in certain situations). Low-level languages like Rust use this approach, because there is no thread scheduling needed at the language level.

The *many-to-many* model maps a certain number of user threads over a set of kernel threads. This model allows the user to create as many threads as they like (such as one thread for every client request), and those threads will be mapped by the library to the corresponding kernel threads in an efficient way. This allows great levels of concurrency and parallelism as the one-to-one case, but without the thread creation limitation. This is the approach taken by most high level languages (such as Go, for example).

Thread cancellation can be done in one of two ways:
- asynchronous cancellation cancels the target thread immediately. This is quick, but does not allow the thread to cleanup any of its resources before it closes, and so the program might be left in an inconsistent state.
- in deferred cancellation, the target thread periodically checks if it should terminate, which allows itself to terminate in an orderly fasion. In pthreads, a *cancellation point* is a point where the thread can be canceled safely.

A *synchronous signal* is delivered to the same process that caused the signal to be generated (such as a division by 0). An *asynchronous signal* is one that is generated, and then sent to another process.

Synchronous signals are delivered to the thread that caused the signal. With asynchronous signals, they are sent to all threads (I think).

*Thread pools* have a fixed number of pre-created threads that jobs are submitted to for work. Thread pools have several advantages:
- Reusing an existing thread is faster than creating and then destroying a thread for every job
- A thread pool places a limit on the number of threads that are created, which prevents resource exhaustion from creating too many threads.

Normally, threads all share the same static and global variables. However, threads can also have *thread local variables*, which are global variables that are specific to each thread.

Many-to-many thread models often have *light-weight processes* to manage the mapping of user threads to kernel threads.

Linux actually implements processes and threads in terms of the same system call, `clone()`. This call specifies the amount of sharing to be had between the parent and child tasks, as they're called.

# CPU Scheduling

Generally, a multiprogramming system consists of many processes running concurrently. Processes often make I/O requests or block for something else (such as acquiring a lock). In these cases, the CPU can swap the process out while it is waiting and run another process instead. This is done by the scheduler.

Processes generally cycle between two states: CPU execution and waiting for I/O. The CPU executions are called *CPU bursts*, and waiting for I/O is an *I/O burst*. A process begins with a CPU burst when it starts, then alternates to an I/O burst when it performs I/O, and on and on until it ends with a CPU burst when it terminates. Generally, an I/O bound program has short CPU bursts (and long I/O bursts), while a CPU bound program has long CPU bursts and short I/O ones.

Whenever a process has an I/O burst (it is idle waiting for I/O), the short-term scheduler selects a process from the ready queue and begins its execution. The exact structure of this queue (queue, stack, priority queue, etc.) will depend on the scheduling algorithm.

CPU-scheduling decisions take place in the following four circumstances:
1. When a process switches from the running state to the waiting state (for example, waiting for I/O or a child process to terminate)
2. When a process switches from the running state to the ready state (such as when an interrupt occurs)
3. When a process switches from the waiting state to the ready state (such as when I/O completes)
4. When a process terminates

*Cooperative* or *non-preemptive* scheduling is when the scheduler only starts another process because the current process idled on its own. This happens in cases 1 and 4, for example. *Preemptive* scheduling is when the scheduler can interrupt a process and schedule another one. Preemptive scheduling may lead to data races or inconsistent state, so the kernel and user programs must be careful to use proper synchroniztion techniques.

The scheduler decides which process to run next. The *dispatcher* does the necessary context switch. The *dispatcher latency* is the time the dispatcher takes to stop one process and start another, and should be, of course, as short as possible.

Scheduling algorithms have different properties, and they can be judged on these properties to select the best algorithm for the current job. These properties include:
- CPU utilization: the CPU should be kept as busy as possible
- Throughput: This is the rate at which processes are completed. This of course varies on the lengths of those processes.
- Turnaround time: The time from process initialization to completion. This includes time spent on the CPU and time spent in the ready queue (thus the wallclock time).
- Waiting time: The amount of time spent waiting in the ready queue
- Response time: The time it takes from adding a process to the ready queue and when it is first scheduled

When analyzing scheduling algorithms, we will often use the *Gantt chart*. This is a diagram of processes when the arrive and when they are scheduled.

## First-Come First-Served Scheduling (Queue)

- very simple, no starvation
- can have long average waiting times depending on the order processes arrive in

## Shortest Job First

- optimal for non-preemptive algorithms
- can lead to starvation

## Shortest Remaining Time First

- preempts the current process if there is another one that has arrived that is shorter
- optimal for preemptive algorithms
- also lead to starvation

## Priority Scheduling

SJF and SRTF are both special cases of *priority scheduling*. With this algorithm, every process is assigned a priority, and the CPU is scheduled to the process with the current highest priority, with equal priority processes scheduled in a FCFS basis. In this case, the ready queue could be implemented as a priority queue. (We generally assume lower numbers have higher priority, eg. priority number 1 is higher than priority 2).

Process priorities can be based off of:
- internal properties, such as memory usage, CPU time, number of open files etc.
- external properties, such as the niceness or process group priority

As with SJF and SRTF, priority scheduling can be preemptive or non-preemptive. With preemptive scheduling, the priority of a newly arrived process is compared to the priority of the currently running process. If the currently running process has a lower priority, it is replaced with the incoming process. For non-preemptive scheduling, the new process is simply placed at the head of the ready queue.

Priority scheduling algorithms can be susceptible to starvation: lower-priority processes could always be not run in favour of higher priority ones. To prevent this, we can use a technique called *aging*, where processes that have been in the ready queue for longer have their priority increase.

## Round-Robin Scheduling

The *round-robin* scheduling algorithm is designed for time sharing systems. A small unit of time, called a *time slice*, is defined, usually between 10 and 100 milliseconds. Processes are added to the ready queue in a FCFS basis. The scheduler pops off the first process from the queue and allocates it to a processor for a time slice. During a time slice, a process may finish its CPU burst, and then remove itself from the queue. If it isn't finished by the time the slice expires, the process is pre-empted and pushed onto the end of the queue, and the next process is run.

The performance of the RR algorithm depends on the selected time slice. If the time slice is large, the algorithm will approximate FCFS, and so may have large waiting times, causing interactive processes to suffer. If the time slice is small, a lot of time will be spent on context switches. Turnaround time can be improved if most processes can finish their CPU burst within the time slice. In general, the time slice should be significantly larger than the context switch time.

## Multilevel Queue

The processes of a system can be classified into different groups. For example, one common classification is into *foreground* jobs, which are interactive, and *background* jobs, which are not. Each group may have different scheduling requirements. So, the ready queue can be partitioned into multiple queues, one for each process type. This is called a *multi-level queue*. Each queue can then implement its own scheduling algorithm. The queues also need a scheduling algorithm to decide which one to schedule next, which is often implemented as fixed-priority preemptive scheduling. Another common thing is to give each queue a dedicated time slice for running its processes, eg. 80%.

A *multi-level feedback queue* allows processes to move between queues. For example, a process with long-running CPU bursts may be place in a lower priority queue, then perhaps moved up if it has aged for too long. These queues are the most general scheduling algorithms, but are complex to implement.

## Scheduling Threads

A user-level thread library uses *process contention scope* to determine which user threads are mapped onto the kernel threads, since the user threads only compete with other user threads in the process. The kernel uses *system contention scope*, since all kernel threads compete with all other kernel threads on the system.

## Scheduling with Multiple Processors

With *asymetric multiprocessing*, one processor is in charge of all scheduling decisions and schedules processes to the other processors as necessary. With *symmetric multiprocessing*, each processor has its own scheduler and selects processes to run from individual or common ready queues.

## Real-Time Systems

A *soft real-time system* provides no guarantee as to when a time critical process will finish, only that it has priority over non-critical processes. A *hard real-time system* will finish all processes by their deadlines.

*Event latency* is the amount of time that occurs from when an event occurs to when it is serviced (dealt with and resolved). Different events can have different latencies.
*Interrupt latency* is the period of time from the arrival of an interrupt at the CPU to the start of the routine that services the interrupt.
*Dispatch latency* is the time the dispatcher takes to stop one process and start another. The *conflict phase* of dispatch latency has two comonents: preemption of any process running in the kernel, and release by lower priority processes of resources needed by higher priority processes.

The scheduler for a RTOS must support a priority based algorithm with preemption. This is enough for a soft RTOS, but not a hard one. A hard RTOS also requires that the event by serviced by its deadline, which requires additional scheduling algorithms.

RT processes are considered *periodic*. They require the CPU at constant intervals and have a period *p*. Each process also has a time *t* that tells how long it takes to finish, and deadline *d* it must finish in (usually by the time the next period starts). The *rate* of the process is 1/p.

Each process declares its deadline when it arrives at the scheduler. The scheduler has an *admission-control* algorithm, which may deny the process if it cannot be completed in time, or accept it otherwise.

## Rate-Monotonic Algorithm

The *rate-monotonic* algorithm schedules periodic tasks using a priority algorithm with preemption. Each process gets a priority which is the inverse of its period. That is, the shorter the period, the higher the priority, and vice versa. This is based on the assumption that processes which require more CPU time should have higher priority. This algorithm also assumes that the CPU burst for each process is the same whenever it is scheduled onto a processor.

For a bunch of processes that need to be scheduled, take the ratio of time to period, t_i / p_i, and add them all together. If you get something less than n(2^(1/n) - 1) (where n is the number of processes), then scheduling is possible. If you get a higher value, scheduling may not be possible.

The rate-monotonic algorithm is optimal in the sense that if this algorithm cannot schedule the processes, then no other algorithm that uses static priorities can.

## Earliest-Deadline First

The *earliest-deadline first* algorithm schedules priorities according to deadlines. The earlier the deadline, the higher the priority. The processes run until they are pre-empted by the one with the earliest deadline. This algorithm does not require the processes to be periodic, or have a constant CPU time burst. The only requirement is that processes declare their deadlines up front. In theory, this algorithm is optimal.

# Process Synchronization

The biggest problem with concurrent systems is shared mutable state. Multiple processes reading or writing to shared mutable state can lead to data races and inconsistent conditions.

Note that these problems can occur even in a single-threaded concurrent system. Suppose two processes A and B are attempting to modify a local shared variable. B can begin to modify the variable, but become interrupted (say by a hardware interupt), and not finish, leaving the variable in an inconsistent state. Process A is then swapped in, and attempts to read the garbage value.

Specifically, a *race condition* is one where several concurrent processes attempt to modify the same shared data, and (crucially) the outcome depends on the order in which the processes access the data. To avoid this problem, we need to synchronize the data access.

## Critical-Section Problem

Suppose you have a series of n processes, each of which is modifying some shared data. For one of these processes, its *critical section* is the segment of code that accesses the shared state. For example, this could be when the process updates a table within a database. To avoid race conditions, it has to be that only one process can execute its critical section at a time. If there multiple processes are executing their critical sections, then it is possible for data corruption to occur. The *critical-section problem* is to design a protocol that allows the processes to communicate so that only one process will execute its critical section at a time. In general, a solution to the critical-section problem will look like this:

```
entry section   // allows one process at a time to enter the critical section
critical section
exit section    // indicates to other processes that this critical section is done
remainder section
```

The entry and exit sections are responsible for ensuring that the processes synchronize with each other to ensure only one processes is in its critical section at a time.

A solution to the critical-section problem must satisfy the following three requirements:

- *Mutual exclusion*: Only one process can be executing in its critical section at a time.
- *Progress*: If no process is executing in its critical section, and some processes wish to enter their critical sections, the decision of which process to do so is done in a finite amount of time. (So the algorithm doesn't deadlock, ie. at least one process will progress.)
- *Bounded waiting*: There is a bound on the number of times other processes are allowed to enter their critical sections after a process has a made a request to enter its critical section before that request is granted (so all processes will make progress at some point). In other words, there is no starvation. It is guaranteed under all conditions that all processes will be able to execute their critical sections in a bounded amount of time. Note that this property subsumes the second.

The *memory interlock* property: a single read or write cycle to memory is not interrupted (so reads and writes are atomic).

## Peterson's Algorithm

Peterson's algorthim is one such solution to the critical-section problem. This algorithm assumes that memory loads and stores are atomic (which may not be true for all architectures). Note that having basic memory operations atomic is not enough to solve the critical-section problem. A process may want to perform some large operation (such as updating a database) that, even though it is composed of atomic operations, in total may lead to a race condition (such as the counter++ example). Thus, we use the basic memory atomic operations to create a safe method for entering the critical section, which can then perform the large operation.

Suppose you have two processes P0 and P1 that are trying to enter their critical sections.

```c
int turn;   // which process's turn it is
bool ready[2];  // if a process is ready to enter its critical section

while (true) {

    ready[i] = true;
    turn = j;   // other process can enter
    while (ready[j] && turn == j);  // loop

    critical section

    ready[i] = false;

    remainder
}
```

In the beginning, both processes will set their ready flags to true (no problem here), and set the turn to the other process. One of these assignments will be overwritten (we are safe here, because of atomic ops), so the one that executes first will enter its section first.

This algorithm preserves mutual exclusion. Indeed, for contradiction suppose that both processes are in the critical section at the same time. Then,

From p0,
ready[0] = true;
ready[1] == false or turn == 0

From p1,
ready[1] = true;
ready[0] == false or turn == 1

From the assignments, this implies ready = {true, true}, so then turn == 0 and turn == 1. Contradiction. Thus, this preserves mutual exclusion.

This algorithm also has progress. Suppose that neither process is in the critical section. If process P0 is attempting to access the critical section.

It also has bounded waiting. The other process can enter the critical section at most once before the waiting one does.

## Synchronization Hardware

A process is called *preemptive* if other processes can stop the process and take over. A process is called *non-preemptive* if it only stops on its own. In theory, on a single-processor computer, the critical-section problem can be solved by not preempting a process while it is in its critical section.

Abstractly, we can say that solutions to the critical-section problem involve *locks*. In the entry section, a process attempts to acquire the lock, and if successful, executes its critical section, and then releases the lock at the end. Only one process can acquire the lock at any one time, and the proceduce for acquiring the lock must allow progress and bounded waiting.

Many instruction set architectures provide test-and-set and swap instructions atomically. That is, what normally takes place as several operations only takes place as one; it is impossible to interleave another instruction in between. If two processors attempt to execute two atomic operations at once, then one operation will be done, and then the other.

The critical-section problem can be solved using test-and-set as follows. This code assumes there is a global boolean `lock` variable that indicates if a process is inside its critical section. If the lock is false, then no process is in their critical section, so it is safe for a process that acquires the lock to enter.

```c
bool test_and_set(bool* target) {
    bool rv = *target;
    *target = true;
    return rv;
}

while (true) {
    // This attempts to set the lock to true
    while (test_and_set(&lock));

    // critical section

    lock = false;

    // remainder section
}
```

The critical-section problem can be solved using swap as follows. In this case, we again assume there is a global `lock` variable, and we assume each process has their own `key` variable.

```c
// Atomically swap two values
void swap(bool* a, bool* b) {
    bool temp = *a;
    *a = *b
    *b = temp;
}

while (true) {
    key = true;
    while (key == true) {
        swap(&lock, &key);
    }

    // critical section

    lock = false;

    // remainder section
}
```

The critical-section problem can also be solved using compare-and-swap.

```c
int compare_and_swap(int* value, int expected, int new_value) {
    int temp = *value;
    if (*value == expected) {
        *value = new_value;
    }

    return temp;
}

while (true) {
    while (compare_and_swap(&lock, 0, 1) != 0);

    // critical section

    lock = 0;

    // remainder section
}
```

All of the above algorithms satisfy mutual exclusion and progress, but they do not satisfy bounded waiting, since a single process has no guarantee that it will run after a certain time. Note that these techniques work for any number of processes.

Again, the lock is initialized to 0 at the beginning.

## Semaphores

A *semaphore* is an integer variable that is initialized to a certain value, and then after only accessed through two atomic operations: wait() and signal(). We can define wait() as follows:

```c
void wait(S) {
    while (S <= 0);
    S--;
}

void signal(S) {
    S++;
}
```

So, `wait()` simply waits until the semaphore is greater than 0, and then decrements it by 1. `signal()` simply increments it by 1. Remember that these operations are atomic (no other process can call wait() or signal() on the sempahore until the old ones are over).

There are two types of semaphores: *counting semaphores* can range over an unrestricted domain. A *binary semaphore* only ranges between 0 and 1. Binary semaphores are sometimes known as *mutex locks*.

A binary semaphore can be used to solve the critical-section problem. Each process shares a global `mutex` variable that is originally initialized to 1.

```c
sem = 1;    // happens once for all processes
while (true) {
    wait(mutex);

    // critical section

    signal(mutex);

    // remainder section
}
```

Counting semaphores can be used to control access to a resources with a finite number of instances. Initially, the semaphore is initialized to the number of resources avaliable. Each process (there may be more processes than resources) then calls `wait()` to decrement the count and obtain a resource. When the process is finished with its critical section, it calls `signal()` to increment the count and release its resource. When the semaphore has a value of 0, all resources are being used. Other processes that try to obtain a resource will block until the count is greater than 0. (Note that binary semaphore is simply a special case of the counting semaphore where there is only one resource.)

Semaphores can also be used to solve other synchronization problems. For example, consider two processes P1 and P2, with statements S1 and S2. Suppose we require that S2 only be executed after S1 has completed. We can do this by inserting a semaphore `synch` that is originally set to 0, and then using wait and signal as follows.

```c
// process 1
S1;
signal(synch);

// process 2
wait(synch);
S2;
```

The main disadvantage of a semaphore is that it requires *busy waiting*. While the mutex is locked, any process that `waits()` on the semaphore will be idly looping until the mutex is released. This type of semaphore is called a *spinlock* because it spins until the lock is obtained. This, unfortunately, wastes CPU cycles. Thus, spinlocks are usually only used when the waiting is expected to be short.

We can modify the definition of wait() so that when the mutex is locked, the process instead blocks and places itself into a waiting queue associated with the semaphore. The process then switches to the waiting state, which allows it to be swapped out. When another process does a signal() on the semaphore, the first process in the waiting queue is woken up and put back in the ready queue. We can implement this as follows:

```c
struct {
    int value;
    struct process* list;
};

void wait(semaphore* s) {
    s->value--;
    if (s->value < 0) {
        append(this process, s->list);
        block();
    }
}

void signal(semaphore* s) {
    s->value++;
    if (s->value <= 0) {
        p = pop(s->list);
        wakeup(p);
    }
}
```

In the above implementation, the semaphore value is allowed to be negative, which indicates the number of processes waiting on the semaphore. Also, using a FIFO queue enforces a bounded waiting time on the processes.  If you remove processes from the queue in LIFO order, the ones at the start may never get popped off, which leads to *starvation*.

With semaphores, it is possible that two processes *deadlock*, and do not proceed.

```c
// Process 1        // Process 2
wait(S);            wait(Q);
wait(Q);            wait(S);
...                 ...
signal(S);          signal(Q);
signal(Q);          signal(S);
```

In the above example, processes 1 and 2 will wait forever, because the other process never has the chance to release their lock.

In a *priority-inheritance protocol*, processes with lower priorities that are accessing resources needed by higher priority processes temporarily inherit the higher priority until they release the resource, and then go back to normal. This prevents them from being preempted by a medium priority process, which would increase the wait time for the higher priority process even more.

## Monitors

A *monitor* is a convenient data type for describing synchronization problems. It consists of several shared variables, initialization code, and then various functions that operate on those variables. (It's almost like a Java class.) The monitor ensures that only one function within the monitor can execute at a time (mutual exclusion).

Monitors can contain variables of type `condition`, which have `wait()` and `signal()` methods. For a condition variable `x`, calling `x.wait()` suspends the calling process until another process calls `x.signal()`. The signal() operation resumes exactly one suspended process, and has no effect if no process is suspended. When a process calls `x.signal()`, it immediately terminates, since only one process can run within the monitor at a time.

For example, consider the following solution to the dining philosophers problem. This solution is deadlock-free, because it mandates that a philosopher can only pick up his forks if both of them are avaliable.

```c
monitor dining_philosophers {
    enum {thinking, hungry, eating} state[5];
    condition self[5];

    void pickup(int i) {
        state[i] = hungry;
        test(i);

        // If our attempt to eat was unsuccessful,
        // wait until we can do so
        if (state[i] != eating) {
            self[i].wait();
        }
    }

    void putdown(int i) {
        state[i] = thinking;
        test((i + 4) % 5);
        test((i + 1) % 5);
    }

    void test(int i) {
        // if philosopher i is hungry, and both his neighbors
        // are not eating
        if ((state[(i + 4) % 5] != eating) &&
            (state[i] == hungry) &&
            (state[(i + 1) % 5] != eating)) {
                // then pickup the forks and start to eat
                state[i] = eating;
                self[i].signal();
        }
    }

    void initialize() {
        // in the beginning, everyone is thinking
        for (int i = 0; i < 5; ++i) {
            state[i] = thinking;
        }
    }
}
```

To eat, each philosopher must invoke `pickup(i)`, eat, and then `putdown(i)`. This solution is deadlock free, but not starvation free (it is possible that a philosoher will never get to eat).

We can implement a monitor using semaphores. Some stuff, yada yada yada, I'll look at it later.

Suppose that several processes are waiting on a condition x. When another process calls signal, which one should be resumed? One can use a simple FIFO order. Another option is using a *conditional-wait*, which has the form `x.wait(p)`. The `p` is the *priority* of the process. When `x.signal()` is executed, the process with the highest priority is resumed.

```c
monitor ResourceAllocator {
    bool busy;
    condition x;

    void acquire(int time) {
        if (busy) {
            x.wait(time);
        }

        busy = true;
    }

    void release() {
        busy = false;
        x.signal();
    }

    initialize() {
        busy = false;
    }
}
```

In this monitor, the process with the smallest time requested to use the resource will be resumed first (in this case, we view smaller times as higher priorities).

In a single-processor system, the critical-section problem can be solved by disabling process interrupts while a process is in its critical section. However, this does not scale to multi-processor systems, and is slow to implement. One must be careful when doing this though, because it is possible that a process could loop forever and never re-enable its interrupts.

## Classical Problems

In the *bounded-buffer problem*, there is a produce and consumer that share a common buffer.

```c
in = 0;
out = 0;
counter = 0;

// out is always one value behind in

// So this just overwrites values in the array as
// it goes along
// The counter is the number of unread values
// So if the counter is BUF_SIZE, then don't put anything
// in
while (true) {
    while (counter == BUF_SIZE);

    buf[in] = next_produced;
    in = (in + 1) % BUF_SIZE;
    counter++;
}

while (true) {
    while (counter == 0);

    next_consumed = buf[out];
    out = (out + 1) % BUF_SIZE;
    counter--;
}

semaphore mutex = 1;    // is this variable necessary?
semaphore empty = n;
semaphore full = 0;

// producer
while (true) {
    // produce item
    wait(empty);
    wait(mutex);

    // add it to the buffer

    signal(mutex);
    signal(full);
}

// consumer
while (true) {

    wait(full);
    wait(mutex);

    // remove item from buffer

    signal(mutex);
    signal(empty);

    // consume item
}
```

In the readers-writers problem, we have a bunch of readers and writers that want to access a database. In the first type, readers take priority over writers. That is, if a new reader is created, it begins before any other waiting writers. In the second type, writers take priority over readers. After the writer has requested access, no other readers start until the writer is done. Note that solutions to these problems may result in starvation. We can have as many readers as we want, but only one writer.

```c
semaphore rw_mutex = 1;
semaphore mutex = 1;
int read_count = 0;     // how many readers we currently have

while (true) {
    wait(rw_mutex);
    // write
    signal(rw_mutex);
}

while (true) {
    // The mutex is used when modifying the read_count
    // The first reader acquires the rw_mutex, and the
    // last reader releases it. This happens because we
    // only allow writing if there are no readers.
    // As long as the rw_mutex is held by a single reader,
    // it is safe for all the readers to read, since no
    // writer can be writing during this time

    wait(mutex);
    read_count++;
    if (read_count == 1) {
        wait(rw_mutex);
    }
    signal(mutex);

    // do reading

    wait(mutex);
    read_count--;
    if (read_count == 0) {
        signal(rw_mutex);
    }
    signal(mutex);
}

// now for the second variant, where writers take priority
semaphore writing_mutex = 1;
semaphore rw_mutex = 1;
semaphore mutex = 1;
int write_count = 0;    // number of attempting writers
bool writing = false;

while (true) {
    wait(mutex);
    write_count++;
    if (write_count == 1) {
        wait(writing_mutex);
        // writers are starting
    }
    signal(mutex);

    // prevent other writers from writing now
    wait(rw_mutex);
    // do writing
    signal(rw_mutex);

    wait(mutex);
    write_count--;
    if (write_count == 0) {
        signal(writing_mutex);
    }
    signal(mutex);

}

while (true) {
    // can allow as many readers as we want, as long as there
    // are no writers
    wait(mutex);
    if (!writing) {
        wait(reading_mutex);
    }

    if (write_count == 0) {
        wait(rw_mutex);
    } else {
        if there are writers, then we skip?
    }
    signal(mutex);

}
```

Bounded-buffer problem:

```c
// assume integers are the thing we are producing/consuming
monitor bb {

    int buffer[BUF_SIZE];

    int counter;
    int in;
    int out;

    condition full;
    condition empty;


    void produce(int item) {

        while (counter == BUF_SIZE) {
            full.wait();
        }

        buffer[in] = item;

        in = (in + 1) % BUF_SIZE;

        counter++;

        empty.signal();
    }

    int consume() {
        while (counter == 0) {
            empty.wait();
        }

        item = buffer[out];

        out = (out + 1) % BUF_SIZE;

        --counter;

        full.signal();

        return item;
    }

    initialize() {
        counter = 0;
        in = 0;
        out = 0;
    }
}
```

# Deadlocks

Operating systems have various resources that a process can access. For example, CPU cycles, memory allocation, file access, I/O devices, speakers, and printers are all resources. Other more abstract things can also be considered resources, such as locks and semaphores. Many processes on a system want to access a limited number of system resources, and we want to ensure they are done in a consistent manner. This is done through synchronization of critical sections. In this chapter, we will look at a specific aspect of the solution to the critical-section problem. A solution to this problem must have *progress*, that is, if multiple process are attempting to enter their critical sections, there is a bounded amount of time before one of those processes can enter. These processes are in a *deadlock* if there is no progress: each process is waiting for an event that will never occur.

For example, consider the following two processes attempting to access semaphores S and Q.

```c
// Process 1        // Process 2
wait(S);            wait(Q);
wait(Q);            wait(S);
...                 ...
signal(S);          signal(Q);
signal(Q);          signal(S);
```

In the above example, processes 1 and 2 could possibly deadlock, because the other process never has the chance to release their semaphore.

In general, there are two types of resources:

- *Preemptable* resources can be forcibly taken away from another process and returned with no ill effect. For example, a scheduler interrupting a process because its time slice has expired.
- *Non-preemptable* resources can not be taken away from another process. They can only be released voluntarily. For example, an open file that a process is writing to is non-preemptable.

In general, we have a set of multiple processes trying to access a finite set of resources. This will lead to a deadlock if the following conditions hold:

- *Mutual exclusion*: Access to at least one of the resources is mutually exclusive. That is, at most one process can acquire the resource at any time. Mutexes and printers are a mutually exclusive resources. Read-only files, for example, are not.
- *Hold and wait*: Each process is holding a resource and waiting to acquire resources held by the other processes.
- *No preemption*: Each resource can only be voluntarily released by the process that acquired it. Resources can not be forcibly taken away, so every process has to wait for the resource to be released.
- *Circular waiting*: There is a cycle of processes {P1, P2, ..., Pn}, such that each process Pi is waiting for a resource held by Pi+1 (and Pn is waiting for P1).

## System Resource-Allocation Graph

## Handling Deadlocks

Deadlocks can be handled in several ways:

- Use resource protocols to ensure deadlocks will never happen. This can be done through deadlock prevention or deadlock avoidance.
- Detect deadlocks and recover.
- Ignore the problem and pretend deadlocks never occur. This is the approach taken by Windows and Linux. It is then up to the application developer to ensure their program does not create deadlocks. If a deadlock does occur, the user must then manually restart the computer. This is done because deadlock prevention algorithms are somewhat expensive, and deadlocks occur so infrequently that it is easiest just to ignore them.

We investigate each of these more closely.

## Deadlock Prevention

A system can prevent a deadlock by ensuring that at least one of the four necessary deadlock conditions does not hold. This ensures that no deadlocks can ever happen.

- Mutual exclusion. In general, this condition cannot be prevented. Some resources, like mutexes and locks, are mutually exclusive by nature.
- Hold and wait. To prevent this, we must ensure that each process cannot make a resource requests while it is holding another resource. This can be done by acquiring resources, releasing them, and then making a request for more resources. However, this approach can lead to starvation.
- No preemption. The way to prevent this is to make all resources pre-emptable. This works well for some things like CPU registers and memory, which can easily be saved and restored, but not so well for semaphores and mutexes. Suppose a process requests a pre-emptible resource. If the resource is not avaliable, we check if they are being held by a process waiting for some other resource. If so, they are pre-empted from that process and allocated to this one. If the resource is unavaliable and not held by a waiting process, this process waits. During its wait, some if its resources may be pre-empted by other processes. In this case, the process can only restart when it acquires the resource it was waiting for and the ones that were taken from it.
- Circular wait. This can be prevented by imposing a total ordering on the resources types and requiring that a process can only request a resource if it has released all resources of higher ordering. So once a process has acquired a resource, it can after only request resources of higher ordering. It cannot request a resource of lower order.

    Formally, suppose we have a set of resources R. Then we define an function `P : R -> N`. If a process is holding resource Ri, it can only request a resource Rj if `P(Ri) < P(Rj)`. Using a proof-by-contradiction, one can then show that a circular wait with this condition is impossible.

    Note that it is necessary that applications respect this ordering when requesting resources. If they do not, the OS denies the resource and delivers a warning to the program.

## Deadlock Avoidance

Deadlocks can also be avoided by having upfront knowledge about the type and number of resources that a process will request. Upon creation, each process declares the maximum number of resources of each type that it will use. With this information, the system can guarantee that a deadlock will never happen. This can be done using the Banker's algorithm.

## Banker's Algorithm

Deadlock can be avoided using the Banker's Algorithm. With this algorithm, we have a state defined as
    - the total number of resources of each type
    - the resources currently allocated to each process
    - the maximum number of resources of each type that each process may need
    - the maximum number of resources they can still request
    - the maximum number of resources of each type currently available

For example, for resources A, B, and C with processes P0, P1, and P2, we might have the following state:

    |A| = 10, |B| = 5, |C| = 7
    Maximum         Allocated       Need = Max - Alloc      Available
    A   B   C       A   B   C       A   B   C               A   B   C
P0  7   5   3       0   1   0       7   4   3               8   4   5
P1  3   2   2       2   0   0       1   2   2
P2  4   3   3       0   0   2       4   3   1

So, we get two big matrices. The maximum demand matrix M, and the currently allocated matrix A. Indexing M[i] and A[i] give the row for process i in the matrices. We also have the Need matrix defined as Max - Alloc. We also have a vector of available resources, which measures the number of currently unused resources.

Defn: Given two rows X and Y, we will say `X <= Y` if `X[i] <= Y[i]` for each i. So this is just the lexicographical ordering.

Given a state S, we can check if it is a safe state using the following algorithm:

1. Mark all processes as unfinished
2. Repeat until you get stuck or all processes can be finished:
        Find an unfinished process Pi whose need vector can be satisfied. That is `Need[i] <= Avail`.
        Add that process to the safe sequence, release all its resources, and mark it is finished.
3. Return "safe" if all processes can be finished. If so, then you have a safe sequence. If not, then the state is unsafe.

Now, for a request vector R for a process i, we can check if satisfying that request leads to a safe state.
1. Check if the request vector is leq the process's need vector. That is, if `Request <= Need[i]`. If not, then the request is invalid, and flag an error.
2. If `Request <= Avaliable`, then proceed. Else, the request cannot be satisfied with the current resources, so it must wait.
3. If we have succeeded so far, then update the state as if the request were granted. This will update the Allocation, Available, and Need information.
4. Check if the new state is a safe state. If so, then accept the allocation. Else, have the process will have to wait.

So, banker's algorithm. Very straightforward.

## Deadlock Detection

For a system with a single resource of each type, deadlocks can be detected by checking for cycles in the resource-allocation graph, as usual.

For a system with multiple resources of each type, we can adapt the banker's algorithm.

We have
- Allocation vector, which has the number of available resources for each type
- Allocation matrix - a matrix of the number of allocated resources for each process
- Request matrix - a matrix with the number of requested resources for each process

Now run the safe state check algorithm with the Need matrix replaced by the Request matrix. If a safe sequence is found, then the state is not in a deadlock. If one is not found, then the unfinished processes are in a deadlock.

If a deadlock is likely to occur frequently, then a deadlock detection algorithm should be run frequently. Until a deadlock is broken, processes and their resources will be idling in the deadlock, and other processes may get caught in the deadlock until the system freezes.

## Deadlock Recovery

There are generally two ways of recovoring from a deadlock: cancel one or more of the processes in the deadlock, or preempt some of the resources held by processes in the deadlock.

For aborting processes, there are two general techniques:
- Abort all deadlocked processes. This breaks the deadlock, but at the cost of loosing all the work that the processes have done so far.
- Abort one process at a time until the deadlock is broken. This minimizes the number of processes that have to be aborted, but the deadlock detection algorithm has to be run after each process is aborted. The selection of which process to abort first depends on many factors, such as its priority, current running time, and the types of resources it has been using.

Note that terminating a process can leave any resources it has been using in an inconsistent state.

For preemption, resources are preempted from the deadlocked processes until the cycle is broken. For this, several questions are asked, such as
- Which victim process do we select?
- How do we restart the preempted process? Simply abort it and start again from scratch?
- Starvation. How do we ensure that this process isn't always the victim process when deadlocks occur?

# Main Memory

The speed up memory access, the processor has several layers of *caches* between itself and main memory. If a piece of information is not in the caches, this causes a memory stall because it has to be retrieved from RAM.

In order to run a multiprocess computer, multiple processes need to be loaded into main memory at once. For security reasons, we need to ensure that each process can only access its own memory in RAM. Note that these security features are usually implemented in the hardware, not in the OS.

## Code Location

A program is an executable file on disk. When run, that file is converted into a process and loaded into main memory. Generally, the process can be anywhere in main memory, so how do we resolve the memory addresses in the executable? There are three ways:

- *Absolute code* hardcodes the memory addresses into the binary. This is only possible if you know where the process will reside in memory at compile time. Because in general you don't know this, this approach is not used very often. Compile time.
- *Relocatable code* gives all memory references as relative positions (say, from the beginning of the program). These relative addresses are then resolved at load time when the program is loaded into memory. Load time.
- Sometimes processes can be moved around in memory once they are already running. In this case, addresses are resolved at execution time, which requires special hardware support. Most operating systems use this approach actually.

In the first two cases of compile-time and load-time address resolving, the memory addresses generated by the CPU match those of the hardware. In the execution-time case, this is not so in general. In this case, we distinguish between the addresses generated by the CPU (the *virtual addresses*), and the actual *physical address* in memory. The *memory-management unit* (MMU) is responsible for converting virtual addresses to physical addresses, and operates transparently from user programs.

The *virtual address space* is the set of addresses referenced by a program. The *physical address space* is the set of physical addresses corresponding to those virtual addresses. This abstraction between the virtual addresses of a program and the physical addresses of memory is one of the most important ideas in modern OS development.

## Shared Libraries

A technqiue called *dynamic loading* can reduce the memory size of a process. With this technique, all procedures are kept in the executable in a relocatable load format. When the program calls one of these procedures, it checks to see if the routine has been loaded. If not, then the loader loads the procedure into memory and passes control to it. This technqiue, which might be called lazy loading, only loads parts of the executable into memory as needed.

With *static linking*, all system libraries are linked into the executable at link time. With *dynamic linking*, this is not done. Instead, *stubs* for each procedure for the library are inserted into the binary. When the stub is executed, it loads the routine from disk and replaces itself with the routine. This reduces the size of the executable on disk, and allows for transparently updating the library without relinking the code. Libraries that can be used this way are known as *shared libraries*. Generally, dynamic linking requires support from the OS.

## Swapping

A process must be in memory to execute. However, a process can be *swapped out* temporarily into a *backing store* (usually the disk). These processes are stored in the ready queue. When a scheduler selects a process to run next, the dispatcher checks if it is in main memory. If not, it swaps the process in from disk, and possible swaps out another process from memory to disk to make room for it. So the swapper will swap out entire processes from memory to disk and back again. Later on, we will see the pager, which only swaps in and out portions of a processes memory. Swapping allows for the total address spaces of the processes to exceed the physical memory on the system. Processes that currently do not fit into memory will merely be swapped out to disk.

## Memory Allocation

Space for the OS and user processes must be allocated in memory. The hardware must also prevent processes from accessing other processes's memory and the memory of the kernel. We now look at several techniques for doing so.

## Contigious Memory Allocation

With *contiguous memory allocation*, each process is allocated a contiguous piece of memory.

With this technique, the virtual memory of the process goes from a range of 0 to some length. The MMU has a *limit register* (contains length) and *relocation register* (contains offset) that it uses to convert virtual addresses to physical addresses. First, it checks if the virtual address is less than the limit. If not, a trap is generated. If it is valid, the virtual address is added to the relocation register, which is then the physical address. Each process has its own limit and relocation values, which are loaded into the MMU registers from the PCB during a context switch.

Initially, the memory is empty and one large hole. When a process is loaded into memory, it splits it into two holes (depending on where it is placed). As more processes are loaded, more and more holes are created. When processes are terminated, they are removed from memory, and a hole is added back in. Under this scheme, we need to select which hole is best for the current process. This is an instance of the *dynamic storage-allocation problem*, and there are several techniques:

- First-fit: Choose the first hole that is big enough. This generally requires searching through a portion of the list.
- Best-fit: Choose the smallest hole that is big enough.
- Worst-fit: Choose the largest hole that is big enough. This produces a larger leftover hole than the best-fit.

First-fit and best-fit suffer from *external fragmentation*: the memory could be split apart into many holes that are too small to be useful. In this case, there may be enough free memory to allocate another process, but none of the holes are large enough on their own. This can be solved by *compaction*, which combines all the holes together into one big hole. This is very easy to do using the relocation address.

Another problem is *internal fragmentation*. In general, keeping track of small holes has more overhead than the size of the hole itself. To prevent this, memory is usually split into fixed sized blocks, and each process is allocated a certain number of these blocks. However, the process may not use all the space within its last block, which leads to *internal fragmentation*.

## Segmentation

*Segmentation* splits the logical memory into various segments. To access something in a segment, the programmer then refers to a segment number, and an offset within the segment. For example, a C compiler generally creates different segments for the text, data, stack, heap, and external libraries. The loader than gives each of these segments a segment number.

The MMU maps the (segment number, offset) pair to a physical address. This is implemented using a *segment table* (which is stored in the PCB). Each entry in the table contains a *segment base* and *segment limit*. The base is the address of the start of the segment in physical memory, and the limit is the length of the segment. Given a (segment number, offset) pair, the MMU finds the segment entry for the number. Then it checks if the offset is within the limit, and if so, adds the base to the offset to get the physical memory address.

Contiguous allocation is essentially a special case of segmentation with only one segment.

Segmentation allows noncontiguous allocation of process memory. It has several advantages over contiguous allocation. Each segment can have its own access controls (eg. the text segment can be read-only), and sharing code between processes is very easy. For example, if multiple processes want to access a shared dynamic library, the shared library can simply be put in one segment that all processes can access. Internal and external fragmentation continue to exist, but are not as bad as contiguous allocation.

## Paging

Paging is another non-contiguous allocation technique, but has no external fragmentation or compaction. Thus, it is very popular on modern operating systems.

The general idea is break the physical memory into fixed size *frames*, and the virtual memory into equal sized *pages*. Every virtual memory address is then split apart into a *page number* and *page offset*. The page number is then used to index into the *page table* (again stored in the PCB), which converts the page number to a corresponding frame number. The frame number and page offset are then combined again, which gives the physical address.

The page size is always a power of 2, say 2^n. If the virtual memory size is 2^m, then the top m - n bits of a memory address are the page number, and the lower n bits are the page offset.

Note that since the physical memory is split evenly into frames, we can fill any frame we want with a page, so there is no external fragmentation. However, there is some internal fragmentation, since the last page of a process may not be filled all the way. At worst, there will be almost an extra frame wasted per process. Having smaller pages will reduce the internal fragmentation, but smaller pages also have higher overhead and worse caching. So, as always, there is a tradeoff.

Note that with this setup, the process is unable to access memory outside of its own, because it can only access memory within its own page table. We can also specify protection settings for each page (eg. read-write, read-only, execute-only, etc.) which is also stored within the page table. Most systems also have a *page-table length register*, which contains the length of the page table and is used to check that the virtual address is in the valid range for the process.

The OS keeps track of which frames are allocated using a *frame table*. The table has one entry per frame, and indicates if the frame is allocated or not, and if so, to which process.

For systems with small page tables, the table can generally be implemented using registers in the processor. For systems with big page tables, the page table is kept in main memory, and the *page-table base register* points to the location within memory. However, doing a page-table lookup now requires a memory access. The solution is to use a *translation look-aside buffer* TLB, which is a cache for the most common page-table lookups. Some TLBs have *address-space identifiers* (ASIDs), which allow them to contain page information for multiple processes. This is nice, because it avoids a TLB flush for every context switch.

*Reentrant* or pure code can be shared between processes by having them reference the same page, since the code does not modify itself.

There are several data structures used to implement page tables. They include:
- Hierarchal paging, which uses several layers of paged page-tables. Doesn't work for 64-bit processes
- Hashed page tables. The page table is implemented as one big hash-map
- Inverted page table. Normally, each process needs to have its own page table. We can instead combine all the page tables together for one large inverted page table for the entire system. This means that each entry in the table needs to have an ASID to ensure that address references are valid.

# Paging

Paging has several distinct advantages aside from what we have already seen. In particular, paging allows for a process to not entirely be in memory while it executes. Instead, we can have a subset of pages in memory with the rest of the pages on disk, and then bring in pages as they are used. This allows each process to pretend it has access to the entire physical memory, when in reality it doesn't.

- Programs are no longer contrained by the physical memory
- Using demand paging, programs will use less memory, since only the needed parts of the process will be in memory. This increases the degree of multiprogramming, since now we can run more processes with all the extra memory.
- Less I/O is needed, since entire processes no longer need to be swapped out

The usual stack - space - heap - data - text view of memory is a virtual address space. This address space is *sparse*, because it contains a hole.

- Processes can share libraries by using the same pages
- Likewise, processes can share memory very easily using paging
- Pages can be shared during process creation from `fork()`

The *pager* is the program that moves pages in and out of memory, and we say a page is "paged" in or "paged" out.

*Demand paging* only loads pages into memory as they are referenced. Pages that are never referenced are left on disk and never used. Normally, the pager guesses which pages from a program will be used, and loads those into memory first (such as the `main()` function, for example). With *pure demand paging*, no pages are loaded initially, and all pages need to be loaded as needed. Normally, the page-table maps page numbers to frame numbers. However, some pages will be on disk and not in physical memory. In this case, the page-table contains information that indicates this. We say a page is *memory resident* if it is inside memory.

A *page fault* is when a process tries to access a page that is not in main memory. In this case, the hardware generates a trap, which the kernel catches. The kernel then loads the desired page from memory, updates the page-table, and restarts the instruction that caused the fault.

Note that it is possible for an instruction to cause multiple page faults, say one for the instruction itself and then several for its arguments. However, programs tend to have *locality of reference*, so this doesn't happen that much. (Locality of reference states that pages that have been referenced recently or are in close proximity to currenty pages are likely to be referenced again in the future.) A big problem is with instructions that modify multiple memory addresses. For example, there is an `MVC` instruction that can move up to 256 bytes of memory from one page to another. A page fault could occur in the middle of this instruction. If the source and destination buffers overlap, we cannot just restart the instruction once the page has been paged in. One solution is to read from the beginning and end of the array to trigger a page fault preemptively.

The *swap space* is the portion of the disk reserved for paging. Due to its structure, it generally has faster access times than other parts of disk. In some systems, text pages are paged in and out from the file system, and stack and heap segments are paged in and out from the swap.

A page fault can take a significant amount of time to service, so it is important that the page-fault rate is as low as possible. This is because the OS now has to do some I/O for the process, and so the process stops running and is put in the waiting queue. Then, once the memory has been paged in, the process is woken up, put in the ready queue, and rescheduled. So a big time.

## Copy-On-Write

Normally, process creation using `fork()` is very expensive, the entire address space of a process needs to be copied over. In many cases this isn't even necessary, since the child then calls `exec()`, which overwrites the address space anyway. With paging, this overhead can be reduced. Initially after a call to `fork()`, the child shares all pages with the parent, and these pages are marked as *copy-on-write*. If either process writes to one of these pages, a copy is created, and the changes are written to the copy. Some pages, such as those for text, are read-only, and can be shared without any problems. This is similar to the old `vfork()` command, which wouldn't copy any pages in anticipation of `exec()` being called.

## Page Replacement

Suppose a page fault occurs, and the kernel tries to load the page from memory, but there are no empty frames! In this case, we need to choose a victim frame and overwrite it with the new page. The choice of which victim to choose depends on our *page replacement algorithm*.

Note that a page replacement requires two page transfers. One to retrieve the requested page, and one to write the victim page to disk. However, we can improve this by giving each page a *dirty bit*. This bit indicates if the page has been written to since it was brought into memory. If the victim page has a set dirty bit, then it must be written back to the disk, but if it does not, then the page hasn't been modified, so we can freely overwrite it.

A *frame-allocation algorithm* determines how many frames are allocated to each process.

In general, we want a page-replacement algorithm with the fewest page faults. We can simulate this using a *reference string*, which is a string of page numbers generated by a program. In general, as the number of frames increases, the number of page faults will decrease. However, this is not always the case. There are a few algorithms that suffer from *Belady's anomaly*: as the number of frames increases, the page fault rate might increase too. Note that there is a general class of algorithms, called *stack algorithms*, that never suffer from Belady's anomaly. For stack algorithms, the set of pages that would be in memory for *n* frames is always a subset of the set of pages that would be in memory for *n* + 1 frames. OPT and LRU are stack algorithms, so they don't suffer from Belady's anomaly.

- FIFO: The pages form a queue. New pages are added onto the end of the queue. When a page needs to be replaced, the page at the front of the queue (which was brought in first) is chosen as the victim. This algorithm doesn't have great performance, and suffers from Belady's anomaly.
- OPT: This is the optimal page-replacement algorithm. When a victim needs to be selected, choose the page that won't be needed for the longest period of time. Unfortunately of course, OPT requires knowledge of the future, which isn't always available.
- LRU: The pages form a stack, with the most recently used page at the top, and the least recently used at the bottom. When a page needs to be replaced, the least recently used page at the bottom is selected as the victim. This algorithm may require updating the stack at every memory reference, and so is quite computationally expensive.
- SEC: The second-chance algorithm uses a reference bit to approximate LRU. Every page has a reference bit, which is set to 1 when the page is referenced. The pages in memory are stored in a circular queue. When a victim page needs to be selected, the pointer in the queue checks the current page to see if it has its reference bit set. If so, it gives it a second chance, clears the reference bit, and checks the next page. It continues until it finds a page with a 0 reference bit, and replaces that. This is commonly called the *clock* algorithm. One can also enhance the second chance algorithm to take into account the dirty bit, and favour for replacement pages which are not dirty (since those don't need to be flushed). If the clock pointer is moving fast, then there are many page faults, and each page is referenced in between the time that its bit is cleared and when it is checked again. If the clock pointer is moving slowly, then there are few page faults, and only a few pages are being referenced. Second chance degenerates to FIFO if all the second chance bits are set, and FIFO suffers from Belady's anomaly, so second chance does too.

Some systems also keep a pool of free-frames around, which reduces the I/O time, since the process can restart as soon as the new page has been read into a free frame and before the old one is freed.

Some specialized applications, such as databases, are better at managing their own I/O, since they know their access patterns better than the OS does.

## Frame Allocation

Generally, each process should have a minimum number of frames, where the minimum number is the maximum number of memory references that a single instruction can make. This way, we can be sure that all of the pages for an instruction are in memory before we restart it. Thus, the minimum number of frames depends on the computer architecture. For the maximum number of frames, generally each process is allocated frames in proportion to its memory size.

Page-replacement algorithms can either be local or global. *Local* algorithms will only select victim pages from a processes own pages. *Global* algorithms can steal frames from any process. Global replacement may result in better throughput, but now the page faulting rate of a process is affected by the page faulting rate of other processes.

## Thrashing

A process can *thrash* if it spends more time doing paging I/O than executing on the CPU. In this case, the process does not have enough frames to contain all its active pages. For example, suppose we have multiple processes running on a computer. We add one, which steals some frames from other processes. Those processes need those frames, so they all line up in a device queue to retrieve them. The scheduler then notices that CPU activity is down, so it schedules more processes, which makes the problem even worse. This can be prevented using a local replacement algorithm.

Note that processes move from locality to locality. That is, there are certain parts of memory the process will access more (say, parts of the same array, or within the same function), and then move on to other parts (this is the idea behind caching). The *working set model* works under this assumption. Moniter the last delta frames referenced by a process. Call this the working set. Do not victimize any pages in the working set of a process, and a process should be run iff its working set is in memory. Of course, choosing a suitable delta is difficult in general. Thrashing can be prevented by noting the page-fault frequency of a process. If it is too high, then the process is thrashing, and we need to give it more frames. If it is too low, then we can take some frames away and give them to another process.

## Memory-Mapping

A file can be memory-mapped, which means that they are copied and modified in memory. Periodically, the file is written to the disk. Using a memory-mapped file decreases the overhead of accessing or modifying a file, since the process only needs to read from memory, not the disk. This also allows easily sharing the file between multiple processes, which might require synchronization.

Device registers can also be mapped to memory addresses, which is called *memory-mapped I/O*.

## Other Stuff

When a process is started initially or is swapped back in, all of its memory is on disk. Each memory access would then be an immediate page fault for the first little while. *Prepaging* is an attempt to bring in pages to prevent this high initial paging.

In general, it is better to increase program locality, because this will reduce page faults (and help caching too).

Small page size decreases internal fragmentation, but increase the page fault rate and the page-table size.

When performing I/O from a device to memory, it is sometimes necessary to lock a page into memory. Each frame has a lock bit, and frames that have this bit set cannot be replaced.

# File Systems

Disk Structure

The most common harddrive used today is a *magnetic disk*. Each disk has *platter* is like a CD, and is covered with magnetic material on both sides. The surface of each platter is divided into circular *tracks*, and each track is subdivided into *sectors*. A read-write head moves above the surface of every platter, and each head is attached to a *disk arm*, which moves the heads in unison. A *cylinder* is the set of tracks on all platters of a certain radius.

Each drive has a cylinder/head/sector (C/H/S) count. Multiply them all together to get the number of sectors on a drive.

When in use, each drive rotates at a certain RPM. *Transfer rate* or *bandwidth* is the rate of transfer of data from the drive to the computer. *Positioning time* or *random-access-time* consists of two parts:
- *seek time* is the time necessary to move the arm to the desired cylinder
- *rotational latency* is the time required for the desired sector to rotate to the disk head

A disk drive is connected to the computer using an *I/O bus*. *Controllers* are special processors that carry out the data transfer on the bus. The *host controller* connects the bus to the computer, and the *disk controller* connects the disk to the bus.

Generally, it is faster if files are stored contiguously on disk. This means the head only has to do one seek and rotate, and can then begin reading data. If the file is scattered around on the disk, the head might have to do one seek and rotate for each sector of the file.

Magnetic disk drives are addressed as one large array of *logical blocks*, where a block is the smallest unit of transfer to and from the disk. The size of a block is usually 512 bytes. This array of blocks is mapped onto sequential sectors of the disk. For example, sector 0 might be the first sector of the first track on the outermost cylinder. The mapping then proceeds through that track, then through the rest of the tracks in the cylinder, then in a smaller cylinder.

*Constant linear velocity*: speeds the disk up as the head moves from the outside to the inside to keep the data rate constant
*Constant angular velocity*: increases the density of bits from the inside to the out to keep the data rate constant

## Disk Scheduling

Whenever a process requests disk I/O, it issues a system call to the OS. The request specifies the following information:

- Whether the operation is a read or a write
- What the disk address for the transfer is
- What the memory address for the transfer is
- The number of sectors that should be transferred (the size of the transfer)

All requests will be sent to the device queue for the drive. Thus, as with the ready queue, the choice of which request to service first is determined by a disk-scheduling algorithm. Note that these algorithms are essentially instances of the travelling salesman problem.

- *First-come first served*: With this algorithm, requests form a queue and are serviced in a first-come-first-served basis. This algorithm is fair and doesn't lead to starvation, but may not be the fastest. It is possible that the disk head could swing wildly from one location to another and back again.

- *Shortest-seek-time first*: With this algorithm, the head moves to the pending request with the closest position to the current head location. It is essentially a greedy algorithm. This algorithm is similar to SJF, and like SJF it can lead to starvation. If a request is far away from the current position, the head may constantly choose requests closer as they arrive and never make it to the far away one.

- *Scan algorithm*: This algorithm starts the head at one end of the disk (say the outside), and then moves inward toward the center servicing requests. Once it reaches the other end of the disk, it reverses and goes the other way.

- *C-scan*: This is the *circular scan algorithm*. It seems odd to retrace recent steps once you reach the inside of the cylinder and reverse. So instead, you go back to the beginning, and start rescanning from there (like you wrap around from the beginning to the end and continue from there).

- *Look and C-Look*: These are variants of scan and C-scan that only go as far as the furtherst request before reversing direction.

SSTF is good for low disk I/O, while Scan CScan are better for large I/O, because they prevent starvation.

If performance were the only consideration, then all scheduling could be done by the disk hardware. However, some requests might be more urgent than others, so the OS has some control of scheduling.

## Disk Formatting

*Low-level formatting* fills the disk with a special data structure for each sector. The data structure consists of a header, a data area (usually 512 bytes in size), and a trailer. The header and trailer contain sector information and an error-correcting code. This code contains information that is calculated when data is written, and then checked when data is read. If there is a mismatch, the disk might be able to fix the bad bytes. If not, then the drive may be able to fix the sector. If so, this is a *soft error*.

Next, the operating system creates *partitions* on the disk into groups of cylinders. Next, it creates a *file system* on the disk. This is called *logical formatting*. Most file systems group blocks together into chunks called *clusters*. Disk I/O is done by block, but file system I/O is done by cluster.

Some specialized applications, like databases, work better with *raw disk*, which has no file system structure.

## Booting

System usually have a small *bootloader* stored in a read-only part of the disk. This small bootloader loads the full bootloader, which then initializes the registers, device controllers, and starts the kernel.

## Bad Blocks

Most disks come with some *bad sectors*, often called bad blocks. The OS or disk keeps track of these bad blocks and prevents data from being written to them. Most disks also come with several spare empty sectors. When a sector becomes bad, the disk remaps its block to a spare good sector. This is called *sector sparing* or *forwarding*. For optimization purposes, most disks have some spare sectors on each cylinder so the replacement will be close to the original sector. A *hard error* is when a sector turns bad, and the file in that sector is then permanently corrupted.

## Swap Space

Swap space for paging or swapping can be implemented as a file, or as a separate partition. The partition is faster because it does not have the file system structure, but also cannot be easily resized or moved around. Solaris, for example, doesn't write text or read-only pages to swp (why write it to swap when you can just read it back from the file system), and only writes stack and heap pages.

## BSD

In BSD, each chunk is broken into multiple *entries*. Each entry stores a filename and an *inode* pointer. An inode is a fixed length structure that contains information about the file, including
- read/write permissions
- owners
- creation and modification timestamps
- file size
- pointers to the data blocks

A *hard link* is a pointer from a directory to an inode. This prevents directly linking to the data of a file, and is preserved even if the file is renamed. Hard links cannot point to other directories to prevent loops.

A *soft link* or *symbolic link* is a file that contains another filename. If the file is renamed or deleted, then the soft link is broken.

## RAID

RAID stands for *redundant arrays of indepedent disks* and refer to disk setups that increase storage capacity, performance, and reliability. RAID disks can be attached directly to the computer or through its own controller.

RAID can be used to increase system redundancy. This way, even if a disk fails, its data can still be recovered. The simplest way to do this is with *mirroring*, which keeps a second copy of each disk. Each write is performed to two disks: the original and the *mirrored volume*. This significantly increases the *mean time to data loss*, but note that disk failure may not be independent. For example, natural disaster or power failure will affect both disks. Note that mirroring also increases data read time, since reads can be parallelized between the two disks.

Another method is *data striping*. This splits the data being written to each disk. For example, with *bit-level striping*, half of each byte may be written to one disk, and half to the other. This doubles the access rate time, since we read or write one half of the byte from one disk and the other half from the other. Another option is *block-level striping*, which writes alternating blocks to each disk.

Mirroring improves reliability and performance, and striping improves performance. We can combine these techniques to get different RAID levels:

- RAID 0: uses block striping with no redundancy
- RAID 1: mirroring with no striping
- RAID 2: use bit-level striping, with extra parity bit disks. If a main disk fails, its portion of the bytes can be reconstructed using the other disks and the parity bits.
- RAID 3: bit level striping with extra parity bit disk. Better than RAID 2 for some reason
- RAID 4: block-level striping with block parity disks.
- RAID 5: block-level striping with block parity, but the parity is mixed among the disks. Note that no disk stores parity for its own data (to prevent loss of data and parity)
- RAID 6: like RAID 5, but can deal with two disk failures

Note that RAID protects against hardware failure, but not software errors. File integrity can be enforced using *checksums*.

Unfair in disk requests: paging, writing metadata, servicing real-time processes.
