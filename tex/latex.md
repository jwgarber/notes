Packages
Unicode Math
STIX Fonts

One advantage of LuaLaTeX over XeTeX is that the former has better support for the microtype package.

If you want to use CM, make sure you enable the Latin Modern fonts (lmodern)

classic thesis
Tufte style

memoir
KOMA script

Being independent of ZFC doesn't mean the statement is true or false at some level. If, for example, the Riemann hypothesis were independent, then that means it's true! Indeed, if it were false, we could after some finite amount of time calculate the zero off the line, which would contradict it being independent! Same thing with Goldback Conjecture. These are Pi_1 statements.
