Notes for SICP

I think I'm going to do this one first. The ebook is prettier, it is a slower introduction to Lisp, and it's a CS classic.

# Preface

Some philosophical stuff, lisp is awesome.

# Chapter 1

Everything is Lisp is written in *prefix notation*. Should just state that for the record.

Note that in a function application, it is often useful for clarity to write the arguments all lined up, like this

```racket
(fn arg1
    arg2
    ...
    argN)
```

## Evaluation Techniques

Most programs evaluate functions according to *applicative-order evaluation*. This evaluates both arguments of a function first, and then applies the function to those arguments. This is a strict evaluation.

Some lazy languages, like Haskell, instead use *normal-order evaluation*, which expands every computation, and then evaluates it as needed at the end.

```racket
(define (my-abs x)
  (cond [(> x 0) x]
        [(= x 0) 0]
        [(< x 0) (- x)]))
```

```racket
(define (factorial n)
  (if (= n 0)
      1
      (* n (factorial (sub1 n)))))
```

## Recursive Functions

Note that all recursive functions generate a tree of recursive calls. For example, we can define the basic factorial function recursively.

```racket
(define (factorial n)
  (if (= n 0)
      1
      (* n (factorial (sub1 n)))))
```

Calling `(factorial 5)` generates the following recursive tree.

```
(factorial 5)
      |
      v
(factorial 4)
      |
      v
(factorial 3)
      |
      v
(factorial 2)
      |
      v
(factorial 1)
      |
      v
(factorial 0)
```

This is an example of *linear recursion*, because the number of recursive calls form a straight path (the number of calls is linear in the size of the argument).

Recursive functions can lead to more complicated recursion trees. For example, consider the Fibonacci function.

```racket
(define (fib n)
  (cond [(= n 0) 0]
        [(= n 1) 1]
        [else (+ (fib (- n 1)
                 (fib (- n 2))))]))
```

Calling `(fib 5)` generates a binary recursive tree, since each call needs to make two subcalls.

For more general recursive functions, the recursive tree could look even crazier, having a different number of subcalls each time.

However, in all cases, the time complexity of a recursive call is proportional to the number of vertices in the recursive tree, and the space complexity (from the stack) is proportional to the maximum depth of the tree (since at each vertex we only need to remember which functions called us, ie the parents in the tree).

Note that it is possible for a recursive procedure to give rise to an iterative process. This gives rise to *tail recursion*, which ensures the procedure runs in constant space, even though it is recursive. Other languages like C need to resort to loops to describe iterative processes because they don't have tail recursion; describing these processes recursively leads to O(n) space, not constant.

## Iterative Functions

Some recursive functions can also be described iteratively. For example, the factorial function can be written like this.

```racket
(define (factorial n)
  (define (iter product counter)
    (if (> counter n)
        product
        (iter (* counter product)
              (add1 counter))))
  (iter 1 1))
```

Translating linear recursive functions to iterative ones is rather straightforward. For more complicated trees it may be rather tricky. However, this is an iterative version of the Fibonacci function.

```racket
(define (fib n)
  (define (iter a b counter)
    (if (zero? counter)
        a
        (iter b (+ a b) (sub1 counter))))
  (iter 0 1 n))
```

Note that though these functions are implemented using recursion, they describe iterative processes. Iterative processes rely on state, any single function call will sum up the entire state of the evaluation. This is different than recursion, where you need to take into account every function call to get the total picture.

Iterative functions have a *state* and rules for transforming that state from one iteration to the next. You usually have to define some iterative helper, and then seed that helper with the initial values for the computation.

## Tail Recursion

Consider the above `iter` function in `fib`. It is recursive, but it also satisfies a special property. In many cases, the result of the function is simply the result of calling the same function with different arguments. So it's just "return the result of this function". Normally, an extra call frame needs to be added to the stack for every recursive call. However, because we are simply returning a result from the same function with different arguments, the existing call frame can be modified in place. This reduces the total storage requirements from linear to constant space, just like using a loop. This is called *tail-recursion*, and increases the performance of your code. Note however, that this technique only works with iterative processes. The nice recursive ones from above do not work like this.

Thus, (assuming all math operations are constant time), we get the following general framework for recursive ops

- recursive definition: usually exponential time, linear space
- memoizatian: linear time, linear space
- iterative version: linear time, constant space
- closed form: constant time and space

This exactly works for various implementations of the Fibonacci function, for example.

The basic idea of writing an iterative version is to keep track of the previous values, and then continually combine them until it works.

# 1.2.4

Exponentiation can be calculated in the straightforward recursive manner.

```racket
(define (pow b n)
  (if (zero? n)
      1
      (* b (pow b (sub1 n)))))
```

This is a linear recursive process, which takes `theta(n)` time and space. We can reformulate it into a linear process.

```racket
(define (pow b n)
  ; counter goes from n to 0, since all we need is n iterations
  (define (iter prod counter)
    (if (zero? counter)
        prod
        (iter (* prod b) (sub1 counter))))
  (iter 1 n))
```

This takes `theta(n)` time as before, but only `theta(1)` space from tail recursion.

However, we can do better. Using exponentiation by squaring, we can get a `theta(log(n))` solution (assuming math operations are constant time).

```racket
(define (fast-pow b n)
  (cond
    [(zero? n) 1]   ; base case
    ; recursive cases
    [(even? n) (square (fast-pow b (/ n 2)))]
    [else (* b (fast-pow b (sub1 n)))]))
```

This procedure takes up `theta(log(n))` time and space. Note that it is still linearly recursive, since only one of the recursive cases is called in each call.

As above, we can write an iterative version, which takes `theta(log(n)))` time but `O(1)` space.

```racket
; 1.16
(define (fast-pow-iter b n)
  (define (iter )))
```

## 1.3

*Higher-order functions* are ones that manipulate other functions (accept them as arguments or return them as values). The `map` function is higher-order, for example.

```racket
; sum of f(i) from a to b, where next produces the next value
(define (sum fn a next b)
  (if (> a b)
      0
      (+ (fn a)
         (sum fn (next a) next b))))

(define (sum-cubes a b)
  (sum cube a add1 b))

(define (sum-integers a b)
  (sum (lambda (x) x) a add1 b))

(define (pi-sum a b)
  (define (pi-term x)
    (/ 1.0 (* x (+ x 2))))
  (define (pi-next x)
    (+ x 4))
  (sum pi-term a pi-next b))

; approximation of an integral
(define (integral f a b dx)
  (* (sum f (+ a (/ dx 2.0)) (curry + x) b)
     dx))
```

Blah, blah, blah, skipping a bunch of stuff. Objects in a programming language are said to be *first-class* if they can be

- named by identifiers
- passed as arguments to functions
- returned from functions
- stored in data structures

```racket
(define (compose f g)
  (lambda (x) (f (g x))))

(define (repeated fn n)
  (if (zero? n)
      (lambda (x) x)
      (compose fn (repeated fn (sub1 n)))))
```

Alright, skipping to chapter 2.

# Chapter 2

From an abstract point of view, what exactly is data? Consider the implementation of a rational number. Abstracting away the actual implementation of the number, it should have three functions `make-rat`, `numer`, and `denom`, and if `x = (make-rat n d)`, then

```
(numer x) / (denom x) = n / d
```

Thus, we have three functions, as well as an identity that those functions must satisfy. This uniquely sums up the representation of a rational number.

For example, consider a pair. A pair has three functions `cons`, `car`, and `cdr`, and for all `x`, `y`, these functions must satisfy

```
(car (cons x y)) = x
(cdr (cons x y)) = y
```

For example, we could implement these functions just using procedures.

```racket
(define (cons x y)
  (lambda (m)
    (match m
      [0 x]
      [1 y])))

(define (car z)
  (z 0))

(define (cdr z)
  (z 1))
```

Here, `cons` returns an unevaluated lambda function, which we then evaluate to get at the elements inside.

Here is an even more sophisticated, and perhaps more elegant, version.

```racket
; cons returns a function that accepts a function
; a -> a -> ((a -> a -> a) -> a)
(define (cons x y)
  (lambda (m) (m x y)))

; z is the lambda function
; it takes another function that
(define (car z)
  (z (lambda (p q) p)))

; as above
(define (cdr z)
  (z (lambda (p q) q)))
```

Holy crap, you can also implement natural numbers using lambdas.

```racket
; returns a function that takes any value, and then returns id
(define zero (lambda (f) (lambda (x) x)))

(define (add-1 n)
  (lambda (f) (lambda (x) (f ((n f) x)))))
```

These are called *Church numerals*. In fact, you can encode any data structure as lambda functions using a *Church encoding*. Lambda the ultimate, I suppose.

Example implementations of list functions. Note that lists are essentially just 1D trees, which explains their nice recursive properties.

```racket
; tail recursive
(define (list-ref lst n)
  (if (zero? n)
      (first lst)
      (list-ref (rest lst) (sub1 n))))

; not tail recursive
(define (length lst)
  (if (empty? lst)
      0
      (add1 (length (rest lst)))))

; iterative
(define (length lst)
  (define (length-iter a count)
    (if (empty? a)
        count
        (length-iter (rest a) (add1 count))))
  (length-iter lst 0))

(define (append list1 list2)
  (if (empty? list1)
      list2
      (cons (first list1) (append (rest list1) list2))))
```

## Mapping

```racket
(define (map func items)
  (if (empty? items)
      empty
      (cons (func (first items))
            (map func (rest items)))))
```

## Trees

```racket
(define (count-leaves tree)
  (match tree
    [(list) 0]
    [(cons x y) (+ (count-leaves x) (count-leaves y))]
    [_ 1]))
```

```haskell
-- The trees you can construct in SICP using cons look like this
data Tree a = Empty
            | Leaf a
            | Branch (Tree a) (Tree a)
            deriving (Show, Eq)

fringe :: Tree a -> [a]
fringe Empty = []
fringe (Leaf x) = [x]
fringe (Branch x y) = (fringe x) ++ (fringe y)
```

```racket
(define (scale-tree tree factor)
  (match tree
    [(list) empty]
    [(list x xs ...) (cons (scale-tree x factor) (scale-tree xs factor))]
    [x (* factor x)]))

(define (scale-tree tree factor)
  (map (lambda (sub-tree)
         (if (pair? sub-tree)
             (scale-tree sub-tree factor)
             (* sub-tree factor)))
       tree))
```

```haskell
powerset :: [a] -> [[a]]
powerset [] = [[]]
powerset (x : xs) = nox ++ (map ((:) x) nox)
                    where nox = (powerset xs)
```

## 2.2.3

We have already defined `map`. Now let's look at `filter` and `accumulate`.

```racket
(define (filter pred seq)
  (match seq
    [(list) empty]
    [(list x xs ...)
      (define other (filter pred xs))
      (if (pred x)
          (cons x other)
          other)]))

; initial is the base value
; this is a foldr
(define (accumulate op initial seq)
  (match seq
    [(list) initial]
    [(list x xs ...) (op x (accumulate op initial xs))]))

(define (enumerate-interval low high)
  (if (> low high)
      empty
      (cons low (enumerate-interval (add1 low) high))))

(define (enumerate-tree tree)
  (if (cons? tree)
      (apply append (map enumerate-tree tree))
      (list tree)))

(define (sum-odd-squares tree)
  (apply + (map square (filter odd? (enumerate-tree tree)))))

(define (even-fibs n)
  (filter even? (map fib (enumurate-interval 0 n))))

(define (square x)
  (* x x))

(define (fib n)
  (match n
    [0 0]
    [1 1]
    [n (+ (fib (sub1 n))
          (fib (- n 2)))]))

(define (list-fib-squares n)
  (map (compose1 square fib) (enumerate-interval 0 n)))

(define (product-squares-odd-elements seq)
  (apply * (map square (filter odd? seq))))

(define (highest-programmer-salary records)
  (accumulate max 0 (map salary (filter programmer? records))))
```

The moral of the story is you should almost never have to explicitly do recursion on a list. Most of the time, you can just compose the three higher-order list functions.

## Nested Mappings

```racket
(define (flatmap proc seq)
  (accumulate append empty (map proc seq)))

(define (flatmap proc seq)
  (apply append (map proc seq)))

(define flatmap (compose1 (curry apply append) map))

(define (prime-sum? pair)
  (prime? (first pair) (rest pair)))

(define (make-pair-sum pair)
  (list (car pair)
        (cdr pair)
        (+ (car pair) (cdr pair))))

(define (generate-pairs n)
  (flatmap (lambda (i)
             (map (lambda (j)
                    (list i j))
                  (enumerate-interval 1 (sub1 i))))
            (enumerate-interval 1 n)))

(define (prime-sum-pairs n)
  (map make-pair-sum (filter prime-sum? (generate-pairs n))))

(define prime-sum-pairs (compose1 (curry map make-pair-sum)
                                  (curry filter prime-sum?)
                                  generate-pairs))

(define (permutations s)
  (if (empty? s)
      (list empty)
      (flatmap (lambda (x)
                 (map (curry cons x)
                      (permutations (remove x s))))
               s)))

;(define (remove item seq)
;  (filter (lambda (x) (not (equal? item x))) seq))

(define (remove item seq)
  (filter (compose1 not (curry equal? item)) seq))
```

## 2.3.4 Huffman Encoding Trees

Consider encoding an alphabet in binary. The most obvious way is to use a *fixed width encoding*. In this technique, every character is identified with a sequence of bits, which is the same length for each character. For example, ASCII uses 7 bits per character, which can encode 128 different characters (2^7). This is nice, because we don't need a special "seperator" to indicate the spaces between characters (like a pause in Morse Code). Simply take a stream of bits, and read them off in 7-bit chunks starting from the beginning.

For example, if our alphabet consists of the letters A, B, C, D, E, F, G, and H, we could encode these with three bits per character.

A 000   B 001   C 010   D 011
E 100   F 101   G 110   H 111

With this code, the message

BACADAEAFABBAAAGAH

can be encoded as a string of 54 bits

001000010000011000100000101
000001001000000000110000111

Note that if you want to encode n character, you will need at least `ceil(log2(n))` bits per character.

Fixed-length codes are nice, but perhaps a bit wasteful. For example, the letter Z is the least common English letter in the alphabet, but it takes up exactly the same space as E, the most common. To same some space, we can use *variable-length* encodings, which gives each character a possible different-length encoding.

The only issue now is decoding. How do you know when you've reached the next character? You can include some sort of separater character, like a pause in Morse Code, or you can be more clever. You can design the codes so that no code forms the prefix of the code for another character. This way, you know unambigously when a character code ends. Such codes are called *prefix codes*. By the most commonly used characters the shortest codes, we can often have significant space savings.

For example, we can use the following prefix code:

A 0     B 100   C 1010  D 1011
E 1100  F 1101  G 1110  H 1111

With this encoding, the above message can be encoded using 42 bits, a 20% saving.

100010100101101100011
010100100000111001111

Suppose we have a bunch of characters, each with an associated weight (higher weight = more commonly used). One way of deriving an efficient prefix representation for this is using a Huffman encoding. A Huffman code can be represented as a binary decision tree where

- leaves are the symbols to be encoded
- branches have a set of the symbols at leaves below the branch

Taking the left child of a branch is 0, and taking the right is a 1.

Encoding Symbol:
    - Start at the root of the tree
    - Check to see which branch contains the symbol in its set, or is the leaf node
    - Take the branch, adding the appropriate bit
    - Continue until reach the leaf symbol

Decoding Bit Sequence:
    - Start at the beginning of the sequence, following left and right branches as you go along
    - Every time you reach a leaf node, write down the symbol, and repeat again from the root

## Generating Huffman Tree

Given an alphabet with relative frequencies for each symbol, how do we construct a Huffman tree? The idea is to have symbols with the lowest frequency farthest away from the root.

- Begin with a bunch of vertices, where each is weighted according to their relative frequency.
- Take the vertices with the two lowest weights, and make a vertex with the vertices as its left and right branches. The weight of this vertex is the sum of the weights of its branches.
- Continue this process until there is only one vertex left, which is the root of the tree.

Notice that this is a greedy algorithm. Also note that the resulting tree may not be unique, since there may be ties for the lowest weight, and the choice of which vertex goes on the left and right branches is arbitrary (from the symmetry of 0 and 1).

TODO finish this stuff on Huffman trees. Right now it requires more brain power than I can provide

One of the problems with this book is that it is so verbose. It really takes a long time to get anywhere, and then the ideas expressed aren't very concrete. This might be an artifact of this being a book for beginners, but it is still rather annoying. Not only that, but *a lot* of what we talk about in this book can be implemented more cleanly and correctly using static typing. For example, type tags. It is so much better to just create a sum type in the type system. Static typeing formalizes so many of the conventions and structures that we have in our heads.
