#lang racket

#|(module grammar racket)|#

;; First implementation, uses functions
(define (random-element lst)
  (list-ref lst (random (length lst))))

(define (one-of lst)
  (list (random-element lst)))

(define (sentence)
  (append (noun-phrase) (verb-phrase)))

(define (noun-phrase)
  (append (article) (ks adj) (noun) (ks pp)))

(define (verb-phrase)
  (append (verb) (noun-phrase)))

(define (pp)
  (append (prep) (noun-phrase)))

(define (adj)
  (one-of (quote (big little blue green adiabatic))))

(define (prep)
  (one-of (quote (to in by with on))))

(define (article)
  (one-of (quote (the a))))

(define (noun)
  (one-of (quote (man ball woman table))))

(define (verb)
  (one-of (quote (hit took saw liked))))

; equivalent to fn*
(define (ks fn)
  (if (= (random 2) 0)
      empty
      (append (fn) (ks fn))))

;; Second implementation, uses hash table and lists

; symbol -> rewrites
(define simple-grammar
  (hash 'sentence '((noun-phrase verb-phrase))
        'noun-phrase '((article noun))
        'verb-phrase '((verb noun-phrase))
        'article '(the a)
        'noun '(man ball woman table)
        'verb '(hit took saw liked)))


;; extending the grammar is super-duper easy
(define larger-grammar
  (hash 'sentence '((noun-phrase verb-phrase))
        'noun-phrase '((article adj* noun pp*) (name) (pronoun))
        'verb-phrase '((verb noun-phrase pp*))
        'pp* '(() (pp pp*))
        'adj* '(() (adj adj*))
        'pp '(prep noun-phrase)
        'prep '(to in by with on)
        'adj '(big little blue green adiabatic)
        'article '(the a)
        'name '(pat kim lee terry robin)
        'noun '(man ball woman table)
        'verb '(hit took saw liked)
        'pronoun '(he she it these those that)))

; basic grammar for propositional calculus
; totally correct, but there are so many recursive calls that generating a sentence takes a very long time. There is only one recursive case, which is 'atom.
(define prop-calculus
  (hash 'prop '(not and or implies atom)
        'not '(prop)
        'and '((prop prop))
        'or '((prop prop))
        'implies '((prop prop))
        'atom '(a b c)))


; the grammar used by generate
(define grammar larger-grammar)

; get a list of the possible rewrites for this linguistic category
#|(define (rewrites category)|#
  #|(rule-rhs (assoc category grammar)))|#

(define mappend (compose1 (curry apply append) map))

(define (generate phrase)
  (cond
    [(hash-has-key? grammar phrase) (generate (random-element (hash-ref grammar phrase)))]
    ; recursively apply to each element
    [(list? phrase) (mappend generate phrase)]
    ; if there is a rewrite, get a random element, and rewrite according to the phrase
    ; no rewrite, so just return a list
    [else (list phrase)]))



(define (generate-tree grammar phrase)
  (cond
    [(hash-has-key? grammar phrase) (cons phrase (generate-tree  grammar (random-element (hash-ref grammar phrase))))]
    ; recursively apply to each element
    [(list? phrase) (map (curry generate-tree grammar) phrase)]
    ; if there is a rewrite, get a random element, and rewrite according to the phrase
    ; no rewrite, so just return a list
    [else (list phrase)]))

; what the crap is going on?
; not having to lay out the types you expect is making this
; rather confusing. I like type signatures
(define (generate-all grammar phrase)
  (cond
    [(empty? phrase) (list empty)]
    [(hash-has-key? grammar phrase) (mappend (curry generate-all grammar) (hash-ref grammar phrase))]
    [(list? phrase) (combine-all (generate-all grammar (first phrase)) (generate-all grammar (rest phrase)))]
    [else (list (list phrase))]))


; Apply the given function to every element of set1 x set2,
; and return those elements as a list
; very easy to do in Haskell, how do we do it in Racket?
; cross-product fn set1 set2 = [fn x y | x <- set1, y <- set2]
; so we want fn(set1 x set2)
; still trying to get a point free way of doing this
(define (cross-product fn xlist ylist)
  (mappend (lambda (x)
             (map (curry fn x) ylist))
           xlist))

;; Return a list of lists formed by appending a y to an x
; In Haskell, it would look something like this
; combine-all xlist ylist = [ x ++ y | x <- xlist, y <- ylist ]
(define combine-all (curry cross-product append))

; the assoc essentially implements a hash table using lists
; it takes a key and a lists of lists, and returns the list whose first element is that key. It returns #f if no such list exists
