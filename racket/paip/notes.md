# Chapter 1

Lisp is awesome.

# Chapter 2

A basic grammar of English looks something like this:

```haskell
data Sentence = Sentence NounPhrase VerbPhrase
data NounPhrase = NounPhrase Article Noun
data VerbPhrase = VerbPhrase Verb NounPhrase

data Article = The
             | A
             | ...

data Noun = Man
          | Ball
          | Woman
          | Table
          | ...

data Verb = Hit
          | Took
          | Saw
          | Liked
          | ...
```

This is a *context-free phrase-structure grammar*, and the underlying paradigm is called *generative syntax*. This formalism is context-free because the rules apply anywhere, regardless of the surrounding words. This approach is generative because it defines the complete set of sentences of a language, as well as the nonsentences as well.

Suppose we want to add the following rules:

```
Noun-Phrase -> Article + Adj* + Noun + PP*
PP -> Prep + Noun-Phrase
Adj -> big, little, blue, green, ...
Prep -> to, in, by, with, ...
```

The `*` is called a *Kleene star*, and represents 0 or more of the previous symbol.

This is nice and all, but implementing these rules in terms of functions is starting to get rather complicated. We need a lot of Lisp to make sure everything follows the rules - ideally, everything should just depend on linguistic conventions

A better implementation is to construct the data rules, and worry later about how they will be processed.

There are two alternative approaches in developing programs:
- Use the most straightforward mapping of the problem directly into code
- Formalize the problem with natural notation, and then write an interpreter for the notation

The second approach is slightly more work, but gives you more extensible and elegant solutions.

So in general, write a data structure for the problem at hand, and then write functions that work with that structure.

# Chapter 2

- Be specific
- Use abstractions
- Be concise
- Use the provided tools
- Don't be obscure
- Be consistent

#
