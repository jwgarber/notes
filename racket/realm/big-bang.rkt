#lang racket

(require 2htdp/universe 2htdp/image)

(define (add-3-to-state current-state)
  (+ current-state 3))

(define (draw-a-ufo-onto-an-empty-scene current-state)
  (place-image IMAGE-of-UFO (/ WIDTH 2) current-state
               (empty-scene WIDTH HEIGHT)))

; empty-scene creates a scene WIDTH pixels wide and HEIGHT tall

(define IMAGE-of-UFO (cirle 20 "solid" "blue"))

(define (state-is-300 current-state)
  (>= current-state 300))

(big-bang 0     ; this is the initial state, and is updated as we go
          (on-tick add-3-to-state)
          (to-draw draw-a-ufo-onto-an-empty-scene)
          (stop-when state-is-300))

(string-append)



