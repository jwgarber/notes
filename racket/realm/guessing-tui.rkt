#lang racket

(define lower 1)
(define upper 100)

(define (start n m)
  (set! lower (min n m))
  (set! upper (max n m))
  (guess))


;; this function has no arguments
(define (guess)
  (quotient (+ lower upper) 2))

(define (smaller)
  (set! upper (max lower (sub1 (guess))))     ; sub1 is subtract 1 (aka decrement)
  (guess))                                    ; set! reassigns a variable

(define (bigger)
  (set! lower (min upper (add1 (guess))))
  (guess))
