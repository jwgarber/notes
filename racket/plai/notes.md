# Chapter 1

Typed PLAI is a statically typed version of racket (hooray!), with some extra goodies, like `define-type`, `type-case`, and `test`.

`define-type` is used to create ADTs (huzzah!). For example,

```racket
(define-type MisspelledAnimal
  [caml (humps : number)]
  [yacc (height : number)])
```

Instances of a datatype can be constructed as follows:

```racket
(caml 2)
(yacc 1.9)
```

You can bind these instances to names:

```racket
(define ma1 : MisspelledAnimal (caml 2))
(define ma2 : MisspelledAnimal (yacc 1.9))
```

In fact, you could have omitted the types of the above definitions, since the language will infer them. Whether you leave them in or out is a personal choice.

You can use `type-case` to pattern match on ADTs.

```racket
(define (good? [ma : MisspelledAnimal]) : boolean
  (type-case MisspelledAnimal ma
    [caml (humps) (>= humps 2)]
    [yacc (height) (> height 2.1)]))
```

You can also use `test` to do assertions.

```racket
(test (good? ma1) #t)   ; will spit out error if does not return true
(test (good? ma2) #f)   ; likewise for false
```

Algebraic data types also come with builtin functions for extracting fields (similar to record syntax in Haskell). This is simply done by concatenating the value constructor with the name of the field to be extracted (such as `caml-humps`). For example,

```racket
(define (good? [ma : MisspelledAnimal]) : boolean
  (cond
    [(caml? ma) (>= (caml-humps ma) 2)]
    [(yacc? ma) (> (yacc-height ma) 2.1)]))
```

# Chapter 2

**Parsing** is the act of turning an input character stream into a structured, internal representation.

Typed PLAI has an `s-expression` type. Putting a quotation mark in front of a list makes it into an s-expression. Since primitive types can occur inside an s-expression, they are also of type s-expression. To use them normally, you will need to cast them to their native type.

```racket
> '+
- symbol
'+
> (define l '(+ 1 2))
> l
- s-expression
'(+ 1 2)
> (first l)     ; doesn't work
> (define f (first (s-exp->list l)))
> (define x (symbol->string (s-exp->symbol f)))
```

The `read` function is great for splitting apart an input stream into tokens, but it would be better if we translated these tokens into datatypes we can work with.
