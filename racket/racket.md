Racket is a dialect of Lisp and a descendant of Scheme.

We will use several conventions in the rest of the book for describing the syntax of Racket. Anything between `<>` will be treated as descriptions for things, `*` means zero or more of the preceding argument, `+` means one or more of the preceding argument, and `{}` groups a sequence as an element for repetition.

# Language Identifiers

Racket is an extremely powerful language and can be used to build other languages. As such, you must specify which language you are using at the top of each source code file. The syntax is

```racket
#lang <langname> <topform>*
```

where `<topform>` is a `<definition>` or `<expr>`. For example, to use Racket proper, use the following declaration.

```racket
#lang racket
```

## Definitions

A **definition** is used to bind identifiers to expressions. There are two kinds of definitions. The first is for identifiers (aka zero argument functions). The basic syntax is this:

```racket
(define <id> <expr>)
```

This binds the result of `<expr>` to `<id>`. For example,

```racket
(define pi 3)
```

defines `pi` to be `3`.

The second type is for defining functions, also known as *procedures*. The basic syntax for these are

```racket
(define (<id> <id>*) <expr>+)
```

This binds the first `<id>` to a function/procedure whose arguments are the remaining `<id>`s. The `<expr>`s form the body of the function and are evaluated at each function call. The result of the last `<expr>` is the return value of the function. For example,

```racket
(define (piece str)
  (substring str 0 pie))
```

Note that only the return value of the last `<expr>` is returned, so all other body `<expr>`s are only useful for their side effects. Thus to keep things functional, most functions only have one body `<expr>`.

```racket
(define (bake flavor)
  (printf "pre-heating oven...\n")
  (string-append flavor "pie"))
```

Also note that the definition syntax makes a distinction between values and zero-argument functions. For example,

```racket
(define x 3)    ; a value
(define (y) 3)  ; a zero-argument function
```

In Haskell, there is no difference between these concepts because all functions are pure. As we have just seen however, Racket can be impure, which is why there is this distinction.

You can define a function that takes an arbitrary number of arguments using the *dotted-tail notation*. If you place a `.` before the last parameter of the function, all arguments not used up already will be collected into a list. This is similar to `*args` in Python.

```racket
(define (f x y . z)
  <something>)

(f 1 2 3 4 5)

(define (g . w)
 <body>)

(g 1 2 3 4 5)
```

The syntax for lambdas is similar, but slightly different.

```racket
(lambda (x y . z) <body>)
(lambda w <body>)   ; note that there are no parens around w
```

However, be careful when using dotted-tail in a recursive function. Making the recursive call usually doesn't work out well.

## Identifiers

An **identifier** is a name used for a variable or function. Racket is very liberal with the possible identifiers. Except for `(`, `)`, `[`, `]`, `{`, `}`, `"`, `,`, `'`, `\``, `;`, `#`, `|`, `\\\`, numeric literals, and whitespace characters, any combination of characters forms a identifier. For example, all the following are valid identifiers

```racket
+
Hfuhruhurr
integer?
pass/fail
jacob-garber
a-b-c+1-2-3
```

## Procedure Applications

Procedure applications, or function calls, are done like this

```racket
(<id> <expr>*)
```

where `<id>` is the name of the function and `<expr>`s are the arguments. For example,

```racket
(equal? 6 "half dozen")
```

checks if the two arguments are equal.

Note there is an important note for zero argument functions. For these functions, `<id>` is just the function object, while `(<id>)` is the function call. This explains the difference between `exit` and `(exit)`.

Actually, I lied. The general syntax for a function call is

```racket
(<expr> <expr>*)
```

The first `<expr>` simply has to be anything that evaluates to a function, including an `<id>`. Racket has first class functions, so this works beautifully.

```racket
(define (double v)
  ((if (string? v) string-append +) v v))
```

The expression `(if (string? v) string-append +)` will either return the `string-append` or the `+` functions, depending on the type of `v`.

## Boolean Values

In Racket, `#t` stands for true and `#f` for false.

There are also boolean operators.

```racket
(and <expr>*)   ; returns #t if all are true and #f otherwise
(or <expr>*)    ; returns #t if at least one is true
```

These beautiful operators can take any number of expressions. Both of these operators short-circuit as well, so be careful of side effects.

```racket
(define (reply s)
  (if (and (string? s)
           (>= (string-length s) 5)
           (equal? "hello" (substring s 0 5)))
      "hi?"
      "huh?"))
```

Note that unfortunately, `#f` is false, and everything else counts for true. This makes me very grumpy.

## Numbers

Numbers are of type `number`, and there are quite a few of them.

- Integers of type `Integer`.
- Rationals
- Complex numbers with integral or rational coefficients
- Floats of type `Flonum`.
-

You can use the `exact->inexact` and `inexact->exact` type conversions.

The `=` procedure compares numbers for numerical equality. If given both inexact and exact numbers to compare, it converts the inexact numbers to exact before comparing.

In contrast, the `eqv?` and `equal?` procedures compare numbers considering exactness and numerical equality.

```racket
> (= 1 1.0)
#t
> (eqv? 1 1.0)
#f
```

As always, be careful with numbers and their types.

## Characters

Characters are of type `Char`, and correspond to a single UTF-8 character. Printable characters normally print as `#\\` followed by the character. Unprintable characters print as `#\u` followed by the Unicode hexadecimal number.

The `char=?` procedure compares two or more characters.


# Conditional Expressions

The basic `if` expression looks like this:

```racket
(if <expr> <expr> <expr>)
```

This is very similar to the `if-then-else` expression in Haskell. The first expression is the predicate. If it evaluates to a `#t` value, the second expression is evaluated and returned. Otherwise if the predicate is `#f`, the third expression is evaluated and returned. For example,

```racket
(if (> 2 3)
    "bigger"
    "smaller")

(define (reply s)
  (if (equal? "hello" (substring s 0 5))
      "hi!"
      "huh?"))

(define (reply s)
  (if (string? s)
      (if (equal? "hello" (substring s 0 5))
          "hi!"
          "huh?")
       "huh?"))

; can rewrite the above as
(define (reply s)
  (if (if (string? s)
          (equal? "hello" (substring s 0 5))
          #f)
      "hi!"
      "huh?"))
```


## Cond

An `if-elif-else` sequence of statements can be done using a `cond` form.

```racket
(cond [<expr> <expr>*]*)
```

A `cond` consists of several *clauses*. In each clause, the first `<expr>` is evaluated. If it is true, the remaining `<expr>`s are evaluated, and the last one provides the return value for the `cond`. Only the first clause to return true is actually evaluated; the rest are ignored. You can use `else` as a synonym for `#t` as a fallthrough case.

```racket
(define (reply-more s)
  (cond
    [(equal? "hello" (substring s 0 5)) "hi"]
    [(equal? "goodbye" (substring s 0 9)) "bye"]
    [(equal? "?" (substring s (- (substring-length s) 1))) "I don't know"]
    [else "huh?"]))
```

It is convention to use square brackets for the clauses, to improve readability.

## when and unless

A **when conditional** is an if statement without an `else` branch. It is useful for doing operations with side effects. It looks like this.

```racket
(when predicate action)
```

When the predicate returns `#t`, the action is done.

```racket
(when file-modified (save-file))
```

The **unless form** is the opposite of `when`. It performs an action when a predicate is false.

```racket
(unless keep-file (delete-file))
```

## Case

You can use `case` for matching of literal values.

```racket
(case expr
  [(datum ...+) body ...+]...)
```

Each `datum` will be compared to the result of `expr` using `equal?`, and then the corresponding bodies are evaluated.

For example, you can translate the following `cond` into a case as such.

```racket
(cond
  [(equal? x 1) 'one]
  [(equal? x 2) 'two]
  [(equal? x 3) 'three]
  [(or (equal? x 4) (equal? x 5)) 'big]
  [else 'bigger])

(case x
  [(1) 'one]
  [(2) 'two]
  [(3) 'three]
  [(4 5) 'big]
  [else 'bigger])
```

Like a `cond`, the last clause of a `case` can be `else`. For matching against literals, `case` will be faster than `match`.

## Match

Pattern matching is done with `match`.

```racket
(match expr
  [<pattern> <expr>+]*)
```

The `expr` is evaluated, and compared against each of the patterns. When a pattern matches, all the `<expr>`s are evaluated, and the result of the last one is returned.

```racket
> (match (sqrt 4)
    [1 'one]
    [2 'two]
    [3 'three])
'two
> (match #f
    [#t 'yes]
    [#f 'no])
'no
```

You can also match type constructors.

```racket
(match '(1 2)
  [(list 0 1) 'one]
  [(list 1 2) 'two])

(match '(1 . 2)
  [(list 1 2) 'list]
  [(cons 1 2) 'pair])

(match #(1 2)
  [(list 1 2) 'list]
  [(vector 1 2) 'vector])
```

You can also use *pattern variables*. Note that `_` is used for ignoring things (like in Haskell).

```racket
(match '(1)
  [(list x) (+ x 1)]
  [(list x y) (+ x y)])

(match '(1 2)
  [(list x) (+ x 1)]
  [(list x y )])

(struct shoe (size color))
(struct hat (size style))

(match (hat 23 'bowler)
  [(shoe sz _) sz]
  [(hat sz _) sz])

(match (hat 11 'cowboy)
  [(shoe sz 'black) 'a-good-shoe]
  [(hat sz 'bolwer) 'a-good-hat]
  [_ 'something-else])
```

An ellipses `...` acts like a Kleene star of the last given thing. This looks really powerful, though I'm not sure exactly how to use it.

```racket
(match '(1 1 1)
  [(list 1 ...) 'ones]
  [_ 'other])

(match '(1 1 2)
  [(list 1 ...) 'ones]
  [_ 'other])

(match '(1 2 3 4)
  [(list 1 x ... 4) x])

(match (list (hat 23 'bowler) (hat 22 'pork-pie))
  [(list (hat sz stl) ...) (apply + sz)])

> (match '((! 1) (! 2 2) (! 3 3 3))
  [(list (list '! x ...) ...) x])
'((1) (2 2) (3 3 3))
```

There are also `match-let` and `match-lambda`, which look pretty cool.

```racket
(struct shoe (size color))
(struct hat (size style))
(match (hat 23 'bowler)
  [(shoe )])
```

## match\*

You can use `match*` to match multiple expressions at once.

```racket
(match* (list1 list2)
  [((list) (list)) 0]
  [((list x1 xs1 ...) (list x2 xs2 ...)) (+ 2 x1 x2)])
```

## define/match

You can use `define/match` to do a `match*` straight from a function definition.

# Lambda Functions

You can create an anonymous function using the `lambda` keyword.

```racket
(lambda (<id>*) <expr>+)
```

The `<id>`s are the function arguments. The `<expr>`s are evaluated in order, and as usual the return value of the last `<expr>` is the return value of the lambda.

For example,

```racket
(lambda (s) (string-append s "!"))
```

```racket
(define (twice f v)
  (f (f v)))

(twice (lamda (s) (string-append s "!")) "hello")
```

You can also return lambda's from other functions.

```racket
(define (make-add-suffix s2)
  (lambda (s) (string-append s s2)))
```

Racket is a *lexically scoped* language, so the `s2` in the function returned by `make-add-suffix` always refers to the proper value.

You can also define a variable to be a lambda. In this case, it is equivalent to just declaring a function.

```racket
; these two definitions are equivalent
(define (louder s)
  (string-append s "!"))

(define louder
  (lambda (s) (string-append s "!")))
```

## Local Bindings

Actually, we lied. The actual syntax of function and lambda declarations is:

```racket
(define (<id> <id>*) <definition>* <expr>+)

(lambda (<id>*) <definition>* <expr>+)
```

So you can put as many definitions as you want inside a function. These definitions are local to the function, and are similar to `where` in Haskell. They are useful if you don't want to compute a value multiple times or nest expressions too deeply.

```racket
(define (converse s)
  (define (starts? s2)
    (define len2 (string-length s2))
    (and (>= (string-length s) len2)
         (equal? s2 (substring s 0 len2))))
  (cond
    [(starts? "hello") "hi!"]
    [(starts? "goodbye") "bye"]
    [else "huh?"]))
```

These local definitions can also go inside a `cond`.

```racket
(define (random-stars n)
  (cond
    [(zero? n) empty]
    [else (define location (random-location 200 300))
          (if (inside-moon? location)
              (random-stars n)
              (cons location (random-stars (sub1 n))))]))

(define (winners lst pred)
  (cond
    [(empty? lst) (list pred)]
    [else
      (define fst (first lst))
      (if (score> (record-core pred) (record-core fst))
          (list pred)
          (cons pred (winners (rest lst) fst)))]))
```

## Let

Another way of creating local bindings is with a `let` expression. The `let` form can be used in any expression position, and can bind many identifiers at once.

```racket
(let ([<id> <expr>]*) <expr>+)
```

Each binding clause is an `<id>` and an `<expr>` surrounded by square brackets, and the expressions after the clause are the body of the `let`. As usual, the return type of the last expression is the return type of the entire `let` expression.

```racket
(let ([x (random 4)]
      [o (random 4)])
  (cond
    [(> x o) "X wins"]
    [(> o x) "O wins"]
    [else "cat's game"]))
```

The bindings of a `let` form are available only in the body of the `let`, so the binding clauses cannot refer to each other in other bindings. The `let*` form on the other hand allows later clauses to use earlier bindings:

```racket
(let* ([x (random 4)]
       [o (random 4)]
       [diff (number->string (abs (- x o)))])
  (cond
    [(> x o) (string-append "X wins by " diff)]
    [(> o x) (string-append "O wins by " diff)]
    [else "cat's game"]))
```

In general, a `let` expression is just shorthand for a lambda. For example, the xs and os can be rewritten as

```racket
; create a lambda that returns the <expr>
((lambda (x o)
  (cond
    [(> x o) "X wins"]
    [(> o x) "O wins"]
    [else "cat's game"]))
; then evaluate x and o
(random 4) (random 4))
```

So this is equivalent to

```racket
((lambda (var1 var2 ... varN) body) expr1 expr2 ... exprN)
```

Thus, a `let` expression is just syntactic sugar for a lambda application.

# Comments

```racket
; Here is a line comment
; Line comments at the beginning of a line have two semicolons for emphasis

#|
a block comment
the # starts an S-expression comment, which is somehow interesting
|#
```

You can use `[]` and `{}` anywhere you use `()`. They are completely interchangeable, as long as each open matches with a corresponding close.

# Lists

Lists are the fundamental data type of Lisp. All Lisp really does is manipulate lists.

The `list` function takes an arbitrary number of values and returns a list of those values.

```racket
(list 1 2 3)
```

## Cons Cells

A **cons cell** is an object consisting of two pointers, which can point to anything. A cons cell can be constructed using the `cons` function.

```racket
> (cons 1 2)
'(1 . 2)
```

You can extract the left and right elements of the cons cell using `car` and `cdr` (remember them alphabetically).

```racket
> (define cell (cons 'a 'b))
> (car cell)
'a
> (cdr cell)
'b
```

You can use cons cells to construct linked lists, which are the most important data structure in Lisp. To do this we create a cons cell, where the right entry is the empty list `empty`.

```racket
> (cons 1 empty) ; creates a list
'(1)
```

In this context, the left element is the payload of the node, and the right is a pointer to the rest of the list. You can combine multiple cons to create a longer list.

```racket
> (cons 1 (cons 2 empty))
'(1 2)
```

The `list` function is a convenient shorthand for creating a list without worrying about consing.

```racket
> (list 'one 'two 'three)
'(one two three)
```

Note that unlike Haskell, lists can consist of elements of different types, including nested lists.

```racket
> (list 'cat (list 'duck 'bat) 'ant)
'(cat (duck bat) ant)
```

Here are some predefined functions on lists.

```racket
> (length (list "hop" "skip" "jump"))     ; count the number of elements in a list
3
> (list-ref (list "hop" "skip" "jump") 0)     ; list indexing, like [0]
"hop"
> (append (list "hop" "skip") (list "jump"))  ; concat lists
'("hop" "skip" "jump")
> (reverse (list "hop" "skip" "jump"))      ; reverse order
'("jump" "skip" "hop")
> (member "fall" (list "hop" "skip" "jump")) ; check if in list
#f
```

## Iteration

The `map` function takes a function, and applies it to every element of a list.

```racket
> (map sqrt (list 1 4 9 16))
'(1 2 3 4)
> (map (lambda (i)
         (string-append i "!"))
       (list "peanuts" "popcorn" "crackerjack"))
'("peanuts!" "popcorn!" "crackerjack!")
```

The `andmap` and `ormap` functions map the given predicate, then `and` or `or` the results together.

```racket
> (andmap string? (list "a" "b" "c"))
#t
> (andmap string? (list "a" "b" 6))
#f
> (ormap number? (list "a" "b" 6))
#t
```

The `filter` function keeps elements for which the boolean function is true.

```racket
> (filter string? (list "a" "b" 6))
'("a" "b")
> (filter positive? (list 1 -2 6 7 0))
'(1 6 7)
```

The `map`, `andmap`, `ormap`, and `filter` functions can all accept multiple lists. The lists must all have the same length, and the given function must accept one argument for each list.

```racket
> (map (lambda (s n) (substring s 0 n))
       (list "peanuts" "popcorn" "crackerjack")
       (list 6 3 7))
'("peanut" "pop" "cracker")
```

There are two functions useful for accumulating the entries of a list in some way: `foldl` and `foldr`. These two functions have the same type signatures

function, called `reduce` in Python, is used to "accumulate" the entries of a list based on a function and an initial value. For example,

```racket
; elem is the element of the list
; v is the current value
> (foldl (lambda (elem v)
           (+ v (* elem elem)))
         0
         '(1 2 3))
14

(define (fold-left op initial seq)
  (define (iter result todo)
    (match todo
      [(list) result]
      [(list x xs ...) (iter (op x result) xs)]))
  (iter initial seq))
```

The `foldl` and `foldr` functions are equivalent if the given `op` is commutative and associative (unless the `initial` value is an identity, in which case you only need associativity).

Another handy higher-order functions function is `apply`. It applies a function to a list, where the elements of the list are the arguments of the function (similar to tuple unpacking in Python). This is particularily useful for functions that accept an arbitrary number of arguments.

```racket
> (apply + (list 1 2 3 4 5))
15
> (define sum (lst)
    (apply + lst))
> (sum '(1 2 3 4 5))
15
```

## Recursion

The `empty?` function tells you if a list is empty, and `cons?` tells you if it is non-empty.

```racket
> (empty? empty)
#t
> (empty? (cons "head" empty))
#f
> (cons? empty)
#f
> (cons? (cons "head" empty))
#t
```

To use recursion on a linked list, there are two important functions:

- `first`: get the first element of the list
- `rest`: get everything after the first element

```racket
> (first (list 1 2 3))
1
> (rest (list 1 2 3))
'(2 3)
```

To access the second element of a list, you can use a combination of `first` and `rest`.

```racket
> (define (snd lst) (first (rest lst)))
> (snd (list 1 2 3))
2
```

However, this is so common that the functions `second` all the way to `tenth` are predefined.

```racket
> (second (list 1 2 3))
2
> (third (list 1 2 3))
3
```

There is also the `last` function, which gives you the last element of a list.

You can use these to do basic recursion on a list.

```racket
(define (my-length a-list)
  (if (empty? a-list)
      0
      (add1 (my-length (rest a-list)))))

(define (my-map f lst)
  (if (empty? lst)
      empty
      (cons (f (first lst))
               (my-map rest lst))))
```

Note that Racket implements tail call optimization, so don't be afraid of using recursion. Here is a program that removes consecutive duplicates in a list.

```racket
(define (remove-dups l)
  (cond
    [(empty? l) empty]
    [(empty? (rest l)) l]   ; has length 1
    [else
      (let ([i (first l)])
        (if (equal? i (second l))
            (remove-dups (rest l))
            (cons i (remove-dups (rest l)))))]))
            ; I miss pattern matching
```

## Composition and Currying

A function can be curried using the `curry` function. Simply partially apply the function to its arguments, and put `curry` in front. This returns a procedure, which can then be passed to other functions expecting procedures (like `map`).

```racket
> (define add-one (curry + 1))
> (add-one 2)
3
```

You can compose functions using the `compose` function. It simply takes a bunch of procedures and composes them together (going backwards as usual).

```racket
> (define neg-sqrt (compose - sqrt))
> (neq-sqrt 9)
-3
```

You can combine these functions to make a swanky point-free version of the dot-product.

```racket
(define dot-product (compose1 (curry apply +) (curry map *)))
```

Note that `compose1` is a special version of `compose`: it expects all functions to return one value.

## Read

The `read` function can be used on the command line to turn parethesized input into a Racket list. It is very useful for parsing things.

## Structs

A **struct** can be defined as follows:

```racket
(struct struct-name (names of elements in the struct))
```

For example,

```racket
(struct student (name id# dorm))
```

An instance of the struct can be constructed easily.

```racket
(define freshman (student 'Joe 1234 'NewHall))
```

Defining a struct also defines several accessor functions for getting the fields.

```racket
> (student-name freshman)
'Joe
> (student-id# freshman)
1234
```

These functions are often called **selectors**.

A struct also comes with a conditional, which checks if an object is an instance of the struct.

```racket
> (student? freshman)
#t
```

## Opaque vs Transparent

By default, all structures are opaque. This means you can't view their internal representation or something, so like having private fields in Java. In particular, printing an opaque struct will be rather unhelpful.

```racket
> (struct example (x y z))
> (define ex1 (example 1 2 3))
> ex1
#<example>
```

A transparent struct allows you to look in the inside. To make a struct transparent, add `#:transparent` to the end of the struct definition.

```racket
(struct example (x y z)
        #:transparent)
```

This allows you to see what's inside a struct when you print it.

```racket
> (define ex (example 1 2 3))
> ex
(example 1 2 3)
```

# Forms

Most things in Racket are functions, which are pretty straightforward: they evaluate all their arguments, and then the function is applied to those arguments. Some things in Racket however are not functions, like `if`, `let`, `quote`, and `define`. These are called *special forms*, and are part of the syntax of Racket.


# Data Types

## Booleans

There are two boolean values: `true` and `false`, which are short for `#t` and `#f` respectively.

## Symbols

A **symbol** is like a string, but is instead meant for parsing and symbolic manipulation. For example, you can't get a substring from a symbol, you can't concatonate them, etc.

The `quote` form creates symbols.

```racket
> (quote map)
'map
> (quote (one two three))
'(one two three)
```

Note that a symbol has absolutely no relation to any function or object that the name usually refers to. It's just a special string.

You can also create a symbol by preceding an expression with a quotation mark. For example, `'hello`, `'my-money`, and `'--<<==>>--`. When you precede a list by a quote, the quote is moved recursively inside the list until it finds the symbols. This allows you to concisely write lists.

```racket
'(1 2 3) -> (list 1 2 3)
'(a (1 b) c) -> (list 'a (list 1 'b) 'c)
```

## Keywords

**Keywords** have a `#:` appended to the front of them. They are very similar to symbols, and are mostly used to indicate optional arguments to functions.

```racket
(define dir (find-system-path 'temp-dir))
(with-output-to-file (build-path dir "stuff.txt")
  (lambda () (printf "example\n"))
  ; optional #:mode argument can be 'text or 'binary
  #:mode 'text
  ; optional #:exists argument can be 'replace, 'truncate, ...
  #:exists 'replace)
```

## Numbers

Racket has floats, integers, rationals, and complex numbers (quite impressive!). The presence or absence of a decimal point indicates if a number is an integer or a float.

```racket
> (expt 53 35)
; some big number
> (sqrt -1)
0+1i
> (* (sqrt -1) (sqrt -1))
-1
> (/ 4 6)
2/3     ; a rational!
> (/ 4.0 6)
0.66666 ; a float
```

## Strings

A **string** is a sequence of characters surrounded by double quotes.

```racket
> "hello world"
"hello world"
> (string-append "hello" "world")
"helloworld"
> (string-append "hello" " " " world")  ; arbitrary number of arguments
"hello world"
```

## Vectors

A **vector** is a fixed-length array. Unlike a list, a vector is a contiguous array of values in memory, so it has constant time access to its elements. As usual, vectors are 0 indexed.

You can create a vector literal by prefacing a list with a hastag `#`.

```racket
> #("a" "b" "c")
'#("a" "b" "c")
> #(name (that tune))
'#(name (that tune))
> #4(baldwin bruce)
'#(baldwin bruce bruce bruce)
```

You can also the `vector` function to create a vector (similar to `list`). By default, these vectors are mutable.

```racket
> (vector "a" "b" "c")
'#("a" "b" "c")
```

You can create an immutable vector using `vector-immutable`.

You can use `vector-ref` to access a particular element of a vector.

```racket
> (vector-ref #("a" "b" "c") 1)
"b"
```

Honestly though, you use lists a lot more than vectors in Lisp.

## Hash Tables



## Type Predicates

Every built-in type of data comes with at **type predicate**, which identifies if an object is of that particular type. All of these are of the form `type-name?`.

```racket
> (number? 'a)
#f
> (string? "hello world")
#t
> (symbol? 'a)
#t
> (image? 10)
#f
> (boolean? "false")
#f
```

There are also predicates for lists.

```racket
> (list? 'eh)
#f
> (cons? '(what is that aboot?))
#t
> (empty? 'a)
#f
```

Note that in Typed Racket, most of these functions are unnecessary, because all the types are known up front.

**Equality predicates** check if two things are equal. The general form for these are `type-name=?`.

```racket
> (= 1 2)
#f
> (= (sqrt -1) 0+1i)
#t
> (boolean=? #f #f)
#t
> (string=? "hello world" "good bye")
#f
```

So there are specialized predicates for certain types, but there is also the universal equality predicate `equal?`, which works for all data types. In Typed Racket, having these specialized ones is unnecessary.

```racket
> (equal? "hello" (list 'a 'b))
#f
> (equal? 3 3)
#t
```

There is also the `eq?` predicate, which tests if two values refer to the same object in memory.

```racket
> (struct point (x y))
> (define p1 (point 1 3))
> (define p2 p1)
> (eq? p1 p2)
#t
```

Note this operation is only necessary because of mutability, which I freakin' hate.

Note: a **predicate** is a function that returns a boolean value.

# Modules

A *module* is used to organize code. Each module usually resides in its own file, like in Python. For example, consider the following file:

```racket
#lang racket

(provide print-cake)

; draws a cake with n candles
(define (print-cake n)
  (show "   ~a   " n #\.)
  (show " .-~a-. " n #\|)
  (show " | ~a | " n #\space)
  (show "---~a---" n #\-))

(define (show fmt n ch)
  (printf fmt (make-string n ch))
  (newline))
```

By default, all definitions in a module are private. You can export one to make it public using the `provide` function, as show above. You can then import the above module using the `require` function.

```racket
#lang racket

(require "cake.rkt")

(print-cake (random 30))
```

# Testing

Testing is one of the most important aspects of real life programming. Racket comes with the `rackunit` library for unit testing.

The most basic and common function is `check-equal?`, which compares two values using `equal?`.

```racket
(check-equal? (add1 5) 6)
```

If it passes, hooray! If it fails, some output is produced. Check the docs of `rackunit` for more info.

# Macros

Macros allow you to add new syntax to a language. They are expanded at runtime, and are extremely powerful.

# Evaluation Rules

- Every expression is either a *list* or an *atom*.
- A list is evaluated as a *special form* or a *function*.
- If the first thing in the list is a special form operator, the list is evaluated according to the rule of that form. This is part of the syntax of the language, and cannot be decomposed further into.
- A function application is done by evaluating the arguments, and then applying the function to those results.
- Every atom is a *symbol* or a *nonsymbol*.
- Evaluating a symbol gives back the last value bound to that symbol in the current environment.
- A nonsymbol, aka a literal, returns itself (like numbers and strings).

# Typed Racket

Typed Racket is a statically typed version of Racket (hooray!). The type system isn't as strong as Haskell, but it does give us some guarantees about the code.

To start Typed Racket from the command line, run

```
racket -I typed/racket
```

To make a file use Typed Racket, use

```racket
#lang typed/racket
```

Generally, all top level declarations and structs are given types (as they are in Haskell).

## Structs

The fields of a struct now have type annotations. They look like this:

```racket
(struct struct-name ([id : Type]*))
```

For example,

```racket
(struct point ([x : Real] [y : Real]))
```

## Definitions

All definitions, whether top-level or local, can be given type signatures. A `:` is used for type signatures of variables. There are two ways of annotating it.

```racket
; My preferred version
(: x Number)
(define x 7)

(define x : Number 7)
```

Functions are annotated similarly. The last type is the return type, and everything before it are the argument types.

```racket
(: distance (-> point point Real))
(define (distance p1 p2)
    (hypot (p1 p2)))

(define (distance [p1 : point] [p2 : point])
    (hypot (p1 p2)))
```

The parameters of the function are called *bound variables* (just like in logic). When a function is called, the arguments are bound to the parameters. Note that functions can reference free variables in their environment. This is called *lexical scoping*. This even works for functions defined within other functions. For example,

```racket
; we can reference x within each of the nested functions, which simplifies the logic somewhat
(define (sqrt x)
  (define (good-enough? guess)
    (< (abs (- (square guess) x)) 0.001))
  (define (improve guess)
    (average guess (/ x guess)))
  (define (sqrt-iter guess)
    (if (good-enough? guess)
        guess
        (sqrt-iter (improve gues))))
  (sqrt-iter 1.0))
```

## Basic Types

Typed Racket provides many basic types:

- `True` (yes, `true` has its own type)
- `False`
- `Boolean`
- `String`
- `Char`
- `Symbol` for symbols, and furthermore each symbol is given a unique type containing only that symbol. Eg, `'foo` also has type `'foo`.
- There are many number types arranged into a hierarchy.

## Type Synonym

You can declare a type synonym using `define-type`.

```racket
(define-type )
```

## Union Types

You can create an ADT from several structs using the union constructor `U`.

Typed Racket isn't the best. Racket is a dynamically typed language, and typed racket is kinda bolted on on top of that. It isn't the happiest marriage. I think if you're going to use Racket, stick with the dynamic version. If you want a good type system, then Haskell is right around the corner. As a side note, plai-typed is kinda what Racket could have been if it were typed from the beginning, but it is only an educational language, not something to actually implement stuff in. I think if you want to have a language that is dynamic and static, it is better to design it as a static first, and then simply drop the type annotations to make it dynamic. It is much cleaner to do that than to bolt a type system onto a dynamic language.

