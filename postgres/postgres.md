# Introduction

PostgreSQL is a *relational database management system* (RDBMS). An RDBMS stores data in *tables*. Each table has a name, and consists of a number of *rows* and *columns*. Each row and column have a name, and each column also has its own datatype (hooray!).

Tables are grouped into databases, and a collection of databases managed by a single PostgreSQL server constitutes a database cluster.

Postgres uses a client/server model. In this model, a *server* process manages the database and performs actions on the database on behalf of the client. The server program is called *postgres*. The *client* is a frontend application that accesses the database (this is usually a user program). It is possible for the client and server programs to be on different computers, in which case they communicate over a TCP/IP connection.

The Postgres server can handle multiple concurrent connections from clients. The master process launches a new process for each connection, which the client uses to access the database. Thus, the master server is always around, and launches per-client processes as needed.

## Removing Postgres

Run

```
brew uninstall --force postgres
sudo rm -r /usr/local/var/postgres
```

It seems this deletes the existing databases too, so watch out for that.

## Starting the Server

Before you interact with any PostgreSQL databases, you need to launch the server. Homebrew details how to do this.

## Creating a Database

Use the `createdb` command to create a database.

```
$ createdb my_awesome_db
```

You can delete a database you created with the `dropdb` command.

```
$ dropdb my_awesome_db
```

## Accessing a Database

You can access a database in four ways:

- Using the `psql` interative terminal program, which allows you to type in SQL commands. (Or the nicer version `pgcli`)
- Using GUI tool
- Accessing it from a program using language bindings

# Basic Syntax

Note that SQL is case-insensitive about keywords and identifiers. So, I will code in lower case, because upper case reminds me of Fortran. Comments are done using `--`.

All statements end with a semicolon, like C and Java. Because of this, whitespace and newlines mean absolutely nothing, so you can be very free with how you format your code.

# Data Types

Note that type names are not keywords in the syntax.

## Numeric Types

You can use `int2`, `int4`, and `int8` for 2, 4, and 8 *byte* (not bit) integers. You can also use `float4` and `float8` for floating point numbers.

## Text

- `varchar(num)` stores strings up to `num` characters in length
- `text` stores an arbitrarily long string

## Enums

You can create an enum using the `create type` command.

```pgsql
create type mood as enum ('sad', 'ok', 'happy');

create table person (
    name text,
    current_mood mood,
);

insert into person values ('Moe', 'happy');
```

## Boolean

The `boolean` type is used to represent boolean values.

## Arrays

## Composite Types

A *composite type* is simply a struct of values. Composite types can be used in essentially the same way as builtin types.

```pgsql
create type complex as (
    r   float8,
    i   float8
);
```

The syntax is similar to `create table`, except that constraints such as `not null` cannot be included.

# Tables

You can create a table by specifying the table name, along with the column names and their types.

```pgsql
create table weather (
    city    text,
    temp_lo int4,    -- low temperature
    temp_hi int4,    -- high temperature
    prcp    float8,  -- precipitation
    date    date
);
```

You can delete a table with the `drop table` command.

```pgsql
drop table weather;
```

## Null

To prevent a column in a table from containing the null datatype, you can use the `not null` constraint.

```pgsql
create table products (
    x   int4    not null,
    y   int4    not null,
    z   int4    not null
);
```

## Unique Constraints

Unique constrain

## Primary Keys

A `primary key` constraint is equivalent to `unique not null`. This means each row must have a unique value for the given column.

```pgsql
create table products (

);
```

A table can have at most one primary key, and every table should have one. This is not enforced by Postgres, but it is good to keep in mind.

## Constraints

## Inserting a Row

The `insert` statement adds a row to a table.

```pgsql
insert into weather values ('San Francisco', 46, 50, 0.25, '1994-11-27');
```

All data types use obvious input formats, and constants that are not numeric values are usually surrounded by single quotes `'`.

This syntax initializes the elements in order implicitly. There is another syntax that allows you to list the columns explicitly. This is considered good style.

```pgsql
insert into weather (city, temp_lo, temp_hi, prcp, date)
    values ('San Francisco', 43, 79, 0.0, '1994-11-29');
```

You can omit some columns in an insert, and if this happens,

You can use `copy` to insert rows from a text file.

```pgsql
copy weather from '/home/user/weather.txt';
```

This file must be avaliable on the machine running the backend process.

## Querying a Table

The `select` statement is used to retrieve rows from a table.

To retrieve all rows and all columns of the table `weather`, use

```pgsql
select * from weather;
```

To only look at certain columns, you can do something like this:

```pgsql
select city, temp_lo, temp_hi from weather;
```

You can also write expressions in the select list:

```pgsql
select city, (temp_hi + temp_lo)/2 as temp_avg, date from weather;
```

The `as` clause gives the name of that column.

You can filter the output of query using a `where` clause. The `where` clause a boolean expression, where you can use the usual boolean operators `and`, `or`, `not`.

```pgsql
select * from weather
    where city = 'San Francisco' and prcp > 0.0;
```

You can order the results of a query by a specific column.

```pgsql
select * from weather
    order by city, temp_lo;     -- order by city first, then by temp_lo
```

You can use the `distinct` keyword to remove duplicate rows.

```pgsql
select distinct city from weather;
select distinct city
    from weather
    order by city;
```

## Deleting a Row

Rows can be removed from a table using the `delete` command. For example,

```pgsql
delete from weather
    where city = 'Hayward';
```

This deletes all rows from `weather` where the city is Hayward.

The `delete from tablename;` command will delete all rows from the given table, so be careful.

## Join

## Aggregate Functions

## Updates

You can update existing rows using the `update` command.

```pgsql
update weather
    set temp_hi = temp_hi - 2, temp_lo = temp_lo - 2
    where date > '1994-11-28';
```

# Views

A `view` is a named query that you can refer to like an ordinary table.

```pgsql
create view myview as
    select city, temp_lo, temp_hi
        from weather
        where city = 'San Fransisco'

select * from myview;
```

## Foreign Keys

# Transactions

A *transaction* is an atomic database operation. It either happens completely, or not at all. In Postgres, a transaction is set up by surrounding the SQL commands of the transaction with `begin` and `commit` commands. For example,

```pgsql
begin;
update accounts
    set balance = balance - 100.00
    where name = 'Alice';
update branches
    set balance = balance - 100.00
    where name = (select branch_name from accounts where name = 'Alice');
commit;
```

All the commands within the transaction are successfully executed, or none of them are. Partway through the transaction, if you decide to cancel the changes, you can use the `rollback` command instead of `commit`.

Every SQL statement is implicity within its own transaction block. So, every SQL statement succeeds, or fails with no side effects.

You can also use `savepoints`.

# Window Functions

# Inheritance

# Concurrency

Postgres uses Multiversion Concurrency Control, which is an awesome way of doing concurrency without locking.

There are four levels of transaction isolation in SQL. The one we want to use is *Serializable*, which means the result of running several operations concurrently must be identical to the result of running them in serial. The phenomena prohibited at various levels are

- dirty read, A transaction reads uncommitted data from another transaction
- nonrepeatable read,
- phantom read,
- serialization anomaly

You can use the `set transaction` command to set the isolation level of a transaction.

Serializable transactions ensure that no matter what serial order the commands could be run in, the result will always be the same. That is, it is disallowed for one command to change the result of the other. If commands like this are run, one of them will cancel with an error.

You can declare transactions as `read only` when possible. Also, look at deferrable stuff. Maybe look at default_transaction_isolation.

# Partitions



# Schemas

These are like "namespaces" for things in your database.

# Indexes

You can use indexes to "organize" a database for faster querying.

# Server Administration

It is assumed that the `postgres` user is running the postgres daemon (in the tutorial).

A *database cluster* is a collection of databases that is managed by a single instance of the database server. The cluster is a single directory where all the databases are stored. This is called the *data directory*. You use the `initdb` command to create a data directory. For example, on my mac the following command is run when postgres is installed:

```fish
initdb -D /usr/local/var/postgres
```

All databases now will be stored under this directory. Normally, the user running the server daemon owns this directory. In the above case, that is my account `jacobgarber`, which is good, because `initdb` was run under that account.

## Starting the Server

The database server is called `postgres`. You must start the server before you can connect to any databases. To start the server, simply go

```fish
postgres -D /path/to/the/cluster
```

On my computer, this is

```fish
postgres -D /usr/local/var/postgres
```

This must be done while logged into the account that owns the cluster. It is a good idea to run the server in the background, and even better to start it at login. Homebrew explains how to do this. You can also use `pg_ctl`, which wraps some common tasks for you.

## Upgrading a Cluster

Incrementing the last digit of the version number is always compatible, eg moving from 9.5.1 to 9.5.2. To upgrade a minor version change, kill the server, upgrade using homebrew, and restart the server. It's that simple.

Incrementing one of the first two numbers could change the internal format, and so requires using `pg_upgrade`. Make sure you also always read the *Migration* section of the release notes when upgrading. Upgrading a database is always risky, so double triple check you have a backup that works before you do anything.

## Accessing the Database

Once the server has started, you can access a database using `psql` (or `pgcli`, which is much nicer). Simply go

```
pgcli my_database_name
```

## Roles

By default, a `jacobgarber` role is created, so I wouldn't worry about it.

## Creating a Database

Initializing a cluster automatically creates two or three default databases: `postgres`, which is used to administer other databases, and `template0`/`template1`, which new databases are based off of. Open up the `postgres` database, and run

```pgsql
create database my_db_name;
```

to create a new database. You can also use the `createdb` shell command, as described above.

## Destroying a Database

You can destroy a database with

```pgsql
drop database db_name;
```

Only the owner or superuser can drop a database, and this is a permanent operation. There is also the shell command `dropdb` that does the same thing.

# Backups

You can dump a database to a Postgres specific binary file like so.

```
$ pg_dump -Fc $DBNAME -f $DBNAME.dump
```

You can then create a new database, and restore the dump into that database.

```
$ createdb $NEW_DBNAME
$ pg_restore -O -d $NEW_DBNAME $DBNAME.dump
# Using -O, the owner of the new database becomes the owner of all the created objects in the new db
# Without this option, pg_restore will try to make the original owner of the database the new owner as well
```

# PL/pgSQL

You can only run SQL commands from within Postgres. However, SQL has rather limited programming facilities, so what if you want to do something more complicated? The answer is PL/pgSQL. It is an extension of SQL that adds extra programming features. You can only use pgSQL within a function. You cannot run it within the `psql` interpreter, because only SQL works there.
